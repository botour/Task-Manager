var clearAssignToInFollowUpList = false;

var updateTaskDetailsPanel = function (taskObject) {
    $("#comment_form_task_id").val(taskObject["task-id"]);
    $("#title-container h4").html(taskObject["task-title"]);
    $("#description-container p").html("");
    $("#due-to-date-container").hide();
    if(taskObject["task-due-to-date"]){
        $("#due-to-date-container").show();
        $("#due-to-date-container span span").html(taskObject['task-due-to-date']);
    }
    if(taskObject["task-description"].length>0){
        $("#description-container .task-details-text").html(taskObject["task-description"]);
    }else{
        $("#description-container .task-description-empty-text").html("No Description Available");
    }
    if(taskObject["task-note"].length>0){
        $("#note-container span").html(taskObject["task-note"]);
    }else{
        $("#note-container span").html("");
    }
    $("#edit-task-button").attr("task-id", taskObject["task-id"]);
    $("#task-prompt").hide();
    $("#task-details").show();
    $("#task-id-edit-field").val(taskObject["task-id"]);
    $("#task-title-edit-field").val(taskObject["task-title"]);
    $("#task-description-edit-field").val(taskObject["task-description"]);
    $("#header-caption").html(taskObject["task-serial-no"]);
    updateCommentsList(taskObject["task-id"]);
    /*$("#comment_form_task_id").val($tableRow.attr("task-id"));
    $("#title-container h4").html($tableRow.attr("task-title"));
    $("#description-container p").html("");
    $("#due-to-date-container").hide();
    if($tableRow[0].hasAttribute("task-due-to-date")){
        $("#due-to-date-container").show();
        $("#due-to-date-container span span").html($tableRow.attr('task-due-to-date'));
    }
    if($tableRow.attr("task-description").length>0){
        $("#description-container .task-details-text").html($tableRow.attr("task-description"));
    }else{
        $("#description-container .task-description-empty-text").html("No Description Available");
    }
    if($tableRow.attr("task-note").length>0){
        $("#note-container span").html($tableRow.attr("task-note"));
    }else{
        $("#note-container span").html("");
    }
    $("#edit-task-button").attr("task-id", $tableRow.attr("task-id"));
    $("#task-prompt").hide();
    $("#task-details").show();
    $("#task-id-edit-field").val($tableRow.attr("task-id"));
    $("#task-title-edit-field").val($tableRow.attr("task-title"));
    $("#task-description-edit-field").val($tableRow.attr("task-description"));
    updateCommentsList($tableRow.attr("task-id"));*/
};

var updateCommentsList = function (taskId) {

    $.ajax({
        "data": {
            "task_id": parseInt($("#task-id-edit-field").val())
        },
        "url": getCommentsURL,
        "type": "GET",
        "success": function (data) {
            $("#comments-loader").hide();
            $("#comments-container").html(data);
            document.getElementById('comments-container').scrollTop = 99999999999;
            $(".delete-comment-link").click(function () {
                var $link = $(this);
                $link.parent().fadeOut(400);
                $.ajax({
                    "type": "POST",
                    "dataType": "json",
                    "url": deleteCommentURL,
                    "data": {
                        "comment_id": $link.attr("comment-id")
                    },
                    "success": function (data) {
                        if (data.status) {
                            $link.parent().remove();
                        } else {
                            $link.parent().fadeIn(400);
                        }
                        if (data.commentsCount == 0) {
                            updateCommentsList();
                        }
                    },
                    "error": function () {
                        $link.parent().fadeIn(400);
                    }
                });
            });
        },
        "beforeSend": function () {
            $("#comments-loader").show();
        }
    });

};

var updateSelectRow = function () {
    $(".task-grid-row").click(function (event) {
        event.preventDefault();
        $("#add-comments-form-container").show();
        $(".task-grid-row").each(function () {
            $(this).removeClass("selected-row");
        });
        $(this).addClass("selected-row");
        $("#edit_task_control_panel").hide();
        $(".task-control-panel").hide();
        if ($(this).hasClass("archived-task-row")) {
            $("#add-comments-form-container").hide();
        }
        if ($(this).hasClass("todo-task-row")) {
            $(".edit-todo-task-control-panel").show();
        }
        if ($(this).hasClass("follow-up-task-row")) {
            $(".edit-follow-up-task-control-panel").show();
        }
        if ($(this).hasClass("complete-task-row")) {
            $(".edit-complete-task-control-panel").show();
        }
        if (!$(this).hasClass("follow-up-task-row")) {
            $(this).removeClass("unread");
        }
        var taskObject = {
            'task-title':$(this).attr('task-title'),
            'task-id':$(this).attr('task-id'),
            'task-description':$(this).attr('task-description'),
            'task-note':$(this).attr('task-note'),
            'task-serial-no':$(this).attr('task-serial-no')
        };
        if($(this)[0].hasAttribute("task-due-to-date")){
            taskObject['task-due-to-date'] = $(this).attr('task-due-to-date');
        }
        updateTaskDetailsPanel(taskObject);
    });
};

var updateTaskReminderLinks = function(){
    $('.task-remind-button').click(function () {
        var taskId = $(this).attr("task-id");
        $.ajax({
            "url": changeTaskRemindedURL,
            "dataType": "json",
            "type": "POST",
            "data": {
                "task_id": taskId
            },
            "success": function (data) {
                if (data.status) {
                    $("#follow-up-task-grid").yiiGridView("update");
                }
            },
            "beforeSend": function () {
                $("#loading-indicator").show();
                $(this).attr('disabled', true);
            }
        });
    });
}

var updateTaskAssignmentLinks = function () {
    $(".reassign-link").click(function (event) {
        event.preventDefault();
        $.ajax({
            'url': taskReassignLink,
            'type': 'GET',
            'data': {
                'id': $(this).attr("task-id")
            },
            'success': function (data) {
                $(".reassign-link").each(function () {
                    $(this).attr("disabled", false);
                });
                $("#taskAssignmentModal .modal-body").html(data);
            },
            'beforeSend': function () {
                $("#taskAssignmentModal .modal-body").html("Please Wait ...");
                $("#taskAssignmentModal").modal("show");
                $(".reassign-link").each(function () {
                    $(this).attr("disabled", true);
                });
            }
        });
    });
}

function updateCloseTaskButton() {
    $('#task-done-button').click(function (event) {
        var message = [];
        $closeTaskButton = $(this);
        $('.complete-grid-checkbox').each(function () {
            $checkBox = $(this).parent("div");
            if ($checkBox.hasClass("checked")) {
                $tableRow = $(this).parent().parent().parent("tr");
                message.push(parseInt($(this).val()));
                $tableRow.addClass("checked-row");
            }
        });

        message = message.toString();
        $.ajax({
            "type": "POST",
            "url": changeTaskStatusToCloseURL,
            "dataType": "json",
            "data": {
                "task_Ids": message
            },
            "beforeSend": function () {
                $('.complete-grid-checkbox').iCheck('disabled');
                $('.complete-grid-checkbox-all').iCheck('disabled');
                $closeTaskButton.attr("disabled", true);
                $("#loading-indicator").show();
            },
            "success": function (data) {
                $closeTaskButton.attr("disabled", false);
                $("#loading-indicator").hide();
                $('.complete-grid-checkbox-all').iCheck('enable');
                $('.complete-grid-checkbox').iCheck('enable');
                $('.complete-grid-checkbox').iCheck('uncheck');
                if (data.status) {
                    $(".checked-row").remove();
                    var count = parseInt($(".complete-summery-container").html());
                    count = count - data.deleteCount;
                    $(".complete-summery-container").html(count);
                    $("#archived-task-grid").yiiGridView("update");
                } else {
                    $(".checked-row").removeClass("checked-row");
                }
            }
        });
    });
}
var updateCheckBoxColumnForToDoList = function () {
    $('.complete-grid-checkbox').tooltip();
    $('.complete-grid-checkbox-all').tooltip();
    $('.todo-grid-checkbox').tooltip();
    $(".todo-grid-checkbox").iCheck({
        checkboxClass: "icheckbox_flat-green",
        radioClass: "iradio_flat-green",
        increaseArea: "50%" // optional
    });
    $(".complete-grid-checkbox-all").iCheck({
        checkboxClass: 'icheckbox_square-red',
        radioClass: 'icheckbox_square-red',
        increaseArea: "20%" // optional
    });
    $(".complete-grid-checkbox").iCheck({
        checkboxClass: 'icheckbox_square-red',
        radioClass: 'icheckbox_square-red',
        increaseArea: "20%" // optional
    });
    $(".complete-grid-checkbox-all").on("ifChecked", function () {
        $('.complete-grid-checkbox').iCheck('check');
    });
    $(".complete-grid-checkbox-all").on("ifUnchecked", function () {
        $('.complete-grid-checkbox').iCheck('uncheck');
    });
    $(".todo-grid-checkbox").on("ifChecked", function () {
        checkBox = this;
        $.ajax({
            "type": "post",
            "dataType": "json",
            "url": changeTaskStatusToCompleteURL,
            "data": {
                "task_id": $(checkBox).attr("task-id")
            },
            "success": function () {
                $checkedRow = $(checkBox).parent().parent().parent();
                $checkedRow.fadeOut(600);
                $("#loading-indicator").hide();
                var todoTaskCount = parseInt($(".todo-summery-container").html());
                todoTaskCount--;
                $(".todo-summery-container").html(todoTaskCount);
                var completeTaskCount = parseInt($(".complete-summery-container").html());
                completeTaskCount++;
                $(".complete-summery-container").html(completeTaskCount);
                $("#complete-task-grid").yiiGridView("update");
                $("#follow-up-task-grid").yiiGridView("update");
                $(".todo-grid-checkbox").iCheck("enable");
            },
            "error": function () {
                $(checkBox).iCheck("uncheck");
                $(".todo-grid-checkbox").iCheck("enable");
            },
            "beforeSend": function () {
                $(".todo-grid-checkbox").iCheck("disable");
                $("#loading-indicator").show();
            }
        });
    });
    $(".todo-grid-checkbox").on("ifUnchecked", function () {
        checkBox = this;
        $.ajax({
            "type": "post",
            "dataType": "json",
            "url": changeTaskStatusToNotCompleteURL,
            "data": {
                "task_id": $(checkBox).attr("task-id")
            },
            "success": function () {
                $(checkBox).parent().parent().parent().fadeOut(600);
                var todoTaskCount = parseInt($(".todo-summery-container").html());
                todoTaskCount++;
                $(".todo-summery-container").html(todoTaskCount);
                var completeTaskCount = parseInt($(".complete-summery-container").html());
                completeTaskCount--;
                $(".complete-summery-container").html(completeTaskCount);
                $("#task-grid").yiiGridView("update");
                $("#follow-up-task-grid").yiiGridView("update");
                $(".todo-grid-checkbox").iCheck("enable");
                $("#loading-indicator").hide();
            },
            "error": function () {
                $(checkBox).iCheck("check");
                $(checkBox).iCheck("enable");
                $("#loading-indicator").hide();
            },
            "beforeSend": function () {
                $("#loading-indicator").show();
                $(".todo-grid-checkbox").iCheck("disable");
            }
        });
    });
};

var initTaskDetailsPanel = function () {
    $(".edit-task-form-control").each(function () {
        $(this).hide();
    });
}

var closeEditTaskPanel = function () {

    $("#edit_task_control_panel").hide();
    $(".edit-task-form-control").hide();
    $(".task-details-text").show();
    $(".task-control-panel").hide();
    if ($(".selected-row").hasClass("todo-task-row")) {
        $(".edit-todo-task-control-panel").show();
    }
    if ($(".selected-row").hasClass("follow-up-task-row")) {
        $(".edit-follow-up-task-control-panel").show();
    }
    if ($(".selected-row").hasClass("complete-task-row")) {
        $(".edit-complete-task-control-panel").show();
    }
}

var makeTableSortable = function(tableId){
    $(tableId+" table")
}

var updateChevron = function(){
    $(".sort-link.asc").after(" <span class='glyphicon glyphicon-chevron-up' style='color:#77CCDD;font-size: 12px;'></span>");
    $(".sort-link.desc").after(" <span class='glyphicon glyphicon-chevron-down' style='color:#77CCDD;font-size: 12px;'></span>");
    $(".sort-link").removeClass("asc");
    $(".sort-link").removeClass("desc");
}

var playSound;

$(function () {
    updateChevron();
    $(".tooltip-link").tooltip();
    $(".task-control-panel").hide();
    $(".notification-indicator").hide();
    playSound = function playSound(){
        $("#sound").html('<audio autoplay="autoplay"><source src="' + notificationSoundURL+ '" /><source src="'+notificationSoundURL+'" /><embed hidden="true" autostart="true" loop="false" src="'+ notificationSoundURL+'" /></audio>');
    }
    $(".reset-dropdown").val("");
    $("#project-id-field").val(projectId);
    $("#due-to-date-container").hide();
    updateCloseTaskButton();
    updateCheckBoxColumnForToDoList();
    updateSelectRow();
    initTaskDetailsPanel();
    updateTaskAssignmentLinks();
    updateTaskReminderLinks();
    updateTabs();
    $(".date-field").datepicker({
        format: "dd-mm-yyyy",
        weekStart: 6,
        startDate: currentDate,
        todayBtn: true,
        daysOfWeekDisabled: "5",
        autoclose: true,
        todayHighlight: true
    });
    /*$.ajax({
     "url":getToDOListViewURL,
     "type":"GET",
     "success":function(data){
     $("#task-list-container .loader").remove();
     $("#task-list-container").html(data);
     updateCheckBoxColumnForToDoList();
     updateSelectRow();
     updateTaskAssignmentLinks();
     $(".todo-summery-container").html($(".todo-summery").html());
     $(".todo-summery").hide();
     $(".Tabled table").addClass("table");
     $(".Tabled table").addClass("table-condensed");
     }
     });*/
    $("#message-feedback").hide();
    $('.edit-task-control').tooltip();
    $('.edit-task-info-button').click(function () {
        var taskId = $("#task-id-edit-field").val();
        $(".task-grid-row").each(function(){
            $(this).removeClass("selected-for-update");
        });

        $(".selected-row").addClass("selected-for-update");
        $.ajax({
            "url": getUpdateTaskInfoFormURL,
            "type": "GET",
            "data": {
                task_id: taskId
            },
            "success": function (data) {
                $(this).attr('disabled', false);
                $("#editTaskInfoModal .modal-body").html(data);

            },
            "beforeSend": function () {
                $(this).attr('disabled', true);
                $("#editTaskInfoModal .modal-body").html("Please wait ...");
                $("#editTaskInfoModal").modal("show");
            }
        });
    });
    $('.task-remind-button').click(function () {
        var taskId = $("#task-id-edit-field").val();
        $.ajax({
            "url": changeTaskRemindedURL,
            "dataType": "json",
            "type": "POST",
            "data": {
                "task_id": taskId
            },
            "success": function (data) {
                $(this).attr('disabled', true);
                if (data.status) {
                    if (data.reminded) {
                        $("#add-follow-up-task-reminder-button").hide();
                        $("#remove-follow-up-task-reminder-button").show();
                    } else {
                        $("#add-follow-up-task-reminder-button").show();
                        $("#remove-follow-up-task-reminder-button").hide();
                    }
                    $("#task-grid").yiiGridView("update");
                    $("#follow-up-task-grid").yiiGridView("update");
                }
            },
            "beforeSend": function () {
                $(this).attr('disabled', true);
            }
        });
    });
    $(".edit-task-priority-button").click(function () {
        var taskId = $("#task-id-edit-field").val();
        $.ajax({
            "url": getPriorityLevelChangeFormURL,
            "dataType": "html",
            "type": "GET",
            "data": {
                "id": taskId
            },
            "success": function (data) {
                $("#changeTaskPriorityModal .modal-body").html(data);

            },
            "beforeSend": function () {
                $("#changeTaskPriorityModal .modal-body").html("Please Wait...");
                $("#changeTaskPriorityModal").modal("show");
            }
        });
    });

    /*$("#edit-task-button").click(function(event){
     event.preventDefault();
     $(".task-control-panel").hide();
     $("#edit_task_control_panel").show();
     $(".task-details-text").hide();
     $(".edit-task-form-control").show();
     $("#task-id-edit-field").val($(".selected-row").attr("task-id"));
     $("#task-title-edit-field").val($(".selected-row").attr("task-title"));
     $("#task-description-edit-field").val($(".selected-row").attr("task-description"));
     });*/
    $("#edit-todo-task-close-button").click(function (event) {
        closeEditTaskPanel();
    });
    /*$("#edit-todo-task-button").click(function(){
     $.ajax({
     "url":getTaskOptionsURL,
     "data":{
     "task_id":$(this).attr("task-id")
     },
     "type":"GET",
     "success":function(data){
     $(this).removeClass();
     $("#showTaskOptions .modal-body").html(data);
     $(this).attr("disabled",false);
     },
     "beforeSend":function(){
     $(this).attr("disabled",true);
     $("#showTaskOptions .modal-body").html("Please wait...");
     $("#showTaskOptions").modal("show");
     }
     });
     });*/
    $(".Tabled table").addClass("table");
    $(".Tabled table").addClass("table-condensed");
    $(".todo-summery-container").html($(".todo-summery").html());
    $(".todo-summery").hide();
    $(".follow-summery-container").html($(".follow-up-summery").html());
    $(".follow-up-summery").hide();


    $(".complete-summery-container").html($(".complete-summery").html());
    $(".complete-summery").hide();
    $(".archived-summery-container").html($(".archived-summery").html());
    $(".archived-summery").hide();

    /*$("#datepicker-container input").datepicker({
     format: "yyyy-mm-dd",
     weekStart: 6,
     startDate: "2014-11-4",
     endDate: currentDate,
     todayBtn: true,
     daysOfWeekDisabled: "5",
     autoclose: true,
     todayHighlight: true
     });*/
});

window.setInterval(function () {
    $.ajax({
        "type": "GET",
        "url": getNewTasksURL,
        "dataType": "json",
        "data":{
            'project_id':projectId
        },
        "success": function (data) {
            if (data.todoStatus) {
                showNotificationMessage("info",data.message);
                //$("#task-grid").yiiGridView("update");
                var newTodoTaskCount = parseInt($(".todo-new-task-container").html());
                newTodoTaskCount+=data.taskCount;
                $(".todo-new-task-container").show();
                $(".todo-new-task-container").html(newTodoTaskCount);
            }
            if(data.followUpTaskNotCompleteStatus){
                showNotificationMessage("info",data.followUpTaskNotCompleteMessage);
                var newFollowUpTaskCount = parseInt($(".follow-up-new-task-container").html());
                newFollowUpTaskCount+=data.followUpTaskNotCompleteCount;
                $(".follow-up-new-task-container").show();
                $(".follow-up-new-task-container").html(newFollowUpTaskCount);
            }
            if(data.followUpTaskReAssignStatus){
                showNotificationMessage("info",data.followUpTaskReAssignMessage);
                var newFollowUpTaskCount = parseInt($(".follow-up-new-task-container").html());
                newFollowUpTaskCount+=data.followUpTaskReAssignCount;
                $(".follow-up-new-task-container").show();
                $(".follow-up-new-task-container").html(newFollowUpTaskCount);
            }
            if(data.followUpTaskCompleteStatus){
                showNotificationMessage("info",data.followUpTaskCompleteMessage);
                var newFollowUpTaskCount = parseInt($(".complete-new-task-container").html());
                newFollowUpTaskCount+=data.followUpTaskCompleteCount;
                $(".complete-new-task-container").show();
                $(".complete-new-task-container").html(newFollowUpTaskCount);
            }

            if(data.changeToNotCompleteByMemberStatus){
                showNotificationMessage("info",data.changeToNotCompleteByMemberMessage);
                var newFollowUpTaskCount = parseInt($(".follow-up-new-task-container").html());
                newFollowUpTaskCount+=data.changeToNotCompleteByMemberCount;
                $(".follow-up-new-task-container").show();
                $(".follow-up-new-task-container").html(newFollowUpTaskCount);
            }
            if (data.changeToNotCompleteStatus) {
                showNotificationMessage("info",data.changeToNotCompleteMessage);
                //$("#task-grid").yiiGridView("update");
                var newTodoTaskCount = parseInt($(".todo-new-task-container").html());
                newTodoTaskCount+=data.changeToNotCompleteCount;
                $(".todo-new-task-container").show();
                $(".todo-new-task-container").html(newTodoTaskCount);
            }
            if(data.reassignedByMemberStatus){
                showNotificationMessage("info",data.reassignedByMemberMessage);
                var newFollowUpTaskCount = parseInt($(".follow-up-new-task-container").html());
                newFollowUpTaskCount+=data.reassignedByMemberTaskCount;
                $(".follow-up-new-task-container").show();
                $(".follow-up-new-task-container").html(newFollowUpTaskCount);
            }
            if (data.reassignedStatus) {
                showNotificationMessage("info",data.reassignedMessage);
                //$("#task-grid").yiiGridView("update");
                var newTodoTaskCount = parseInt($(".todo-new-task-container").html());
                newTodoTaskCount+=data.reassignedTaskCount;
                $(".todo-new-task-container").show();
                $(".todo-new-task-container").html(newTodoTaskCount);
            }
            if (data.changeToRemindedStatus) {
                showNotificationMessage("info",data.changeToRemindedMessage);
                //$("#task-grid").yiiGridView("update");
                var newTodoTaskCount = parseInt($(".todo-new-task-container").html());
                newTodoTaskCount+=data.changeToRemindedCount;
                $(".todo-new-task-container").show();
                $(".todo-new-task-container").html(newTodoTaskCount);
            }
            if(data.followUpStatus){
                showNotificationMessage("info",data.followUpMessage);
                //$("#follow-up-task-grid").yiiGridView("update");
                var newFollowUpTaskCount = parseInt($(".follow-up-new-task-container").html());
                newFollowUpTaskCount+=data.followUpTaskCount;
                $(".follow-up-new-task-container").show();
                $(".follow-up-new-task-container").html(newFollowUpTaskCount);
            }
        }
    });
}, 5000);
$(".search-tasks form").submit(function () {
    $("#task-grid").yiiGridView("update", {
        data: $(this).serialize()
    });
    return false;
});
var clearTaskDetails = function(){
    $(".task-control-panel").hide();
    $("#due-to-date-container").hide();
    $("#header-caption").html("Task Details");
    $("#task-details").hide();
    $("#task-prompt").show();
    $(".selected-row").each(function(){
        $(this).removeClass("selected-row");
    });
}
var updateTabs = function(){
    $(".task-content-tab-follow-up").click(function(){
        var newFollowUpTasks = parseInt($(".follow-up-new-task-container").html());
        if(newFollowUpTasks>0){
            newFollowUpTaskCount=0;
            $(".follow-up-new-task-container").hide();
            $(".follow-up-new-task-container").html(newFollowUpTaskCount);
            $($(this).attr('grid-id')).yiiGridView("update");
        }
        clearTaskDetails();
    });

    $(".task-content-tab-todo").click(function(){
        var newToDoTasks = parseInt($(".todo-new-task-container").html());
        if(newToDoTasks>0){
            newFollowUpTaskCount=0;
            $(".todo-new-task-container").hide();
            $(".todo-new-task-container").html(newFollowUpTaskCount);
            $($(this).attr('grid-id')).yiiGridView("update");
        }
        clearTaskDetails();
    });
    $(".task-content-tab-complete").click(function(){
        var newCompleteTasks = parseInt($(".complete-new-task-container").html());
        if(newCompleteTasks>0){
            newCompleteTaskCount=0;
            $(".complete-new-task-container").hide();
            $(".complete-new-task-container").html(newCompleteTaskCount);
            $($(this).attr('grid-id')).yiiGridView("update");
            $("#follow-up-task-grid").yiiGridView("update");
        }
        clearTaskDetails();
    });
    $(".task-content-tab-archived").click(function(){
        clearTaskDetails();
    });
    $(".create-task-tab").click(function(){
        clearTaskDetails();
    });
}
/*
 $("#datepicker-container input").datepicker({
 format: "yyyy-mm-dd",
 weekStart: 6,
 startDate: "2014-11-4",
 endDate: currentDate,
 todayBtn: true,
 daysOfWeekDisabled: "5",
 autoclose: true,
 todayHighlight: true
 });
 */

var showModalStatus = 0;
$("#search-button").click(function () {
    if (showModalStatus == 0) {
        showModalStatus = 1;
        $("#search-task-modal").modal("show");
    } else {
        showModalStatus = 0;
        $("#search-task-modal").modal("hide");
    }
});

var createTaskFormReset = function(){
    $("#task-form input[type=text]").each(function(){
        $(this).val("");
    });
    $("#task-form textarea").each(function(){
        $(this).val("");
    });
}

var stack_bottomleft = {"dir1": "up", "dir2": "right", "firstpos1": 25, "firstpos2": 25};
var showNotificationMessage = function(type,message) {
    var opts = {
        title: "Success",
        text: "Check me out. I'm in a different stack.",
        addclass: "stack-bottomleft",
        stack: stack_bottomleft
    };
    switch (type) {
        case "error":
            opts.title = "Error";
            opts.text = message;
            opts.type = "error";
            break;
        case "info":
            opts.title = "Notice";
            opts.text = message;
            opts.type = "info";
            break;
        case "success":
            opts.title = "New Task Created";
            opts.text = message;
            opts.type = "success";
            break;
    }
    playSound();
    new PNotify(opts);
}

window.onload = function () {
    $("#loader").hide();
    $("nav").show();
    $("#task-manager-panel").show();
}