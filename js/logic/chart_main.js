var chart;
var renderChart = function(chartData){
    chart = new CanvasJS.Chart("chart-container",chartData);
    chart.render();
}
$(function(){
    $(".date-field").datepicker({
        format: "yyyy-mm-dd",
        weekStart: 6,
        todayBtn: true,
        daysOfWeekDisabled: "5",
        autoclose: true,
        todayHighlight: true
    });
    var chart = new CanvasJS.Chart("chart-container", {
        title:{
            text:"Lamaas"
        },
        animationEnabled: true,
        axisX:{
            interval: 2,
            gridThickness: 2,
            labelFontSize: 10,
            labelFontStyle: "normal",
            labelFontWeight: "normal",
            labelFontFamily: "Lucida Sans Unicode"

        },
        axisY2:{
            interlacedColor: "rgba(1,77,101,.2)",
            gridColor: "rgba(1,77,101,.1)"
        },
        data: [
            {
                type: "bar",
                name: "companies",
                axisYType: "secondary",
                color: "#014D65",
                dataPoints: [
                    {y: 5, label: "Sweden"  },
                    {},
                    {y: 7, label: "Russia"  },
                    {},
                    {y: 8, label: "Brazil"  },
                    {},
                    {y: 9, label: "Italy"  },
                    {},
                    {y: 12, label: "Canada"  },
                    {},
                    {y: 13, label: "Netherlands"  },
                    {},
                    {y: 28, label: "Britain" },
                    {},
                    {y: 32, label: "France"  },
                    {},
                    {y: 73, label: "China"},
                    {}
                ]
            }
        ]
    });
    chart.render();
})