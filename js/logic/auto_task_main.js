/**
 * Created by Dell 3521 on 09/02/15.
 */
var resetAutoTasksCreateForm = function () {
    $("#end-repeat").attr("disabled", true);
    $("#second-priority-days-field").attr("disabled", true);
    $("#third-priority-days-field").attr("disabled", true);
    document.getElementById("auto-task-form").reset();
}
var updateSelectRow = function () {
    $(".task-grid-row").click(function (event) {
        $("#task-details").show();
        $("#task-prompt").hide();
        $("#options-container").show();

        event.preventDefault();
        $(".task-grid-row").each(function () {
            $(this).removeClass("selected-row");
        });
        $(this).addClass("selected-row");
        var taskObject = {
            "id": $(this).attr("auto-task-id"),
            "serial_no": $(this).attr("auto-task-serial-no"),
            "title":$(this).attr("auto-task-title"),
            "description":$(this).attr("auto-task-description"),
            "note":$(this).attr("auto-task-note")
        };

        updateTaskDetailsPanel(taskObject);
    });
};

var updateCommentsList = function(taskId){
    $.ajax({
        "data": {
            "task_id": parseInt(taskId)
        },
        "url": getCommentsURL,
        "type": "GET",
        "success": function (data) {
            $("#comments-loader").hide();
            $("#comments-container").html(data);
            document.getElementById('comments-container').scrollTop = 99999999999;
        },
        "beforeSend": function () {
            $("#comments-loader").show();
        }
    });
}

var updateTaskDetailsPanel = function (taskObject) {
    $("#auto-task-id").val(taskObject["id"]);
    $("#header-caption").html(taskObject["serial_no"]);
    $("#comment_form_task_id").val(taskObject["id"]);
    $("#title-container h4").html(taskObject["title"]);
    $("#description-container p").html("");

    if(taskObject["description"]){
        if(taskObject["description"].length>0){
            $("#description-container .task-details-text").html(taskObject["description"]);
        }else{
            $("#description-container .task-description-empty-text").html("No Description Available");
        }
    }
    if(taskObject["task-note"]){
        if(taskObject["task-note"].length>0){
            $("#note-container span").html(taskObject["task-note"]);
        }else{
            $("#note-container span").html("");
        }
    }
    updateCommentsList(taskObject["id"]);
}
updateAutoTaskActionsLink = function () {
    $(".auto-task-delete-link").click(function (event) {
        if (confirm("Are You Sure You want to delete the task?")) {
            $.ajax({
                "url": deleteAutoTaskLink,
                "data": {
                    "id": $(this).attr("auto-task-id")
                },
                "type": "POST",
                "dataType": "json",
                "success": function (data) {
                    $("#loading-indicator").hide();
                    if (data.status) {
                        $("#auto-task-grid").yiiGridView("update");
                    }
                    $("#deleteAutoTaskModal .modal-body").html(data.message);
                    setTimeout(function () {
                        $("#deleteAutoTaskModal").modal("hide");
                    }, 2000);
                },
                "error": function () {
                    $("#deleteAutoTaskModal .modal-body").html("Error Occurred!");
                    setTimeout(function () {
                        $("#deleteAutoTaskModal").modal("hide");
                    }, 2000);
                    $("#loading-indicator").hide();
                },
                "beforeSend": function () {
                    $("#deleteAutoTaskModal .modal-body").html("Please wait");
                    $("#deleteAutoTaskModal").modal("show");
                    $("#loading-indicator").show();
                }
            });
        }

    });
    $(".auto-task-edit-link").click(function () {
        $.ajax({
            "url": getAutoTaskEditFormLink,
            "type": "GET",
            "data": {
                "id": $(this).attr("auto-task-id")
            },
            "success": function (data) {
                $("#editAutoTaskModal .modal-body").html(data);
            },
            "beforeSend": function () {
                $("#editAutoTaskModal .modal-body").html("please wait");
                $("#editAutoTaskModal").modal("show");
            },
            "error": function () {
                $("#editAutoTaskModal .modal-body").html("Error Occurred!");
                setTimeout(function () {
                    $("#editAutoTaskModal").modal("hide");
                }, 2000);
            }
        });
    });
}
var init = function () {
    updateSelectRow();
    updateAutoTaskActionsLink();
    $(".Tabled table").addClass("table");
    $(".Tabled table").addClass("table-condensed");
    $('.radio-input-container input').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green',
        increaseArea: '20%' // optional
    });
    $(".tooltip-link").tooltip();
    $(".edit-task-control").tooltip();
    $("#options-container").hide();
    //Set the active/Inactive link

    $(".create-task-tab").click(function () {
        $("#options-container").hide();
        $("#task-details").hide();
        $("#task-prompt").show();
        $("#header-caption").html("Auto Task Details");
        $(".task-grid-row").each(function () {
            $(this).removeClass("selected-row");
        });
    });
    $(".auto-task-list-tab").click(function () {
        resetAutoTasksCreateForm();
    });
    updateChangeTaskStatusActionLink();
}

var updateChangeTaskStatusActionLink = function () {
    $("#change-task-status-button").click(function () {
        $.ajax({
            "url": changeStatusLink,
            "data": {
                "id": $("#auto-task-id").val()
            },
            "type": "POST",
            "dataType": "json",
            "success": function (data) {
                $("#auto-task-grid").yiiGridView("update");
            },
            "beforeSend": function () {
                $("#loading-indicator").fadeIn(200);
            }
        });
    });
}

$(function () {
    init();
});
