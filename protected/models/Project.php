<?php

/**
 * This is the model class for table "tbl_project".
 *
 * The followings are the available columns in table 'tbl_project':
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $create_date
 * @property integer $create_id
 * @property string $update_date
 * @property integer $update_id
 * @property string $note
 *
 * The followings are the available model relations:
 * @property ProjectUserAssignment[] $projectUserAssignments
 * @property Task[] $tasks
 * @property integer $pendingTasksNo
 * @property integer $onGoingTasksNo
 * @property integer $holdTasksNo
 * @property integer $completedTasksNo
 * @property Task[] $pendingTasks
 * @property Task[] $onGoingTasks
 * @property Task[] $holdTasks
 * @property Task[] $completedTasks members
 * @property User[] members
 */
class Project extends ProjStarActiveRecord
{

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_project';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
            array('name', 'unique'),
			array('create_id, update_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>255),
			array('description, note', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, description, create_date, create_id, update_date, update_id, note', 'safe', 'on'=>'search'),
		);
	}

    /**
	 * @return array relational rules.
	 */

    public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'projectUserAssignments' => array(self::HAS_MANY, 'ProjectUserAssignment', 'project_id'),
			'tasks' => array(self::HAS_MANY, 'Task', 'project_id'),
            'tasksCount' => array(self::STAT, 'Task', 'project_id'),
            'archivedTasksCount' => array(self::STAT, 'Task', 'project_id','condition'=>'status=:status','params'=>array(
                ':status'=>Task::STATE_VERIFIED,
            )),
            'usersCount' => array(self::STAT, 'User', 'tbl_project_user_assignment(project_id,user_id)'),
            /*'pendingTasksNo' => array(self::STAT,'Task','project_id','condition' => 'status=:status', 'params' => array(':status' => Task::STATE_PENDING)),
            'onGoingTasksNo' => array(self::STAT,'Task','project_id','condition' => 'status=:status', 'params' => array(':status' => Task::STATE_ON_GOING)),
            'holdTasksNo' => array(self::STAT,'Task','project_id','condition' => 'status=:status', 'params' => array(':status' => Task::STATE_HOLD)),
            'completedTasksNo' => array(self::STAT,'Task','project_id','condition' => 'status=:status', 'params' => array(':status' => Task::STATE_COMPLETE)),
            'pendingTasks' => array(self::HAS_MANY,'Task','project_id','condition' => 'status=:status', 'params' => array(':status' => Task::STATE_PENDING)),
            'onGoingTasks' => array(self::HAS_MANY,'Task','project_id','condition' => 'status=:status', 'params' => array(':status' => Task::STATE_ON_GOING)),
            'holdTasks' => array(self::HAS_MANY,'Task','project_id','condition' => 'status=:status', 'params' => array(':status' => Task::STATE_HOLD)),
            'completedTasks' => array(self::HAS_MANY,'Task','project_id','condition' => 'status=:status', 'params' => array(':status' => Task::STATE_COMPLETE)),*/
            'members'=>array(self::MANY_MANY,'User','tbl_project_user_assignment(project_id,user_id)'),
            'membersCount'=>array(self::STAT,'User','tbl_project_user_assignment(project_id,user_id)'),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Workspace Name',
			'description' => 'Workspace Description',
			'create_date' => 'Create Date',
			'create_id' => 'Create',
			'update_date' => 'Update Date',
			'update_id' => 'Update',
			'note' => 'Note',
		);
	}

    public static function getReportOptionsArray(){
        return array(
            self::REPORT_TYPE_CREATE_TASK=>'Created Tasks',
            self::REPORT_TYPE_COMPLETE_TASK=>'Completed Tasks',
            self::REPORT_TYPE_NOT_COMPLETE_TASK=>'Revision Counts',
            self::REPORT_TYPE_END_TASK=>'Ended Tasks',
        );
    }

    public function isDeletable(){
        $isDeletable = true;
        if($this->usersCount!=0){
            if($this->tasksCount != $this->archivedTasksCount){
                $isDeletable = false;
            }else{
                $isDeletable = true;
            }
        }
        return $isDeletable;
    }

    public function getMembersList(){
        return CHtml::listData($this->members,'id','username');
    }

    public static function getProjectsList(){
        return CHtml::listData(Project::model()->findAll(),'id','name');
    }

    public function getNote(){
        if(strlen($this->note)==0){
            return "No Note Available";
        }else{
            return $this->note;
        }
    }
    public function getDescription(){
        if(strlen($this->description)==0){
            return "No Description Available";
        }else{
            return $this->description;
        }
    }

    public function getProjectList($userRoleType,$user){
        if($userRoleType==User::ROLE_ADMIN){
            $projects = Project::model()->findAll();
        }else{
            $projects = $user->projects;
        }
        return CHtml::listData($projects,'id','name');
    }
    public function getProjectListForSearch(){
        $projects = Project::model()->findAll();
        return CHtml::listData($projects,'id','name');
    }

	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('create_id',$this->create_id);
		$criteria->compare('update_date',$this->update_date,true);
		$criteria->compare('update_id',$this->update_id);
		$criteria->compare('note',$this->note,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function addMember($userId){
        try{
            Yii::app()->db->createCommand()->insert('tbl_project_user_assignment',array(
                'user_id'=>$userId,
                'project_id'=>$this->id,
            ));
        }catch(CDbException $e){

        }

    }
    public function deleteMember($userId){
        try{
            Yii::app()->db->createCommand()->delete('tbl_project_user_assignment',"user_id=:user_id AND project_id=:project_id",array(
                ':user_id'=>$userId,
                ':project_id'=>$this->id,
            ));
        }catch(CDbException $e){

        }
    }
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Project the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
