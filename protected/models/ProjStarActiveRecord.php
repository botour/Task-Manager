<?php

class ProjStarActiveRecord extends CActiveRecord{

    public $skipBeforeSave = false;
    protected function beforeSave(){
       if($this->skipBeforeSave){
            return parent::beforeSave();
       }
       if($this->isNewRecord){
            $this->create_id = Yii::app()->user->id;
            $this->create_date = date('Y-m-d H:i:s');
       }
        $this->update_id = Yii::app()->user->id;
        $this->update_date = date('Y-m-d H:i:s');
        return parent::beforeSave();
    }
} 