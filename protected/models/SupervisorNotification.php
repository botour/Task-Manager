<?php

/**
 * This is the model class for table "tbl_supervisor_notification".
 *
 * The followings are the available columns in table 'tbl_supervisor_notification':
 * @property integer $id
 * @property integer $task_id
 * @property integer $supervisor_id
 * @property integer $supervisor_notified
 * @property integer $update_type
 *
 * The followings are the available model relations:
 * @property Task $task
 * @property User $supervisor
 */
class SupervisorNotification extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    const UPDATE_TYPE_TASK_CREATE = 0;
    const UPDATE_TYPE_TASK_REMINDED = 5;
    const UPDATE_TYPE_TASK_REASSIGN = 1;
    const UPDATE_TYPE_TASK_COMPLETE = 2;
    const UPDATE_TYPE_TASK_NOTIFIED = 3;
    const UPDATE_TYPE_NOT_TASK_COMPLETE = 4;

    const SUPERVISOR_NOT_NOTIFIED = 0;

    public function tableName()
    {
        return 'tbl_supervisor_notification';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('task_id, supervisor_id, supervisor_notified, update_type', 'required'),
            array('task_id, supervisor_id, supervisor_notified, update_type', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, task_id, supervisor_id, supervisor_notified, update_type', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'task' => array(self::BELONGS_TO, 'Task', 'task_id'),
            'supervisor' => array(self::BELONGS_TO, 'User', 'supervisor_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'task_id' => 'Task',
            'supervisor_id' => 'Supervisor',
            'supervisor_notified' => 'Supervisor Notified',
            'update_type' => 'Update Type',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('task_id', $this->task_id);
        $criteria->compare('supervisor_id', $this->supervisor_id);
        $criteria->compare('supervisor_notified', $this->supervisor_notified);
        $criteria->compare('update_type', $this->update_type);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SupervisorNotification the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
