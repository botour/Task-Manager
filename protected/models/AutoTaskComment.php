<?php

/**
 * This is the model class for table "tbl_auto_task_comment".
 *
 * The followings are the available columns in table 'tbl_auto_task_comment':
 * @property integer $id
 * @property integer $auto_task_id
 * @property string $body
 * @property string $create_date
 * @property integer $create_id
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property AutoTask $autoTask
 * @property User $create
 */
class AutoTaskComment extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */

    const STATUS_LOG = 0;
    const STATUS_COMMENT = 1;

	public function tableName()
	{
		return 'tbl_auto_task_comment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('auto_task_id, body, create_date, create_id, status', 'required'),
			array('auto_task_id, create_id, status', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, auto_task_id, body, create_date, create_id, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'autoTask' => array(self::BELONGS_TO, 'AutoTask', 'auto_task_id'),
			'create' => array(self::BELONGS_TO, 'User', 'create_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'auto_task_id' => 'Auto Task',
			'body' => 'Body',
			'create_date' => 'Create Date',
			'create_id' => 'Create',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('auto_task_id',$this->auto_task_id);
		$criteria->compare('body',$this->body,true);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('create_id',$this->create_id);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AutoTaskComment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
