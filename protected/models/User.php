<?php

/**
 * This is the model class for table "tbl_user".
 *
 * The followings are the available columns in table 'tbl_user':
 * @property integer $id
 * @property string $emp_no
 * @property integer $current_project_id
 * @property string $first_name
 * @property string $last_name
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $login_name
 * @property integer $status
 * @property string $note
 * @property integer $role
 * @property string $create_date
 * @property integer $create_id
 * @property string $update_date
 * @property integer $update_id
 *
 * The followings are the available model relations:
 * @property ProjectUserAssignment[] $projectUserAssignments
 * @property Task[] $tasks
 * @property User[] $supervisors
 */
class User extends ProjStarActiveRecord
{
    /**
     * @return string the associated database table name
     */
    const SALT = "jas#&12a1@(/as";

    public $role;
    public $password_repeat;

    public $oldPassword;
    const STATE_BREAK = 0;
    const STATE_ONLINE = 1;
    const STATE_OFFLINE = 2;

    const ROLE_MEMBER = 0;
    const ROLE_ADMIN = 1;
    const ROLE_PROJECT_SUPERVISOR = 2;

    public $projectId = 0;
    public $followUpUsersArray = array();
    public $supervisorId;
    public $groupId = 0;
    public $isAdmin = false;
    public $isSupervisor = false;
    public $isMember = false;

    public $viewArchivePermission=false;
    public $endTaskPermission=false;
    public $reassignPermission=false;
    public $viewReportPermission=false;
    public $manageAutoTaskPermission=false;

    public function tableName()
    {
        return 'tbl_user';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('first_name, last_name,login_name,username,password,role,viewArchivePermission,endTaskPermission,reassignPermission,viewReportPermission,manageAutoTaskPermission', 'required', 'message' => 'The field {attribute} cannot be blank.', 'on' => 'create'),
            array('first_name, last_name,login_name, username,role,viewArchivePermission,endTaskPermission,reassignPermission,viewReportPermission,manageAutoTaskPermission', 'required', 'on' => 'update'),
            array('username', 'unique', 'message' => 'The username is already been used'),
            array('emp_no', 'unique', 'message' => 'The employee number is already been used'),
            array('login_name', 'unique', 'message' => 'The Login name is already been used'),
            array('current_project_id, status, create_id, update_id', 'numerical', 'integerOnly' => true),
            array('first_name, last_name, email, password', 'length', 'max' => 255),
            array('note,email,emp_no', 'safe'),
            array('password_repeat,password', 'required', 'on' => 'password_reset'),
            array('password', 'compare', 'on' => 'password_reset'),
            array('password_repeat,password,oldPassword', 'required', 'on' => 'member_password_reset'),
            array('password', 'compare', 'on' => 'member_password_reset'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, current_project_id, first_name, last_name, email, password, status, note, create_date, create_id, update_date, update_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'projectUserAssignments' => array(self::HAS_MANY, 'ProjectUserAssignment', 'user_id'),
            'tasks' => array(self::HAS_MANY, 'Task', 'user_id'),
            'editTasks'=> array(self::HAS_MANY, 'Task', 'update_id'),
            'tasksCount' => array(self::STAT, 'Task', 'user_id'),
            'projects' => array(self::MANY_MANY, 'Project', 'tbl_project_user_assignment(user_id,project_id)'),
            'followUpUsers' => array(self::MANY_MANY, 'User', 'tbl_supervision(supervisor_id,user_id)'),
            'supervisors' => array(self::MANY_MANY, 'User', 'tbl_supervision(user_id,supervisor_id)'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'role' => 'Role',
            'current_project_id' => 'The Current Project',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'username' => 'Display Name',
            'login_name' => 'Login Name',
            'oldPassword'=>'Old Password',
            'password' => 'Password',
            'status' => 'Status',
            'note' => 'Note',
            'create_date' => 'Create Date',
            'create_id' => 'Create',
            'emp_no'=>'Emp No',
            'update_date' => 'Update Date',
            'update_id' => 'Update',
            'viewArchivePermission'=>'View Archive',
            'endTaskPermission'=>'End Task',
            'viewReportPermission'=>'View Report',
            'reassignPermission'=>'Reassign',
            'manageAutoTaskPermission'=>'Manage Auto Tasks',
        );
    }
    public function validateOldPassword($attribute,$params)
    {
        if(!$this->hasErrors())
        {
            if($this->password!=$this->hashPassword($this->oldPassword)){
                $this->addError('oldPassword','Old Password is not correct');
            }
        }
    }
    public function getFullName()
    {
        return $this->first_name . " " . $this->last_name;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('current_project_id', $this->current_project_id);
        $criteria->compare('first_name', $this->first_name, true);
        $criteria->compare('last_name', $this->last_name, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('note', $this->note, true);
        $criteria->compare('create_date', $this->create_date, true);
        $criteria->compare('create_id', $this->create_id);
        $criteria->compare('update_date', $this->update_date, true);
        $criteria->compare('update_id', $this->update_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function getStatusOptionsArray()
    {
        return array(
            self::STATE_BREAK => 'Break',
            self::STATE_ONLINE => 'Online',
            self::STATE_OFFLINE => 'Offline',
        );
    }

    public function getCurrentProjectId()
    {
        if (isset($this->current_project_id)) {
            return $this->current_project_id;
        } else {
            return 0;
        }
    }

    public function getCurrentProject()
    {
        if (isset($this->current_project_id)) {
            return Project::model()->findByPk($this->current_project_id);
        } else {
            return null;
        }

    }

    public function setStatus($status)
    {
        $this->status = $status;
        return $this->save(false);
    }
    /* ======================================================================= */
    // Authentication/Validation-related methods ...
    /* ======================================================================= */
    public function assignAdmin($userId)
    {
        try {
            Yii::app()->authManager->assign('admin', $userId);
        } catch (CDBExcetion $e) {

        }
    }

    public function getRoleText(){
        $role = $this->getRole($this->id);
        return CommonFunctions::getLabel($role,CommonFunctions::USER_ROLE);
    }
    public function removeAdmin($userId)
    {
        Yii::app()->authManager->revoke('admin', $userId);
    }

    public function isAdmin($userId)
    {
        $users = Yii::app()->db->createCommand()
            ->select('*')
            ->from('authassignment')
            ->where('itemname=:itemname AND userid=:userid', array(':userid' => $userId, ':itemname' => 'administrator'))
            ->queryAll();
        return !empty($users);
    }

    public function isSupervisor($userId)
    {
        $users = Yii::app()->db->createCommand()
            ->select('*')
            ->from('authassignment')
            ->where('itemname=:itemname AND userid=:userid', array(':userid' => $userId, ':itemname' => 'project_supervisor'))
            ->queryAll();
        return !empty($users);
    }

    public function hasViewArchivePermission($userId)
    {
        $users = Yii::app()->db->createCommand()
            ->select('*')
            ->from('authassignment')
            ->where('itemname=:itemname AND userid=:userid', array(':userid' => $userId, ':itemname' => 'view_archive'))
            ->queryAll();
        return !empty($users);
    }

    public function hasReassignPermission($userId)
    {
        $users = Yii::app()->db->createCommand()
            ->select('*')
            ->from('authassignment')
            ->where('itemname=:itemname AND userid=:userid', array(':userid' => $userId, ':itemname' => 'reassign'))
            ->queryAll();
        return !empty($users);
    }

    public function hasEndTaskPermission($userId)
    {
        $users = Yii::app()->db->createCommand()
            ->select('*')
            ->from('authassignment')
            ->where('itemname=:itemname AND userid=:userid', array(':userid' => $userId, ':itemname' => 'end_task'))
            ->queryAll();
        return !empty($users);
    }
    public function hasViewReportPermission($userId)
    {
        $users = Yii::app()->db->createCommand()
            ->select('*')
            ->from('authassignment')
            ->where('itemname=:itemname AND userid=:userid', array(':userid' => $userId, ':itemname' => 'view_report'))
            ->queryAll();
        return !empty($users);
    }
    public function hasManageAutoTaskPermission($userId)
    {
        $users = Yii::app()->db->createCommand()
            ->select('*')
            ->from('authassignment')
            ->where('itemname=:itemname AND userid=:userid', array(':userid' => $userId, ':itemname' => 'manage_auto_tasks'))
            ->queryAll();
        return !empty($users);
    }

    public function availableProjectsForAssignments()
    {
        $projects = Project::model()->findAll();
        return CHtml::listData($projects, 'id', 'name');
    }

    public function assignToProject($project_id)
    {
        $project = Project::model()->findByPk($project_id);
        if (!is_null($project)) {
            $projects = Yii::app()->db->createCommand()
                ->select('*')
                ->from('tbl_project_user_assignment')
                ->where('user_id=:user_id AND project_id=:project_id', array(':user_id' => $this->id, ':project_id' => $project->id))
                ->queryAll();
            if (empty($projects)) {

                $status = Yii::app()->db->createCommand()
                    ->insert('tbl_project_user_assignment', array(
                        'user_id' => $this->id,
                        'project_id' => $project->id,
                    ));
                return $status;
            } else {
                return false;
            }
            return $status;
        } else {
            return false;
        }
    }

    public function projectsArray()
    {
        $projects = $this->projects;
        return CHtml::listData($projects, 'id', 'name');
    }

    public function isMemberInProject($projectId)
    {
        if ($this->projectId == 0) {
            return false;
        } else {
            $projects = Yii::app()->db->createCommand()
                ->select('*')
                ->from('tbl_project_user_assignment')
                ->where('user_id=:user_id AND project_id=:project_id', array(':user_id' => $this->id, ':project_id' => $projectId))
                ->queryAll();
            return !empty($projects);
        }

    }

    public function addToFollowUpList($userId)
    {

        if (is_array($userId)) {
            foreach ($userId as $id) {
                try {
                    if ($this->id != $id) {
                        Yii::app()->db->createCommand()
                            ->insert('tbl_supervision', array(
                                'user_id' => $id,
                                'supervisor_id' => $this->id,
                            ));

                    }

                } catch (CDbException $e) {

                }
            }
            return true;
        } else {
            try {
                if ($this->id != $userId) {
                    Yii::app()->db->createCommand()
                        ->insert('tbl_supervision', array(
                            'user_id' => $userId,
                            'supervisor_id' => $this->id,
                        ));
                }


            } catch (CDbException $e) {
                return false;
            }
            return true;
        }
    }

    public function removeFromFollowUpList($userId)
    {
        return Yii::app()->db->createCommand()->delete('tbl_supervision', 'user_id=:user_id AND supervisor_id=:supervisor_id', array(
            ':user_id' => $userId,
            ':supervisor_id' => $this->id,
        )) > 0;
    }

    public function clearFollowUpList()
    {
        Yii::app()->db->createCommand()->delete('tbl_supervision', 'supervisor_id=:supervisor_id', array(
            ':supervisor_id' => $this->id,
        ));
    }
    public function clearPermissions(){
        $users = Yii::app()->db->createCommand()
            ->delete('authassignment','itemname=:itemname AND userid=:userid',array(
                ':userid' => $this->id, ':itemname' => 'end_task')
            );
        $users = Yii::app()->db->createCommand()
            ->delete('authassignment','itemname=:itemname AND userid=:userid',array(
                    ':userid' => $this->id, ':itemname' => 'reassign')
            );
        $users = Yii::app()->db->createCommand()
            ->delete('authassignment','itemname=:itemname AND userid=:userid',array(
                    ':userid' => $this->id, ':itemname' => 'view_archive')
            );

    }

    public function hasSupervisor($userId)
    {
        $supervisors = $this->supervisors;
        $hasSupervisor = false;
        foreach ($supervisors as $supervisor) {
            if ($supervisor->id == $userId) {
                $hasSupervisor = true;
                break;
            }
        }
        return $hasSupervisor;

    }

    public function isMemberInGroup($groupId)
    {
        if ($this->groupId == 0) {
            return false;
        } else {
            $groups = Yii::app()->db->createCommand()
                ->select('*')
                ->from('tbl_user_group_assignment')
                ->where('user_id=:user_id AND group_id=:group_id', array(':user_id' => $this->id, ':group_id' => $groupId))
                ->queryAll();
            return !empty($groups);
        }

    }


    protected function afterValidate()
    {
        parent::afterValidate();
        if ($this->isNewRecord) {
            $this->password = $this->hashPassword($this->password);
        }
    }

    protected function afterFind()
    {
        /* parent::afterFind(); // TODO: Change the autogenerated stub
         if ($this->isAdmin($this->id)) {
             $this->isAdmin = true;
         } else if ($this->isSupervisor($this->id)) {
             $this->isSupervisor = true;
             $users = $this->followUpUsers;
             foreach($users as $user){
                 $this->followUpUsersArray[] = $user->id;
             }
         } else {
             $this->isMember = true;
         }*/
    }


    public function getRole($userId)
    {
        if ($this->isAdmin($userId)) {
            return User::ROLE_ADMIN;
        } else if ($this->isSupervisor($userId)) {
            return User::ROLE_PROJECT_SUPERVISOR;
        } else {
            return User::ROLE_MEMBER;
        }
    }

    public function getProjectList()
    {
        $projects = $this->projects;
        return CHtml::listData($projects, 'id', 'name');
    }

    public function getFollowUpList()
    {
        $followUpUsers = $this->followUpUsers;
        return CHtml::listData($followUpUsers, 'id', 'username');
    }

    public function clearAssignment()
    {
        Yii::app()->db->createCommand()->delete('authassignment', 'userid=:userid', array(
            ':userid' => $this->id,
        ));
    }

    public static function getRolesOptionsList()
    {
        return array(
            self::ROLE_ADMIN => 'Admin',
            self::ROLE_PROJECT_SUPERVISOR => 'Supervisor',
            self::ROLE_MEMBER => 'Member',
        );
    }


    public function hashPassword($password)
    {
        return sha1(self::SALT . $password);

    }


    public function getUserList()
    {
        $users = User::model()->findAll();
        $usersArray = array();
        foreach ($users as $user) {
            $usersArray[$user->id] = $user->getFullName();
        }
        return $usersArray;
    }

    public function validatePassword($password)
    {
        return $this->password === $this->hashPassword($password);
    }

    /* ======= End of authentication and validation methods ======== */

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
