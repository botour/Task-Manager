<?php

/**
 * This is the model class for table "tbl_group".
 *
 * The followings are the available columns in table 'tbl_group':
 * @property integer $id
 * @property string $name
 * @property integer $status
 * @property string $description
 * @property string $note
 * @property string $create_date
 * @property integer $create_id
 * @property string $update_date
 * @property integer $update_id
 * @property User[] $users
 * @property integer $usersCount

 *
 * The followings are the available model relations:
 * @property UserGroupAssignment[] $userGroupAssignments
 */
class Group extends ProjStarActiveRecord
{

    const STATUS_IDEL = 0;
    const STATUS_ACTIVE = 1;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_group';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name,create_id, create_date', 'required'),
			array('status, create_id, update_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>255),
			array('description, note', 'safe'),

			array('id, name, status, description, note, create_date, create_id, update_date, update_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'userGroupAssignments' => array(self::HAS_MANY, 'UserGroupAssignment', 'group_id'),
            'users'=>array(self::MANY_MANY,'User','tbl_user_group_assignment(group_id,user_id)'),
            'usersCount'=>array(self::STAT,'User','tbl_user_group_assignment(group_id,user_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Group Name',
			'status' => 'Group Status',
			'description' => 'Group Description',
			'note' => 'Notes',
			'create_date' => 'Create Date',
			'create_id' => 'Create',
			'update_date' => 'Update Date',
			'update_id' => 'Update',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('note',$this->note,true);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('create_id',$this->create_id);
		$criteria->compare('update_date',$this->update_date,true);
		$criteria->compare('update_id',$this->update_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function addMember($userId){
        try{
            Yii::app()->db->createCommand()->insert('tbl_user_group_assignment',array(
                'user_id'=>$userId,
                'group_id'=>$this->id,
            ));
        }catch(CDbException $e){

        }

    }

    public function deleteMember($userId){
        try{
            Yii::app()->db->createCommand()->delete('tbl_user_group_assignment',"user_id=:user_id AND group_id=:group_id",array(
                ':user_id'=>$userId,
                ':group_id'=>$this->id,
            ));
        }catch(CDbException $e){

        }
    }
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Group the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
