<?php
/**
 *
 */

class ReportModel  extends CFormModel{
    const REPORT_TYPE_CREATE_TASK = 0;
    const REPORT_TYPE_COMPLETE_TASK = 1;
    const REPORT_TYPE_NOT_COMPLETE_TASK = 2;
    const REPORT_TYPE_END_TASK = 3;
    public $from_date;
    public $to_date;
    public $project_id;
    public $report_option;

    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('from_date,to_date,project_id,report_option','required'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'from_date' => 'From Date',
            'to_date' => 'To Date',
            'project_id' => 'Workspace',
            'report_option' => 'Report Option',

        );
    }

    public static function getReportOptionsArray(){
        return array(
            self::REPORT_TYPE_CREATE_TASK=>'Created Tasks',
            self::REPORT_TYPE_COMPLETE_TASK=>'Completed Tasks',
            self::REPORT_TYPE_NOT_COMPLETE_TASK=>'Revision Counts',
            self::REPORT_TYPE_END_TASK=>'Ended Tasks',
        );
    }
} 