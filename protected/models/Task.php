<?php

/**
 * This is the model class for table "tbl_task".
 *
 * The followings are the available columns in table 'tbl_task':
 * @property integer $id
 * @property integer $project_id
 * @property integer $user_id
 * @property string $serial_no
 * @property string $title
 * @property string $description
 * @property string $note
 * @property integer $priority
 * @property string $tag
 * @property integer $status
 * @property integer $notified
 * @property integer $expended_work_time
 * @property string $create_date
 * @property integer $create_id
 * @property string $update_date
 * @property integer $update_id
 * @property string $close_date
 * @property string $first_start_time
 * @property integer $close_id
 * @property string $start_time
 * @property string $end_time
 * @property integer $type
 * @property integer $second_priority
 * @property integer $second_priority_days
 * @property integer $third_priority
 * @property integer $third_priority_days
 *
 *
 * The followings are the available model relations:
 * @property Project $project
 * @property User $user
 */
class Task extends ProjStarActiveRecord
{

	public function tableName()
	{
		return 'tbl_task';
	}



    const SECOND_PRIORITY_NEVER = 0;
    const SECOND_PRIORITY_LOW = 1;
    const SECOND_PRIORITY_MEDIUM = 2;
    const SECOND_PRIORITY_HIGH = 3;

    const THIRD_PRIORITY_NEVER = 0;
    const THIRD_PRIORITY_LOW = 1;
    const THIRD_PRIORITY_MEDIUM = 2;
    const THIRD_PRIORITY_HIGH = 3;

    const TYPE_MANUAL = 0;
    const TYPE_AUTOMATIC = 1;

    const PRIORITY_LOW = 0;
    const PRIORITY_MEDIUM = 1;
    const PRIORITY_HIGH = 2;

    const STATE_NOT_READ = 0;
    const STATE_VERIFIED = 3;
    const STATE_COMPLETE = 2;
    const STATE_INCOMPLETE = 1;

    const SUPERVISOR_NOT_NOTIFIED = 0;
    const SUPERVISOR_NOTIFIED = 1;
    const SUPERVISOR_NOT_RE_NOTIFIED = 2;
    const USER_NOTIFIED = 1;
    const USER_NOT_NOTIFIED = 0;
    const USER_NOT_RE_NOTIFIED = 2;

    const UPDATE_TYPE_REMINDED = 5;
    const UPDATE_TYPE_REASSIGN = 1;
    const UPDATE_TYPE_COMPLETE =2;
    const UPDATE_TYPE_NOTIFIED = 3;
    const UPDATE_TYPE_NOT_COMPLETE = 4;

    const TASK_REMINDED = 1;
    const TASK_NOT_REMINDED = 0;

	/**
	 * @return array validation rules for model attributes.
	 */

    public $createUsername;
    public $daysNo;
    public $endByUsername;
    public $fromUsername;
    public $toUsername;
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title,priority,user_id,project_id', 'required'),
            array('project_id, user_id, priority, status, create_id, update_id, close_id', 'numerical', 'integerOnly'=>true),
			array('title, tag', 'length', 'max'=>255),
			array('description, note, update_date, close_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
            array('description,note','safe'),
			array('id, project_id, user_id, title, description, note, priority, tag, status, create_date, create_id, update_date, update_id, close_date, close_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'project' => array(self::BELONGS_TO, 'Project', 'project_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
            'creatorUser'=>array(self::BELONGS_TO, 'User', 'create_id'),
            'updaterUser'=>array(self::BELONGS_TO, 'User', 'update_id'),
            'comments'=>array(self::HAS_MANY,'Comment','task_id','order'=>'id DESC'),
            'supervisorNotifications'=>array(self::HAS_MANY,'SupervisorNotification','task_id'),
		);
	}

    public function setTag($tag){
        $project = Project::model()->find('name=:name',array(
            ':name'=>$tag,
        ));
        if(!is_null($project)){
            $this->tag = $project->name;
            $this->project_id = $project->id;
        }

    }

    public function getMembersList(){
        $members = $this->project->members;
        $membersArray = array();
        foreach($members as $user){
            $membersArray[$user->id] = $user->getFullName();
        }
        return $membersArray;
    }


	/**
+	 * @return array customized attribute labels (name=>label)
	 */

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'project_id' => 'Workspace Name',
			'user_id' => 'Member',
			'title' => 'Title',
            'due_to_date'=>'Due Date',
			'description' => 'Description',
			'note' => 'Note',
			'priority' => 'Priority',
			'tag' => 'Tag',
			'status' => 'Status',
			'create_date' => 'Create Date',
			'create_id' => 'Create',
			'update_date' => 'Update Date',
			'update_id' => 'Update',
			'close_date' => 'Close Date',
			'close_id' => 'Close',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search($forceDefaultOrder)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
        $criteria->alias = 'todoTask';
		$criteria->compare('id',$this->id);
		$criteria->compare('project_id',$this->project_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('note',$this->note,true);
		$criteria->compare('priority',$this->priority);
		$criteria->compare('tag',$this->tag,true);
		$criteria->compare('todoTask.status',$this->status);
		$criteria->compare('todoTask.create_date',$this->create_date,true);
		$criteria->compare('todoTask.create_id',$this->create_id);
		$criteria->compare('todoTask.update_date',$this->update_date,true);
		$criteria->compare('todoTask.update_id',$this->update_id);
		$criteria->compare('close_date',$this->close_date,true);
		$criteria->compare('close_id',$this->close_id);
        $criteria->with=array(
            'creatorUser',
        );
        $sort = new CSort();
        $sort->attributes = array(
            'fromUsername'=>array(
                'asc'=>'creatorUser.username',
                'desc'=>'creatorUser.username DESC',
                'label'=>'from',
            ),
            'title',
            'priority',
            'create_date'=>array(
                'asc'=>'todoTask.create_date',
                'desc'=>'todoTask.create_date DESC',
                'label'=>'D',
            ),
            'tag',
        );
        if($forceDefaultOrder){
            $criteria->order = 'reminded DESC,todoTask.create_date DESC';
        }else{
            $sort->applyOrder($criteria);
        }
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort'=>$sort,
            'pagination'=>array(
                'pageSize'=>30,
            ),
		));
	}
    public function searchFollowUp($forceDefaultOrder)
    {
        // @todo Please modify the following code to remove attributes that should not be searched.
        $criteria=new CDbCriteria;

        $criteria->alias = 'followUpTask';

        $criteria->compare('id',$this->id);
        $criteria->compare('project_id',$this->project_id);
        $criteria->compare('followUpTask.user_id',$this->user_id);
        $criteria->compare('title',$this->title,true);
        $criteria->compare('description',$this->description,true);
        $criteria->compare('note',$this->note,true);
        $criteria->compare('priority',$this->priority);
        $criteria->compare('tag',$this->tag,true);
        $criteria->compare('followUpTask.status',$this->status);
        $criteria->compare('followUpTask.create_date',$this->create_date,true);
        $criteria->compare('followUpTask.create_id',$this->create_id);
        $criteria->compare('followUpTask.update_date',$this->update_date,true);
        $criteria->compare('followUpTask.update_id',$this->update_id);
        $criteria->compare('close_date',$this->close_date,true);
        $criteria->compare('close_id',$this->close_id);
        $criteria->with=array(
            'creatorUser',
            'user',
        );
        $sort = new CSort();
        $sort->attributes = array(
            'fromUsername'=>array(
                'asc'=>'creatorUser.username',
                'desc'=>'creatorUser.username DESC',
                'label'=>'from',
            ),
            'toUsername'=>array(
                'asc'=>'user.username',
                'desc'=>'user.username DESC',
                'label'=>'assigned to',
            ),
            'title',
            'priority',
            'create_date'=>array(
                'asc'=>'followUpTask.create_date',
                'desc'=>'followUpTask.create_date DESC',
                'label'=>'D',
            ),
            'tag',
        );
        if($forceDefaultOrder){
            $criteria->order = 'reminded DESC,followUpTask.create_date DESC';
        }else{
            $sort->applyOrder($criteria);
        }

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort'=>$sort,
            'pagination'=>array(
                'pageSize'=>30,
            ),
        ));
    }

    public function getTitleForGrid(){
        if($this->reminded==Task::TASK_REMINDED){
            $title = '<i class="fa fa-bell"></i> '.$this->title;
        }else{
            $title = $this->title;
        }
        return $title;
    }

    public function completeTaskSearch($forceDefaultOrder)
    {
        $criteria=new CDbCriteria;
        $criteria->alias = 'completeTask';
        $criteria->compare('id',$this->id);
        $criteria->compare('project_id',$this->project_id);
        $criteria->compare('completeTask.user_id',$this->user_id);
        $criteria->compare('title',$this->title,true);
        $criteria->compare('description',$this->description,true);
        $criteria->compare('note',$this->note,true);
        $criteria->compare('priority',$this->priority);
        $criteria->compare('tag',$this->tag,true);
        $criteria->compare('completeTask.status',$this->status);
        $criteria->compare('completeTask.create_date',$this->create_date,true);
        $criteria->compare('completeTask.create_id',$this->create_id);
        $criteria->compare('completeTask.update_date',$this->update_date,true);
        $criteria->compare('completeTask.update_id',$this->update_id);
        $criteria->compare('close_date',$this->close_date,true);
        $criteria->compare('close_id',$this->close_id);

        $criteria->with=array(
            'creatorUser',
            'user',
        );
        $sort = new CSort();
        $sort->attributes = array(
            'fromUsername'=>array(
                'asc'=>'creatorUser.username',
                'desc'=>'creatorUser.username DESC',
                'label'=>'from',
            ),
            'toUsername'=>array(
                'asc'=>'user.username',
                'desc'=>'user.username DESC',
                'label'=>'assigned to',
            ),
            'title',
            'priority',
            'create_date'=>array(
                'asc'=>'completeTask.create_date',
                'desc'=>'completeTask.create_date DESC',
                'label'=>'D',
            ),
            'tag',
        );

        if($forceDefaultOrder){
            $criteria->order = 'completeTask.update_date DESC';
        } else {
            $sort->applyOrder($criteria);
        }

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort'=>$sort,
            'pagination'=>array(
                'pageSize'=>30,
            ),
        ));
    }

    public function archivedTaskSearch($forceDefaultOrder)
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;
        $criteria->alias = 'archivedTask';
        $criteria->compare('id',$this->id);
        $criteria->compare('project_id',$this->project_id);
        $criteria->compare('archivedTask.user_id',$this->user_id);
        $criteria->compare('title',$this->title,true);
        $criteria->compare('description',$this->description,true);
        $criteria->compare('note',$this->note,true);
        $criteria->compare('priority',$this->priority);
        $criteria->compare('tag',$this->tag,true);
        $criteria->compare('archivedTask.status',Task::STATE_VERIFIED);
        $criteria->compare('archivedTask.create_date',$this->create_date,true);
        $criteria->compare('archivedTask.create_id',$this->create_id);
        $criteria->compare('archivedTask.update_date',$this->update_date,true);
        $criteria->compare('archivedTask.update_id',$this->update_id);
        $criteria->compare('close_date',$this->close_date,true);
        $criteria->compare('close_id',$this->close_id);

        $criteria->with=array(
            'updaterUser',
            'user',
            'creatorUser'
        );
        $sort = new CSort();
        $sort->attributes = array(
            'toUsername'=>array(
                'asc'=>'user.username',
                'desc'=>'user.username DESC',
                'label'=>'assigned to',
            ),
            'fromUsername'=>array(
                'asc'=>'creatorUser.username',
                'desc'=>'creatorUser.username DESC',
                'label'=>'from',
            ),
            'endByUsername'=>array(
                'asc'=>'updaterUser.username',
                'desc'=>'updaterUser.username DESC',
                'label'=>'end By',
            ),
            'tag',
            'create_date'=>array(
                'asc'=>'archivedTask.create_date',
                'desc'=>'archivedTask.create_date DESC',
                'label'=>'D',
            ),
            'title',
        );
        $criteria->limit = 200;
        if($forceDefaultOrder){
            $criteria->order = 'archivedTask.update_date DESC';
        }else{
            $sort->applyOrder($criteria);
        }
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort'=>$sort,
            'pagination'=>array(
                'pageSize'=>200,
            ),
        ));
    }

    public static function getPriorityOptionsArray(){
        return array(
            self::PRIORITY_LOW=>'L',
            self::PRIORITY_MEDIUM=>'M',
            self::PRIORITY_HIGH=>'H',
        );
    }

    public function getPriorityText(){
        $priorityArray = self::getPriorityOptionsArray();
        return $priorityArray[$this->priority];
    }

    public static function getStatusOptionsArray(){
        return array(
            self::STATE_NOT_READ=>'Not Read',
            self::STATE_INCOMPLETE=>'Incomplete',
            self::STATE_COMPLETE=>'Complete',
            self::STATE_VERIFIED=>'Verified',
        );
    }

    public static function getStatusOptionsForList(){
        return array(
            self::STATE_HOLD=>'Hold',
            self::STATE_ON_GOING=>'On Going',
            self::STATE_COMPLETE=>'Complete',
        );
    }
    public function getStatusText(){
        $statusArray = self::getStatusOptionsArray();
        return $statusArray[$this->status];
    }

    protected function beforeSave(){
        if($this->skipBeforeSave){
            return parent::beforeSave();
        }
        if($this->isNewRecord){
            $this->status = self::STATE_NOT_READ;
            $tasksCount = 0;
            if($this->type==Task::TYPE_MANUAL){
                $tasksCount = intval($count = Task::model()->count('YEAR(create_date)=:date AND type=:type',array(
                    ':date'=>date('Y'),
                    ':type'=>Task::TYPE_MANUAL,
                )));
            }
            $count = $tasksCount+1;
            if($this->type == Task::TYPE_MANUAL){
                $this->serial_no = 'T'.date('y').'-'.$count;
            }
        }
        $this->close_date = date('Y-m-d');
        $this->close_id = Yii::app()->user->id;
        return parent::beforeSave();
    }

    /*public function setUserId($userId){
        if(User::model()->isAdmin($userId)){
            $this->user_id = $userId;
        }else {
            $project = $this->project;
            $user = User::model()->findByPk($userId);
            if($user->isMemberInProject($project->id)){
                $this->user_id = $userId;
            }else{
                $this->addError("user_id","The User Doesn't Belong to the workspace");
            }
        }
    }*/

    public function getTaskClassNames(){
        $classNames = "task-grid-row";
        if($this->status==Task::STATE_NOT_READ){
            $classNames .=" unread";

        }
        if($this->status == Task::STATE_NOT_READ || $this->status == Task::STATE_INCOMPLETE){
            if(!is_null($this->due_to_date)){
                if($this->due_to_date==date('Y-m-d')){
                    $classNames .=" deadline_task_time";
                }else if($this->due_to_date<date('Y-m-d')){
                    $classNames .=" exceeded_task_time";
                }
            }
        }
        if($this->reminded==Task::TASK_REMINDED){
            $classNames .=" reminded";
        }
        return $classNames;
    }
    public function setStatus($status){
        $bool = false;
        if($this->status == Task::STATE_PENDING){
            if($status==Task::STATE_ON_GOING){
                $this->status = $status;
                $this->start_time = date("Y-m-d H:i:s",time());
                $this->expended_work_time = 0;
                $this->first_start_time = date("Y-m-d H:i:s",time());
                $bool =true;
            }
        }else if($this->status == Task::STATE_ON_GOING){
            if($status!= Task::STATE_ON_GOING || $status!= Task::STATE_PENDING){
                if($status == Task::STATE_COMPLETE){
                    $this->end_time = date("Y-m-d H:i:s",time());
                    $this->create_date = date('Y-m-d');
                    $this->close_id = Yii::app()->user->id;
                }
                $this->calculateDifference();
                $this->status = $status;
                $bool =true;
            }
        }else{
            if($status== Task::STATE_ON_GOING){
                $this->start_time = date("Y-m-d H:i:s",time());
                $this->status = $status;
                $bool =true;
            }
        }
        return $bool;
    }

    public function getStateChangeList(){
        if($this->status == Task::STATE_PENDING){
            $array = array(
                self::STATE_ON_GOING =>'On Going',
            );
        }else if($this->status == Task::STATE_ON_GOING){
            $array = array(
                self::STATE_HOLD =>'Hold',
                self::STATE_COMPLETE => 'Complete',
            );
        }else{
            $array = array(
                self::STATE_ON_GOING =>'On Going',
            );
        }
        return $array;
    }

    public function calculateDifference(){
        $currentTime = time();
        $start_time = strtotime($this->start_time);
        $this->expended_work_time += $currentTime - $start_time;
    }

    public function getTaskProductionDetails(){
        $productionArray = CommonFunctions::calculateTime($this->expended_work_time);
        $message = "";
        $message .= "#Days: ".$productionArray['days']." | ";
        $message .= "#Hours: ".$productionArray['hours']." | ";
        $message .= "#Minutes: ".$productionArray['minutes'];
        return $message;
    }

    public function getCreatorUsername(){
        $user = User::model()->findByPk($this->create_id);
        return $user->first_name." ".$user->last_name;
    }
    public function getUpdaterUsername(){
        $user = User::model()->findByPk($this->update_id);
        return $user->first_name." ".$user->last_name;
    }
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Task the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
