<?php

/**
 * This is the model class for table "tbl_auto_task".
 *
 * The followings are the available columns in table 'tbl_auto_task':
 * @property integer $id
 * @property integer $user_id
 * @property integer $project_id
 * @property string $serial_no
 * @property integer $status
 * @property integer $priority
 * @property string $title
 * @property string $description
 * @property string $note
 * @property string $tag
 * @property integer $occurrence
 * @property string $activation_date
 * @property integer $repeat_type
 * @property integer $end_type
 * @property string $end_repeat
 * @property string $due_to_date
 * @property integer $second_priority
 * @property integer $second_priority_days
 * @property integer $third_priority
 * @property integer $third_priority_days
 *
 * The followings are the available model relations:
 * @property User $user
 * @property Project $project
 * @property AutoTaskParam[] $autoTaskParams
 */
class AutoTask extends ProjStarActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_auto_task';
	}

	/**
	 * @return array validation rules for model attributes.
	 */

    const PRIORITY_LOW = 0;
    const PRIORITY_MEDIUM = 1;
    const PRIORITY_HIGH = 2;

    const SECOND_PRIORITY_NEVER = 0;
    const SECOND_PRIORITY_LOW = 1;
    const SECOND_PRIORITY_MEDIUM = 2;
    const SECOND_PRIORITY_HIGH = 3;

    const STATUS_DISABLED = 0;
    const STATUS_ENABLED = 1;

    const THIRD_PRIORITY_NEVER = 0;
    const THIRD_PRIORITY_LOW = 1;
    const THIRD_PRIORITY_MEDIUM = 2;
    const THIRD_PRIORITY_HIGH = 3;

    const REPEAT_TYPE_DAILY = 1;
    const REPEAT_TYPE_WEEKLY = 2;
    const REPEAT_TYPE_MONTHLY = 3;
    const REPEAT_TYPE_YEARLY = 4;

    const END_TYPE_NEVER = 0;
    const END_TYPE_SPECIFIC_DATE=1;

	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, project_id, serial_no, status, priority, title, occurrence, activation_date, repeat_type, end_type, second_priority, third_priority', 'required','on'=>'create'),
            array('user_id, project_id, priority, title, activation_date, repeat_type, end_type, second_priority, third_priority', 'required','on'=>'update'),
			array('user_id, project_id, status, priority, occurrence, repeat_type, end_type, second_priority, second_priority_days, third_priority, third_priority_days', 'numerical', 'integerOnly'=>true),
			array('serial_no', 'length', 'max'=>55),
            array('third_priority_days,second_priority_days','numerical','integerOnly'=>true),
            array('activation_date,end_repeat,due_to_date','date','format'=>"yyyy-MM-dd"),
            array('end_type','checkDate'),
            array('second_priority','checkSecondPriorityValue'),
            array('third_priority','checkThirdPriorityValue'),
            array('activation_date','compareToEndRepeat'),
            array('activation_date','compareToDueToDate'),
			array('title, tag', 'length', 'max'=>255),
			array('description,note,tag', 'safe','on'=>'create'),
            array('description,note,tag', 'safe','on'=>'update'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, project_id, serial_no, status, priority, title, description, note, tag, occurrence, activation_date, repeat_type, end_type, end_repeat, due_to_date, second_priority, second_priority_days, third_priority, third_priority_days', 'safe', 'on'=>'search'),
		);
	}
    // Custom Validators
    public function checkDate($attribute,$params)
    {
        if(!$this->hasErrors())
        {
            if($this->end_type==self::END_TYPE_SPECIFIC_DATE){
                if(strlen($this->end_repeat)==0){
                    $this->addError('end_type','A date must be provided!');
                }
            }
        }
    }

    public function checkSecondPriorityValue($attribute,$params)
    {
        if(!$this->hasErrors())
        {
            if($this->second_priority!=self::SECOND_PRIORITY_NEVER){
                if(strlen($this->second_priority_days)==0){
                    $this->addError('second_priority','A number of days must be provided');
                }
            }else{
                $this->second_priority_days = "";
            }
        }
    }
    public function checkThirdPriorityValue($attribute,$params)
    {
        if(!$this->hasErrors())
        {
            if($this->third_priority!=self::THIRD_PRIORITY_NEVER){
                if(strlen($this->third_priority_days)==0){
                    $this->addError('third_priority','A number of days must be provided');
                }
            }else{
                $this->third_priority_days = "";
            }
        }
    }

    public function compareToEndRepeat($attribute,$params){
        if(!$this->hasErrors())
        {
            if($this->end_type==self::END_TYPE_SPECIFIC_DATE){
                if(strtotime($this->activation_date)>strtotime($this->end_repeat)){
                    $this->addError('end_repeat','End repeat date must be after activation date');
                }
            }
        }
    }

    public function compareToDueToDate($attribute,$params){
        if(!$this->hasErrors())
        {
            if(strlen($this->due_to_date)>0){
                if(strtotime($this->activation_date)>strtotime($this->due_to_date)){
                    $this->addError('due_to_date','Due Date must be after activation date');
                }
            }
        }
    }
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'project' => array(self::BELONGS_TO, 'Project', 'project_id'),
			'autoTaskParams' => array(self::HAS_MANY, 'AutoTaskParam', 'tbl_auto_task_id'),
            'comments'=> array(self::HAS_MANY,'AutoTaskComment','auto_task_id','order'=>'id DESC'),
		);
	}

    public static function getPriorityOptionsArray(){
        return array(
            self::PRIORITY_LOW=>'Low',
            self::PRIORITY_MEDIUM=>'Medium',
            self::PRIORITY_HIGH=>'High',
        );
    }
    public static function getStatusOptionsArray(){
        return array(
            self::STATUS_ENABLED=>'Yes',
            self::STATUS_DISABLED=>'No',
        );
    }
    public static function getSecondPriorityOptionsArray(){
        return array(
            self::SECOND_PRIORITY_NEVER=>'Never',
            self::SECOND_PRIORITY_LOW=>'Low',
            self::SECOND_PRIORITY_MEDIUM=>'Medium',
            self::SECOND_PRIORITY_HIGH=>'High',
        );
    }
    public static function getThirdPriorityOptionsArray(){
        return array(
            self::THIRD_PRIORITY_NEVER=>'Never',
            self::THIRD_PRIORITY_LOW=>'Low',
            self::THIRD_PRIORITY_MEDIUM=>'Medium',
            self::THIRD_PRIORITY_HIGH=>'High',
        );
    }

    public static function getRepeatOptionsArray(){
        return array(
            self::REPEAT_TYPE_DAILY=>'Daily',
            self::REPEAT_TYPE_WEEKLY=>'Weekly',
            self::REPEAT_TYPE_MONTHLY=>'Monthly',
            self::REPEAT_TYPE_YEARLY=>'Yearly',
        );
    }

    public static function getEndTypeOptionsArray(){
        return array(
            self::END_TYPE_NEVER=>'Never',
            self::END_TYPE_SPECIFIC_DATE=>'Specific Date',
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'Assign To',
			'project_id' => 'Workspace',
			'serial_no' => 'Serial No',
			'status' => 'Active',
			'priority' => 'Priority 1',
			'title' => 'Title',
			'description' => 'Description',
			'note' => 'Note',
			'tag' => 'Tag',
			'occurrence' => 'Occurrence',
			'activation_date' => 'Activation Date',
			'repeat_type' => 'Repeat Type',
			'end_type' => 'End Type',
			'end_repeat' => 'End Repeat',
			'due_to_date' => 'Due Date',
			'second_priority' => 'Priority 2',
			'second_priority_days' => 'After',
			'third_priority' => 'Priority 3',
			'third_priority_days' => 'After',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('project_id',$this->project_id);
		$criteria->compare('serial_no',$this->serial_no,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('priority',$this->priority);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('note',$this->note,true);
		$criteria->compare('tag',$this->tag,true);
		$criteria->compare('occurrence',$this->occurrence);
		$criteria->compare('activation_date',$this->activation_date,true);
		$criteria->compare('repeat_type',$this->repeat_type);
		$criteria->compare('end_type',$this->end_type);
		$criteria->compare('end_repeat',$this->end_repeat,true);
		$criteria->compare('due_to_date',$this->due_to_date,true);
		$criteria->compare('second_priority',$this->second_priority);
		$criteria->compare('second_priority_days',$this->second_priority_days);
		$criteria->compare('third_priority',$this->third_priority);
		$criteria->compare('third_priority_days',$this->third_priority_days);
        $criteria->order = 'create_date DESC';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function getTaskClassNames(){
        return "task-grid-row";
    }
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AutoTask the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
