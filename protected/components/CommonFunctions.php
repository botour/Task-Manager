<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Johnny & Ahmad
 * Date: 10/23/13
 * Time: 10:56 AM
 * To change this template use File | Settings | File Templates.
 */

Yii::import('ext.Browser');

class CommonFunctions
{

    /*
   * Generate random password
     */
    const TYPE_active = 1;
    /*
   * Get status id active or not
   * $type => 1= active ,2= direction
     * 1=read | 0=new | 2=new reply | 3=waiting for reply|4=closed
   */
    const TASK_STATUS = 1;
    const TASK_PRIORITY = 2;
    const USER_STATUS = 3;
    const USER_ROLE = 4;
    const AUTO_TASK_STATUS = 5;
    public static function generatePassword()
    {
        return Process::random_string(10, 10);
    }

    public static function getLabel($status, $type = Self::TASK_STATUS)
    {
        if ($type == self::TASK_STATUS) {
            $array = array(
                Task::STATE_PENDING => '<span class="label label-primary">' . 'Pending' . '</span>',
                Task::STATE_ON_GOING => '<span class="label label-danger">' . 'On Going' . '</span>',
                Task::STATE_HOLD => '<span class="label label-warning">' . 'Hold' . '</span>',
                Task::STATE_COMPLETE => '<span class="label label-success">' . 'Complete' . '</span>',
            );
        }
        if ($type == self::TASK_PRIORITY) {
            $array = array(
                Task::PRIORITY_LOW => '<span></span>',
                Task::PRIORITY_MEDIUM => '<span class="label label-warning">' . "M" . '</span>',
                Task::PRIORITY_HIGH => '<span class="label label-danger">' . "H" . '</span>',
            );
        }
        if ($type == self::AUTO_TASK_STATUS) {
            $array = array(
                AutoTask::STATUS_DISABLED => '<span class="label label-default">' . "No" . '</span>',
                AutoTask::STATUS_ENABLED => '<span class="label label-success">' . "Yes" . '</span>',
            );
        }
        if($type == self::USER_ROLE){
            $array = array(
                User::ROLE_MEMBER => '<span>Member</span>',
                User::ROLE_PROJECT_SUPERVISOR => '<span class="supervisor-label">Supervisor</span>',
                User::ROLE_ADMIN => '<span class="admin-label">Admin</span>',
            );
        }
        if ($type == self::USER_STATUS) {
            $array = array(
                User::STATE_OFFLINE => '<span class="label label-danger">' . "Offline" . '</span>',
                User::STATE_BREAK => '<span class="label label-info">' . "Break" . '</span>',
                User::STATE_ONLINE => '<span class="label label-success">' . "Online" . '</span>',
            );
        }
        return $array[$status];
    }

    public static function getBootstrapModal($id = 'modal', $label = 'modalLabel', $title, $content, $width = 'auto', $height = 'auto')
    {

        $modal = '<!-- Boostrap modal dialog -->
                    <div class="modal fade" id="' . $id . '" tabindex="-1" role="dialog" aria-labelledby="' . $label . '" aria-hidden="true">
                        <div class="modal-dialog" style="width:' . $width . ';">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">' . $title . '</h4>
                                </div>
                                <div class="modal-body" style="height:' . $height . ';">' . $content . '</div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->';
        return $modal;
    }

    /*
     * Tracking function
     * track visitors ad users
     */

    public static function Tracking()
    {
        $model = new Visitors;
        $b = new Browser;

        $model->link = $b->getUrl();
        $model->ip = $b->getIp();
        $model->os = $b->getPlatform();

        $model->browser = $b->getBrowser();
        $model->lang = $b->getLanguage();
        if (isset(Yii::app()->user->id)) {
            $model->user_id = Yii::app()->user->id;
        }
        $model->product_id = Helper::PRODUCT;
        $model->country = $b->getCountry();
        $model->save(false);


    }

    /*
       * detect language
       */

    public static function detectLanguage()
    {
        $app = Yii::app();
        $b = new Browser();
        $lang = $b->getLanguage();

        if (isset(yii::app()->request->cookies['lang'])) {
            if (yii::app()->request->cookies['lang'] == 'ar') {
                $app->theme = 'Genyx-ar';
                $app->language = 'ar';
            } else {
                $app->theme = 'Genyx';
                $app->language = 'en';
            }

        } else {
            if ($lang == 'ar') {
                $app->theme = 'Genyx-ar';
                $app->language = 'ar';
            } else {
                $app->theme = 'Genyx';
                $app->language = 'en';
            }
        }

    }

    public static function getMonthsArray()
    {

        return array(
            '1' => Yii::t('app', 'Jan'),
            '2' => Yii::t('app', 'Feb'),
            '3' => Yii::t('app', 'Mar'),
            '4' => Yii::t('app', 'Apr'),
            '5' => Yii::t('app', 'May'),
            '6' => Yii::t('app', 'Jun'),
            '7' => Yii::t('app', 'Jul'),
            '8' => Yii::t('app', 'Aug'),
            '9' => Yii::t('app', 'Sep'),
            '10' => Yii::t('app', 'Oct'),
            '11' => Yii::t('app', 'Nov'),
            '12' => Yii::t('app', 'Dec'),
        );
    }

    public static function getDaysArray()
    {
        return array(1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24, 25 => 25, 26 => 26, 27 => 27, 28 => 28, 29 => 29, 30 => 30, 31 => 31);

    }

    public static function getYearsArray()
    {
        $years = array();
        $year = date('Y');
        for ($i = 0; $i < 100; $i++) {
            $years[$year] = $year;
            $year = date("Y", strtotime(-1 * $i . " year"));
        }
        if (Yii::app()->language == 'en') {

        }
        return $years;


    }


    /*
     * product languages
     *
     */

    public static function languagesList()
    {
        return array('en' => 'English Language', 'ar' => 'اللغة العربية', 'fr' => 'French Language');
    }


    /*
     * product Languages array
     */

    public static function languagesArray()
    {
        return array('en', 'ar', 'fr');
    }

    /*
 * get  related to language
 */

    public static function getDbSuffix()
    {
        if (Yii::app()->language == 'ar') {
            return 'ar_';
        } else {
            return 'en_';
        }
    }

    /*
     * sent language
     */

    public static function setPrefix()
    {
        if (language() == 'ar') {
            return '_ar';
        }
        if (language() == 'en') {
            return '_en';
        }
        if (language() == 'fr') {
            return '_fr';
        }
    }


    /*
     * Return user id
     */

    public static function getUserID()
    {
        if (isset(Yii::app()->user->user_id)) {
            return Yii::app()->user->user_id;
        } else {
            return null;
        }
    }


    /*
     * fix dublicate files with Ajax requests
     *
     */

    public static function fixClientScriptForTaskManager(){

        Yii::app()->clientScript->scriptMap['datepicker3.css'] = false;
        Yii::app()->clientScript->scriptMap['green.css'] = false;
        Yii::app()->clientScript->scriptMap['red.css'] = false;
        Yii::app()->clientScript->scriptMap['bootstrap-datepicker.js'] = false;
        Yii::app()->clientScript->scriptMap['notify.min.js'] = false;
        Yii::app()->clientScript->scriptMap['icheck.min.js'] = false;
        Yii::app()->clientScript->scriptMap['task_scripts.js'] = false;
        Yii::app()->clientScript->scriptMap['tasks_main.js'] = false;
        Yii::app()->clientscript->scriptMap['jquery.js'] = false;
        Yii::app()->clientscript->scriptMap['jquery.min.js'] = false;
        if(Yii::app()->clientScript->isScriptRegistered("task-grid-script")){
            Yii::app()->clientscript->scripts['task-grid-script'] = "//";
        }
    }
    public static function fixAjax()
    {
        if (Yii::app()->request->isAjaxRequest) {
//css
            Yii::app()->clientScript->scriptMap['app.css'] = false;
            Yii::app()->clientScript->scriptMap['bootstrap.css'] = false;
            Yii::app()->clientScript->scriptMap['bootstrap-theme.css'] = false;
            Yii::app()->clientScript->scriptMap['custom.css'] = false;
            Yii::app()->clientScript->scriptMap['bootstrapSwitch.css'] = false;
            Yii::app()->clientScript->scriptMap['spectrum.css'] = false;
            //Yii::app()->clientScript->scriptMap['datepicker.css'] = false;
            Yii::app()->clientScript->scriptMap['select2.css'] = false;
            Yii::app()->clientScript->scriptMap['ui.multiselect.css'] = false;
            Yii::app()->clientScript->scriptMap['bootstrap-wysihtml5.css'] = false;
//js
            Yii::app()->clientscript->scriptMap['jquery.js'] = false;
            // Yii::app()->clientscript->scriptMap['jquery.min.js'] = false;
            // Yii::app()->clientscript->scriptMap['jquery.yiiactiveform.js'] = false;
            // Yii::app()->clientscript->scriptMap['jquery.ba-bbq.js'] = false;
            // Yii::app()->clientscript->scriptMap['jquery.ba-bbq.min.js'] = false;
            Yii::app()->clientscript->scriptMap['jquery.pnotify.min.js'] = false;
            Yii::app()->clientscript->scriptMap['jquery.autosize-min.js'] = false;
            Yii::app()->clientscript->scriptMap['jquery.inputlimiter.1.3.min.js'] = false;
            Yii::app()->clientscript->scriptMap['jquery.mask.min.js'] = false;
            Yii::app()->clientscript->scriptMap['bootstrapSwitch.js'] = false;
            Yii::app()->clientscript->scriptMap['globalize.js'] = false;
            Yii::app()->clientscript->scriptMap['spectrum.js'] = false;
           // Yii::app()->clientscript->scriptMap['bootstrap-datepicker.js'] = false;
            Yii::app()->clientscript->scriptMap['select2.js'] = false;
            Yii::app()->clientscript->scriptMap['ui.multiselect.js'] = false;
            Yii::app()->clientscript->scriptMap['bootstrap-wysihtml5.js'] = false;
            Yii::app()->clientscript->scriptMap['form-elements.js'] = false;
            Yii::app()->clientscript->scriptMap['bootstrap.js'] = false;
            Yii::app()->clientscript->scriptMap['conditionizr.min.js'] = false;
            Yii::app()->clientscript->scriptMap['jquery.nicescroll.min.js'] = false;
            Yii::app()->clientscript->scriptMap['jRespond.min.js'] = false;
            // Yii::app()->clientscript->scriptMap['jquery.genyxAdmin.js'] = false;
            //  Yii::app()->clientscript->scriptMap['jquery.uniform.min.js'] = false;
            //  Yii::app()->clientscript->scriptMap['app.js'] = false;
            //  Yii::app()->clientscript->scriptMap['domready.js'] = false;
            //   Yii::app()->clientscript->scriptMap['jquery.yiigridview.js'] = false;
        }

    }

    /*
     * return files icons
     * @ext = extension
     */
    public static function getFilesIcons($ext)
    {

        $icon = "";
        switch ($ext) {

            case "jpg":
                $icon = "<i class='i-image'></i>";
                break;
            case "png":
                $icon = "<i class='i-image'></i>";
                break;
            case "gif":
                $icon = "<i class='i-image'></i>";
                break;
            case "doc":
                $icon = "<i class='i-file-word'></i>";
                break;
            case "docx":
                $icon = "<i class='i-file-word'></i>";
                break;
            case "pdf":
                $icon = "<i class='i-file-pdf'></i>";
                break;
            case "xlsx":
                $icon = "<i class='i-file-excel'></i>";
                break;
            case "rar":
                $icon = "<i class='i-file-zip'></i>";
                break;
            case "zip":
                $icon = "<i class='i-file-zip'></i>";
                break;
            default:
                $icon = "<i class='i-libreoffice'></i>";

        }
        return $icon;
    }

    public static function calculateTime($seconds){
        $time = $seconds;
        $days = (int)($time/86400);
        $time = $time - $days*86400;
        $hours =  (int)($time/3600);
        $time = $time - $hours*3600;
        $minutes = (int)($time/60);
        return array(
            'hours'=>$hours,
            'days'=>$days,
            'minutes'=>$minutes,
        );
    }

}

