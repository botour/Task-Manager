<?php /* @var $this Controller */

$setting = Setting::model()->find('name=:name',array(
    ':name'=>Setting::SETTING_APP_NAME,
));
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="en"/>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <!-- Core Script Files -->
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.js"></script>

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/styles.css"/>

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome.css"/>

    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . "/js/main.js", CClientScript::POS_END); ?>
    <title><?php echo $setting->value; ?></title>

</head>
<body>
<!-- Fixed navbar -->
<nav
    style="display: <?php echo Yii::app()->controller->id == "task" && Yii::app()->controller->action->id == "index" ? "none" : "block"; ?>"
    class="navbar navbar-static" role="navigation">
    <div id="loading-indicator" style="display: none;">
        <span>Loading, please wait ...</span>
    </div>
    <div class="container">
        <?php if (!Yii::app()->user->isGuest): ?>
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav no-hover">
                    <li><?php echo CHtml::link($setting->value,'',array(
                            'id'=>'app-name-link',
                        ));?>
                    </li>
                    <?php
                        $user = User::model()->findByPk(Yii::app()->user->id);
                        if ($user->current_project_id != 0) {
                            $project = Project::model()->findByPk($user->current_project_id);
                            if(!is_null($project)){
                                $projectName = $project->name;
                            }else{
                                $projectName = "All-Workspaces";
                            }

                        } else {
                            $projectName = "All-Workspaces";
                        }
                    ?>


                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle large-nav-text nav-link" data-toggle="dropdown" role="button"
                           aria-expanded="false"><?php echo $projectName; ?><span
                                class="caret"></span></a>
                        <?php
                        $projects = Project::model()->findAll();
                        ?>
                        <ul class="dropdown-menu" role="menu">
                            <li><?php echo CHtml::link("All-Workspaces", $this->createUrl("task/index", array('project_id' => 0))) ?></li>
                            <li class="divider"></li>
                            <?php foreach ($projects as $project): ?>
                                <li><?php echo CHtml::link($project->name, Yii::app()->createUrl('task/index', array('project_id' => $project->id))); ?></li>
                            <?php endforeach; ?>
                        </ul>
                    </li>

                </ul>

                <ul class="nav navbar-nav navbar-right" class="navbar-font-icon">
                    <?php if (!Yii::app()->user->isGuest): ?>
                        <li><?php echo CHtml::link('<i class="fa fa-sign-out"></i>', array('site/logout'), array(
                                'style' => 'font-size: 21px;',
                                'class'=>'nav-link',
                            )); ?></li>
                    <?php endif; ?>
                    <?php if (Yii::app()->user->isGuest): ?>
                        <li><?php echo CHtml::link('Login', array('site/login')); ?></li>
                    <?php endif; ?>
                </ul>
                <ul class="nav navbar-nav navbar-right" class="navbar-font-icon">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" id="userStatusLabel" data-toggle="dropdown" role="button"
                           aria-expanded="false"><span
                                class="connection_status"> </span><span class="caret"> </span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li class="connection_status_option" status_code="<?php echo User::STATE_ONLINE ?>"><a
                                    href="#"><span
                                        class="connection_status connection_status_online"></span> Online</a></li>
                            <li class="connection_status_option" status_code="<?php echo User::STATE_BREAK ?>"><a
                                    href="#"><span class="connection_status connection_status_break"></span> Break</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <?php if (!Yii::app()->user->isGuest && User::model()->isAdmin(Yii::app()->user->id)): ?>
                <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" role="button"
                               aria-expanded="false"><span class="glyphicon glyphicon-cog" style="font-size: 21px;"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><?php echo CHtml::link('manage Users', array('user/index')); ?></li>
                                <li><?php echo CHtml::link('manage Groups', array('group/index')); ?></li>
                                <li><?php echo CHtml::link('manage Workspaces', array('project/index')); ?></li>
                                <?php if(Yii::app()->user->checkAccess('manage_auto_tasks')):?>
                                    <li><?php echo CHtml::link('Manage Auto Tasks', array('autoTask/index')); ?></li>
                                <?php endif;?>
                                <?php if(Yii::app()->user->checkAccess('view_report')):?>
                                    <li><?php echo CHtml::link('Reports', array('project/report')); ?></li>
                                <?php endif;?>
                                <li><?php echo CHtml::link('Settings', array('setting/index')); ?></li>
                            </ul>
                        </li>

                </ul>
                <?php endif; ?>
                <?php if (!Yii::app()->user->isGuest && !User::model()->isAdmin(Yii::app()->user->id)): ?>
                    <ul class="nav navbar-nav navbar-right">

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" role="button"
                               aria-expanded="false"><span class="glyphicon glyphicon-cog" style="font-size: 21px;"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <?php if(Yii::app()->user->checkAccess('manage_auto_tasks')):?>
                                    <li><?php echo CHtml::link('Manage Auto Tasks', array('autoTask/index')); ?></li>
                                <?php endif;?>
                                <?php if(Yii::app()->user->checkAccess('view_report')):?>
                                    <li><?php echo CHtml::link('Reports', array('project/report')); ?></li>
                                <?php endif;?>
                                <li><?php echo CHtml::link('Reset Password', array('user/memberPasswordReset','id'=>User::model()->findByPk(Yii::app()->user->id)->id)); ?></li>
                            </ul>
                        </li>

                    </ul>
                <?php endif; ?>
                <ul class="nav navbar-nav navbar-right no-hover">
                    <li><a class="large-nav-text"><?php echo !Yii::app()->user->isGuest ? Yii::app()->user->username : "Projstar"; ?></a></li>
                </ul>
            </div>
            <!--/.nav-collapse -->
        <?php endif; ?>
    </div>
</nav>
<div class="container-fluid content" style="min-height: 530px;">
    <?php echo $content; ?>
</div>
<hr/>

<!-- script files -->
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/scripts.js"></script>

<?php if (!Yii::app()->user->isGuest): ?>
    <?php
    $currentUser = User::model()->findByPk(Yii::app()->user->id);

    Yii::app()->clientScript->registerScript('user_status', '
    var statusCssClassArray = ["connection_status_break","connection_status_online","connection_status_offline"];
    var statusCode = '.$currentUser->status.';

    $(function(){
        $("#userStatusLabel span.connection_status").addClass(statusCssClassArray[statusCode]);
    });
    $(".connection_status_option").click(function( event ){
        $.ajax({
            "url":' . CJavaScript::encode(Yii::app()->createUrl('user/changeStatus')) . ',
            "data":{
                status_code:$(this).attr("status_code")
            },
            "type":"post",
            "dataType":"json",
            "success":function(data){
                if(data.status){
                    $("#userStatusLabel span.connection_status").removeClass(statusCssClassArray[data.p_status_code]);
                    $("#userStatusLabel span.connection_status").addClass(statusCssClassArray[data.status_code]);
                    $("#user-grid").yiiGridView("update");
                }
            }
        });
    });

');
    ?>
<?php endif; ?>
</body>
</html>
