<?php
/* @var $this SiteController */
/* @var $loginModel LoginForm */
/* @var $form CActiveForm */
Yii::app()->clientScript->registerScript("course_script", '
$(function(){
    $(".ok-sign").hide();
    $(".error-sign").hide();
});
', CClientScript::POS_END);
?>

<div style="min-height: 600px;">
    <div class="form" style="margin-top: 110px;">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => 'project-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
            'focus' => array($loginModel, 'email'),
            'clientOptions' => array(
                'successCssClass' => 'has-success',
                'errorCssClass' => 'has-error',
                'validatingErrorMessage' => '',
                'inputContainer' => '.form-group',
                'afterValidateAttribute' => 'js:function(form, attribute, data, hasError){
                $("#"+attribute.inputID).siblings(".error-sign").hide();
                $("#"+attribute.inputID).siblings(".ok-sign").hide();
                if(hasError){
                    $("#"+attribute.inputID).siblings(".error-sign").show();
                }else {
                    $("#"+attribute.inputID).siblings(".ok-sign").show();
                }
            }',
            ),
            'htmlOptions' => array(
                'role' => 'form',
                'class' => 'form-horizontal',
            ),
        )); ?>
        <div class="row" style="">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-success">
                    <div class="panel-heading"><span class="header-caption">Login</span></div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group has-feedback">
                                    <?php echo $form->labelEx($loginModel, 'login_name', array('class' => 'col-md-4 control-label')); ?>
                                    <div class="col-md-8 input-container">
                                        <?php echo $form->textField($loginModel, 'login_name', array('placeholder' => 'Enter Login name', 'size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
                                        <span
                                            class="glyphicon glyphicon-remove form-control-feedback error-sign"></span>
                                        <span class="glyphicon glyphicon-ok form-control-feedback ok-sign"></span>
                                        <?php echo $form->error($loginModel, 'login_name'); ?>
                                    </div>
                                </div>

                                <div class="form-group has-feedback">
                                    <?php echo $form->labelEx($loginModel, 'password', array('class' => 'col-md-4 control-label')); ?>
                                    <div class="col-md-8 input-container">
                                        <?php echo $form->passwordField($loginModel, 'password', array('placeholder' => 'Enter Password', 'size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
                                        <span
                                            class="glyphicon glyphicon-remove form-control-feedback error-sign"></span>
                                        <span class="glyphicon glyphicon-ok form-control-feedback ok-sign"></span>
                                        <?php echo $form->error($loginModel, 'password'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-4 col-md-8">
                                        <?php echo CHtml::submitButton('Login', array('class' => 'btn btn-block btn-danger btn-sm')) ?>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>

        <?php $this->endWidget(); ?>
    </div>
    <!-- form -->
</div>