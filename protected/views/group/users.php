<?php
/**
 * Created by PhpStorm.
 * User: Dell 3521
 * Date: 07/01/15
 * Time: 05:28 م
 */

$this->widget('application.components.Ajaxmodal', array(
    'name' => 'deleteUserModal',
    'title' => 'Deleting User From Group',
    'width' => '600px',
));

Yii::app()->clientScript->registerScript('group-script','
    $(".Tabled table").addClass("table");
    $(".Tabled table").addClass("table-condensed");
', CClientScript::POS_END);

?>
<h4 class="header-caption">Update user data for Group: <?php echo CHtml::encode($groupModel->name)?></h4>
<hr/>
<?php echo CHtml::link("<span class='glyphicon glyphicon-backward'></span> Back to Group Management",array('group/index'),array(
    'class'=>'action-link',
))?>
<br/>
<br/>
<?php echo CHtml::beginForm(array('group/updateUsers'),'POST')?>
<?php $this->renderPartial('partials/group-user-list', array('groupModel'=>$groupModel,'userDataProvider'=>$userDataProvider)); ?>
<hr/>
<?php echo CHtml::ajaxSubmitButton("Update Group Users",Yii::app()->createUrl("group/updateUsers",array('id'=>$groupModel->id)),array(
    'dataType'=>'json',
    'success'=>'js:function(){
                $("#update-group-user-submit-button").attr("disabled",false);
                $("#user-grid").yiiGridView("update");
            }',
    'beforeSend'=>'js:function(){
                $("#update-group-user-submit-button").attr("disabled",true);
            }'
),array(
    'class'=>'btn btn-success',
    'id'=>'update-group-user-submit-button',
))?>
<?php echo CHtml::endForm();?>