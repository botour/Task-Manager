<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'group-grid',
    'showTableOnEmpty' => false,

    'rowHtmlOptionsExpression' => 'array("class"=>"group-grid-row","group-id"=>$data->id,"group-name"=>$data->name,"group-description"=>$data->description,"group-note"=>$data->note,"group-status"=>$data->status)',
    'summaryText' => '<span>Total No Of Groups: {count}</span><br/>',
    'emptyText' => 'No Groups Available',
    'afterAjaxUpdate' => 'js:function(){
                        $("#loading-indicator").hide();
                        $(".Tabled table").addClass("table");
                        $(".Tabled table").addClass("table-condensed");
                        updateGroupGridSelectRow();
                   }',
    'beforeAjaxUpdate'=>'js:function(){
        $("#loading-indicator").show();
    }',
    'htmlOptions' => array(
        'class' => 'Tabled',
    ),
    'cssFile' => Yii::app()->baseUrl . '/css/main.css',
    'dataProvider' => $groupModel->search(),
    'columns' => array(
        'name',
        'note',
        array(
            'header'=>'Members count',
            'value'=>'$data->usersCount',
        ),
        array(
            'header' => 'Update Group Info',
            'class' => 'CButtonColumn',
            'template' => '{options}',
            'buttons' => array(
                'options' => array(
                    'label' => '<span class="glyphicon glyphicon-pencil"></span>',
                    'url' => 'Yii::app()->createUrl("group/getUpdateForm",array("id"=>$data->id))',
                    'options' => array(
                        'class' => 'action-link',
                        'ajax'=>array(
                            'type'=>'GET',
                            'url'=>'js:$(this).attr("href")',
                            'success'=>'js:function(data){
                                $("#updateGroupModal .modal-body").html(data);
                            }',
                            'beforeSend'=>'js:function(){
                                $("#updateGroupModal .modal-body").html("Please Wait...");
                                $("#updateGroupModal").modal("show");
                            }'
                        )
                    ),
                )
            )
        ),
        array(
            'header'=>'Manage Group Users',
            'class' => 'CButtonColumn',
            'template' => '{options}',
            'buttons' => array(
                'options' => array(
                    'label' => '<span class="glyphicon glyphicon-user"></span>',
                    'url' => 'Yii::app()->createUrl("group/users",array("id"=>$data->id))',
                    'options' => array(
                        'class' => 'action-link',
                    ),
                )
            ),
        ),
        array(
            'header' => 'Delete Group',
            'class' => 'CButtonColumn',
            'template' => '{options}',
            'buttons' => array(
                'options' => array(
                    'label' => '<span class="glyphicon glyphicon-trash"></span>',
                    'url' => 'Yii::app()->createUrl("group/delete")',
                    'options' => array(
                        'class' => 'action-link',
                        'confirm'=>'Are You Sure you want to delete?',
                        'ajax'=>array(
                            'type'=>'POST',
                            'data'=>'js:{
                                "id":$(this).parent().parent().attr("group-id")
                            }',
                            'dataType'=>'json',
                            'url'=>'js:$(this).attr("href")',
                            'success'=>'js:function(data){
                                $("#deleteGroupModal .modal-body").html(data.message);
                                if(data.status){
                                    $("#group-grid").yiiGridView("update");
                                }
                            }',
                            'beforeSend'=>'js:function(){
                                $("#deleteGroupModal .modal-body").html("Please Wait...");
                                $("#deleteGroupModal").modal("show");
                            }'
                        )
                    ),
                )
            )
        ),
    ),
)); ?>