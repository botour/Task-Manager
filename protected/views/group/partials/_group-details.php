<div class="details-panel panel panel-success">
    <div class="panel-heading">
        <span class="header-caption">Group Details</span>
    </div>
    <div class="panel-body">
        <div id="group-prompt">
            <div class="select-prompt">Select a Group show its members</div>
        </div>
        <div id="group-details" style="display: none;">
        </div>
    </div>
</div>