<?php
/**
 * Created by PhpStorm.
 * User: Dell 3521
 * Date: 07/01/15
 * Time: 05:32 م
 */

$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'user-grid',
    'summaryText' => '',
    'afterAjaxUpdate' => 'js:function(){
        	 $(".Tabled table").addClass("table");
            $(".Tabled table").addClass("table-condensed");
    }',
    'htmlOptions' => array(
        'class' => 'Tabled',
    ),
    'cssFile' => Yii::app()->baseUrl . '/css/main.css',
    'dataProvider' => $userDataProvider,
    'columns' => array(
        array(
            'class' => 'CCheckBoxColumn',
            'htmlOptions' => array('style' => 'width: 40px;'),
            'id' => 'user_id',
            'selectableRows' => 2,
            'name' => 'id',
            'disabled'=>'$data->isMemberInGroup($data->groupId)',
        ),
        'username',
        array(
            'header' => 'Delete User From Group',
            'class' => 'CButtonColumn',
            'template' => '{options}',

            'buttons' => array(
                'options' => array(
                    'label' => '<span class="glyphicon glyphicon-trash"></span>',
                    'visible'=>'$data->isMemberInGroup($data->groupId)',
                    'url' => 'Yii::app()->createUrl("group/deleteFromGroup",array("userId"=>$data->id,"groupId"=>$data->groupId))',
                    'options' => array(
                        'class' => 'action-link',
                        'confirm'=>'Are you sure you want to delete this user?',
                        'ajax' => array(
                            'url' => 'js:$(this).attr("href")',
                            'type' => 'GET',
                            'dataType'=>'json',
                            'success' => 'js:function(data){
                                            $("#deleteUserModal .modal-body").html(data.message);
                                            if(data.status){
                                                $("#user-grid").yiiGridView("update");
                                            }
                                    }',

                            'beforeSend' => 'js:function(){
                                            $("#deleteUserModal .modal-body").html("Please wait...");
                                            $("#deleteUserModal").modal("show");
                                    }'

                        ),
                    ),
                )
            )
        ),
    ),
)); ?>

