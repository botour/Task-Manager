<?php if(empty($members)):?>
    <div class="select-prompt">No members in this group</div>
<?php endif;?>

<?php if(!empty($members)):?>
    <ul class="member-list">
    <?php foreach($members as $member):?>
        <li><?php echo $member->username;?></li>
    <?php endforeach;?>
    </ul>
<?php endif;?>
