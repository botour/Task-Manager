<?php
/* @var $this GroupController */
/* @var $groupModel Group */
/* @var $form CActiveForm */


Yii::app()->clientScript->registerScript("course_script", '
$(function(){
    $(".ok-sign").hide();
    $(".error-sign").hide();
});
', CClientScript::POS_END);
CommonFunctions::fixAjax();
?>

<div class="form">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'task-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'focus' => array($groupModel, 'name'),
        'clientOptions' => array(
            'successCssClass' => 'has-success',
            'errorCssClass' => 'has-error',
            'validatingErrorMessage' => '',
            'inputContainer' => '.form-group',
            'afterValidateAttribute' => 'js:function(form, attribute, data, hasError){
                $("#"+attribute.inputID).siblings(".error-sign").hide();
                $("#"+attribute.inputID).siblings(".ok-sign").hide();
                if(hasError){
                    $("#"+attribute.inputID).siblings(".error-sign").show();
                }else {
                    $("#"+attribute.inputID).siblings(".ok-sign").show();
                }
            }',
        ),
        'htmlOptions' => array(
            'role' => 'form',
            'class' => 'form-horizontal',
        ),
    )); ?>
    <?php if(!$groupModel->isNewRecord):?>
        <?php echo $form->hiddenField($groupModel,'id')?>
    <?php endif;?>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group has-feedback">
                <?php echo $form->labelEx($groupModel, 'name', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-9 input-container">
                    <?php echo $form->textField($groupModel, 'name', array('placeholder'=>'Add the task title ...','size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
                    <span class="glyphicon glyphicon-remove form-control-feedback error-sign"></span>
                    <span class="glyphicon glyphicon-ok form-control-feedback ok-sign"></span>
                    <?php echo $form->error($groupModel, 'name'); ?>
                </div>
            </div>

            <div class="form-group has-feedback">
                <?php echo $form->labelEx($groupModel, 'description', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-9 input-container">
                    <?php echo $form->textArea($groupModel, 'description', array('class' => 'form-control')); ?>
                    <?php echo $form->error($groupModel, 'description'); ?>
                </div>
            </div>


            <div class="form-group has-feedback">
                <?php echo $form->labelEx($groupModel, 'note', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-9 input-container">
                    <?php echo $form->textArea($groupModel, 'note', array('class' => 'form-control')); ?>
                    <?php echo $form->error($groupModel, 'note'); ?>
                </div>
            </div>

            <div class="col-md-offset-3 col-md-9">
                <?php if($groupModel->isNewRecord):?>
                <?php echo CHtml::ajaxSubmitButton('Create a new Group',$this->createUrl('create'),array(
                    'type'=>"POST",
                    'dataType'=>'json',
                    'success'=>'js:function(data){
                       $("#statusModal .modal-body").html(data.message);
                       $("#statusModal").modal("show");
                       if(data.status){
                            $("#group-grid").yiiGridView("update");
                       }
                    }',
                    'beforeSend'=>'js:function(){
                        $("#createGroupModal").modal("hide");
                    }'
                ),array(
                    'class'=>'btn btn-danger',
                    'id'=>'link-'.uniqid(),
                ))?>
                <?php endif;?>
                <?php if(!$groupModel->isNewRecord):?>
                    <?php echo CHtml::ajaxSubmitButton('Update Group Info',$this->createUrl('update'),array(
                        'type'=>"POST",
                        'dataType'=>'json',
                        'success'=>'js:function(data){
                       $("#statusModal .modal-body").html(data.message);
                       $("#statusModal").modal("show");
                       if(data.status){
                            $("#group-grid").yiiGridView("update");
                       }
                    }',
                        'beforeSend'=>'js:function(){
                        $("#updateGroupModal").modal("hide");
                    }'
                    ),array(
                        'class'=>'btn btn-danger',
                        'id'=>'link-'.uniqid(),
                    ))?>
                <?php endif;?>
            </div>
        </div>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- form -->