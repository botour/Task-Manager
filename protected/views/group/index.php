<?php
/* @var $this GroupController */
/* @var $dataProvider CActiveDataProvider */

$this->widget('application.components.Ajaxmodal', array(
    'name' => 'statusModal',
    'title' => 'Status',
    'width' => '600px',
));

$this->widget('application.components.Ajaxmodal', array(
    'name' => 'createGroupModal',
    'title' => 'Create a new Group',
    'width' => '600px',
));


$this->widget('application.components.Ajaxmodal', array(
    'name' => 'updateGroupModal',
    'title' => 'Update Group',
    'width' => '600px',
));

$this->widget('application.components.Ajaxmodal', array(
    'name' => 'deleteGroupModal',
    'title' => 'Delete Group',
    'width' => '600px',
));
Yii::app()->clientScript->registerScript('group-script','
    $(".Tabled table").addClass("table");
    $(".Tabled table").addClass("table-condensed");
    var getGroupMembersURL = "'.$this->createUrl('group/getGroupMembers') .'";
    var updateGroupDetails = function(){
        groupId = $(".selected-row").attr("group-id");
        $.ajax({
            "url":getGroupMembersURL,
            "type":"GET",
            "data":{
                "id":groupId
            },
            "success":function(data){
                $("#group-details").show();
                $("#group-prompt").hide();
                $("#group-details").html(data);
            }
        });
    }
    var updateGroupGridSelectRow = function(){
            $(".group-grid-row").click(function (event) {
                $(".group-grid-row").each(function () {
                    $(this).removeClass("selected-row");
                });
                $(this).addClass("selected-row");
                updateGroupDetails();
       });
    }
    $(function(){
        updateGroupGridSelectRow();
    });
', CClientScript::POS_END);
?>
<div class="row">
    <div class="col-md-9">
        <div class="row">
            <div class="col-md-12">
                <?php echo CHtml::ajaxButton('Create Group', $this->createUrl('getCreateForm'), array(
                    'type' => 'GET',
                    'success' => 'js:function(data){
                            $("#createGroupModal .modal-body").html(data);
                        }',
                    'beforeSend' => 'js:function(){
                            $("#createGroupModal .modal-body").html("Please Wait...");
                            $("#createGroupModal").modal("show");
                        }'
                ), array(
                    'class' => 'btn btn-success btn-sm',
                    'id' => 'link-' . uniqid(),
                ))?>
                <hr/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" id="group-list-container">
                <?php $this->renderPartial('partials/group-list',array(
                    'groupModel'=>$groupModel,
                ));?>
            </div>
        </div>
    </div>
    <div id="group-details-container" class="col-md-3">
        <?php $this->renderPartial('partials/_group-details');?>
    </div>
</div>

