<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs = array(
    'Users' => array('index'),
    $model->id => array('view', 'id' => $model->id),
    'Update',
);

$this->menu = array(
    array('label' => 'List User', 'url' => array('index')),
    array('label' => 'Create User', 'url' => array('create')),
    array('label' => 'View User', 'url' => array('view', 'id' => $model->id)),
    array('label' => 'Manage User', 'url' => array('admin')),
);
?>

    <h3>Update User Info: <?php echo $model->first_name . " " . $model->last_name; ?>  -
        <small>fields with * are required</small>
    </h3>
    <hr/>

<?php echo CHtml::link("<span class='glyphicon glyphicon-backward'></span> Back to User Management",array('user/index'),array(
    'class'=>'action-link',
))?>
    <br/>
    <br/>

<?php $this->renderPartial('_form', array('model' => $model)); ?>