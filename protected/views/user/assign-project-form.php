<?php
/**
 * Created by PhpStorm.
 * User: Dell 3521
 * Date: 30/11/14
 * Time: 10:44 ص
 */

Yii::app()->clientScript->registerScript("course_script", '
$(function(){
    $(".ok-sign").hide();
    $(".error-sign").hide();
});
', CClientScript::POS_END);
CommonFunctions::fixAjax();
?>

<div class="form">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'task-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,

        'clientOptions' => array(
            'successCssClass' => 'has-success',
            'errorCssClass' => 'has-error',
            'validatingErrorMessage' => '',
            'inputContainer' => '.form-group',
            'afterValidateAttribute' => 'js:function(form, attribute, data, hasError){
                $("#"+attribute.inputID).siblings(".error-sign").hide();
                $("#"+attribute.inputID).siblings(".ok-sign").hide();
                if(hasError){
                    $("#"+attribute.inputID).siblings(".error-sign").show();
                }else {
                    $("#"+attribute.inputID).siblings(".ok-sign").show();
                }
            }',
        ),
        'htmlOptions' => array(
            'role' => 'form',
            'class' => 'form-horizontal',
        ),
    )); ?>

    <div class="row">
        <div class="col-md-12">
            <?php echo $form->hiddenField($user,'id');?>
            <div class="form-group has-feedback">
                <?php echo CHtml::label('Project','project_id',array('class' => 'col-md-3 control-label'))?>
                <div class="col-md-9 input-container">
                    <?php echo CHtml::dropDownList('project_id','',$user->availableProjectsForAssignments(), array('prompt'=>'Select A Project','class' => 'form-control')); ?>
                </div>
            </div>

            <div class="col-md-offset-3 col-md-9">
                <?php echo CHtml::ajaxSubmitButton('Assign Project',$this->createUrl('assignProject'),array(
                    'type'=>"POST",
                    'dataType'=>'json',
                    'success'=>'js:function(data){
                       $("#statusModal .modal-body").html(data.message);
                       $("#statusModal").modal("show");
                       if(data.status){
                            $("#user-grid").yiiGridView("update");
                       }
                    }',
                    'beforeSend'=>'js:function(){
                         $("#assignToProjectModal").modal("hide");
                    }'
                ),array(
                    'class'=>'btn btn-danger',
                    'id'=>'link-'.uniqid(),
                ))?>
            </div>
        </div>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- form -->