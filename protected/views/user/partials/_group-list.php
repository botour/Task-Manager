<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'group-grid',
    'showTableOnEmpty' => false,
    'summaryText' => 'No Of Groups: {count}',
    'summaryCssClass'=>'group-grid-summery',
    'emptyText' => 'No Groups Available',
    'afterAjaxUpdate' => 'js:function(){
                        $(".group-summery-container").html($(".group-grid-summery").html());
                        $(".group-grid-summery").hide();
                        $(".Tabled table").addClass("table");
                        $(".Tabled table").addClass("table-condensed");
    }',
    'htmlOptions' => array(
        'class' => 'Tabled',
    ),
    'cssFile' => Yii::app()->baseUrl . '/css/main.css',
    'dataProvider' => $groupsDataProvider,

    'columns' => array(
        array(
            'class' => 'CCheckBoxColumn',
            'htmlOptions' => array('style' => 'width: 40px;'),
            'id' => 'group_id',
            'selectableRows' => 2,
            'name' => 'id',
        ),
        'name',
    ),
)); ?>
