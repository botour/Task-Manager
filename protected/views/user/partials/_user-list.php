<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'user-grid',
    'showTableOnEmpty' => false,
    'summaryText' => 'No Of Users: {count}',
    'summaryCssClass'=>'user-grid-summery',
    'emptyText' => 'No Groups Available',
    'rowHtmlOptionsExpression' => 'array("user-id"=>$data->id,"supervisor-id"=>$data->supervisorId)',

    'afterAjaxUpdate' => 'js:function(){
                        $(".user-summery-container").html($(".user-grid-summery").html());
                        $(".user-grid-summery").hide();
                        $(".Tabled table").addClass("table");
                        $(".Tabled table").addClass("table-condensed");
                        $(".readonly-c input").attr("readonly",true);
                        updateDeleteLink();
    }',
    'htmlOptions' => array(
        'class' => 'Tabled',
    ),
    'cssFile' => Yii::app()->baseUrl . '/css/main.css',
    'dataProvider' => $groupsDataProvider,
    'columns' => array(
        array(
            'class' => 'CCheckBoxColumn',
            'htmlOptions' => array('style' => 'width: 40px;'),
            'id' => 'user_id',
            'selectableRows' => 2,
            'name' => 'id',
            'cssClassExpression'=>'$data->hasSupervisor($data->supervisorId)?"readonly-c":""',
            'checked'=>'$data->hasSupervisor($data->supervisorId)',
            'disabled'=>'$data->hasSupervisor($data->supervisorId)',
        ),
        'username',
        /*array(
            'header' => 'Delete From Follow Up List',
            'class' => 'CButtonColumn',
            'template' => '{options}',
            'buttons' => array(
                'options' => array(
                    'label' => '<span class="glyphicon glyphicon-trash"></span>',
                    'visible'=>'$data->hasSupervisor($data->supervisorId)',
                    'options'=>array(
                        'class'=>'delete-user-link',
                    ),
                )
            )
        ),*/
    ),
)); ?>
