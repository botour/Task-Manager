<div class="panel panel-success" style="min-height: 400px;">
    <div class="panel-heading">
        <span class="header-caption">Follow Up List for user: <?php echo $supervisor->username;?></span>
    </div>
    <div class="panel-body">
        <?php if(empty($followUpUsers)):?>
            <div style="font-size: 18px;color:#DEDEDE;font-weight: 650;text-align: center;margin-top: 160px;">No Members to follow</div>
        <?php endif;?>
        <?php if(!empty($followUpUsers)):?>
            <ul class="list-group">
                <?php foreach($followUpUsers as $user):?>
                    <li class="list-group-item" user-id="<?php echo $user->id;?>" supervisor-id="<?php echo $supervisorId;?>">
                        <div class="row">
                            <div class="col-sm-6">
                                <?php echo $user->username;?>
                            </div>
                            <div class="col-sm-6">
                                <?php echo CHtml::link('<i class="fa fa-trash"></i>','#',array(
                                    'class'=>'delete-user-link  fa-hover',
                                ))?>
                            </div>
                        </div>
                    </li>
                <?php endforeach;?>
            </ul>
        <?php endif;?>

    </div>
</div>