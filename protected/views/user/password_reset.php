<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */

Yii::app()->clientScript->registerScript("course_script", '
$(function(){
    $(".ok-sign").hide();
    $(".error-sign").hide();
});
', CClientScript::POS_END);

?>
<h3>Update password for user: <?php echo $model->first_name . " " . $model->last_name; ?>  -
    <small>fields with * are required</small>
</h3>
<hr/>
<?php echo CHtml::link("<span class='glyphicon glyphicon-backward'></span> Back to User Management",array('user/index'),array(
    'class'=>'action-link',
))?>
<br/>
<br/>
<div class="form">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'task-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
        'enableClientValidation' => false,
        'focus' => array($model, 'password'),
        'clientOptions' => array(
            'successCssClass' => 'has-success',
            'errorCssClass' => 'has-error',
            'validatingErrorMessage' => '',
            'inputContainer' => '.form-group',
            'afterValidateAttribute' => 'js:function(form, attribute, data, hasError){
                $("#"+attribute.inputID).siblings(".error-sign").hide();
                $("#"+attribute.inputID).siblings(".ok-sign").hide();
                if(hasError){
                    $("#"+attribute.inputID).siblings(".error-sign").show();
                }else {
                    $("#"+attribute.inputID).siblings(".ok-sign").show();
                }
            }',
        ),
        'htmlOptions' => array(
            'role' => 'form',
            'class' => 'form-horizontal',
        ),
    )); ?>
    <div class="row">
        <div class="col-md-offset-3 col-md-6">
            <div class="form-group has-feedback">
                <?php echo $form->hiddenField($model,'id');?>
                <?php echo $form->labelEx($model, 'password', array('class' => 'col-md-4 control-label')); ?>
                <div class="col-md-8 input-container">
                    <?php echo $form->passwordField($model, 'password', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
                    <span class="glyphicon glyphicon-remove form-control-feedback error-sign"></span>
                    <span class="glyphicon glyphicon-ok form-control-feedback ok-sign"></span>
                    <?php echo $form->error($model, 'password'); ?>
                </div>
            </div>
            <div class="form-group has-feedback">
                <?php echo $form->labelEx($model, 'password_repeat', array('class' => 'col-md-4 control-label')); ?>
                <div class="col-md-8 input-container">
                    <?php echo $form->passwordField($model, 'password_repeat', array('placeholder'=>'Repeat to validate Password','size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
                    <span class="glyphicon glyphicon-remove form-control-feedback error-sign"></span>
                    <span class="glyphicon glyphicon-ok form-control-feedback ok-sign"></span>
                    <?php echo $form->error($model, 'password_repeat'); ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-offset-4 col-md-8">
                    <?php echo CHtml::submitButton("Reset Password" ,array(
                        'class'=>'btn btn-danger',
                    ))?>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- form -->