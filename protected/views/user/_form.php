<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */

Yii::app()->clientScript->registerScript("course_script", '
$(function(){
    $(".ok-sign").hide();
    $(".error-sign").hide();
});
', CClientScript::POS_END);

?>

<div class="form">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'task-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
        'enableClientValidation' => false,
        'focus' => array($model, 'first_name'),
        'clientOptions' => array(
            'successCssClass' => 'has-success',
            'errorCssClass' => 'has-error',
            'validatingErrorMessage' => '',
            'inputContainer' => '.form-group',
            'afterValidateAttribute' => 'js:function(form, attribute, data, hasError){
                $("#"+attribute.inputID).siblings(".error-sign").hide();
                $("#"+attribute.inputID).siblings(".ok-sign").hide();
                if(hasError){
                    $("#"+attribute.inputID).siblings(".error-sign").show();
                }else {
                    $("#"+attribute.inputID).siblings(".ok-sign").show();
                }
            }',
        ),
        'htmlOptions' => array(
            'role' => 'form',
            'class' => 'form-horizontal',
        ),
    )); ?>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group has-feedback">
                <?php echo $form->labelEx($model, 'first_name', array('class' => 'col-md-4 control-label')); ?>
                <div class="col-md-8 input-container">
                    <?php echo $form->textField($model, 'first_name', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
                    <span class="glyphicon glyphicon-remove form-control-feedback error-sign"></span>
                    <span class="glyphicon glyphicon-ok form-control-feedback ok-sign"></span>
                    <?php echo $form->error($model, 'first_name'); ?>
                </div>
            </div>
            <div class="form-group has-feedback">
                <?php echo $form->labelEx($model, 'last_name', array('class' => 'col-md-4 control-label')); ?>
                <div class="col-md-8 input-container">
                    <?php echo $form->textField($model, 'last_name', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
                    <span class="glyphicon glyphicon-remove form-control-feedback error-sign"></span>
                    <span class="glyphicon glyphicon-ok form-control-feedback ok-sign"></span>
                    <?php echo $form->error($model, 'last_name'); ?>
                </div>
            </div>
            <div class="form-group has-feedback">
                <?php echo $form->labelEx($model, 'username', array('class' => 'col-md-4 control-label')); ?>
                <div class="col-md-8 input-container">
                    <?php echo $form->textField($model, 'username', array('placeholder' => 'This must be unique', 'size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
                    <span class="glyphicon glyphicon-remove form-control-feedback error-sign"></span>
                    <span class="glyphicon glyphicon-ok form-control-feedback ok-sign"></span>
                    <?php echo $form->error($model, 'username'); ?>
                </div>
            </div>

            <div class="form-group has-feedback">
                <?php echo $form->labelEx($model, 'login_name', array('class' => 'col-md-4 control-label')); ?>
                <div class="col-md-8 input-container">
                    <?php echo $form->textField($model, 'login_name', array('placeholder' => 'This must be unique', 'size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
                    <span class="glyphicon glyphicon-remove form-control-feedback error-sign"></span>
                    <span class="glyphicon glyphicon-ok form-control-feedback ok-sign"></span>
                    <?php echo $form->error($model, 'login_name'); ?>
                </div>
            </div>

            <?php if ($model->isNewRecord): ?>
                <div class="form-group has-feedback">
                    <?php echo $form->labelEx($model, 'password', array('class' => 'col-md-4 control-label')); ?>
                    <div class="col-md-8 input-container">
                        <?php echo $form->passwordField($model, 'password', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
                        <span class="glyphicon glyphicon-remove form-control-feedback error-sign"></span>
                        <span class="glyphicon glyphicon-ok form-control-feedback ok-sign"></span>
                        <?php echo $form->error($model, 'password'); ?>
                    </div>
                </div>
            <?php endif; ?>
            <hr/>
            <div class="form-group">
                <?php echo $form->labelEx($model, 'emp_no', array('class' => 'col-md-4 control-label')); ?>
                <div class="col-md-8 input-container">
                    <?php echo $form->textField($model, 'emp_no', array('placeholder' => 'Employee No', 'size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'emp_no'); ?>
                </div>
            </div>

            <div class="form-group">
                <?php echo $form->labelEx($model, 'email', array('class' => 'col-md-4 control-label')); ?>
                <div class="col-md-8 input-container">
                    <?php echo $form->textField($model, 'email', array('placeholder' => 'Email', 'size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'email'); ?>
                </div>
            </div>

            <div class="form-group">
                <?php echo $form->labelEx($model, 'note', array('class' => 'col-md-4 control-label')); ?>
                <div class="col-md-8 input-container">
                    <?php echo $form->textArea($model, 'note', array('placeholder' => 'Note', 'size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'note'); ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">


            <h4>Permissions:</h4>
            <hr/>
            <div class="form-group has-feedback">
                <?php echo $form->labelEx($model, 'role', array('class' => 'col-md-4 control-label')); ?>
                <div class="col-md-8 input-container">
                    <?php echo $form->dropDownList($model, 'role', User::model()->getRolesOptionsList(), array('class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'role'); ?>
                </div>
            </div>
            <div class="form-group">
                <?php echo $form->labelEx($model, 'viewArchivePermission', array('class' => 'col-md-4 control-label')); ?>
                <div class="col-md-8 input-container">
                    <?php echo $form->checkBox($model, 'viewArchivePermission', array('class' => '')); ?>
                    <?php echo $form->error($model, 'viewArchivePermission'); ?>
                </div>
            </div>
            <div class="form-group">
                <?php echo $form->labelEx($model, 'endTaskPermission', array('class' => 'col-md-4 control-label')); ?>
                <div class="col-md-8 input-container">
                    <?php echo $form->checkBox($model, 'endTaskPermission', array('class' => '')); ?>
                    <?php echo $form->error($model, 'endTaskPermission'); ?>
                </div>
            </div>
            <div class="form-group">
                <?php echo $form->labelEx($model, 'reassignPermission', array('class' => 'col-md-4 control-label')); ?>
                <div class="col-md-8 input-container">
                    <?php echo $form->checkBox($model, 'reassignPermission', array('class' => '')); ?>
                    <?php echo $form->error($model, 'reassignPermission'); ?>
                </div>
            </div>
            <div class="form-group">
                <?php echo $form->labelEx($model, 'viewReportPermission', array('class' => 'col-md-4 control-label')); ?>
                <div class="col-md-8 input-container">
                    <?php echo $form->checkBox($model, 'viewReportPermission', array('class' => '')); ?>
                    <?php echo $form->error($model, 'viewReportPermission'); ?>
                </div>
            </div>
            <div class="form-group">
                <?php echo $form->labelEx($model, 'manageAutoTaskPermission', array('class' => 'col-md-4 control-label')); ?>
                <div class="col-md-8 input-container">
                    <?php echo $form->checkBox($model, 'manageAutoTaskPermission', array('class' => '')); ?>
                    <?php echo $form->error($model, 'manageAutoTaskPermission'); ?>
                </div>
            </div>
            <hr/>
            <div class="form-group">
                <div class="col-md-offset-4 col-md-8">
                    <?php echo CHtml::submitButton($model->isNewRecord ? "Create User" : "Update User Data", array(
                        'class' => 'btn btn-danger',
                    ))?>
                </div>
            </div>
        </div>


    </div>
    <div class="clearfix"></div>


    <?php $this->endWidget(); ?>
</div><!-- form -->