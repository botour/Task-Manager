<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List User', 'url'=>array('index')),
	array('label'=>'Manage User', 'url'=>array('admin')),
);
?>

<h3>Create A New User - <small>fields with * are required</small></h3>
<hr/>

<?php echo CHtml::link("<span class='glyphicon glyphicon-backward'></span> Back to User Management",array('user/index'),array(
    'class'=>'action-link',
))?>
    <br/>
    <br/>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>