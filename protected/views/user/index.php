<?php
/* @var $this UserController */
/* @var $dataProvider CActiveDataProvider */

Yii::app()->clientScript->registerScript('user-grid', "
$(function(){
        $('.Tabled table').addClass('table');
});
",CClientScript::POS_END);

$this->widget('application.components.Ajaxmodal', array(
    'name' => 'assignToProjectModal',
    'title' => 'Assign to a Project',
    'width' => '600px',
));
$this->widget('application.components.Ajaxmodal', array(
    'name' => 'setCurrentProjectModal',
    'title' => 'Set Current Project',
    'width' => '600px',
));

$this->widget('application.components.Ajaxmodal', array(
    'name' => 'deleteUserModal',
    'title' => 'Deleting User',
    'width' => '600px',
));

$this->widget('application.components.Ajaxmodal', array(
    'name' => 'statusModal',
    'title' => 'Status',
    'width' => '600px',
));

$this->widget('application.components.Ajaxmodal', array(
    'name' => 'showUserInfoModal',
    'title' => 'Status',
    'width' => '600px',
));
?>
<?php echo CHtml::link('Create User',$this->createUrl('user/create'),array(
    'class'=>'btn btn-success btn-sm',
))?>

<hr/>



<div class="row">
    <div class="col-md-12">
        <br/>
        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'user-grid',
            'summaryText' => '<span>Total No Of Users: {count}</span><br/>',
            'afterAjaxUpdate' => 'js:function(){
        	$(".Tabled table").addClass("table");
            }',
            'htmlOptions' => array(
                'class' => 'Tabled',
            ),
            'cssFile' => Yii::app()->baseUrl . '/css/main.css',
            'dataProvider' => $dataProvider,
            'columns' => array(
                'first_name',
                'last_name',
                array(
                    'header'=>'User Name',
                    'name'=>'username',
                    'cssClassExpression'=>'',
                ),
                array(
                    'header'=>'Status',
                    'value'=>'CommonFunctions::getLabel($data->status,CommonFunctions::USER_STATUS)',
                    'type'=>'raw',
                ),
                array(
                    'header' => 'Edit',
                    'class' => 'CButtonColumn',
                    'template' => '{options}',
                    'buttons' => array(
                        'options' => array(
                            'label' => '<span class="glyphicon glyphicon-pencil"></span>',
                            'url' => 'Yii::app()->createUrl("user/update",array("id"=>$data->id))',
                            'options' => array(
                                'class' => 'action-link',
                            ),
                        )
                    )
                ),
                array(
                    'type'=>'raw',
                    'header'=>'Role',
                    'value'=>'$data->getRoleText()',
                ),
                array(
                    'header'=>'Follow Up count',
                    'value'=>'count($data->followUpUsers)',
                    'visible'=>'$data->isSupervisor($data->id)',
                ),
                array(
                    'header' => 'Manager Follow Up List',
                    'class' => 'CButtonColumn',
                    'template' => '{options}',
                    'buttons' => array(
                        'options' => array(
                            'label' => '<span class="glyphicon glyphicon-pencil"></span>',
                            'url' => 'Yii::app()->createUrl("user/editFollowUpList",array("id"=>$data->id))',
                            'visible'=>'$data->isSupervisor($data->id)',
                            'options' => array(
                                'class' => 'action-link',
                            ),
                        ),

                    )
                ),
                array(
                    'header' => 'Reset Password',
                    'class' => 'CButtonColumn',
                    'template' => '{options}',
                    'buttons' => array(
                        'options' => array(
                            'label' => '<span class="glyphicon glyphicon-lock"></span>',
                            'url' => 'Yii::app()->createUrl("user/passwordReset",array("id"=>$data->id))',
                            'options' => array(
                                'class' => 'action-link',
                            ),
                        )
                    )
                ),
                array(
                    'header' => 'Delete User',
                    'class' => 'CButtonColumn',
                    'template' => '{options}',
                    'buttons' => array(
                        'options' => array(
                            'label' => '<span class="glyphicon glyphicon-trash"></span>',
                            'url' => 'Yii::app()->createUrl("user/delete",array("id"=>$data->id))',
                            'options' => array(
                                'class' => 'action-link',
                                'confirm'=>'Are you sure you want to delete this user?',
                                'ajax' => array(
                                    'url' => 'js:$(this).attr("href")',
                                    'type' => 'GET',
                                    'dataType'=>'json',
                                    'success' => 'js:function(data){
                                            $("#deleteUserModal .modal-body").html(data.message);
                                            if(data.status){
                                                $("#user-grid").yiiGridView("update");
                                            }else{
                                            }
                                    }',

                                    'beforeSend' => 'js:function(){
                                            $("#deleteUserModal .modal-body").html("Please wait...");
                                            $("#deleteUserModal").modal("show");
                                    }'

                                ),
                            ),
                        )
                    )
                ),
                array(
                    'header' => 'Get User Info',
                    'class' => 'CButtonColumn',
                    'template' => '{options}',
                    'buttons' => array(
                        'options' => array(
                            'label' => '<span class="glyphicon glyphicon-user"></span>',
                            'url' => 'Yii::app()->createUrl("user/view",array("id"=>$data->id))',
                            'options' => array(
                                'class' => 'action-link',
                                /*'ajax' => array(
                                    'url' => 'js:$(this).attr("href")',
                                    'type' => 'GET',
                                    'success' => 'js:function(data){
                                            $("#showUserInfoModal .modal-body").html(data);
                                        }',
                                    'beforeSend' => 'js:function(){
                                            $("#showUserInfoModal .modal-body").html("Please wait...");
                                            $("#showUserInfoModal").modal("show");
                                        }'

                                ),*/
                            ),
                        )
                    )
                ),

                /*array(
                    'header' => 'Assign to Project',
                    'class' => 'CButtonColumn',
                    'template' => '{options}',
                    'buttons' => array(
                        'options' => array(
                            'label' => 'Assign to Project',
                            'click' => 'js:function(event){
                        var ajaxUrl = $(this).attr("href");
                        $.ajax({
                          "url":ajaxUrl,
                          "type":"GET",
                          "success":function(data){
                            $("#assignToProjectModal .modal-body").html(data);
                          },
                          "beforeSend":function(){
                            $("#assignToProjectModal .modal-body").html("Please Wait...");
                            $("#assignToProjectModal").modal("show");
                          }
                        });
                        return false;
                    }',
                            'url' => 'Yii::app()->createUrl("user/getAssignToProjectForm",array("uId"=>$data->id))',
                            'options' => array(
                                'class' => 'btn btn-danger',
                                'id'=>'link-'.uniqid(),
                            ),
                        )
                    )
                ),*/
            ),
        )); ?>

    </div>
</div>
<br/>