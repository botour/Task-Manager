<?php
/**
 *
 * Created by PhpStorm.
 * User: Dell 3521
 * Date: 09/01/15
 * Time: 05:59 م
 */

Yii::app()->clientScript->registerScript("edit-follow-up-list",'
$(function(){
    $(".Tabled table").addClass("table");
    $(".Tabled table").addClass("table-condensed");
    $(".group-summery-container").html($(".group-grid-summery").html());
    $(".group-grid-summery").hide();
    $(".user-summery-container").html($(".user-grid-summery").html());
    $(".user-grid-summery").hide();
    $(".readonly-c input").attr("readonly",true);
    updateDeleteLink();

});
var updateDeleteLink = function(){
    $(".delete-user-link").click(function(event){
        var userId = $(this).parent().parent().parent("li").attr("user-id");
        var supervisorId = $(this).parent().parent().parent("li").attr("supervisor-id");
        $.ajax({
            "url":"'.$this->createUrl('user/removeFromFollowList').'",
            "type":"POST",
            "dataType":"json",
            "data":{
                "uId":userId,
                "sId":supervisorId
            },
            "success":function(data){
                if(data.status){
                    $("#user-grid").yiiGridView("update");
                    updateFollowingList(supervisorId);
                }
                $(this).attr("disabled",false);
            },
            "beforeSend":function(){
                $(this).attr("disabled",true);
                $("#loading-indicator").show();
            },
            "error":function(){
                $("#loading-indicator").hide();
            }
        });
    });
}

var updateFollowingList = function(supervisorId){
    $.ajax({
        "url":"'.$this->createUrl('user/getFollowingUserList').'",
        "data":{
            "id":supervisorId
        },
        "type":"GET",
        "success":function(data){
            $("#followup-list-container").html(data);
            $("#loading-indicator").hide();
        }
    });
}
',CClientScript::POS_END);

?>
<div class="row">
    <div class="col-md-9">
        <div class="row">
            <div class="col-md-12">
                <h4>Edit FollowUp List for user: <?php echo $supervisor->username;?></h4>
                <hr/>

                <?php echo CHtml::link("<span class='glyphicon glyphicon-backward'></span> Back to User Management",array('user/index'),array(
                    'class'=>'action-link',
                ))?>
                <br/>
                <br/>
            </div>
        </div>
        <?php echo CHtml::beginForm($this->createUrl('user/updateFList'),'POST')?>
        <div class="row">
            <?php echo CHtml::hiddenField('supervisor_id',$supervisor->id)?>
            <div class="col-md-6" id="group-list-container">
                <?php
                    $this->renderPartial('partials/_group-list',array(
                        'groupsDataProvider'=>$groupsDataProvider,
                    ));
                ?>
                <div class="group-summery-container"></div>
            </div>
            <div class="col-md-6" id="user-list-container">
                <?php
                    $this->renderPartial('partials/_user-list',array(
                        'groupsDataProvider'=>$usersDataProvider,
                ));
                ?>
                <div class="user-summery-container"></div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-4">
                <?php echo CHtml::ajaxSubmitButton("Update FollowUp List",Yii::app()->createUrl("user/updateFList"),array(
                    'dataType'=>'json',
                    'success'=>'js:function(data){
                        $("#update-follow-up-submit-button").attr("disabled",false);
                        if(data.status){
                            updateFollowingList(data.sId);
                            $("#user-grid").yiiGridView("update");
                        }
                    }',
                    'beforeSend'=>'js:function(){
                        $("#update-follow-up-submit-button").attr("disabled",true);
                        $("#loading-indicator").show();
                    }'
                ),array(
                    'class'=>'btn btn-success',
                    'id'=>'update-follow-up-submit-button',
                ));?>
            </div>
        </div>
    </div>
    <?php echo CHtml::endForm()?>
    <div id="followup-list-container" class="col-md-3">
        <?php
            $this->renderPartial('partials/_follow-up-list',array(
                'followUpUsers'=>$followUpUsers,
                'supervisorId'=>$supervisorId,
                'supervisor'=>$supervisor,
            ));
        ?>
    </div>
</div>
