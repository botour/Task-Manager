<?php
/*
 * @var $comments []Comment
 */
?>
<div id="comments-loader" class="center-block" style="display: none;"></div>
<?php if(empty($comments)):?>
    <br>
    <span style="color: #999;font-size: 13px;font-weight: 700 ;">There are no comments for this task ....</span>
<?php endif;?>
<?php if(!empty($comments)):?>
    <?php foreach($comments as $comment):?>
        <?php if($comment->status==Comment::STATUS_LOG):?>
            <div style="margin: 6px 0px;"><small style="color: #AAA;"><?php echo $comment->body;?></small></div>
        <?php endif;?>
        <?php if($comment->status==Comment::STATUS_COMMENT):?>
            <div class="media">
                <a class="pull-left" href="#">
                    <img class="media-object" style="width: 23px;height: 21px;" src="<?php echo Yii::app()->baseUrl."\images\comment.png"?>" alt="">
                </a>

                <div class="media-body">
                    <div class="media-heading" style="font-size: 11px;"><?php echo User::model()->findByPk($comment->create_id)->username;?><?php echo " : ".$comment->body;?></div>
                    <div ><small style="color: #AAA;"><?php echo date('Y-M-j h:i A',strtotime($comment->create_date));?></small></div>
                </div>
            </div>
        <?php endif;?>
    <?php endforeach;?>
<?php endif;?>

