<div id="task-details-panel" class="details-panel panel panel-success">
    <div class="panel-heading">
        <!-- Options Container-->
        <div href="#" id="options-container">
            <div class="task-control-panel">
                <?php /* echo CHtml::link('<i class="fa fa-lock"></i>', "#", array(
                    'class' => 'action-link edit-task-control change-task-status-button fa-hover',
                    'id' => 'change-task-status-button',
                    'data-toggle' => 'tooltip',
                    'data-placement' => 'bottom',
                    'title' => 'Active/Inactive',
                )); */?>
            </div>
        </div>
        <!-- End of Options Container-->
        <span class="header-caption" id="header-caption">Auto Task Details</span>
    </div>
    <div class="panel-body">
        <?php echo CHtml::hiddenField('auto-task-id',"",array(
            'id'=>'auto-task-id',
        ));?>
        <div id="task-prompt">
            <div class="select-prompt">Select a Task to show its details</div>
        </div>
        <div id="task-details" style="display: none;">
            <div href="#" id="title-container">
                <h4 class="task-details-text"></h4>
            </div>

            <div href="#" id="description-container" style="min-height: 40px;">
                <p class="task-details-text"></p>
                <p class="task-description-empty-text" style="font-size: 14px;color: #BBB;font-weight: 650;"></p>
            </div>
            <div href="#" id="note-container">
                <span style="min-height: 40px;font-size: 12px;font-weight: 500;color: #888;" class="task-details-text"></span>
            </div>
            <hr style="border-color: #888;"/>
            <div id="comments-container" style="height: 230px;max-height: 230px !important;overflow-y: scroll;">
            </div>
            <!-- Add comment form container -->
            <div id="add-comments-form-container" class="row">
                <?php echo CHtml::beginForm(array('autoTask/addComment'), 'post', array(
                    'role' => 'form',
                    'class' => 'form-horizontal',
                    'id' => 'create-comment-form',
                ))?>
                <?php echo CHtml::hiddenField('task_id', "Borhan", array(
                    'id' => 'comment_form_task_id',
                ));?>
                <div class="col-md-12">
                    <div class="form-group has-feedback">
                        <div class="col-md-12 input-container">
                            <?php echo CHtml::textArea('comment_body', '', array(
                                'class' => 'form-control',
                                'id' => 'comment-body',
                            ));?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <?php echo CHtml::ajaxSubmitButton('Add A Comment', $this->createUrl('autoTask/addComment'), array(
                                'data' => 'js:$("#create-comment-form").serialize()',
                                'dataType' => 'json',
                                'beforeSend' => 'js:function(){
                                                $("#comment-submit-btn").attr("disabled",true);
                                                $("#comment-body").val("");
                                            }',
                                'success' => 'js:function(data){
                                                if(data.status==true){
                                                    updateCommentsList(data.taskId);
                                                }
                                                $("#comment-submit-btn").attr("disabled",false);
                                            }',
                                'error' => 'js:function(){
                                    $("#comment-body").notify(data.message, "info");
                                    $("#comment-submit-btn").attr("disabled",false);
                                }',
                            ), array(
                                'class' => 'form-submit-button',
                                'id' => 'comment-submit-btn'
                            ));?>
                        </div>
                    </div>

                </div>
                <?php echo CHtml::endForm(); ?>

            </div>
        </div>

    </div>

</div>
