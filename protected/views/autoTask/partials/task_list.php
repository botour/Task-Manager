<?php
/**
 * Created by PhpStorm.
 * User: Dell 3521
 * Date: 08/02/15
 * Time: 05:20 م
 */

$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'auto-task-grid',
    'rowHtmlOptionsExpression' => 'array("class"=>$data->getTaskClassNames(),
                                            "auto-task-id"=>$data->id,
                                            "auto-task-title"=>$data->title,
                                            "auto-task-description"=>$data->description,
                                            "auto-task-note"=>$data->note,
                                            "auto-task-serial-no"=>$data->serial_no,
                                        )',
    'summaryText' => 'Auto Tasks Count: {count}',
    'summaryCssClass'=>'auto-task-summery',
    'emptyText' => 'No Auto Tasks Available',
    'afterAjaxUpdate' => 'js:function(){
            $(".Tabled table").addClass("table");
            $(".Tabled table").addClass("table-condensed");
            $(".tooltip-link").tooltip();
            updateAutoTaskActionsLink();
            updateSelectRow();
            $("#loading-indicator").fadeOut(200);
    }',
    'htmlOptions' => array(
        'class' => 'Tabled',
    ),
    'cssFile' => Yii::app()->baseUrl . '/css/main.css',
    'dataProvider' => $dataProvider,
    'columns' => array(
        array(
            'header' => 'Actions',
            'type' => 'raw',
            'value' => array($this, 'renderAutoTaskActionsColumn'),
        ),
        array(
            'header' => 'create By',
            'name'=>'fromUsername',
            'value' => 'User::model()->findByPk($data->create_id)->username',
        ),
        array(
            'header' => 'Title',
            'name'=>'title',
            'type'=>'html',
            'value'=>'$data->title',
            'htmlOptions' => array('style' => 'white-space: nowrap;max-width: 450px;width: 450px;text-overflow :ellipsis;overflow:hidden;'),
        ),
        array(
            'name'=>'status',
            'type'=>'raw',
            'value'=>'CommonFunctions::getLabel($data->status,CommonFunctions::AUTO_TASK_STATUS)',
        ),
        'tag',
        'serial_no',
        array(
            'name' => 'toUsername',
            'header'=>'assign to',
            'value' => '$data->user->username;',
            'htmlOptions' => array('style' => 'width: 100px;'),
        ),
    ),
)); ?>






