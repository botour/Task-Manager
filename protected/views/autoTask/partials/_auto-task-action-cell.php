<div style="display: inline">
    <?php echo CHtml::link('<i class="fa fa-trash"></i>', "#", array(
        'class' => 'action-link auto-task-delete-link tooltip-link',
        'data-toggle' => 'tooltip',
        'data-placement' => 'bottom',
        'auto-task-id'=>$task->id,

        'title' => 'Delete AutoTask',
    ));?>

    <?php echo CHtml::link('<i class="fa fa-pencil"></i>', "#", array(
        'class' => 'action-link auto-task-edit-link tooltip-link',
        'data-toggle' => 'tooltip',
        'data-placement' => 'bottom',
        'auto-task-id'=>$task->id,
        'title' => 'Edit AutoTask',
        'style'=>'margin-left:4px;'
    ));?>
</div>


