<?php
/* @var $this AutoTaskController */
/* @var $autoTaskModel AutoTask */
/* @var $form CActiveForm */

if (!$autoTaskModel->isNewRecord) {
    CommonFunctions::fixAjax();
    Yii::app()->clientscript->scriptMap['jquery.yiiactiveform.js'] = false;
}else{
    Yii::app()->clientScript->registerScript("update_task_form_script", '
$("#end-repeat").attr("disabled",true);
$("#second-priority-days-field").attr("disabled",true);
$("#third-priority-days-field").attr("disabled",true);
',CClientScript::POS_END);
}

Yii::app()->clientScript->registerScript("update_task_form_script", '
var currentDate = "' . CJavaScript::encode(date("Y-m-d")) . '";
var endTypeNever = "' . CJavaScript::encode(AutoTask::END_TYPE_NEVER) . '";
var endTypeSpecificDate = "' . CJavaScript::encode(AutoTask::END_TYPE_SPECIFIC_DATE) . '";
var secondPriorityNeverOption = "' . CJavaScript::encode(AutoTask::SECOND_PRIORITY_NEVER) . '";
var thirdPriorityNeverOption = "' . CJavaScript::encode(AutoTask::THIRD_PRIORITY_NEVER) . '";
var deleteAutoTaskLink = "' . $this->createUrl('autoTask/delete'). '";
var changeStatusLink = "' . $this->createUrl('autoTask/changeStatus'). '";
var getAutoTaskEditFormLink = "' . $this->createUrl('autoTask/getAutoTaskEditForm'). '";
    $(".ok-sign").hide();
    $(".error-sign").hide();
$(function(){
    $("#end-type-dropdown").change(function(){
        if($(this).val()==endTypeSpecificDate){
            $("#end-repeat").attr("disabled",false);
        }else{
            $("#end-repeat").attr("disabled",true);
        }
    });
    $(".radio-input-container input").iCheck({
        checkboxClass: "icheckbox_flat-green",
        radioClass: "iradio_flat-green",
        increaseArea:"20%" // optional
    });
    $("#second-priority-dropdown").change(function(){
        if($(this).val()!=secondPriorityNeverOption){
            $("#second-priority-days-field").attr("disabled",false);
        }else{
            $("#second-priority-days-field").val("");
            $("#second-priority-days-field").attr("disabled",true);
        }
    });
    $("#third-priority-dropdown").change(function(){
        if($(this).val()!=secondPriorityNeverOption){
            $("#third-priority-days-field").attr("disabled",false);
        }else{
            $("#third-priority-days-field").val("");
            $("#third-priority-days-field").attr("disabled",true);
        }
    });


    $(".ok-sign").hide();
    $(".error-sign").hide();

    $(".date-field").datepicker({
         format: "dd-mm-yyyy",
         weekStart: 6,
         startDate: currentDate,
         todayBtn: true,
         daysOfWeekDisabled: "5",
         autoclose: true,
         todayHighlight: true
     });
});

    $(".date-field").datepicker({
         format: "dd-mm-yyyy",
         weekStart: 6,
         startDate: currentDate,
         todayBtn: true,
         daysOfWeekDisabled: "5",
         autoclose: true,
         todayHighlight: true
     });
', CClientScript::POS_END);
?>


<div class="form">
<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'auto-task-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'focus' => array($autoTaskModel, 'title'),
    'clientOptions' => array(
        'successCssClass' => 'has-success',
        'errorCssClass' => 'has-error',
        'validatingErrorMessage' => '',
        'inputContainer' => '.form-group',
        'afterValidateAttribute' => 'js:function(form, attribute, data, hasError){
                $("#"+attribute.inputID).siblings(".error-sign").hide();
                $("#"+attribute.inputID).siblings(".ok-sign").hide();
                if(hasError){
                    $("#"+attribute.inputID).siblings(".error-sign").show();
                }else {
                    $("#"+attribute.inputID).siblings(".ok-sign").show();
                }
            }',
    ),
    'htmlOptions' => array(
        'role' => 'form',
        'class' => 'form-horizontal',
    ),
)); ?>

<?php if (!$autoTaskModel->isNewRecord): ?>
    <?php echo $form->hiddenField($autoTaskModel, 'id') ?>
<?php endif; ?>
<div class="row">
    <div class="col-md-12">
        <?php if(!$autoTaskModel->isNewRecord):?>
        <div class="form-group has-feedback">
            <?php echo $form->labelEx($autoTaskModel, 'serial_no', array('class' => 'col-md-3 control-label task-control-label')); ?>
            <div class="col-md-9 input-container task-form-input-container">
                <?php echo $form->textField($autoTaskModel, 'serial_no', array('placeholder' => 'Serial No', 'size' => 60, 'maxlength' => 255, 'class' => 'form-control ','disabled'=>true)); ?>

            </div>
        </div>
        <?php endif;?>
        <div class="form-group has-feedback">
            <?php echo $form->labelEx($autoTaskModel, 'status', array('class' => 'col-md-3 control-label task-control-label')); ?>
            <div class="col-md-9 input-container task-form-input-container radio-input-container">
                <?php echo $form->radioButtonList($autoTaskModel,'status',AutoTask::getStatusOptionsArray()) ?>
            </div>
        </div>
        <div class="form-group has-feedback">
            <?php echo $form->labelEx($autoTaskModel, 'title', array('class' => 'col-md-3 control-label task-control-label')); ?>
            <div class="col-md-9 input-container task-form-input-container">
                <?php echo $form->textField($autoTaskModel, 'title', array('placeholder' => 'Add auto task title', 'size' => 60, 'maxlength' => 255, 'class' => 'form-control ')); ?>
                <span class="glyphicon glyphicon-remove form-control-feedback error-sign"></span>
                <span class="glyphicon glyphicon-ok form-control-feedback ok-sign"></span>
            </div>
        </div>
        <div class="form-group has-feedback">
            <?php echo $form->labelEx($autoTaskModel, 'description', array('class' => 'col-md-3 control-label task-control-label')); ?>
            <div class="col-md-9 input-container task-form-input-container">
                <?php echo $form->textArea($autoTaskModel, 'description', array('placeholder' => 'Add auto task description', 'class' => 'form-control')); ?>
            </div>
        </div>

        <div class="form-group has-feedback">
            <?php
                $disableRepeatTypeStatus = !$autoTaskModel->isNewRecord;
            ?>
            <?php echo $form->labelEx($autoTaskModel, 'repeat_type', array('class' => 'col-md-3 control-label task-control-label')); ?>
            <div class="col-md-3 input-container task-form-input-container">
                <?php echo $form->dropDownList($autoTaskModel, 'repeat_type', AutoTask::getRepeatOptionsArray(), array('prompt' => 'Repeat Type', 'class' => 'form-control','disabled'=>$disableRepeatTypeStatus)); ?>
            </div>
            <?php echo $form->labelEx($autoTaskModel, 'end_type', array('class' => 'col-md-2 control-label task-control-label')); ?>
            <div class="col-md-2 input-container task-form-input-container" style="padding-right: 10px !important;">
                <?php echo $form->dropDownList($autoTaskModel, 'end_type', AutoTask::getEndTypeOptionsArray(), array('id' => 'end-type-dropdown', 'prompt' => 'End Type', 'class' => 'form-control')); ?>
            </div>
            <div class="col-md-2 input-container task-form-input-container" style="padding-right: 5px !important;">
                <?php echo $form->textField($autoTaskModel, 'end_repeat', array('id' => 'end-repeat', 'class' => 'form-control date-field', 'placeholder' => 'End Repeat','disabled'=>$fieldsStatusArray['end_repeat_status'])); ?>
            </div>
        </div>
        <div class="form-group has-feedback">
            <?php echo $form->labelEx($autoTaskModel, 'activation_date', array('class' => 'col-md-3 control-label task-control-label')); ?>
            <div class="col-md-3 input-container task-form-input-container" >
                <?php echo $form->textField($autoTaskModel, 'activation_date', array('class' => 'form-control date-field', 'placeholder' => 'Choose Date')); ?>
            </div>
            <?php echo $form->labelEx($autoTaskModel, 'priority', array('class' => 'col-md-2 control-label task-control-label')); ?>
            <div class="col-md-4 input-container task-form-input-container" style="padding-right: 10px !important;">
                <?php echo $form->dropDownList($autoTaskModel, 'priority', AutoTask::getPriorityOptionsArray(), array('class' => 'form-control')); ?>
            </div>
        </div>
        <div class="form-group has-feedback ">
            <?php echo $form->labelEx($autoTaskModel, 'second_priority', array('class' => 'col-md-3 control-label task-control-label')); ?>
            <div class="col-md-2 input-container task-form-input-container">
                <?php echo $form->dropDownList($autoTaskModel, 'second_priority', AutoTask::getSecondPriorityOptionsArray(), array('id' => 'second-priority-dropdown', 'class' => 'form-control')); ?>
            </div>
            <div class="col-md-1 input-container task-form-input-container"
                 style="padding-right: 10px !important;">
                <?php echo $form->textField($autoTaskModel, 'second_priority_days', array('id' => 'second-priority-days-field', 'class' => 'form-control', 'placeholder' => 'After','disabled'=>$fieldsStatusArray['second_priority_status'])); ?>
            </div>
            <?php echo $form->labelEx($autoTaskModel, 'third_priority', array('class' => 'col-md-2 control-label task-control-label')); ?>
            <div class="col-md-2 input-container task-form-input-container" style="padding-right: 10px">
                <?php echo $form->dropDownList($autoTaskModel, 'third_priority', AutoTask::getThirdPriorityOptionsArray(), array('id' => 'third-priority-dropdown', 'class' => 'form-control')); ?>
            </div>
            <div class="col-md-1 input-container task-form-input-container input-group"
                 style="padding-right: 0px !important;">
                <?php echo $form->textField($autoTaskModel, 'third_priority_days', array('id' => 'third-priority-days-field', 'aria-describedby' => 'second-priority-addon', 'class' => 'form-control', 'placeholder' => 'After','disabled'=>$fieldsStatusArray['third_priority_status'])); ?>
            </div>

        </div>

        <div class="form-group">
            <?php echo $form->labelEx($autoTaskModel, 'due_to_date', array('class' => 'col-md-3 control-label task-control-label')); ?>
            <div class="col-md-3 input-container task-form-input-container">
                <?php echo $form->textField($autoTaskModel, 'due_to_date', array('class' => 'form-control date-field', 'placeholder' => 'Due Date')); ?>
            </div>
        </div>
        <div class="form-group">
            <?php if ($currentUser->role == User::ROLE_ADMIN): ?>
                <?php echo $form->label($autoTaskModel, 'project_id', array('class' => 'col-md-3 control-label task-control-label', 'for' => 'name')); ?>
                <div class="col-md-3 input-container task-form-input-container">
                    <?php echo $form->dropDownList($autoTaskModel, 'project_id', Project::model()->getProjectList(User::ROLE_ADMIN, $currentUser), array(
                        'id' => uniqid(),
                        'prompt' => 'Select Workspace',
                        'class' => 'form-control',
                        'ajax' => array(
                            'type' => 'GET',
                            'url' => Yii::app()->createUrl('task/getMemberList'),
                            'data' => 'js:{project_id:$(this).val()}',
                            'update' => '#user_id',
                        ),
                    )); ?>
                    <?php echo $form->error($autoTaskModel, 'project_id'); ?>
                </div>
            <?php endif; ?>
            <?php if ($currentUser->role == User::ROLE_PROJECT_SUPERVISOR): ?>
                <?php echo $form->label($autoTaskModel, 'project_id', array('class' => 'col-md-3 control-label task-control-label', 'for' => 'name')); ?>
                <div class="col-md-3 input-container task-form-input-container">
                    <?php echo $form->dropDownList($autoTaskModel, 'project_id', Project::model()->getProjectList(User::ROLE_PROJECT_SUPERVISOR, $currentUser), array(
                        'id' => uniqid(),
                        'prompt' => 'Select Workspaces',
                        'class' => 'form-control',
                        'ajax' => array(
                            'type' => 'GET',
                            'url' => Yii::app()->createUrl('task/getMemberList'),
                            'data' => 'js:{project_id:$(this).val()}',
                            'update' => '#user_id',
                        ),
                    )); ?>
                </div>
            <?php endif; ?>
            <?php if ($currentUser->role == User::ROLE_ADMIN || $currentUser->role == User::ROLE_PROJECT_SUPERVISOR): ?>
                <div class="col-md-6 input-container task-form-input-container" style="padding-right: 10px !important;">
                    <?php echo $form->dropDownList($autoTaskModel, 'user_id', $autoTaskModel->isNewRecord?array():Project::model()->findByPk($autoTaskModel->project_id)->getMembersList(), array('id' => 'user_id', 'prompt' => 'Select Workspace to Display Users', 'class' => 'form-control select-user-dropdown')); ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="form-group has-feedback">
            <?php echo $form->labelEx($autoTaskModel, 'note', array('class' => 'col-md-3 control-label task-control-label')); ?>
            <div class="col-md-9 input-container task-form-input-container">
                <?php echo $form->textArea($autoTaskModel, 'note', array('class' => 'form-control')); ?>
            </div>
        </div>
        <div class="form-group has-feedback">
            <?php echo $form->labelEx($autoTaskModel, 'tag', array('class' => 'col-md-3 control-label task-control-label')); ?>
            <div class="col-md-9 input-container task-form-input-container">
                <?php echo $form->textField($autoTaskModel, 'tag', array('placeholder' => 'Add the task tag ...', 'size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
                <span class="glyphicon glyphicon-remove form-control-feedback error-sign"></span>
                <span class="glyphicon glyphicon-ok form-control-feedback ok-sign"></span>
            </div>
        </div>
        <div class="form-group">
            <?php if($autoTaskModel->isNewRecord):?>
            <div class="col-md-offset-3 col-md-3" style="padding-left: 0px;margin-left: 190px;">
                <?php echo CHtml::ajaxSubmitButton('Save Task', Yii::app()->createUrl('autoTask/create'), array(
                    'dataType' => 'json',
                    'success' => 'js:function(data){
                    $(this).button("reset");
                    if(data.status){
                        $("#newAutoTaskCreatedModal .modal-body").html("Serial No: "+data.serial_no);
                        $("#newAutoTaskCreatedModal").modal("show");
                        setTimeout(function(){
                            $("#newAutoTaskCreatedModal").modal("hide");
                        },2000);
                        resetAutoTasksCreateForm();
                        $("#auto-task-grid").yiiGridView("update");
                    }else{
                        $("#failMessageModal .modal-body").html(data.message);
                        $("#failMessageModal").modal("show");
                        setTimeout(function(){
                            $("#failMessageModal").modal("hide");
                        },2000);
                    }
                }',
                    'beforeSave' => 'js:function(){
                    $(this).button("loading");
                }',
                    'error' => 'js:function(){
                    $(this).button("reset");
                }'
                ), array(
                    'id' => 'link-' . uniqid(),
                    'class' => 'btn btn-success btn-sm',
                ))?>
            </div>
            <?php endif;?>
            <?php if(!$autoTaskModel->isNewRecord):?>
                <div class="col-md-offset-3 col-md-3" style="padding-left: 0px;margin-left: 199px;">
                    <?php echo CHtml::ajaxSubmitButton('Edit Task', Yii::app()->createUrl('autoTask/update'), array(
                        'dataType' => 'json',
                        'success' => 'js:function(data){
                            $("#autoTaskUpdatedModal .modal-body").html(data.message);
                            $("#autoTaskUpdatedModal").modal("show");
                            $("#loading-indicator").hide();
                            setTimeout(function(){
                                $("#autoTaskUpdatedModal").modal("hide");
                            },2000);
                            if(data.status){
                                updateTaskDetailsPanel(data.taskObject);
                                $("#auto-task-grid").yiiGridView("update");
                            }
                        }',
                        'beforeSend' => 'js:function(){
                            $("#editAutoTaskModal").modal("hide");
                            $("#loading-indicator").show();
                        }',
                        'error' => 'js:function(){
                            $(this).button("reset");
                            $("#loading-indicator").hide();
                        }'
                    ), array(
                        'id' => 'link-' . uniqid(),
                        'class' => 'btn btn-success btn-sm',
                    ))?>
                </div>
            <?php endif;?>
        </div>

    </div>
</div>

<?php $this->endWidget(); ?>

</div><!-- form -->