<?php
/* @var $this AutoTaskController */
/* @var $model AutoTask */

$this->breadcrumbs=array(
	'Auto Tasks'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AutoTask', 'url'=>array('index')),
	array('label'=>'Manage AutoTask', 'url'=>array('admin')),
);
?>

<h1>Create AutoTask</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>