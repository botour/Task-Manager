<?php
/* @var $this AutoTaskController */
/* @var $data AutoTask */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('project_id')); ?>:</b>
	<?php echo CHtml::encode($data->project_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('serial_no')); ?>:</b>
	<?php echo CHtml::encode($data->serial_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('priority')); ?>:</b>
	<?php echo CHtml::encode($data->priority); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('note')); ?>:</b>
	<?php echo CHtml::encode($data->note); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tag')); ?>:</b>
	<?php echo CHtml::encode($data->tag); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('occurrence')); ?>:</b>
	<?php echo CHtml::encode($data->occurrence); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('activation_date')); ?>:</b>
	<?php echo CHtml::encode($data->activation_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('repeat_type')); ?>:</b>
	<?php echo CHtml::encode($data->repeat_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('end_type')); ?>:</b>
	<?php echo CHtml::encode($data->end_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('end_repeat')); ?>:</b>
	<?php echo CHtml::encode($data->end_repeat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('due_to_date')); ?>:</b>
	<?php echo CHtml::encode($data->due_to_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('second_priority')); ?>:</b>
	<?php echo CHtml::encode($data->second_priority); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('second_priority_days')); ?>:</b>
	<?php echo CHtml::encode($data->second_priority_days); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('third_priority')); ?>:</b>
	<?php echo CHtml::encode($data->third_priority); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('third_priority_days')); ?>:</b>
	<?php echo CHtml::encode($data->third_priority_days); ?>
	<br />

	*/ ?>

</div>