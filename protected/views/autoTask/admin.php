<?php
/* @var $this AutoTaskController */
/* @var $model AutoTask */

$this->breadcrumbs=array(
	'Auto Tasks'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List AutoTask', 'url'=>array('index')),
	array('label'=>'Create AutoTask', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#auto-task-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Auto Tasks</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'auto-task-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'user_id',
		'project_id',
		'serial_no',
		'status',
		'priority',
		/*
		'title',
		'description',
		'note',
		'tag',
		'occurrence',
		'activation_date',
		'repeat_type',
		'end_type',
		'end_repeat',
		'due_to_date',
		'second_priority',
		'second_priority_days',
		'third_priority',
		'third_priority_days',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
