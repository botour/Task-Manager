<?php
/* @var $this AutoTaskController */
/* @var $model AutoTask */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user_id'); ?>
		<?php echo $form->textField($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'project_id'); ?>
		<?php echo $form->textField($model,'project_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'serial_no'); ?>
		<?php echo $form->textField($model,'serial_no',array('size'=>55,'maxlength'=>55)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'priority'); ?>
		<?php echo $form->textField($model,'priority'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'description'); ?>
		<?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'note'); ?>
		<?php echo $form->textArea($model,'note',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tag'); ?>
		<?php echo $form->textField($model,'tag',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'occurrence'); ?>
		<?php echo $form->textField($model,'occurrence'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'activation_date'); ?>
		<?php echo $form->textField($model,'activation_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'repeat_type'); ?>
		<?php echo $form->textField($model,'repeat_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'end_type'); ?>
		<?php echo $form->textField($model,'end_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'end_repeat'); ?>
		<?php echo $form->textField($model,'end_repeat'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'due_to_date'); ?>
		<?php echo $form->textField($model,'due_to_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'second_priority'); ?>
		<?php echo $form->textField($model,'second_priority'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'second_priority_days'); ?>
		<?php echo $form->textField($model,'second_priority_days'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'third_priority'); ?>
		<?php echo $form->textField($model,'third_priority'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'third_priority_days'); ?>
		<?php echo $form->textField($model,'third_priority_days'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->