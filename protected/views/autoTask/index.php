<?php
/* @var $this AutoTaskController */
/* @var $dataProvider CActiveDataProvider */
/* @var $autoTask AutoTask */

Yii::app()->clientScript->scriptMap['jquery.js'] = false;
// Register CSS files
Yii::app()->clientScript->registerCSSFile(Yii::app()->baseUrl . '/css/datepicker3.css');
Yii::app()->clientScript->registerCSSFile(Yii::app()->baseUrl . '/css/green.css');
Yii::app()->clientScript->registerCSSFile(Yii::app()->baseUrl . '/css/pnotify.css');
// Register script files
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/pnotify.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/bootstrap-datepicker.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/logic/auto_task_main.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/icheck.min.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScript("update_auto_task_script", '
var currentDate = "' . CJavaScript::encode(date("Y-m-d")) . '";
var endTypeNever = "' . CJavaScript::encode(AutoTask::END_TYPE_NEVER) . '";
var endTypeSpecificDate = "' . CJavaScript::encode(AutoTask::END_TYPE_SPECIFIC_DATE) . '";
var secondPriorityNeverOption = "' . CJavaScript::encode(AutoTask::SECOND_PRIORITY_NEVER) . '";
var thirdPriorityNeverOption = "' . CJavaScript::encode(AutoTask::THIRD_PRIORITY_NEVER) . '";
var deleteAutoTaskLink = "' . $this->createUrl('autoTask/delete'). '";
var changeStatusLink = "' . $this->createUrl('autoTask/changeStatus'). '";
var getAutoTaskEditFormLink = "' . $this->createUrl('autoTask/getAutoTaskEditForm'). '";
var getCommentsURL = "'.$this->createUrl('autoTask/getTaskComments') .'";

', CClientScript::POS_END);
$this->widget('application.components.Ajaxmodal', array(
    'name' => 'newAutoTaskCreatedModal',
    'title' => 'A new Task Created',
    'width' => '600px',
));
$this->widget('application.components.Ajaxmodal', array(
    'name' => 'failMessageModal',
    'title' => 'Error',
    'width' => '600px',
));
$this->widget('application.components.Ajaxmodal', array(
    'name' => 'deleteAutoTaskModal',
    'title' => 'Notice',
    'width' => '600px',
));
$this->widget('application.components.Ajaxmodal', array(
    'name' => 'editAutoTaskModal',
    'title' => 'Edit AutoTask',
    'width' => '60',
));
$this->widget('application.components.Ajaxmodal', array(
    'name' => 'autoTaskUpdatedModal',
    'title' => 'Notice',
    'width' => '60',
));

?>

<div class="row">
    <div class="col-md-8">
        <div class="row" role="tabpanel">
            <div role="tabpanel">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified" role="tablist">
                    <li role="presentation" class="task-content-tab create-task-tab"><a href="#create-task-container"
                                                                                        aria-controls="create-task-container" role="tab"
                                                                                        data-toggle="tab">Create Auto Task</a></li>
                    <li role="presentation" class="task-content-tab auto-task-list-tab active"><a href="#auto-task-list-container"
                                                                                        aria-controls="auto-task-list-container" role="tab"
                                                                                        data-toggle="tab">Auto Tasks</a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <br/>
                    <div role="tabpanel" class="tab-pane fade" id="create-task-container">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-10">
                                        <?php  $this->renderPartial('_form',array(
                                            'autoTaskModel'=>$autoTaskModel,
                                            'currentUser'=>$currentUser,
                                            'fieldsStatusArray'=>array(
                                                'end_repeat_status'=>true,
                                                'second_priority_status'=>true,
                                                'third_priority_status'=>true,
                                            ),
                                        ));?>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade in active" id="auto-task-list-container">
                        <?php  $this->renderPartial('partials/task_list',array(
                            'dataProvider'=>$dataProvider,
                        ));?>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="col-md-4">
        <?php  $this->renderPartial('partials/task_details');?>
    </div>
</div>