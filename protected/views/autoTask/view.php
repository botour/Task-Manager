<?php
/* @var $this AutoTaskController */
/* @var $model AutoTask */

$this->breadcrumbs=array(
	'Auto Tasks'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List AutoTask', 'url'=>array('index')),
	array('label'=>'Create AutoTask', 'url'=>array('create')),
	array('label'=>'Update AutoTask', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete AutoTask', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AutoTask', 'url'=>array('admin')),
);
?>

<h1>View AutoTask #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_id',
		'project_id',
		'serial_no',
		'status',
		'priority',
		'title',
		'description',
		'note',
		'tag',
		'occurrence',
		'activation_date',
		'repeat_type',
		'end_type',
		'end_repeat',
		'due_to_date',
		'second_priority',
		'second_priority_days',
		'third_priority',
		'third_priority_days',
	),
)); ?>
