<?php
/* @var $this ProjectController */
/* @var $projectModel Project */

$this->widget('application.components.Ajaxmodal', array(
    'name' => 'deleteUserModal',
    'title' => 'Deleting User From WorkSpace',
    'width' => '80',
));
?>
<h3>Update data for workspace: <?php echo CHtml::encode($projectModel->name)?></h3>
<hr/>
<?php echo CHtml::link("<span class='glyphicon glyphicon-backward'></span> Back to Workspace Management",array('project/index'),array(
    'class'=>'action-link',
))?>
<br/>
<br/>
<?php $this->renderPartial('_form', array('projectModel'=>$projectModel,'ref'=>$ref,'userDataProvider'=>$userDataProvider,)); ?>
