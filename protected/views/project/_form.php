<?php
/* @var $this ProjectController */
/* @var $projectModel Project */
/* @var $form CActiveForm */
Yii::app()->clientScript->registerScript("course_script", '
$(function(){
    $(".ok-sign").hide();
    $(".error-sign").hide();
    $(".Tabled table").addClass("table");
});

', CClientScript::POS_END);
CommonFunctions::fixAjax();
?>

<div class="form">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'project-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'focus' => array($projectModel, 'name'),
        'clientOptions' => array(
            'successCssClass' => 'has-success',
            'errorCssClass' => 'has-error',
            'validatingErrorMessage' => '',
            'inputContainer' => '.form-group',
            'afterValidateAttribute' => 'js:function(form, attribute, data, hasError){
                $("#"+attribute.inputID).siblings(".error-sign").hide();
                $("#"+attribute.inputID).siblings(".ok-sign").hide();
                if(hasError){
                    $("#"+attribute.inputID).siblings(".error-sign").show();
                }else {
                    $("#"+attribute.inputID).siblings(".ok-sign").show();
                }
            }',
        ),
        'htmlOptions' => array(
            'role' => 'form',
            'class' => 'form-horizontal',
        ),
    )); ?>
    <?php echo CHtml::hiddenField("ref", $ref) ?>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group has-feedback">
                <?php echo $form->labelEx($projectModel, 'name', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-9 input-container">
                    <?php echo $form->textField($projectModel, 'name', array('placeholder' => 'Add the Workspace name ...', 'size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
                    <span class="glyphicon glyphicon-remove form-control-feedback error-sign"></span>
                    <span class="glyphicon glyphicon-ok form-control-feedback ok-sign"></span>
                    <?php echo $form->error($projectModel, 'name'); ?>
                </div>
            </div>

            <div class="form-group has-feedback">
                <?php echo $form->labelEx($projectModel, 'description', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-9 input-container">
                    <?php echo $form->textArea($projectModel, 'description', array('placeholder' => 'Optional ...', 'class' => 'form-control')); ?>
                    <?php echo $form->error($projectModel, 'description'); ?>
                </div>
            </div>


            <div class="form-group has-feedback">
                <?php echo $form->labelEx($projectModel, 'note', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-9 input-container">
                    <?php echo $form->textArea($projectModel, 'note', array('placeholder' => 'Optional ...', 'class' => 'form-control')); ?>
                    <?php echo $form->error($projectModel, 'note'); ?>
                </div>
            </div>

        </div>
        <div class="col-md-6">
            <?php if(!$projectModel->isNewRecord):?>
            <?php $this->widget('zii.widgets.grid.CGridView', array(
                'id' => 'user-grid',
                'summaryText' => '',
                'afterAjaxUpdate' => 'js:function(){
        	$(".Tabled table").addClass("table");
         }',
                'htmlOptions' => array(
                    'class' => 'Tabled',
                ),
                'cssFile' => Yii::app()->baseUrl . '/css/main.css',
                'dataProvider' => $userDataProvider,
                'columns' => array(
                    array(
                        'class' => 'CCheckBoxColumn',
                        'htmlOptions' => array('style' => 'width: 40px;'),
                        'id' => 'user_id',
                        'selectableRows' => 2,
                        'name' => 'id',
                        'disabled'=>'$data->isMemberInProject($data->projectId)',
                    ),
                    array(
                        'header' => 'User Name',
                        'value' => '$data->username',
                    ),
                    array(
                        'header' => 'Delete User From Workspace',
                        'class' => 'CButtonColumn',
                        'template' => '{options}',
                        'buttons' => array(
                            'options' => array(
                                'label' => '<span class="glyphicon glyphicon-trash"></span>',
                                'visible'=>'$data->isMemberInProject($data->projectId)',
                                'url' => 'Yii::app()->createUrl("project/deleteFromProject",array("userId"=>$data->id,"projectId"=>$data->projectId))',
                                'options' => array(
                                    'class' => 'action-link',
                                    'confirm'=>'Are you sure you want to delete this user?',
                                    'ajax' => array(
                                        'url' => 'js:$(this).attr("href")',
                                        'type' => 'GET',
                                        'dataType'=>'json',
                                        'success' => 'js:function(data){
                                            $("#deleteUserModal .modal-body").html(data.message);
                                            if(data.status){
                                                $("#user-grid").yiiGridView("update");
                                            }
                                    }',

                                        'beforeSend' => 'js:function(){
                                            $("#deleteUserModal .modal-body").html("Please wait...");
                                            $("#deleteUserModal").modal("show");
                                    }'

                                    ),
                                ),
                            )
                        )
                    ),
                ),
            )); ?>
            <?php endif;?>
            <?php if($projectModel->isNewRecord):?>
                <?php $this->widget('zii.widgets.grid.CGridView', array(
                    'id' => 'user-grid',
                    'summaryText' => '',
                    'afterAjaxUpdate' => 'js:function(){
        	$(".Tabled table").addClass("table");
         }',
                    'htmlOptions' => array(
                        'class' => 'Tabled',
                    ),
                    'cssFile' => Yii::app()->baseUrl . '/css/main.css',
                    'dataProvider' => $userDataProvider,
                    'columns' => array(
                        array(
                            'class' => 'CCheckBoxColumn',
                            'htmlOptions' => array('style' => 'width: 40px;'),
                            'id' => 'user_id',
                            'selectableRows' => 2,
                            'name' => 'id',
                            'disabled'=>'$data->isMemberInProject($data->projectId)',
                        ),
                        array(
                            'name'=>'username',
                        ),
                    ),
                )); ?>
            <?php endif;?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?php if ($ref == "p_index" && $projectModel->isNewRecord): ?>
                <?php echo CHtml::ajaxSubmitButton('Save Workspace', $this->createUrl('create'), array(
                    'type' => "POST",
                    'dataType' => 'json',
                    'success' => 'js:function(data){
                               $("#statusModal .modal-body").html(data.message);
                               $("#statusModal").modal("show");
                               $("#project-grid").yiiGridView("update");
                               setTimeout(function(){
                                    $("#statusModal").modal("hide");
                               },5000);
                        }',
                    'beforeSend' => 'js:function(){
                            $("#createProjectModal").modal("hide");
                    }'
                ), array(
                    'class' => 'btn btn-danger',
                    'id' => 'link-' . uniqid(),
                ))?>
            <?php endif; ?>
            <?php if (!$projectModel->isNewRecord): ?>
                <?php echo CHtml::submitButton("Update Workspace Data", array(
                    'class' => 'btn btn-danger',
                ))?>
            <?php endif; ?>

        </div>
    </div>
    <?php $this->endWidget(); ?>
</div><!-- form -->