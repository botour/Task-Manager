<?php
/* @var $this ProjectController */
/* @var $projectModel Project */

Yii::app()->clientScript->registerScript('user-grid', "
$(function(){
        $('.Tabled table').addClass('table');
});
",CClientScript::POS_END);

$this->widget('application.components.Ajaxmodal', array(
    'name' => 'deleteUserModal',
    'title' => 'Deleting User From WorkSpace',
    'width' => '600px',
));

?>

<div class="row">
    <div class="col-md-12">
        <h4><?php echo CHtml::encode($this->pageTitle);?> <span class="label label-success"><?php echo $projectModel->name;?></span></h4>
        <?php echo CHtml::link('Show Tasks in Workspace',$this->createUrl('task/index',array('project_id'=>$projectModel->id)),array(
            'class'=>'btn btn-danger btn-sm',

        ))?>
    </div>
</div>
<hr/>
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-success">
            <div class="panel-heading">
                <span>Workspace Details</span>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4"><span>name:</span></div>
                    <div class="col-md-8"><span><?php echo $projectModel->name;?></span></div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-4"><span>description:</span></div>
                    <div class="col-md-8"><span><?php echo $projectModel->getDescription();?></span></div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-4"><span>Creation Date: </span></div>
                    <div class="col-md-8"><span><?php echo date('d-m-Y',strtotime($projectModel->create_date));?></span></div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-4"><span>note:</span></div>
                    <div class="col-md-8"><span><?php echo $projectModel->getNote();?></span></div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-4"><span>Members' count</span></div>
                    <div class="col-md-8"><span><?php echo count($projectModel->members);?></span></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-success">
            <div class="panel-heading">
                <span>Workspace Statistics</span>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4"><span>Not Read: </span></div>
                    <div class="col-md-8"><span><?php echo $notReadTaskCount;?></span></div>
                </div>
                <div class="row">
                    <div class="col-md-4"><span>Not Complete: </span></div>
                    <div class="col-md-8"><span><?php echo $incompleteTaskCount;?></span></div>
                </div>
                <div class="row">
                    <div class="col-md-4"><span>Complete: </span></div>
                    <div class="col-md-8"><span><span><?php echo $completeTaskCount;?></span></span></div>
                </div>
                <div class="row">
                    <div class="col-md-4"><span>Archived: </span></div>
                    <div class="col-md-8"><span><span><?php echo $archivedTaskCount;?></span></span></div>
                </div>
                <div class="row">
                    <div class="col-md-4"><span>Accomplishment ratio: </span></div>
                    <div class="col-md-8"><span><?php echo round(($archivedTaskCount/$taskCount)*100);?> %</span></div>
                </div>

            </div>
        </div>
    </div>
</div>
<br/>
<h3>Users:</h3>
<hr/>
<div class="row">
    <div class="col-md-12">
        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'user-grid',
            'summaryText' => '',
            'afterAjaxUpdate' => 'js:function(){
        	$(".Tabled table").addClass("table");
         }',
            'htmlOptions' => array(
                'class' => 'Tabled',
            ),
            'cssFile' => Yii::app()->baseUrl . '/css/main.css',
            'dataProvider' => $userDataProvider,
            'columns' => array(
                'username',
                array(
                    'header'=>'Status',
                    'value'=>'CommonFunctions::getLabel($data->status,CommonFunctions::USER_STATUS)',
                    'type'=>'raw',
                ),
                array(
                    'header' => 'Delete User From Workspace',
                    'class' => 'CButtonColumn',
                    'template' => '{options}',
                    'buttons' => array(
                        'options' => array(
                            'label' => '<span class="glyphicon glyphicon-trash"></span>',
                            'url' => 'Yii::app()->createUrl("project/deleteFromProject",array("userId"=>$data->id,"projectId"=>$data->projectId))',
                            'options' => array(
                                'class' => 'action-link',
                                'confirm'=>'Are you sure you want to delete this user?',
                                'ajax' => array(
                                    'url' => 'js:$(this).attr("href")',
                                    'type' => 'GET',
                                    'dataType'=>'json',
                                    'success' => 'js:function(data){
                                            $("#deleteUserModal .modal-body").html(data.message);
                                            if(data.status){
                                                $("#user-grid").yiiGridView("update");
                                            }
                                    }',

                                    'beforeSend' => 'js:function(){
                                            $("#deleteUserModal .modal-body").html("Please wait...");
                                            $("#deleteUserModal").modal("show");
                                    }'

                                ),
                            ),
                        )
                    )
                ),
            ),
        )); ?>

    </div>
</div>