<?php
/**
 * Created by PhpStorm.
 * User: Dell 3521
 * Date: 04/02/15
 * Time: 05:38 م
 */

// Initial Code goes here
Yii::app()->clientScript->registerCSSFile(Yii::app()->baseUrl . '/css/datepicker3.css');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/bootstrap-datepicker.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/canvasjs.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/logic/chart_main.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScript("task_manager_script", '
var currentDate = "' . CJavaScript::encode(date("Y-m-d")) . '";
', CClientScript::POS_END);
?>
<h4>Tasks Report</h4>
<hr/>
<div class="row">
    <div class="col-md-8">
        <div class="row">
                <?php $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'tasks-report-form',
                    'action'=>array('project/getReportData'),
                    'focus' => array($report, 'project_id'),
                    'htmlOptions' => array(
                        'role' => 'form',
                        'class' => 'form-inline',
                    ),
                )); ?>
                <div class="col-md-10">
                    <div class="form-group">
                        <?php echo $form->dropDownList($report,'project_id',Project::getProjectsList(),array(
                            'prompt'=>'Select a Workspace',
                            'class'=>'form-control',
                            'id'=>'project-options-dropdown',
                        ));?>
                    </div>
                    <div class="form-group">
                        <?php echo $form->textField($report,'from_date',array(
                            'class'=>'form-control date-field',
                            'placeholder'=>'From',
                        ))?>
                    </div>
                    <div class="form-group">
                        <?php echo $form->textField($report,'to_date',array(
                            'class'=>'form-control date-field',
                            'placeholder'=>'To',
                        ))?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->dropDownList($report,'report_option',ReportModel::getReportOptionsArray(),array(
                            'prompt'=>'Select a Report Type',
                            'class'=>'form-control',
                            'id'=>'report-options-dropdown',
                        ))?>
                    </div>
                </div>
                <div class="col-md-2">
                    <?php
                        echo CHtml::ajaxButton('Generate Report',array('project/getReportData'),array(
                            'success'=>'function(data){
                                $("#please-wait-container").hide();
                                if(data.status){
                                    $("#chart-container").show();
                                    renderChart(data.reportDate);
                                }else{
                                    $("#error-message-container").show();
                                }
                            }',
                            'beforeSend'=>'function(){
                               $("#chart-container").fadeOut(100);
                               $("#error-message-container").fadeOut(100);
                               $("#please-wait-container").fadeIn(100);
                            }',
                            'type'=>'GET',
                            'data'=>'js:$("#tasks-report-form").serialize()',
                            'dataType'=>'json',
                        ),array(
                            'class'=>'btn btn-sm btn-success',
                            'id'=>'link-'.uniqid(),
                        ));
                    ?>
                </div>
            <?php $this->endWidget(); ?>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-12">
                <div style="display: none;" id="please-wait-container">
                    <span style="color:#222;font-size: 15px;font-weight: 600;">Please Wait ...</span>
                </div>
                <div style="display: none;" id="error-message-container">
                    <span style="color:#FF2222;font-size: 15px;font-weight: 600;">Error Generating the report, Please try again</span>
                </div>
                <div style="display: none;" id="chart-container">
                </div>

            </div>
        </div>
    </div>
    <div class="col-md-4">
        <?php $this->renderPartial('partials/report_details');?>
    </div>
</div>