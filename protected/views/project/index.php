<?php
/* @var $this ProjectController */
/* @var $dataProvider CActiveDataProvider */
Yii::app()->clientScript->registerScript('project-grid-script', "
$(function(){
        $('.Tabled table').addClass('table');
});
",CClientScript::POS_END);

$this->widget('application.components.Ajaxmodal', array(
    'name' => 'createProjectModal',
    'title' => 'Create a new Workspace',
    'width' => '80px',
));

$this->widget('application.components.Ajaxmodal', array(
    'name' => 'statusModal',
    'title' => 'Status',
    'width' => '600px',
));
$this->widget('application.components.Ajaxmodal', array(
    'name' => 'deleteWorkspaceModal',
    'title' => 'Status',
    'width' => '600px',
));
?>

<?php echo CHtml::ajaxButton("Create Workspace",$this->createUrl('getCreateForm',array('ref'=>'p_index')),array(
        'type'=>'GET',
        'success'=>'js:function(data){
            $("#createProjectModal .modal-body").html(data);

        }',
        'beforeSend'=>'js:function(){
            $("#createProjectModal .modal-body").html("Please Wait...");
            $("#createProjectModal").modal("show");
        }'
    ),
    array(
        'id'=>'link-'.uniqid(),
        'class'=>'btn btn-success btn-sm',
    ));
?>
<hr/>


<div class="row">
    <div class="col-md-12">
        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'project-grid',
            'summaryText' => '<span>Total No Of Workspaces: {count}</span><br/>',
            'afterAjaxUpdate' => 'js:function(){
        	$(".Tabled table").addClass("table");
         }',
            'htmlOptions' => array(
                'class' => 'Tabled',
            ),
            'cssFile' => Yii::app()->baseUrl . '/css/main.css',
            'dataProvider' => $dataProvider,
            'columns' => array(
                'name',
                array(
                    'header'=>'members count',
                    'value'=>'$data->membersCount',
                ),

                array(
                    'name'=>'create_date',
                    'value'=>'date("d-m-Y",strtotime($data->create_date))'
                ),
                'description',
                'note',
                array(
                    'header' => 'Edit',
                    'class' => 'CButtonColumn',
                    'template' => '{options}',
                    'buttons' => array(
                        'options' => array(
                            'label' => '<span class="glyphicon glyphicon-pencil"></span>',
                            'url' => 'Yii::app()->createUrl("project/update",array("id"=>$data->id,"ref"=>"p_index"))',
                            'options' => array(
                                'class' => 'action-link',
                            ),
                        )
                    )
                ),
                array(
                    'header' => 'Delete Workspace',
                    'class' => 'CButtonColumn',
                    'template' => '{options}',
                    'buttons' => array(
                        'options' => array(
                            'label' => '<span class="glyphicon glyphicon-trash"></span>',
                            'visible'=>'$data->isDeletable()',
                            'url' => 'Yii::app()->createUrl("project/delete",array("id"=>$data->id))',
                            'options' => array(
                                'class' => 'action-link',
                                'confirm'=>'Are you sure you want to delete this workspace?',
                                'ajax' => array(
                                    'url' => 'js:$(this).attr("href")',
                                    'type' => 'POST',
                                    'dataType'=>'json',
                                    'success' => 'js:function(data){
                                            alert(data.status);
                                            $("#deleteWorkspaceModal .modal-body").html(data.message);
                                            if(data.status){
                                                $("#project-grid").yiiGridView("update");
                                            }
                                    }',
                                    'beforeSend' => 'js:function(){
                                            $("#deleteWorkspaceModal .modal-body").html("Please wait...");
                                            $("#deleteWorkspaceModal").modal("show");
                                    }'

                                ),
                            ),
                        )
                    )
                ),
                array(
                    'header' => 'View',
                    'class' => 'CButtonColumn',
                    'template' => '{options}',
                    'buttons' => array(
                        'options' => array(
                            'label' => '<span class="glyphicon glyphicon-list-alt"></span>',
                            'url' => 'Yii::app()->createUrl("project/view",array("id"=>$data->id))',
                            'options' => array(
                                'class' => 'action-link',
                            ),
                        )
                    )
                ),
                /*
                array(
                    'header' => 'Go to Task Manager',
                    'class' => 'CButtonColumn',
                    'template' => '{options}',
                    'buttons' => array(
                        'options' => array(
                            'label' => 'Go to Task Manager',

                            'url' => 'Yii::app()->createUrl("task/index",array("project_id"=>$data->id))',
                            'options' => array(
                                'class' => 'btn btn-danger',
                                'id'=>'link-'.uniqid(),
                            ),
                        )
                    )
                ),
                */
            ),
        )); ?>

    </div>
</div>
<br/>
