<?php
/* @var $this TaskController */
/* @var $model Task */
/* @var $form CActiveForm */
?>

<div class="panel panel-info search-tasks">

    <div class="panel-heading"><span class="glyphicon glyphicon-search"></span> <b>Search Tasks By:</b></div>

    <div class="panel-body">

        <?php $form = $this->beginWidget('CActiveForm', array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'get',
            'htmlOptions' => array(
                'role' => 'form',
                'class' => 'form-horizontal',
            ),
        )); ?>

        <div class="form-group">
            <?php echo $form->label($model, 'status', array('class' => 'col-md-4', 'for' => 'name')); ?>
            <div class="col-md-8">
                <?php echo $form->dropDownList($model, 'status', Task::getStatusOptionsArray(), array('prompt' => 'All States', 'class' => 'form-control')); ?>
            </div>
        </div>

        <div class="form-group">
            <?php echo $form->label($model, 'start_date', array('class' => 'col-md-4', 'for' => 'name')); ?>
            <div class="col-md-8" id="datepicker-container">
                <?php echo $form->textField($model, 'create_date', array('size' => 60, 'maxlength' => 255, 'id' => 'name', 'class' => 'form-control')); ?>
            </div>
        </div>

        <div class="form-group">
            <?php echo $form->label($model, 'priority', array('class' => 'col-md-4', 'for' => 'name')); ?>
            <div class="col-md-8">
                <?php echo $form->dropDownList($model, 'priority', Task::getPriorityOptionsArray(), array('prompt' => 'All Priorities', 'class' => 'form-control')); ?>
            </div>
        </div>

        <div class="form-group">
            <?php echo $form->label($model, 'tag', array('class' => 'col-md-4', 'for' => 'name')); ?>
            <div class="col-md-8">
                <?php echo $form->dropDownList($model, 'tag', Project::model()->getProjectList(), array('prompt' => 'All Workspaces', 'class' => 'form-control')); ?>
            </div>
        </div>

        <?php if(User::model()->isAdmin(Yii::app()->user->id)):?>
        <div class="form-group">
            <?php echo $form->label($model, 'user_id', array('class' => 'col-md-4', 'for' => 'name')); ?>
            <div class="col-md-8">
                <?php echo $form->dropDownList($model, 'user_id', User::model()->getUserList(), array('prompt' => 'Select User', 'class' => 'form-control')); ?>
            </div>
        </div>
        <?php endif;?>
        <div class="form-group">
            <div class="col-md-12">
                <?php echo CHtml::submitButton('Search Tasks', array(
                    'class' => 'btn btn-success btn-sm btn-block',
                    'id' => 'link-' . uniqid(),
                )); ?>
            </div>
        </div>

        <?php $this->endWidget(); ?>

    </div>

</div><!-- search-form -->