<?php
/* @var $this TaskController */
/* @var $taskModel Task */

Yii::app()->clientScript->scriptMap['jquery.js'] = false;
Yii::app()->clientScript->registerCSSFile(Yii::app()->baseUrl . '/css/datepicker3.css');
Yii::app()->clientScript->registerCSSFile(Yii::app()->baseUrl . '/css/animate.css');
Yii::app()->clientScript->registerCSSFile(Yii::app()->baseUrl . '/css/green.css');
Yii::app()->clientScript->registerCSSFile(Yii::app()->baseUrl . '/css/red.css');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/bootstrap-datepicker.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/pnotify.js', CClientScript::POS_END);
Yii::app()->clientScript->registerCSSFile(Yii::app()->baseUrl . '/css/pnotify.css');

Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/notify.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/canvasjs.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/tablesorter.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/icheck.min.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/task_scripts.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/logic/tasks_main.js', CClientScript::POS_END);

// Pre-procecssing the new Task Form

if ($currentUser->current_project_id == 0) {
    $userListContent = '<option value="">Select Workspace to Display Users</option>';
} else {
    $project = Project::model()->find('id=:id', array(
        ':id' => $currentUser->current_project_id,
    ));
    $members = $project->getMembersList();
}
$userListContent = "";
if (empty($members)) {
    $userListContent .= '<option value="">No Users in this workspace</option>';
} else {
    $userListContent .= '<option value="">Select A Member</option>';
    foreach ($members as $id => $member) {
        $userListContent .= '<option value="' . $id . '">' . $member . '</option>';
    }
}


Yii::app()->clientScript->registerScript("task_manager_script", '
$(function(){
    $(".ok-sign").hide();
    $(".error-sign").hide();
    var userListContent = ' . CJavaScript::encode($userListContent) . ';
    $("#user_id").html(userListContent);

});
function playSound(){
       document.getElementById("sound").innerHTML="";
}

var getCommentsURL = "' . $this->createUrl('task/getTaskComments') . '";
var deleteCommentURL = "' . Yii::app()->createUrl('task/deleteComment') . '";
var changeTaskStatusToCompleteURL = ' . CJavaScript::encode(Yii::app()->createUrl('task/ChangeTaskStatusToComplete')) . ';
var changeTaskStatusToNotCompleteURL = ' . CJavaScript::encode(Yii::app()->createUrl('task/ChangeTaskStatusToNotComplete')) . ';
var getNewTasksURL = "' . Yii::app()->createUrl('task/getNewTasks') . '";
var currentDate = "' . CJavaScript::encode(date("Y-m-d")) . '";
var getTaskOptionsURL = "' . Yii::app()->createUrl("task/getTaskOptions") . '";
var currentDate = "' . CJavaScript::encode(date("Y-m-d")) . '";
var taskReassignLink = "' . $this->createUrl('task/assignmentForm') . '";
var getPriorityLevelChangeFormURL = "' . $this->createUrl('task/getPriorityLevelChangeForm') . '";
var changeTaskRemindedURL = "' . $this->createUrl('task/changeTaskReminded') . '";
var getUpdateTaskInfoFormURL = "' . $this->createUrl('task/getUpdateTaskInfoForm') . '";
var projectId = "' . $currentUser->current_project_id . '";
var currentDate = "' . CJavaScript::encode(date("Y-m-d")) . '";
var changeTaskStatusToCloseURL = "' . $this->createUrl('task/changeTaskStatusToClose') . '";
var notificationSoundURL = "' . Yii::app()->baseUrl . "/mp3/pling.mp3" . '";

', CClientScript::POS_END);

// Ajax Modals
$this->widget('application.components.Ajaxmodal', array(
    'name' => 'createTaskModal',
    'title' => 'Create a new Task',
    'width' => '600px',
));
$this->widget('application.components.Ajaxmodal', array(
    'name' => 'statusModal',
    'title' => 'Status',
    'width' => '600px',
));
$this->widget('application.components.Ajaxmodal', array(
    'name' => 'taskAssignmentModal',
    'title' => 'Change Task Assignment',
    'width' => '600px',
));
$this->widget('application.components.Ajaxmodal', array(
    'name' => 'changeTaskStateModal',
    'title' => 'Status',
    'width' => '600px',
));
$this->widget('application.components.Ajaxmodal', array(
    'name' => 'changeTaskPriorityModal',
    'title' => 'Status',
    'width' => '600px',
));
$this->widget('application.components.Ajaxmodal', array(
    'name' => 'showTaskInfoModal',
    'title' => 'Task Info',
    'width' => '600px',
));
$this->widget('application.components.Ajaxmodal', array(
    'name' => 'editTaskInfoModal',
    'title' => 'Update Task Info',
    'width' => '600px',
));
$this->widget('application.components.Ajaxmodal', array(
    'name' => 'showTaskOptions',
    'title' => 'Task Options',
    'width' => '600px',
));

echo CHtml::hiddenField('project_id', '', array(
    'id' => 'project-id-field',
));

?>
<div id="sound"></div>
<div class="row" style="display: none;" id="task-manager-panel">
    <div class="col-md-8">
        <div class="row" role="tabpanel">
            <div role="tabpanel">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified" role="tablist">
                    <li role="presentation" class="task-content-tab create-task-tab"><a href="#create-task-container"
                                                                                        aria-controls="create-task-container"
                                                                                        role="tab"
                                                                                        data-toggle="tab">Create
                            Task</a></li>
                    <li grid-id="#task-grid" role="presentation" class="task-content-tab task-content-tab-todo active">

                        <a href="#task-list-container"
                           aria-controls="task-list-container" role="tab"
                           data-toggle="tab">
                            <div style="display: inline-block;position: relative;"><span
                                    style="left: -17px;top: -10;"
                                    class="animated infinite bounce label label-danger notification-indicator todo-new-task-container">0</span>To
                                Do List
                            </div> <span
                                class="label label-danger todo-summery-container"></span>
                        </a></li>
                    <?php if ($this->isSupervisorOrAdministrator): ?>
                        <li grid-id="#follow-up-task-grid" role="presentation"
                            class="task-content-tab task-content-tab-follow-up">
                            <a href="#follow-list-container" aria-controls="follow-list-container"
                               role="tab" data-toggle="tab">
                                <div style="display: inline-block;position: relative;"><span
                                        style="left: -17px;top: -10;"
                                        class="animated infinite flash label label-danger notification-indicator follow-up-new-task-container">0</span>Follow
                                    Up List
                                </div>
                                <span class="label label-danger follow-summery-container"></span></a></li>
                    <?php endif; ?>
                    <li grid-id="#complete-task-grid" role="presentation"
                        class="task-content-tab task-content-tab-complete">
                        <?php if ($currentUser->role == User::ROLE_PROJECT_SUPERVISOR): ?>
                        <?php endif; ?>
                        <a href="#messages" aria-controls="messages" role="tab"
                           data-toggle="tab">
                            <div style="display: inline-block;position: relative;"><span style="left: -17px;top: -10;"
                                                                                         class="animated infinite flash label label-danger notification-indicator complete-new-task-container">0</span>Completed
                            </div>
                            <span
                                class="label label-success complete-summery-container"></span></a>
                    </li>
                    <?php if (Yii::app()->user->checkAccess("view_archive")): ?>
                        <li role="presentation" class="task-content-tab task-content-tab-archived"><a href="#settings"
                                                                                                      aria-controls="settings"
                                                                                                      role="tab"
                                                                                                      data-toggle="tab">Archived <span
                                    class="label label-default archived-summery-container"></span></a></li>
                    <?php endif; ?>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <br/>

                    <div role="tabpanel" class="tab-pane fade" id="create-task-container">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-10">
                                        <?php $this->renderPartial('_form', array(
                                            'model' => $model,
                                            'currentUser' => $currentUser,
                                        ));?>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade in active" id="task-list-container">
                        <?php $this->renderPartial('partials/todo-task-list', array(
                            'taskModel' => $taskModel,
                            'currentUser' => $currentUser,
                            'forceDefaultToDoTasksOrder' => $forceDefaultToDoTasksOrder,
                        ));?>
                    </div>
                    <?php if ($this->isSupervisorOrAdministrator): ?>
                        <div role="tabpanel" class="tab-pane fade" id="follow-list-container">
                            <?php $this->renderPartial('partials/follow-up-task-list', array(
                                'forceDefaultFollowUpTasksOrder' => $forceDefaultFollowUpTasksOrder,
                                'followTaskModel' => $followTaskModel,
                                'currentUser' => $currentUser,
                            ));?>
                        </div>
                    <?php endif; ?>
                    <div role="tabpanel" class="tab-pane" id="messages">
                        <?php $this->renderPartial('partials/complete-task-list', array(
                            'completeTaskList' => $completeTaskList,
                            'forceDefaultCompleteTasksOrder' => $forceDefaultCompleteTasksOrder,
                            'currentUser' => $currentUser,
                        ));?>
                    </div>
                    <?php if (Yii::app()->user->checkAccess("view_archive")): ?>
                        <div role="tabpanel" class="tab-pane" id="settings">
                            <?php $this->renderPartial('partials/archived-task-list', array(
                                'archivedTaskModel' => $archivedTaskModel,
                                'forceDefaultArchivedTasksOrder' => $forceDefaultArchivedTasksOrder,
                                'currentUser' => $currentUser,
                            ));?>
                        </div>
                    <?php endif; ?>
                </div>

            </div>
        </div>
    </div>

    <div id="task-details-container" class="col-md-4">
        <?php $this->renderPartial('partials/_task-details'); ?>
    </div>
</div>
