<?php
/**
 * Created by PhpStorm.
 * User: Dell 3521
 * Date: 03/01/15
 * Time: 07:37 م
 */
?>
<?php if(Yii::app()->user->checkAccess('reassign')):?>
<div style="display: inline"><?php echo CHtml::link('<i class="fa fa-mail-reply" style="font-size: 20px;"></i>  ','#',array('task-id'=>$task->id,'class'=>'action-link reassign-link'));?></div>
<?php endif;?>
<input type="checkbox" class="todo-grid-checkbox" task-id="<?php echo $task->id;?>" <?php echo $task->status==Task::STATE_COMPLETE?"checked":"";?>/>