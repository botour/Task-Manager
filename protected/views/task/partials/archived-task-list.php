<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'archived-task-grid',
    'summaryText' => '{count}',
    'rowHtmlOptionsExpression' => 'array("class"=>$data->getTaskClassNames()." archived-task-row",
                                            "task-id"=>$data->id,
                                            "task-title"=>$data->title,
                                            "task-description"=>$data->description,
                                            "task-note"=>$data->note,
                                            "task-reminded"=>$data->reminded,
                                            "task-due-to-date"=>date("d-m-Y",strtotime($data->due_to_date)),
                                            "task-serial-no"=>$data->serial_no,
                                        )',
    'summaryCssClass' => 'archived-summery',
    'emptyText' => 'No Tasks Available',
    'beforeAjaxUpdate'=>'js:function(){
        $("#loading-indicator").show();
    }',
    'afterAjaxUpdate' => 'js:function(){
                        updateSelectRow();
                        updateChevron();
                        $("#loading-indicator").hide();
                        //$("#archived-task-grid table").tablesorter();
                        $("#archived-task-grid .reset-dropdown").val("");
                        $(".archived-summery-container").html($(".archived-summery").html());
                        $(".archived-summery").hide();
                        $(".Tabled table").addClass("table");
                        $(".Tabled table").addClass("table-condensed");
    }',
    'htmlOptions' => array(
        'class' => 'Tabled',
    ),
    'filter' => $archivedTaskModel,
    'cssFile' => Yii::app()->baseUrl . '/css/main.css',
    'dataProvider' => $archivedTaskModel->archivedTaskSearch($forceDefaultArchivedTasksOrder),
    'columns' => array(
        array(
            'header'=>'End By',
            'name'=>'endByUsername',
            'value'=>'User::model()->findByPk($data->update_id)->username',
            'htmlOptions' => array('style' => 'max-width: 60px;'),
            'filter' => CHtml::activeTextField($archivedTaskModel, 'endByUsername', array(
                    'style' => 'max-width: 52px !important;',
                    'class' => 'form-control filter-control',
                    'name' => 'ArchivedTask[endByUsername]',
                )),
            'filterHtmlOptions' => array('style' => 'width: 60px !important;padding-left:4px !important;'),
        ),
        array(
            'header' => 'from',
            'name'=>'fromUsername',
            'value' => 'User::model()->findByPk($data->create_id)->username',
            'htmlOptions' => array('style' => 'width: 50px;', 'class' => 'user_id_cell'),
            'filterHtmlOptions' => array('style' => 'width: 50px !important;padding-left:0px !important;'),
            'filter' => CHtml::activeTextField($archivedTaskModel, 'createUsername', array(
                    'style' => 'width: 46px !important;',
                    'class' => 'form-control filter-control',
                    'name' => 'ArchivedTask[createUsername]',
                )),
        ),
        array(
            'header' => 'Title',
            'name'=>'title',
            'value' => '$data->title',
            'htmlOptions' => array('style' => 'white-space: nowrap;max-width: 450px;width: 450px;text-overflow :ellipsis;overflow:hidden;', 'class' => 'title_cell'),
            'filterHtmlOptions' => array('style' => 'width: 450px !important;padding-left:0px !important;'),
            'filter' => CHtml::activeTextField($archivedTaskModel, "title", array(
                    'style' => 'width: 450px !important;',
                    'class'=>'form-control filter-control',
                    'name' => 'ArchivedTask[title]',
                )),

        ),
        array(
            'header' => 'D',
            'name'=>'create_date',
            'value' => 'floor((time()-strtotime($data->create_date))/(3600*24))',
            'htmlOptions' => array('style' => 'max-width: 10px;'),
            'filterHtmlOptions' => array('style' => 'max-width: 50px !important;padding-left:0px !important;'),
            'filter' => CHtml::activeTextField($archivedTaskModel, 'daysNo', array(
                        'style' => 'max-width: 25px !important;width: 25px !important;',
                        'class'=>'form-control filter-control',
                        'name' => 'ArchivedTask[daysNo]',
                    )
                ),
        ),
        array(
            'header' => 'Tag',
            'value' => '$data->tag',
            'name'=>'tag',
            'htmlOptions' => array('style' => 'max-width: 170px;', 'class' => 'tag_cell'),
            'filterHtmlOptions' => array('style' => 'padding-left:0px !important;'),
            'filter' => CHtml::activeTextField($archivedTaskModel, 'tag', array(
                    'style' => 'max-width: 165px !important;',
                    'class'=>'form-control filter-control',
                    'name' => 'ArchivedTask[tag]',
                )),
        ),

        array(
            'name' => 'toUsername',
            'header' => 'assign to',
            'value' => '$data->user->username;',
            'htmlOptions' => array('style' => 'width: 100px;'),
            'filterHtmlOptions' => array('style' => 'max-width: 100px !important;padding-left:0px !important;'),
            'filter' => CHtml::activeDropDownList($archivedTaskModel,'user_id',$currentUser->getFollowUpList(),array(
                    'style' => 'width: 100px !important;',
                    'class'=>'form-control filter-control reset-dropdown',
                    'prompt'=>'All',
                    'name' => 'ArchivedTask[user_id]',
                )),
            'visible' => $this->isSupervisorOrAdministrator,
        ),
))); ?>
