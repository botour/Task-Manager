<?php
/**
 * Created by PhpStorm.
 * User: Dell 3521
 * Date: 03/01/15
 * Time: 07:37 م
 */
?>

<input type="checkbox" name="user_id[]" class="todo-grid-checkbox"
       task-id="<?php echo $task->id; ?>" <?php echo $task->status == Task::STATE_COMPLETE ? "checked" : ""; ?>/>

<?php if(Yii::app()->user->checkAccess('end_task')):?>
<input type="checkbox" name="task_id[]" class="complete-grid-checkbox"
       value="<?php echo $task->id; ?>"/>
<?php endif;?>