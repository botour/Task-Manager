<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'follow-up-task-grid',
    'rowHtmlOptionsExpression' => 'array("class"=>$data->getTaskClassNames()." follow-up-task-row",
                                            "task-id"=>$data->id,
                                            "task-title"=>$data->title,
                                            "task-description"=>$data->description,
                                            "task-note"=>$data->note,
                                            "task-reminded"=>$data->reminded,
                                            "task-due-to-date"=>date("d-m-Y",strtotime($data->due_to_date)),
                                            "task-serial-no"=>$data->serial_no,
                                        )',
    'summaryText' => '{count}',
    'summaryCssClass'=>'follow-up-summery',
    'emptyText' => 'No Tasks Available',
    'beforeAjaxUpdate'=>'js:function(){
        $("#loading-indicator").show();
    }',
    'afterAjaxUpdate' => 'js:function(){
                        $("#loading-indicator").hide();
                        updateSelectRow();
                        updateTaskAssignmentLinks();
                        updateTaskReminderLinks();
                        $(".tooltip-link").tooltip();
                        $(".follow-summery-container").html($(".follow-up-summery").html());
                        //$("#follow-up-task-grid table").tablesorter();
                        $(".follow-up-summery").hide();
                        $(".Tabled table").addClass("table");
                        $(".Tabled table").addClass("table-condensed");
                        //$("#follow-up-task-grid .reset-dropdown").val("");
                        updateChevron();
                        $("#datepicker-container input").datepicker({
                                format: "yyyy-mm-dd",
                                weekStart: 6,
                                startDate: "2014-11-4",
                                endDate: "' . CJavaScript::encode(date("Y-m-d")) . '",
                                todayBtn: true,
                                daysOfWeekDisabled: "5",
                                autoclose: true,
                                todayHighlight: true
                        });
                   }',
    'htmlOptions' => array(
        'class' => 'Tabled',
    ),
    'cssFile' => Yii::app()->baseUrl . '/css/main.css',
    'dataProvider' => $followTaskModel->searchFollowUp($forceDefaultFollowUpTasksOrder),
    'filter' => $followTaskModel,
    'columns' => array(
        array(
            'header' => 'Actions',
            'type' => 'raw',
            'value' => array($this, 'renderFollowUpTaskActionsColumn'),
            'filterHtmlOptions' => array(
                'style' => 'width: 70px;min-width:70px;max-width:70px;',
                'class' => 'complete-action-header',
            ),
        ),
        array(
            'header' => 'from',
            'name'=>'fromUsername',
            'value' => 'User::model()->findByPk($data->create_id)->username',
            'htmlOptions' => array('style' => 'width: 50px;', 'class' => 'user_id_cell'),
            'filterHtmlOptions' => array('style' => 'width: 50px !important;padding-left:0px !important;'),
            'filter' => CHtml::activeTextField($followTaskModel, 'createUsername', array(
                    'style' => 'width: 46px !important;',
                    'class' => 'form-control filter-control',
                    'name' => 'FollowTask[createUsername]',
                )),
        ),

        array(
            'header' => 'Title',
            'name'=>'title',
            'type'=>'html',
            'value'=>'$data->getTitleForGrid()',
            'htmlOptions' => array('style' => 'white-space: nowrap;max-width: 450px;width: 450px;text-overflow :ellipsis;overflow:hidden;', 'class' => 'title_cell'),
            'filterHtmlOptions' => array('style' => 'width: 450px !important;padding-left:0px !important;'),
            'filter' => CHtml::activeTextField($followTaskModel, "title", array(
                    'style' => 'width: 450px !important;',
                    'class'=>'form-control filter-control',
                    'name' => 'FollowTask[title]',
                )),
        ),

        array(
            'header'=>'Pri',
            'name'=>'priority',
            'value' => 'CommonFunctions::getLabel($data->priority,CommonFunctions::TASK_PRIORITY)',
            'type' => 'html',
            //'htmlOptions' => array('style' => 'max-width: 30px;'),
            'filterHtmlOptions' => array('style' => 'width: 50px !important;padding-left:0px !important;'),
            'filter' => CHtml::activeDropDownList($followTaskModel,'priority',Task::getPriorityOptionsArray(),array(
                    'style' => 'max-width: 50px !important;width: 50px !important;',
                    'prompt'=>'All',
                    'class'=>'form-control filter-control',
                    'name' => 'FollowTask[priority]',
                )),
        ),
        array(
            'header'=>'D',
            'name'=>'create_date',
            'value'=>'floor((time()-strtotime($data->create_date))/(3600*24))',
            'htmlOptions' => array('style' => 'max-width: 10px;'),
            'filterHtmlOptions' => array('style' => 'max-width: 50px !important;padding-left:0px !important;'),
            'filter' => CHtml::activeTextField($followTaskModel, 'daysNo', array(
                        'style' => 'max-width: 25px !important;width: 25px !important;',
                        'class'=>'form-control filter-control',
                        'name' => 'FollowTask[daysNo]',
                    )
                ),
        ),
        array(
            'header'=>'Tag',
            'name'=>'tag',
            'value'=>'$data->tag',
            'htmlOptions' => array('style' => 'max-width: 170px;', 'class' => 'tag_cell'),
            'filterHtmlOptions' => array('style' => 'padding-left:0px !important;'),
            'filter' => CHtml::activeTextField($followTaskModel, 'tag', array(
                    'style' => 'max-width: 165px !important;',
                    'class'=>'form-control filter-control',
                    'name' => 'FollowTask[tag]',
                )),
        ),
        array(
            'name' => 'toUsername',
            'header'=>'assign to',
            'value' => '$data->user->username;',
            'htmlOptions' => array('style' => 'width: 100px;'),
            'filterHtmlOptions' => array('style' => 'max-width: 100px !important;padding-left:0px !important;'),
            'filter' => CHtml::activeDropDownList($followTaskModel,'user_id',$currentUser->getFollowUpList(),array(
                    'style' => 'width: 100px !important;',
                    'class'=>'form-control filter-control reset-dropdown',
                    'name' => 'FollowTask[user_id]',
                    'prompt'=>'All',
                )),
            'visible' => $this->isSupervisorOrAdministrator,
        ),
    ),
)); ?>
