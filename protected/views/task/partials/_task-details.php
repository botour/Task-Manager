<div id="task-details-panel" class="details-panel panel panel-success">
    <div class="panel-heading">
        <!-- Options Container-->
        <div href="#" id="options-container">
            <div class="edit-todo-task-control-panel task-control-panel">
                <?php echo CHtml::link('<i class="fa fa-pencil"></i>', "#", array(
                    'class' => 'action-link edit-task-control edit-task-info-button fa-hover',
                    'id' => 'edit-task-button',
                    'data-toggle' => 'tooltip',
                    'data-placement' => 'bottom',
                    'title' => 'Edit Task Info',
                ));?>

            </div>
            <div class="edit-follow-up-task-control-panel task-control-panel">
                <?php echo CHtml::link('<i class="fa fa-pencil"></i>', "#", array(
                    'class' => 'action-link edit-task-control edit-task-info-button fa-hover',
                    'id' => 'edit-task-button',
                    'data-toggle' => 'tooltip',
                    'data-placement' => 'bottom',
                    'title' => 'Edit Task Info',
                ));?>
                <?php echo CHtml::link('<i class="fa fa-warning"></i>', "#", array(
                    'class' => 'action-link edit-task-control edit-task-priority-button fa-hover',
                    'id' => 'edit-follow-up-task-priority-button',
                    'data-toggle' => 'tooltip',
                    'data-placement' => 'bottom',
                    'title' => 'Edit Task Priority',
                ));?>

            </div>
        </div>
        <!-- End of Options Container-->
        <span class="header-caption" id="header-caption">Task Details</span>

    <div style="height:30px;display: inline;padding-top:9px;" class="pull-right" id="due-to-date-container"><span
           style="vertical-align: bottom;color: #BBB;font-size: 13px;">Due date: <span></span></span></div>
    </div>
    <div class="panel-body">
        <div id="task-prompt">
            <div class="select-prompt">Select a Task to show its details</div>
        </div>
        <div id="task-details" style="display: none;">
            <?php echo CHtml::hiddenField("Task[id]", "", array(
                'id' => 'task-id-edit-field',
            ))?>
            <div href="#" id="title-container">
                <h4 class="task-details-text"></h4>
                <?php echo CHtml::textArea('Task[title]', "", array(
                    'class' => 'form-control edit-task-form-control',
                    'id' => 'task-title-edit-field',
                ))?>
            </div>

            <div href="#" id="description-container" style="min-height: 40px;">
                <p class="task-details-text"></p>
                <p class="task-description-empty-text" style="font-size: 14px;color: #BBB;font-weight: 650;"></p>
                <?php echo CHtml::textArea('Task[description]', "", array(
                    'class' => 'form-control edit-task-form-control',
                    'id' => 'task-description-edit-field',
                ))?>
            </div>
            <div href="#" id="note-container">
                <span style="min-height: 40px;font-size: 12px;font-weight: 500;color: #888;" class="task-details-text"></span>
            </div>

            <hr style="border-color: #888;"/>
            <div id="comments-container" style="height: 230px;max-height: 230px !important;overflow-y: scroll;">
            </div>
            <!-- Add comment form container -->
            <div id="add-comments-form-container" class="row">
                <?php echo CHtml::beginForm(array('task/addComment'), 'post', array(
                    'role' => 'form',
                    'class' => 'form-horizontal',
                    'id' => 'create-comment-form',
                ))?>
                <?php echo CHtml::hiddenField('task_id', "Borhan", array(
                    'id' => 'comment_form_task_id',
                ));?>
                <div class="col-md-12">
                    <div class="form-group has-feedback">
                        <div class="col-md-12 input-container">
                            <?php echo CHtml::textArea('comment_body', '', array(
                                'class' => 'form-control',
                                'id' => 'comment-body',
                            ));?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <?php echo CHtml::ajaxSubmitButton('Add A Comment', $this->createUrl('task/addComment'), array(
                                'data' => 'js:$("#create-comment-form").serialize()',
                                'dataType' => 'json',
                                'beforeSend' => 'js:function(){
                                                $("#comment-submit-btn").attr("disabled",true);
                                                $("#comment-body").val("");
                                            }',
                                'success' => 'js:function(data){
                                                if(data.status==true){
                                                    updateCommentsList(data.taskId);
                                                }else{
                                                    $("#comment-body").notify(data.message, "info");
                                                }
                                                $("#comment-submit-btn").attr("disabled",false);
                                            }',
                                'error' => 'js:function(){
                                    $("#comment-body").notify(data.message, "info");
                                }',
                            ), array(
                                'class' => 'form-submit-button',
                                'id' => 'comment-submit-btn'
                            ));?>
                        </div>
                    </div>

                </div>
                <?php echo CHtml::endForm(); ?>

            </div>
        </div>
    </div>

</div>
