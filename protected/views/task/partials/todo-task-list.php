<?php

CommonFunctions::fixAjax();
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'task-grid',
    'rowHtmlOptionsExpression' => 'array("class"=>$data->getTaskClassNames()." todo-task-row",
                                            "task-id"=>$data->id,
                                            "task-title"=>$data->title,
                                            "task-description"=>$data->description,
                                            "task-note"=>$data->note,
                                            "task-reminded"=>$data->reminded,
                                            "task-serial-no"=>$data->serial_no,
                                            "task-due-to-date"=>date("d-m-Y",strtotime($data->due_to_date)),
                                    )',
    'summaryText' => '{count}',
    'summaryCssClass'=>'todo-summery',
    'emptyText' => 'No Tasks Available',
    'beforeAjaxUpdate'=>'js:function(){
        $("#loading-indicator").show();
    }',
    'afterAjaxUpdate' => 'js:function(){
                        $("#loading-indicator").hide();
                        updateCheckBoxColumnForToDoList();
                        updateSelectRow();
                        //$("#task-grid table").tablesorter();
                        $(".todo-summery-container").html($(".todo-summery").html());
                        $(".todo-summery").hide();
                        updateChevron();
                        $(".Tabled table").addClass("table");
                        $(".Tabled table").addClass("table-condensed");
                        $("#datepicker-container input").datepicker({
                                format: "yyyy-mm-dd",
                                weekStart: 6,
                                startDate: "2014-11-4",
                                endDate: "' . CJavaScript::encode(date("Y-m-d")) . '",
                                todayBtn: true,
                                daysOfWeekDisabled: "5",
                                autoclose: true,
                                todayHighlight: true
                        });
                   }',
    'htmlOptions' => array(
        'class' => 'Tabled',
    ),
    'cssFile' => Yii::app()->baseUrl . '/css/main.css',
    'dataProvider' => $taskModel->search($forceDefaultToDoTasksOrder),
    'filter' => $taskModel,
    'columns' => array(
        array(
            'header' => 'Actions',
            'type' => 'raw',
            'value' => array($this, 'renderToDoTaskActionsColumn'),
            'filterHtmlOptions' => array(
                'style' => 'width: 70px;min-width:70px;max-width:70px;',
                'class' => 'complete-action-header',
            ),
        ),

        array(
            'header' => 'from',
            'name'=>'fromUsername',
            'value' => 'User::model()->findByPk($data->create_id)->username',
            'htmlOptions' => array('style' => 'width: 50px;', 'class' => 'user_id_cell'),
            'filterHtmlOptions' => array('style' => 'width: 50px !important;padding-left:0px !important;'),
            'filter' => CHtml::activeTextField($taskModel,'createUsername', array(
                    'style' => 'width: 46px !important;',
                    'class'=>'form-control filter-control',
            )),

        ),
        array(
            'header' => 'Title',
            'type'=>'html',
            'name'=>'title',
            'sortable'=>true,
            'value'=>'$data->getTitleForGrid()',
            'htmlOptions' => array('style' => 'white-space: nowrap;max-width: 450px;width: 450px;text-overflow :ellipsis;overflow:hidden;', 'class' => 'title_cell'),
            'filterHtmlOptions' => array('style' => 'width: 450px !important;padding-left:0px !important;'),
            'filter' => CHtml::activeTextField($taskModel, "title", array(
                    'style' => 'width: 450px !important;',
                    'class'=>'form-control filter-control',
                )),
        ),

        array(
            'header'=>'Pri',
            'name'=>'priority',
            'value' => 'CommonFunctions::getLabel($data->priority,CommonFunctions::TASK_PRIORITY)',
            'type' => 'html',
            //'htmlOptions' => array('style' => 'max-width: 30px;'),
            'filterHtmlOptions' => array('style' => 'width: 50px !important;padding-left:0px !important;'),
            'filter' => CHtml::activeDropDownList($taskModel,'priority',Task::getPriorityOptionsArray(),array(
                    'style' => 'max-width: 50px !important;width: 50px !important;',
                    'prompt'=>'All',
                    'class'=>'form-control filter-control',
                )),
        ),
        /*
        array(
            'name' => 'user_id',
            'value' => '$data->user->getFullName();',
            'htmlOptions' => array('style' => 'max-width: 175px;'),
            'filterHtmlOptions' => array('style' => 'max-width: 175px !important;'),
            'filter' => CHtml::activeDropDownList($taskModel,'user_id',User::model()->getUserList(),array(
                    'style' => 'max-width: 175px !important;',
                    'prompt'=>'Select a mamber',
                )),
            'visible' => 'User::model()->isAdmin(Yii::app()->user->id)',
        ),

        array(
            'name' => 'project_id',
            'value' => '$data->project->name',
            'htmlOptions' => array('style' => 'max-width: 175px;'),
            'filterHtmlOptions' => array('style' => 'max-width: 175px !important;'),
            'filter' => CHtml::activeDropDownList($taskModel,'project_id',Project::model()->getProjectListForSearch(),array(
                    'style' => 'max-width: 175px !important;',
                    'prompt'=>'Select a Workspace',
                )),
        ),
        */
        array(
            'header'=>'D',
            'name'=>'create_date',
            'value'=>'floor((time()-strtotime($data->create_date))/(3600*24))',
            'htmlOptions' => array('style' => 'max-width: 50px;'),
            'filterHtmlOptions' => array('style' => 'max-width: 50px !important;padding-left:0px !important;'),
            'filter' => CHtml::activeTextField($taskModel, 'daysNo', array(
                        'style' => 'max-width: 25px !important;width: 25px !important;',
                        'class'=>'form-control filter-control',
                    )
             ),
        ),
        array(
            'header'=>'Tag',
            'name'=>'tag',
            'value'=>'$data->tag',
            'htmlOptions' => array('style' => 'width: 260px;', 'class' => 'tag_cell'),
            'filterHtmlOptions' => array('style' => 'padding-left:0px !important;'),
            'filter' => CHtml::activeTextField($taskModel, 'tag', array(
                    'class'=>'form-control filter-control',
            )),
        ),
        /* array(
             'header' => 'Info',
             'class' => 'CButtonColumn',
             'htmlOptions' => array('style' => 'width: 40px;'),
             'template' => '{options}',
             'buttons' => array(
                 'options' => array(
                     'label' => '<span class="glyphicon glyphicon-th-list"></span>',
                     'url' => 'Yii::app()->createUrl("task/getTaskInfo",array("task_id"=>$data->id))',
                     'options' => array(
                         'class' => 'btn btn-info btn-sm',
                         'ajax' => array(
                             'url' => 'js:$(this).attr("href")',
                             'type' => 'GET',
                             'success' => 'js:function(data){
                                     $("#showTaskInfoModal .modal-body").html(data);
                                 }',
                             'beforeSend' => 'js:function(){
                                     $("#showTaskInfoModal .modal-body").html("Please wait...");
                                     $("#showTaskInfoModal").modal("show");
                                 }'
                         ),
                     ),
                 )
             )
         ),*/
        /* array(
            'header' => 'Options',
            'class' => 'CButtonColumn',
            'htmlOptions' => array('style' => 'width: 40px;'),
            'template' => '{options}',
            'buttons' => array(
                'options' => array(
                    'label' => '<span class="glyphicon glyphicon-pencil"></span>',
                    'url' => 'Yii::app()->createUrl("task/getTaskOptions",array("task_id"=>$data->id))',
                    'options' => array(
                        'class' => 'btn btn-danger btn-sm',
                        'ajax' => array(
                            'url' => 'js:$(this).attr("href")',
                            'type' => 'GET',
                            'success' => 'js:function(data){
                                    $("#showTaskOptions .modal-body").html(data);
                                }',
                            'beforeSend' => 'js:function(){
                                    $("#showTaskOptions .modal-body").html("Please wait...");
                                    $("#showTaskOptions").modal("show");
                                }'

                        ),
                    ),
                )
            )
        ),*/
    ),
)); ?>
