<div style="display: inline"><?php echo CHtml::link('<i class="fa fa-mail-reply" style="font-size: 16px;"></i>  ','#',array('task-id'=>$task->id,'class'=>'reassign-link action-link'));?><span> </span>
    <?php echo CHtml::link('<i class="fa fa-bell"></i>', "#", array(
        'class' => 'action-link task-remind-button tooltip-link',
        'id' => 'add-follow-up-task-reminder-button',
        'data-toggle' => 'tooltip',
        'data-placement' => 'bottom',
        'task-id'=>$task->id,
        'style'=>$task->reminded==Task::TASK_NOT_REMINDED?"display: inline;":"display: none;",
        'title' => 'Add a Reminder',
    ));?>

    <?php echo CHtml::link('<i class="fa fa-bell-slash"></i>', "#", array(
        'class' => 'action-link task-remind-button tooltip-link',
        'id' => 'remove-follow-up-task-reminder-button',
        'data-toggle' => 'tooltip',
        'data-placement' => 'bottom',
        'task-id'=>$task->id,
        'style'=>$task->reminded==Task::TASK_REMINDED?"display: inline;":"display: none;",
        'title' => 'Remove a Reminder',
    ));?>
</div>


