<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'complete-task-grid',
    'rowHtmlOptionsExpression' => 'array("class"=>$data->getTaskClassNames()." complete-task-row",
                                            "task-id"=>$data->id,
                                            "task-title"=>$data->title,
                                            "task-description"=>$data->description,
                                            "task-note"=>$data->note,
                                            "task-reminded"=>$data->reminded,
                                            "task-due-to-date"=>date("d-m-Y",strtotime($data->due_to_date)),
                                            "task-serial-no"=>$data->serial_no,
    )',
    'summaryText' => '{count}',
    'summaryCssClass' => 'complete-summery',
    'beforeAjaxUpdate'=>'js:function(){
        $("#loading-indicator").show();
    }',
    'afterAjaxUpdate' => 'js:function(){
                        updateCheckBoxColumnForToDoList();
                        updateSelectRow();
                        $("#loading-indicator").hide();
                        updateTaskAssignmentLinks();
                        updateCloseTaskButton();
                        updateChevron();
                        $("#complete-task-grid .reset-dropdown").val("");
                        //$("#complete-task-grid table").tablesorter();
                        $(".complete-summery-container").html($(".complete-summery").html());
                        $(".complete-summery").hide();
                        $(".Tabled table").addClass("table");
                        $(".Tabled table").addClass("table-condensed");

                   }',
    'htmlOptions' => array(
        'class' => 'Tabled',
    ),
    'cssFile' => Yii::app()->baseUrl . '/css/main.css',
    'dataProvider' => $completeTaskList->completeTaskSearch($forceDefaultCompleteTasksOrder),
    'filter' => $completeTaskList,
    'columns' => array(
        array(
            'header' => 'Actions',
            'type' => 'raw',
            'value' => array($this, 'renderCompleteTaskActionsColumn'),
            'filter' => $this->renderEndTaskDoneButton(),
            'filterHtmlOptions' => array(
                'style' => 'width: 70px;min-width:70px;max-width:70px;',
                'class' => 'complete-action-header',
            ),
            'htmlOptions' => array('style' => 'max-width: 150px;'),
        ),
        array(
            'header' => 'from',
            'name'=>'fromUsername',
            'value' => 'User::model()->findByPk($data->create_id)->username',
            'htmlOptions' => array('style' => 'width: 50px;', 'class' => 'user_id_cell'),
            'filterHtmlOptions' => array('style' => 'width: 50px !important;padding-left:0px !important;'),
            'filter' => CHtml::activeTextField($completeTaskList, 'createUsername', array(
                    'style' => 'width: 46px !important;',
                    'class' => 'form-control filter-control',
                    'name' => 'CompleteTask[createUsername]',
                )),
        ),

        array(
            'header' => 'Title',
            'name'=>'title',
            'value'=>'$data->title',
            'htmlOptions' => array('style' => 'white-space: nowrap;max-width: 450px;width: 450px;text-overflow :ellipsis;overflow:hidden;', 'class' => 'title_cell'),
            'filterHtmlOptions' => array('style' => 'width: 450px !important;padding-left:0px !important;'),
            'filter' => CHtml::activeTextField($completeTaskList, "title", array(
                    'style' => 'width: 450px !important;',
                    'class'=>'form-control filter-control',
                    'name' => 'CompleteTask[title]',
                )),
        ),

        array(
            'header'=>'Pri',
            'name'=>'priority',
            'value' => 'CommonFunctions::getLabel($data->priority,CommonFunctions::TASK_PRIORITY)',
            'type' => 'html',
            //'htmlOptions' => array('style' => 'max-width: 30px;'),
            'filterHtmlOptions' => array('style' => 'width: 50px !important;padding-left:0px !important;'),
            'filter' => CHtml::activeDropDownList($completeTaskList,'priority',Task::getPriorityOptionsArray(),array(
                    'style' => 'max-width: 50px !important;width: 50px !important;',
                    'prompt'=>'All',
                    'class'=>'form-control filter-control',
                    'name' => 'CompleteTask[priority]',
                )),
        ),
        array(
            'header'=>'D',
            'name'=>'create_date',
            'value'=>'floor((time()-strtotime($data->create_date))/(3600*24))',
            'htmlOptions' => array('style' => 'max-width: 10px;'),
            'filterHtmlOptions' => array('style' => 'max-width: 50px !important;padding-left:0px !important;'),
            'filter' => CHtml::activeTextField($completeTaskList, 'daysNo', array(
                        'style' => 'max-width: 25px !important;width: 25px !important;',
                        'class'=>'form-control filter-control',
                        'name' => 'CompleteTask[daysNo]',
                    )
                ),
        ),
        array(
            'header'=>'Tag',
            'name'=>'tag',
            'value'=>'$data->tag',
            'htmlOptions' => array('style' => 'max-width: 170px;', 'class' => 'tag_cell'),
            'filterHtmlOptions' => array('style' => 'padding-left:0px !important;'),
            'filter' => CHtml::activeTextField($completeTaskList, 'tag', array(
                    'style' => 'max-width: 165px !important;',
                    'class'=>'form-control filter-control',
                    'name' => 'CompleteTask[tag]',
                )),
        ),
        array(
            'name' => 'toUsername',
            'header'=>'assign to',
            'value' => '$data->user->username;',
            'htmlOptions' => array('style' => 'width: 100px;'),
            'filterHtmlOptions' => array('style' => 'max-width: 100px !important;padding-left:0px !important;'),
            'filter' => CHtml::activeDropDownList($completeTaskList,'user_id',$currentUser->getFollowUpList(),array(
                    'style' => 'width: 100px !important;',
                    'class'=>'form-control filter-control reset-dropdown',
                    'name' => 'CompleteTask[user_id]',
                    'prompt'=>'All',
                )),
            'visible' => $this->isSupervisorOrAdministrator,
        ),
    ),
)); ?>
