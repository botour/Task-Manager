<?php
/**
 * Created by PhpStorm.
 * User: Dell 3521
 * Date: 02/12/14
 * Time: 09:31 م
 *
 * @var $data Task
 */

?>

<div class="row">
    <div class="col-md-12">
        <div class="alert <?php echo $data->status == Task::STATE_COMPLETE ? "alert-success" : "alert-danger" ?>"
             role="alert">
            <b><?php echo CHtml::encode($data->title); ?></b>
            <small><?php echo CHtml::encode($data->description); ?></small>
        </div>
        <div class="row">
            <div class="col-md-4">
                <b><?php echo CHtml::encode($data->getAttributeLabel('note')); ?>:</b>
            </div>
            <div class="col-md-8">
                <?php echo CHtml::encode($data->note); ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-4">
                <b><?php echo CHtml::encode($data->getAttributeLabel('priority')); ?>:</b>
            </div>
            <div class="col-md-8">
                <?php echo CommonFunctions::getLabel($data->priority, CommonFunctions::TASK_PRIORITY); ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-4">
                <b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
            </div>
            <div class="col-md-8">
                <?php echo CommonFunctions::getLabel($data->status, CommonFunctions::TASK_STATUS); ?>
            </div>
        </div>
        <br/>
        <?php if (User::model()->isAdmin(Yii::app()->user->id)): ?>
            <div class="row">
                <div class="col-md-4">
                    <b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
                </div>
                <div class="col-md-8">
                    <span class="label label-info"><?php echo $data->user->getFullName(); ?></span>
                </div>
            </div>
            <br/>
        <?php endif; ?>

        <div class="row">
            <div class="col-md-4">
                <b><?php echo "Production Details"; ?>:</b>
            </div>
            <div class="col-md-8">
                <?php echo $data->getTaskProductionDetails(); ?>
            </div>
        </div>
        <br/>

    </div>
</div>