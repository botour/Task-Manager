
<?php if(empty($members)):?>
    <option value="">No Users in this workspace</option>
<?php endif;?>
<?php if(!empty($members)):?>
    <option value="">Select A Member</option>
    <?php foreach($members as $id=>$member):?>
        <option value="<?php echo $id?>"><?php echo $member?></option>
    <?php endforeach;?>
<?php endif;?>
