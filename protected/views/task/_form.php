<?php
if(!$model->isNewRecord){
    CommonFunctions::fixAjax();
    Yii::app()->clientscript->scriptMap['jquery.yiiactiveform.js'] = false;
}
Yii::app()->clientScript->registerScript("update_task_form_script", '
$(function(){
    $(".ok-sign").hide();
    $(".error-sign").hide();
});
$(".date-field-for-update").datepicker({
         format: "yyyy-mm-dd",
         weekStart: 6,
         startDate: currentDate,
         todayBtn: true,
         daysOfWeekDisabled: "5",
         autoclose: true,
         todayHighlight: true
     });
', CClientScript::POS_END);
?>
<div class="form" style="margin-top: 20px;">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'task-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'focus' => array($model, 'title'),
        'clientOptions' => array(
            'successCssClass' => 'has-success',
            'errorCssClass' => 'has-error',
            'validatingErrorMessage' => '',
            'inputContainer' => '.form-group',
            'afterValidateAttribute' => 'js:function(form, attribute, data, hasError){
                $("#"+attribute.inputID).siblings(".error-sign").hide();
                $("#"+attribute.inputID).siblings(".ok-sign").hide();
                if(hasError){
                    $("#"+attribute.inputID).siblings(".error-sign").show();
                }else {
                    $("#"+attribute.inputID).siblings(".ok-sign").show();
                }
            }',
        ),
        'htmlOptions' => array(
            'role' => 'form',
            'class' => 'form-horizontal',
        ),
    )); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group has-feedback">
                <?php echo $form->labelEx($model, 'title', array('class' => 'col-md-3 control-label task-control-label')); ?>
                <div class="col-md-9 input-container task-form-input-container">
                    <?php echo $form->textField($model, 'title', array('placeholder' => 'Add the task title ...', 'size' => 60, 'maxlength' => 255, 'class' => 'form-control ')); ?>
                    <span class="glyphicon glyphicon-remove form-control-feedback error-sign"></span>
                    <span class="glyphicon glyphicon-ok form-control-feedback ok-sign"></span>
                    <?php echo $form->error($model, 'title'); ?>
                </div>
            </div>

            <div class="form-group has-feedback">
                <?php echo $form->labelEx($model, 'description', array('class' => 'col-md-3 control-label task-control-label')); ?>
                <div class="col-md-9 input-container task-form-input-container">
                    <?php echo $form->textArea($model, 'description', array('class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'description'); ?>
                </div>
            </div>
            <?php if (!$model->isNewRecord): ?>
                <?php echo $form->hiddenField($model, 'id') ?>
            <?php endif; ?>

            <?php if ($model->isNewRecord): ?>
                <div class="form-group has-feedback">
                    <?php echo $form->labelEx($model, 'priority', array('class' => 'col-md-3 control-label task-control-label')); ?>
                    <div class="col-md-3 input-container task-form-input-container">
                        <?php echo $form->dropDownList($model, 'priority', Task::getPriorityOptionsArray(), array('class' => 'form-control')); ?>
                        <span class="glyphicon glyphicon-remove form-control-feedback error-sign"></span>
                        <span class="glyphicon glyphicon-ok form-control-feedback ok-sign"></span>
                        <?php echo $form->error($model, 'priority'); ?>
                    </div>
                    <?php echo $form->labelEx($model, 'due_to_date', array('class' => 'col-md-3 control-label task-control-label')); ?>
                    <div class="col-md-3 input-container task-form-input-container" style="padding-right: 10px !important;">
                        <?php echo $form->textField($model, 'due_to_date', array('class' => 'form-control date-field')); ?>
                        <span class="glyphicon glyphicon-remove form-control-feedback error-sign"></span>
                        <span class="glyphicon glyphicon-ok form-control-feedback ok-sign"></span>
                        <?php echo $form->error($model, 'due_to_date'); ?>
                    </div>
                </div>
            <?php endif; ?>
            <?php if(!$model->isNewRecord):?>
            <div class="form-group has-feedback">
                <?php echo $form->labelEx($model, 'due_to_date', array('class' => 'col-md-3 control-label task-control-label')); ?>
                <div class="col-md-9 input-container task-form-input-container">
                    <?php echo $form->textField($model, 'due_to_date', array('class' => 'form-control date-field-for-update')); ?>
                    <span class="glyphicon glyphicon-remove form-control-feedback error-sign"></span>
                    <span class="glyphicon glyphicon-ok form-control-feedback ok-sign"></span>
                    <?php echo $form->error($model, 'due_to_date'); ?>
                </div>
            </div>
            <?php endif;?>
            <div class="form-group has-feedback">
                <?php echo $form->labelEx($model, 'note', array('class' => 'col-md-3 control-label task-control-label')); ?>
                <div class="col-md-9 input-container task-form-input-container">
                    <?php echo $form->textArea($model, 'note', array('class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'note'); ?>
                </div>
            </div>
            <div class="form-group has-feedback">
                <?php echo $form->labelEx($model, 'tag', array('class' => 'col-md-3 control-label task-control-label')); ?>
                <div class="col-md-9 input-container task-form-input-container">
                    <?php echo $form->textField($model, 'tag', array('placeholder' => 'Add the task tag ...', 'size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
                    <span class="glyphicon glyphicon-remove form-control-feedback error-sign"></span>
                    <span class="glyphicon glyphicon-ok form-control-feedback ok-sign"></span>
                    <?php echo $form->error($model, 'tag'); ?>
                </div>
            </div>
            <?php if ($model->isNewRecord): ?>
                <?php if ($currentUser->role == User::ROLE_MEMBER): ?>
                    <div class="form-group">
                        <?php echo $form->label($model, 'project_id', array('class' => 'col-md-3 control-label task-control-label', 'for' => 'name')); ?>
                        <div class="col-md-9 input-container task-form-input-container">
                            <?php echo $form->dropDownList($model, 'project_id', Project::model()->getProjectList(User::ROLE_MEMBER, $currentUser), array(
                                'class' => 'form-control',
                                'prompt' => 'Select A Workspace',
                            )) ?>
                            <span class="glyphicon glyphicon-remove form-control-feedback error-sign"></span>
                            <span class="glyphicon glyphicon-ok form-control-feedback ok-sign"></span>
                            <?php echo $form->error($model, 'project_id'); ?>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if ($currentUser->role == User::ROLE_ADMIN): ?>
                    <div class="form-group">
                        <?php echo $form->label($model, 'project_id', array('class' => 'col-md-3 control-label task-control-label', 'for' => 'name')); ?>
                        <div class="col-md-9 input-container task-form-input-container">
                            <?php echo $form->dropDownList($model, 'project_id', Project::model()->getProjectList(User::ROLE_ADMIN, $currentUser), array(
                                'id' => uniqid(),
                                'prompt' => 'Select A Workspace',
                                'class' => 'form-control',
                                'ajax' => array(
                                    'type' => 'GET',
                                    'url' => Yii::app()->createUrl('task/getMemberList'),
                                    'data' => 'js:{project_id:$(this).val()}',
                                    'update' => '#user_id',
                                ),
                            )); ?>
                            <span class="glyphicon glyphicon-remove form-control-feedback error-sign"></span>
                            <span class="glyphicon glyphicon-ok form-control-feedback ok-sign"></span>
                            <?php echo $form->error($model, 'project_id'); ?>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if ($currentUser->role == User::ROLE_PROJECT_SUPERVISOR): ?>
                    <div class="form-group">
                        <?php echo $form->label($model, 'project_id', array('class' => 'col-md-3 control-label task-control-label', 'for' => 'name')); ?>
                        <div class="col-md-9 input-container task-form-input-container">
                            <?php echo $form->dropDownList($model, 'project_id', Project::model()->getProjectList(User::ROLE_PROJECT_SUPERVISOR, $currentUser), array(
                                'id' => uniqid(),
                                'prompt' => 'All Workspaces',
                                'class' => 'form-control',
                                'ajax' => array(
                                    'type' => 'GET',
                                    'url' => Yii::app()->createUrl('task/getMemberList'),
                                    'data' => 'js:{project_id:$(this).val()}',
                                    'update' => '#user_id',
                                ),
                            )); ?>
                            <span class="glyphicon glyphicon-remove form-control-feedback error-sign"></span>
                            <span class="glyphicon glyphicon-ok form-control-feedback ok-sign"></span>
                            <?php echo $form->error($model, 'project_id'); ?>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if ($currentUser->role == User::ROLE_ADMIN || $currentUser->role == User::ROLE_PROJECT_SUPERVISOR): ?>
                    <div class="form-group has-feedback">
                        <?php echo $form->labelEx($model, 'user_id', array('class' => 'col-md-3 control-label task-control-label')); ?>
                        <div class="col-md-9 input-container task-form-input-container">
                            <?php echo $form->dropDownList($model, 'user_id', array(), array('id' => 'user_id', 'prompt' => 'Select Workspace to Display Users', 'class' => 'form-control select-user-dropdown')); ?>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
            <div class="form-group">
                <div class="col-md-offset-3 col-md-9" style="padding-left: 0px;margin-left: 191px;">
                    <?php if ($model->isNewRecord): ?>
                        <?php echo CHtml::ajaxSubmitButton('Send Task', $this->createUrl('create'), array(
                            'type' => "POST",
                            'dataType' => 'json',
                            'success' => 'js:function(data){
                               $(".save-task-btn").button("reset");
                               if(data.status){
                                    showNotificationMessage("success",data.message);
                                    $("#task-grid").yiiGridView("update");
                                    $("#follow-up-task-grid").yiiGridView("update");
                               }else{
                                    showNotificationMessage("error",data.message);
                               }
                               createTaskFormReset();
                        }',
                        'beforeSend' => 'js:function(){
                            $(".save-task-btn").button("loading");
                        }'
                        ), array(
                            'class' => 'btn btn-danger save-task-btn',
                            'data-loading-text' => "Saving...",
                            'id'=>'new-task-submit-btn',
                        ))?>
                    <?php endif; ?>
                    <?php if (!$model->isNewRecord): ?>
                        <?php echo CHtml::ajaxSubmitButton('Update Task Info', $this->createUrl('editTaskInfo'), array(
                            'type' => "POST",
                            'dataType' => 'json',
                            'success' => 'js:function(data){
                               $("#statusModal .modal-body").html(data.message);
                               $("#statusModal").modal("show");
                               setTimeout(function(){
                                    $("#statusModal").modal("hide");
                               },2000);
                               if(data.status){
                                   $row = $(".selected-for-update");
                                   if($row.hasClass("todo-task-row")){
                                        $("#task-grid").yiiGridView("update");
                                   }
                                   if($row.hasClass("follow-up-task-row")){
                                        $("#follow-up-task-grid").yiiGridView("update");
                                   }
                                   $(".selected-for-update").removeClass("selected-for-update");
                                   var taskObject = {
                                        "task-title":data.title,
                                        "task-description":data.description,
                                        "task-id":data.id,
                                        "task-note":data.note,
                                   };

                                   if(data["dueToDate"]){
                                        taskObject["due-to-date"] = data.dueToDate;
                                   }
                                   updateTaskDetailsPanel(taskObject);
                                   updateCommentsList(data.id);
                               }
                            }',
                            'beforeSend' => 'js:function(){
                        $("#editTaskInfoModal").modal("hide");
                    }'
                        ), array(
                            'class' => 'btn btn-danger',
                            'id' => 'link-' . uniqid(),
                        ))?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- form -->