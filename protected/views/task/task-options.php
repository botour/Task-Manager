<?php
/**
 * Created by PhpStorm.
 * User: Dell 3521
 * Date: 02/12/14
 * Time: 09:41 م
 * @var $taskModel Task
 */
CommonFunctions::fixAjax();
?>
<?php if (User::model()->isAdmin(Yii::app()->user->id)): ?>

    <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <?php echo CHtml::ajaxButton("Delete Task", $this->createUrl('delete'), array(
                    'type' => 'POST',
                    'dataType' => 'json',
                    'data' => array(
                        'id' => $taskModel->id,
                    ),
                    'success' => 'js:function(data){
                        $("#statusModal .modal-body").html(data.message);
                        $("#statusModal").modal("show");
                        $("#task-grid").yiiGridView("update");
                    }',
                    'beforeSend'=>'js:function(){

                        $("#showTaskOptions").modal("hide");
                    }'
                ), array(
                    'class' => 'btn btn-danger btn-block btn-sm',
                    'id'=>'link-'.uniqid(),
                    'confirm' => 'Are you sure you want to delete the Task?',
                ));?>
            </div>
    </div>
    <br>
<?php endif; ?>

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <?php echo CHtml::ajaxButton('Change Task state', $this->createUrl('getStateChangeForm', array('id' => $taskModel->id)), array(
                'type' => 'GET',

                'success' => 'js:function(data){
            $("#changeTaskStateModal .modal-body").html(data);
        }',
                'beforeSend' => 'js:function(){
            $("#changeTaskStateModal .modal-body").html("Please Wait...");
            $("#changeTaskStateModal").modal("show");
            $("#showTaskOptions").modal("hide");
        }'
            ), array(
                'class' => 'btn btn-success btn-block btn-sm',
                'id'=>'link-'.uniqid(),
            ))?>
        </div>
    </div>
<br>
<?php if (User::model()->isAdmin(Yii::app()->user->id)): ?>

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <?php echo CHtml::ajaxButton('Change Task Priority Level', $this->createUrl('getPriorityLevelChangeForm', array('id' => $taskModel->id)), array(
                'type' => 'GET',

                'success' => 'js:function(data){
            $("#changeTaskPriorityModal .modal-body").html(data);
        }',
                'beforeSend' => 'js:function(){
                $("#showTaskOptions").modal("hide");
            $("#changeTaskPriorityModal .modal-body").html("Please Wait...");
            $("#changeTaskPriorityModal").modal("show");
        }'
            ), array(
                'class' => 'btn btn-success btn-block btn-sm',
                'id'=>'link-'.uniqid(),
            ))?>
        </div>
    </div>
    <br>
<?php endif; ?>

<?php if (User::model()->isAdmin(Yii::app()->user->id)): ?>

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <?php echo CHtml::ajaxButton('Change Task Assignment', $this->createUrl('getChangeTaskAssignment', array('id' => $taskModel->id)), array(
                'type' => 'GET',

                'success' => 'js:function(data){
            $("#taskAssignmentModal .modal-body").html(data);
        }',
                'beforeSend' => 'js:function(){
                $("#showTaskOptions").modal("hide");
            $("#taskAssignmentModal .modal-body").html("Please Wait...");
            $("#taskAssignmentModal").modal("show");
        }'
            ), array(
                'class' => 'btn btn-info btn-block btn-sm',
                'id'=>'link-'.uniqid(),
            ))?>
        </div>
    </div>
    <br>
<?php endif; ?>