<?php
/**
 * Created by PhpStorm.
 * User: Dell 3521
 * Date: 26/11/14
 * Time: 08:52 ص
 */

Yii::app()->clientScript->registerScript("course_script", '
$(function(){
    $(".ok-sign").hide();
    $(".error-sign").hide();
});
', CClientScript::POS_END);
CommonFunctions::fixAjax();
?>
<div class="form">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'task-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'focus' => array($task, 'priority'),
        'clientOptions' => array(
            'successCssClass' => 'has-success',
            'errorCssClass' => 'has-error',
            'validatingErrorMessage' => '',
            'inputContainer' => '.form-group',
            'afterValidateAttribute' => 'js:function(form, attribute, data, hasError){
                $("#"+attribute.inputID).siblings(".error-sign").hide();
                $("#"+attribute.inputID).siblings(".ok-sign").hide();
                if(hasError){
                    $("#"+attribute.inputID).siblings(".error-sign").show();
                }else {
                    $("#"+attribute.inputID).siblings(".ok-sign").show();
                }
            }',
        ),
        'htmlOptions' => array(
            'role' => 'form',
            'class' => 'form-horizontal',
        ),
    )); ?>

    <div class="row">
        <div class="col-md-12">
            <?php echo $form->hiddenField($task,'id');?>
            <div class="form-group has-feedback">
                <?php echo $form->labelEx($task, 'priority', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-9 input-container">
                    <?php echo $form->dropDownList($task, 'priority',Task::getPriorityOptionsArray(), array('class' => 'form-control')); ?>
                    <span class="glyphicon glyphicon-remove form-control-feedback error-sign"></span>
                    <span class="glyphicon glyphicon-ok form-control-feedback ok-sign"></span>
                    <?php echo $form->error($task, 'priority'); ?>
                </div>
            </div>

            <div class="col-md-offset-3 col-md-9">
                <?php echo CHtml::ajaxSubmitButton('Change Priority',$this->createUrl('changePriority'),array(
                    'type'=>"POST",
                    'dataType'=>'json',
                    'success'=>'js:function(data){
                       $("#statusModal .modal-body").html(data.message);
                       $("#statusModal").modal("show");
                       /*$("#task-grid").yiiGridView("update", {
                                data: $(".search-tasks form").serialize()
                       });*/
                       $("#task-grid").yiiGridView("update");
                       $("#follow-up-task-grid").yiiGridView("update");
                       updateCommentsList($("#task-id-edit-field").val());
                    }',
                    'beforeSend'=>'js:function(){
                        $("#changeTaskPriorityModal").modal("hide");
                    }'
                ),array(
                    'class'=>'btn btn-danger',
                    'id'=>'link-'.uniqid(),
                ))?>
            </div>
        </div>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- form -->