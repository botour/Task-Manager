<?php
/* @var $this TaskController */
/* @var $data Task */
?>
<li class="list-group-item">
    <div class="row">
        <div class="col-md-9">
            <div class="alert <?php echo $data->status == Task::STATE_COMPLETE ? "alert-success" : "alert-danger" ?>"
                 role="alert">
                <b><?php echo CHtml::encode($data->title); ?></b>
                <small><?php echo CHtml::encode($data->description); ?></small>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <b><?php echo CHtml::encode($data->getAttributeLabel('note')); ?>:</b>
                </div>
                <div class="col-md-8">
                    <?php echo CHtml::encode($data->note); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <b><?php echo CHtml::encode($data->getAttributeLabel('priority')); ?>:</b>
                </div>
                <div class="col-md-8">
                    <?php echo CommonFunctions::getLabel($data->priority, CommonFunctions::TASK_PRIORITY); ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
                </div>
                <div class="col-md-8">
                    <?php echo CommonFunctions::getLabel($data->status, CommonFunctions::TASK_STATUS); ?>
                </div>
            </div>
            <?php if (User::model()->isAdmin(Yii::app()->user->id)): ?>
                <div class="row">
                    <div class="col-md-4">
                        <b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
                    </div>
                    <div class="col-md-8">
                        <span class="label label-info"><?php echo $data->user->getFullName(); ?></span>
                    </div>
                </div>
            <?php endif; ?>

            <div class="row">
                <div class="col-md-4">
                    <b><?php echo "Production Details"; ?>:</b>
                </div>
                <div class="col-md-8">
                    <?php echo $data->getTaskProductionDetails(); ?>
                </div>
            </div>


        </div>
        <div class="col-md-3">
            <?php if (User::model()->isAdmin(Yii::app()->user->id)): ?>
                <?php echo CHtml::ajaxButton("Delete Task", $this->createUrl('delete'), array(
                    'type' => 'POST',
                    'dataType' => 'json',
                    'data' => array(
                        'id' => $data->id,
                    ),
                    'success' => 'js:function(data){
                $("#statusModal .modal-body").html(data.message);
                $("#statusModal").modal("show");
                $.fn.yiiListView.update("task-list",{});
            }',
                ), array(
                    'class' => 'btn btn-danger btn-sm btn-block',

                    'confirm' => 'Are you sure you want to delete the Task?',
                ));?>
            <?php endif; ?>

            <?php echo CHtml::ajaxButton('Change Task state', $this->createUrl('getStateChangeForm', array('id' => $data->id)), array(
                'type' => 'GET',
                'success' => 'js:function(data){
            $("#changeTaskStateModal .modal-body").html(data);
        }',
                'beforeSend' => 'js:function(){
            $("#changeTaskStateModal .modal-body").html("Please Wait...");
            $("#changeTaskStateModal").modal("show");
        }'
            ), array(
                'class' => 'btn btn-success btn-sm btn-block',

            ))?>
            <?php if (User::model()->isAdmin(Yii::app()->user->id)): ?>

                <?php echo CHtml::ajaxButton('Change Task Priority Level', $this->createUrl('getPriorityLevelChangeForm', array('id' => $data->id)), array(
                    'type' => 'GET',
                    'success' => 'js:function(data){
            $("#changeTaskPriorityModal .modal-body").html(data);
        }',
                    'beforeSend' => 'js:function(){
            $("#changeTaskPriorityModal .modal-body").html("Please Wait...");
            $("#changeTaskPriorityModal").modal("show");
        }'
                ), array(
                    'class' => 'btn btn-success btn-sm btn-block',

                ))?>
            <?php endif; ?>

            <br/>
        </div>
    </div>
</li>
