<?php
/* @var $this SettingController */
/* @var $setting Setting */


Yii::app()->clientScript->registerScript("course_script", '
$(function(){
    $(".ok-sign").hide();
    $(".error-sign").hide();
});
', CClientScript::POS_END);
?>

<h4>Application Settings</h4>
<hr/>
<div class="row">
    <div class="col-md-6">
        <div class="form">
            <?php $form = $this->beginWidget('CActiveForm', array(
                'id' => 'setting-form',
                // Please note: When you enable ajax validation, make sure the corresponding
                // controller action is handling ajax validation correctly.
                // There is a call to performAjaxValidation() commented in generated controller code.
                // See class documentation of CActiveForm for details on this.
                'enableAjaxValidation' => false,
                'enableClientValidation' => true,
                'focus' => array($setting, 'value'),
                'clientOptions' => array(
                    'successCssClass' => 'has-success',
                    'errorCssClass' => 'has-error',
                    'validatingErrorMessage' => '',
                    'inputContainer' => '.form-group',
                    'afterValidateAttribute' => 'js:function(form, attribute, data, hasError){
                $("#"+attribute.inputID).siblings(".error-sign").hide();
                $("#"+attribute.inputID).siblings(".ok-sign").hide();
                if(hasError){
                    $("#"+attribute.inputID).siblings(".error-sign").show();
                }else {
                    $("#"+attribute.inputID).siblings(".ok-sign").show();
                }
            }',
                ),
                'htmlOptions' => array(
                    'role' => 'form',
                    'class' => 'form-horizontal',
                ),
            )); ?>
            <div class="form-group has-feedback">
                <?php echo CHtml::label('Application Name', '', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-9 input-container">
                    <?php echo $form->textField($setting, 'value', array('placeholder' => 'Add the task title ...', 'size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
                    <span class="glyphicon glyphicon-remove form-control-feedback error-sign"></span>
                    <span class="glyphicon glyphicon-ok form-control-feedback ok-sign"></span>
                    <?php echo $form->error($setting, 'value'); ?>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-3 col-md-offset-3">
                    <?php echo CHtml::submitButton('Save Setting', array(
                        'class' => 'btn btn-success btn-sm',
                    ));?>
                </div>
            </div>

            <?php $this->endWidget(); ?>
        </div>
        <!-- form -->

    </div>
</div>