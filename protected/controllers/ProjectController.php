<?php

class ProjectController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'checkAccess',
            'adminAccess -report,getReportData',
            'viewReportAccess +report,getReportData',
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = "Control Panel - Workspace: ";
        $projectModel = $this->loadModel($id);
        $userDataProvider = new CActiveDataProvider('User');
        $users = $projectModel->members;

        foreach ($users as $user) {
            $user->projectId = $projectModel->id;
        }

        $userDataProvider->setData($users);
        $tasks = $projectModel->tasks;
        $incompleteTaskCount = 0;
        $completeTaskCount = 0;
        $archivedTaskCount = 0;
        $notReadTaskCount = 0;

        foreach($tasks as $task){
            switch($task->status){
                case Task::STATE_NOT_READ:
                    $notReadTaskCount +=1;
                    break;
                case Task::STATE_INCOMPLETE:
                    $incompleteTaskCount +=1;
                    break;
                case Task::STATE_COMPLETE:
                    $completeTaskCount +=1;
                    break;
                case Task::STATE_VERIFIED:
                    $archivedTaskCount +=1;
                    break;
            }
        }

        $this->render('view', array(
            'projectModel' => $projectModel,
            'userDataProvider' => $userDataProvider,
            'notReadTaskCount'=>$notReadTaskCount,
            'incompleteTaskCount'=>$incompleteTaskCount,
            'completeTaskCount'=>$completeTaskCount,
            'archivedTaskCount'=>$archivedTaskCount,
            'taskCount'=>count($projectModel->tasks),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */


    public function actionCreate()
    {
        $model = new Project;
        if (isset($_POST['Project'])) {
            $model->name = $_POST['Project']['name'];
            $model->description = $_POST['Project']['description'];
            $model->note = $_POST['Project']['note'];
            if ($model->save()) {
                if (isset($_POST['user_id'])) {
                    foreach ($_POST['user_id'] as $userId) {
                        $model->addMember($userId);
                    }
                }
                echo CJSON::encode(array(
                    'status' => true,
                    'message' => 'A new Workspace was Added to the workspace\'s list!',
                ));
            } else {
                echo CJSON::encode(array(
                    'status' => false,
                    'message' => "Could not add a new Workspace to the system",
                ));
            }
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id, $ref)
    {
        $model = $this->loadModel($id);
        $userDataProvider = new CActiveDataProvider('User');
        $users = User::model()->findAll();
        foreach ($users as $user) {
            $user->projectId = $model->id;
        }
        $userDataProvider->setData($users);
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Project'])) {
            $model->attributes = $_POST['Project'];
            if ($model->save()) {
                if (isset($_POST['user_id'])) {
                    foreach ($_POST['user_id'] as $userId) {
                        $model->addMember($userId);
                    }
                }
                $this->redirect(array('index'));
            }

        }

        $this->render('update', array(
            'projectModel' => $model,
            'ref' => $ref,
            'userDataProvider' => $userDataProvider,
        ));
    }

    public function actionTest()
    {
        $count = Task::model()->count('YEAR(create_date)=:date',array(
            ':date'=>date('Y'),
        ));
        echo $count;
    }

    public function actionReport()
    {
        $report = new ReportModel;
        $this->render('tasks_report', array(
            'report' => $report,
        ));
    }

    public function actionGetReportData()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $reportModel = new ReportModel;
            $reportModel->attributes = $_GET['ReportModel'];
            if ($reportModel->validate()) {
                echo CJSON::encode(array(
                    'status' => true,
                    'reportDate' => $this->generateReport($reportModel),
                ));
            } else {
                echo CJSON::encode(array(
                    'status' => false,
                ));
            }
        }
    }

    public function generateReport($reportModel)
    {
        $project = Project::model()->with('members')->findByPk($reportModel->project_id);
        $dataPoints = array();
        $title = "";
        $legendText = "";
        switch ($reportModel->report_option) {
            case ReportModel::REPORT_TYPE_CREATE_TASK:
                $title = "New Tasks Report for " . $project->name . " workspace";
                $legendText = "Tasks Count";
                $dataPoints = $this->generateCreateTasksDataPoints($reportModel, $project);
                break;
            case ReportModel::REPORT_TYPE_COMPLETE_TASK:
                $title = "Completed Tasks Report for " . $project->name . " workspace";
                $dataPoints = $this->generateCompletedTasksDataPoints($reportModel, $project);
                $legendText = "Tasks Count";
                break;
            case ReportModel::REPORT_TYPE_END_TASK:
                $title = "Ended Tasks Report for " . $project->name . " workspace";
                $dataPoints = $this->generateEndedTasksDataPoints($reportModel, $project);
                $legendText = "Tasks Count";
                break;
            case ReportModel::REPORT_TYPE_NOT_COMPLETE_TASK:
                $title = "Revision Tasks Report for " . $project->name . " workspace";
                $dataPoints = $this->generateNotCompletedTasksDataPoints($reportModel, $project);
                $legendText = "Tasks Count";
                break;
        }
        if($reportModel->report_option==ReportModel::REPORT_TYPE_NOT_COMPLETE_TASK){
            $reportData = array(
                'title' => array(
                    'text' => $title,
                ),
                'animationEnabled' => true,
                'axisX' => array(
                    'interval' => 1,
                    'gridThickness' => 0,
                    'labelFontSize' => 14,
                    'labelFontStyle' => 'normal',
                    'labelFontWeight' => 'normal',
                    'labelFontFamily' => 'Lucida Sans Unicode',
                ),
                'axisY2' => array(
                    'interlacedColor' => 'rgba(1,77,101,.2)',
                    'gridColor' => 'rgba(1,77,101,.1)',
                ),
                'data' => array(
                    array(
                        'type' => 'bar',
                        'name' => 'companies',
                        'axisYType' => "secondary",
                        'color' => '#014D65',
                        'dataPoints' => $dataPoints[0],
                    ),
                    array(
                        'type' => 'bar',
                        'name' => 'companies',
                        'axisYType' => "secondary",
                        'color' => '#FF0000',
                        'dataPoints' => $dataPoints[1],
                    )
                ),
            );
        }else{
            $reportData = array(
                'title' => array(
                    'text' => $title,
                ),
                'animationEnabled' => true,
                'axisX' => array(
                    'interval' => 1,
                    'gridThickness' => 0,
                    'labelFontSize' => 14,
                    'labelFontStyle' => 'normal',
                    'labelFontWeight' => 'normal',
                    'labelFontFamily' => 'Lucida Sans Unicode',
                ),
                'axisY2' => array(
                    'interlacedColor' => 'rgba(1,77,101,.2)',
                    'gridColor' => 'rgba(1,77,101,.1)',
                ),
                'data' => array(
                    array(
                        'type' => 'bar',
                        'name' => 'companies',
                        'axisYType' => "secondary",
                        'color' => '#014D65',
                        'dataPoints' => $dataPoints,
                    ),
                ),
            );
        }
        return $reportData;
    }

    public function generateCreateTasksDataPoints($reportModel, $project)
    {
        $dataPoints = array();
        $members = $project->members(array(
            'with'=>array(
                'tasks'=>array(
                    'alias'=>'task',
                    'select'=>'count(*) as count',
                    'condition' => '(task.create_date BETWEEN :from_date AND :to_date) AND task.status IN(' . implode(',', array(
                            Task::STATE_COMPLETE,
                            Task::STATE_INCOMPLETE,
                            Task::STATE_NOT_READ,
                        )) . ')',
                    'params' => array(
                        ':from_date' => date('Y-m-d', strtotime($reportModel->from_date)-3600*24),
                        ':to_date' => date('Y-m-d', strtotime($reportModel->to_date)+3600*24),
                    ),
                    'order'=>'count ASC',

                ),
            ),
            'group'=>'username',
            'joinType'=>'LEFT OUTER JOIN',
        ));
        foreach ($members as $member) {
            $dataPoints[] = array(
                'y' => intval($member->tasksCount(array(
                    'condition' => 'project_id=:project_id AND (create_date BETWEEN :from_date AND :to_date) AND status IN(' . implode(',', array(
                            Task::STATE_COMPLETE,
                            Task::STATE_INCOMPLETE,
                            Task::STATE_NOT_READ,
                        )) . ')',
                    'params' => array(
                        ':project_id' => $reportModel->project_id,
                        ':from_date' => date('Y-m-d', strtotime($reportModel->from_date)-3600*24),
                        ':to_date' => date('Y-m-d', strtotime($reportModel->to_date)+3600*24),
                    ),
                ))),
                'label' => $member->username,
            );
        }
        return $dataPoints;
    }

    public function generateCompletedTasksDataPoints($reportModel, $project)
    {
        $dataPoints = array();
        $members = $project->members(array(
            'with'=>array(
                'tasks'=>array(
                    'alias'=>'task',
                    'select'=>'count(*) as count',
                    'condition' => '(task.create_date BETWEEN :from_date AND :to_date) AND task.status IN(' . implode(',', array(

                            Task::STATE_VERIFIED,
                        )) . ')',
                    'params' => array(
                        ':from_date' => date('Y-m-d', strtotime($reportModel->from_date)-3600*24),
                        ':to_date' => date('Y-m-d', strtotime($reportModel->to_date)+3600*24),
                    ),
                    'order'=>'count ASC',
                ),
            ),
            'group'=>'username',
            'joinType'=>'LEFT OUTER JOIN',
        ));
        foreach ($members as $member) {
            $dataPoints[] = array(
                'y' => intval($member->tasksCount(array(
                    'condition' => 'project_id=:project_id AND (create_date BETWEEN :from_date AND :to_date) AND status IN(' . implode(',', array(
                            Task::STATE_VERIFIED
                        )) . ')',
                    'params' => array(
                        ':project_id' => $reportModel->project_id,
                        ':from_date' => date('Y-m-d', strtotime($reportModel->from_date)-3600*24),
                        ':to_date' => date('Y-m-d', strtotime($reportModel->to_date)+3600*24),
                    ),
                ))),
                'label' => $member->username,
            );
        }
        return $dataPoints;
    }

    public function generateEndedTasksDataPoints($reportModel, $project)
    {
        $dataPoints = array();
        $members = $project->members(array(
            'with'=>array(
                'editTasks'=>array(
                    'alias'=>'task',
                    'select'=>'count(*) as count',
                    'condition' => '(task.update_date BETWEEN :from_date AND :to_date) AND task.status IN(' . implode(',', array(
                            Task::STATE_VERIFIED,
                        )) . ')',
                    'params' => array(
                        ':from_date' => date('Y-m-d', strtotime($reportModel->from_date)-3600*24),
                        ':to_date' => date('Y-m-d', strtotime($reportModel->to_date)+3600*24),

                    ),
                    'order'=>'count ASC',

                ),
            ),
            'group'=>'username',
            'joinType'=>'LEFT OUTER JOIN',
        ));
        foreach ($members as $member) {
            $dataPoints[] = array(
                'y' => count(Task::model()->findAll('update_id=:update_id AND project_id=:project_id AND (update_date BETWEEN :from_date AND :to_date) AND status IN(' . implode(',', array(
                        Task::STATE_VERIFIED
                    )) . ')', array(
                        ':project_id' => $reportModel->project_id,
                        ':from_date' => date('Y-m-d', strtotime($reportModel->from_date)-3600*24),
                        ':to_date' => date('Y-m-d', strtotime($reportModel->to_date)+3600*24),
                        ':update_id' => $member->id,
                    ))),
                'label' => $member->username,
            );
        }
        return $dataPoints;
    }

    public function generateNotCompletedTasksDataPoints($reportModel, $project){
        $dataPoints1 = array();
        $dataPoints2 = array();
        $members = $project->members(array(
            'with'=>array(
                'tasks'=>array(
                    'alias'=>'task',
                    'select'=>'count(*) as count',
                    'condition' => '(task.create_date BETWEEN :from_date AND :to_date) AND task.status IN(' . implode(',', array(
                            Task::STATE_COMPLETE,
                            Task::STATE_INCOMPLETE,
                            Task::STATE_NOT_READ,
                        )) . ')',
                    'params' => array(
                        ':from_date' => date('Y-m-d', strtotime($reportModel->from_date)-3600*24),
                        ':to_date' => date('Y-m-d', strtotime($reportModel->to_date)+3600*24),
                    ),
                    'order'=>'count ASC',

                ),
            ),
            'group'=>'username',
            'joinType'=>'LEFT OUTER JOIN',
        ));
        $data = array();
        foreach ($members as $member) {

            $returnCount = 0;
            $tasks = $member->tasks(array(
                'condition' => 'project_id=:project_id AND (create_date BETWEEN :from_date AND :to_date) AND status IN(' . implode(',', array(
                        Task::STATE_COMPLETE,
                        Task::STATE_INCOMPLETE,
                        Task::STATE_NOT_READ,
                    )) . ')',
                'params' => array(
                    ':project_id' => $reportModel->project_id,
                    ':from_date' => date('Y-m-d', strtotime($reportModel->from_date)-3600*24),
                    ':to_date' => date('Y-m-d', strtotime($reportModel->to_date)+3600*24),
                ),
            ));
            $dataPoints1[] = array(
                'y' => intval(count($tasks)),
                'label' => $member->username,
            );
            foreach($tasks as $task){
                $returnCount += $task->not_complete_return_count;
            }
            $dataPoints2[] = array(
                'y' => $returnCount,
                'label' => $member->username,
            );
        }
        $data[] = $dataPoints1;
        $data[] = $dataPoints2;
        return $data;
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */

    public function actionDelete($id)
    {
        if (Yii::app()->request->isAjaxRequest) {
            if (isset($_GET['id'])) {
                $project = $this->loadModel(intval($_GET['id']));
                if ($project->isDeletable()) {
                    $projectTasks = $project->tasks;
                    foreach ($projectTasks as $task) {
                        $comments = $task->comments;
                        $supervisorNotifications = $task->supervisorNotifications;
                        foreach ($comments as $comment) {
                            $comment->delete();
                        }
                        foreach ($supervisorNotifications as $notification) {
                            $notification->delete();
                        }
                        $task->delete();
                    }
                    Yii::app()->db->createCommand()->delete('tbl_project_user_assignment', 'project_id=:project_id', array(
                        ':project_id' => $project->id,
                    ));
                    if ($project->delete()) {
                        echo CJSON::encode(array(
                            'status' => true,
                            'message' => "The Workspace was Deleted",
                        ));
                    } else {
                        echo CJSON::encode(array(
                            'status' => false,
                            'message' => "Problem when deleting the Workspace",
                        ));
                    }
                } else {
                    echo CJSON::encode(array(
                        'status' => false,
                        'message' => "The Workspace Couldn\'t be deleted",
                    ));
                }
            } else {
                throw new CHttpException(400);
            }
        } else {
            throw new CHttpException(403);
        }


        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }


    public function actionDeleteFromProject($userId, $projectId)
    {
        if (Yii::app()->request->isAjaxRequest) {
            $user = User::model()->findByPk($userId);
            $project = Project::model()->findByPk($projectId);
            $user->role = $user->getRole($user->id);
            $tasks = $user->tasks(array(
                'condition' => 'project_id=:project_id AND status in (' . implode(',', array(
                        Task::STATE_COMPLETE,
                        Task::STATE_INCOMPLETE,
                        Task::STATE_NOT_READ,
                    )) . ')',
                'params' => array(
                    ':project_id' => $projectId,
                ),
            ));
            $supervisorIsDeletable = true;
            if ($user->role == User::ROLE_PROJECT_SUPERVISOR) {
                $followUpUsers = $user->followUpUsers;
                $followUserIds = array();
                foreach ($followUpUsers as $followUpUser) {
                    $followUserIds[] = $followUpUser->id;
                }
                if (!empty($followUserIds)) {
                    $tasksToFollow = Task::model()->findAll('project_id=:project_id AND status in (' . implode(',', array(
                            Task::STATE_COMPLETE,
                            Task::STATE_INCOMPLETE,
                            Task::STATE_NOT_READ,
                        )) . ') AND user_id in (' . implode(',', $followUserIds) . ')', array(
                            ':project_id' => $projectId,
                        ));
                    if (!empty($tasksToFollow)) {
                        $supervisorIsDeletable = false;
                    }
                }
            }
            if (empty($tasks) && $supervisorIsDeletable) {
                $project->deleteMember($user->id);
                echo CJSON::encode(array(
                    'status' => true,
                    'message' => 'The User Was Deleted!',
                ));
            } else {
                echo CJSON::encode(array(
                    'status' => false,
                    'message' => 'Couldn\'t delete the user form workspace',
                ));
            }
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = "All registered projects in the system";
        $dataProvider = new CActiveDataProvider('Project');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionGetCreateForm()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $userDataProvider = new CActiveDataProvider('User');
            if (isset($_GET['ref'])) {
                $ref = $_GET['ref'];
            } else {
                $ref = 'nav';
            }
            $projectModel = new Project;
            echo $this->renderPartial('_form', array(
                'projectModel' => $projectModel,
                'userDataProvider' => $userDataProvider,
                'ref' => $ref,
            ), false, true);
        }
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Project('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Project']))
            $model->attributes = $_GET['Project'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Project the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Project::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Project $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'project-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function filterCheckAccess($chain){
        if(!Yii::app()->user->isGuest){
            $chain->run();
        }else{
            Yii::app()->request->redirect(array('site/login'));
        }
    }
    public function filterAdminAccess($chain){
        if(Yii::app()->user->checkAccess('administrator')){
            $chain->run();
        }else{
            throw new CHttpException(403);
        }
    }

    public function filterViewReportAccess($chain){
        if(Yii::app()->user->checkAccess('view_report')){
            $chain->run();
        }else{
            throw new CHttpException(403);
        }
    }
}
