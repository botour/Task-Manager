<?php

class AutoTaskController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
            'ajaxRequest +create,update',
            'checkAccess',
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

    /**
     *
     * This method does the following:
     * 1- creates a new AutTask model and store it in the database.
     * 2- Add the "creation of Task entry" to the Task Log
     * @return string
     * @dataType json
     *
     */
	public function actionCreate()
	{
		$autoTaskModel=new AutoTask('create');

		if(isset($_POST['AutoTask']))
		{
            $autoTaskModel->attributes = $_POST['AutoTask'];
            $autoTaskModel->activation_date = date('Y-m-d',strtotime($_POST['AutoTask']['activation_date']));
            if (strlen($_POST['AutoTask']['due_to_date']) > 0) {
                $autoTaskModel->due_to_date = date('Y-m-d',strtotime($_POST['AutoTask']['due_to_date']));
            }
            if(isset($_POST['AutoTask']['end_repeat'])){
                if (strlen($_POST['AutoTask']['end_repeat']) > 0) {
                    $autoTaskModel->end_repeat = date('Y-m-d',strtotime($_POST['AutoTask']['end_repeat']));
                }
            }

            $tasksCount = intval($count = AutoTask::model()->count('YEAR(create_date)=:date AND repeat_type=:repeat_type',array(
                ':date'=>date('Y'),
                ':repeat_type'=>$autoTaskModel->repeat_type,
            )));
            $count = $tasksCount+1;
            $prefixLetter = "";
            switch($autoTaskModel->repeat_type){
                case AutoTask::REPEAT_TYPE_DAILY:
                    $prefixLetter = "D";
                    break;
                case AutoTask::REPEAT_TYPE_WEEKLY:
                    $prefixLetter = "W";
                    break;
                case AutoTask::REPEAT_TYPE_MONTHLY:
                    $prefixLetter = "M";
                    break;
                case AutoTask::REPEAT_TYPE_YEARLY:
                    $prefixLetter = "Y";
                    break;
            }
            $autoTaskModel->occurrence = 0;
            $autoTaskModel->serial_no = $prefixLetter.date('y').'-'.$count;

            if($autoTaskModel->save()){
                // Add to Log
                $this->addStatusLog("<span style='color: #000;'>".Yii::app()->user->username." :</span> <span style='color: #5C5C5C;'>Create</span>  <span class='glyphicon glyphicon-arrow-right'></span> ".$autoTaskModel->user->username . " <span style='color: #000;'>|</span> " . date('j-m-Y h:i A', strtotime($autoTaskModel->create_date)), $autoTaskModel->id);

                echo CJSON::encode(array(
                    'status'=>true,
                    'message'=>'A new Auto Task Was Added',
                    'serial_no'=>$autoTaskModel->serial_no,
                ));
            }else{
                echo CJSON::encode(array(
                    'status'=>false,
                    'message'=>'Task Couldn\'t be created',
                ));
            }
		}
        else
        {
            throw new CHttpException(400);
        }
	}
    /**
     *
     *  This method is called Via Ajax GET request.
     *  It return an update auto task info form for a particular Task.
     *  @param integer $id the id of the Task to be modified
     *  @return string
     *  @dataType html
     *
     *
     */

    public function actionGetAutoTaskEditForm($id){
        $autoTaskModel = $this->loadModel($id);
        $autoTaskModel->setScenario("update");
        $currentUser = User::model()->findByPk(Yii::app()->user->id);
        $currentUser->role = $currentUser->getRole($currentUser->id);
        $fieldsStatusArray = array();
        if($autoTaskModel->end_type!=AutoTask::END_TYPE_SPECIFIC_DATE){
            $fieldsStatusArray['end_repeat_status']=true;
        }else{
            $fieldsStatusArray['end_repeat_status']=false;
        }
        if($autoTaskModel->second_priority==AutoTask::SECOND_PRIORITY_NEVER){
            $fieldsStatusArray['second_priority_status']=true;
        }else{
            $fieldsStatusArray['second_priority_status']=false;
        }
        if($autoTaskModel->third_priority==AutoTask::THIRD_PRIORITY_NEVER){
            $fieldsStatusArray['third_priority_status']=true;
        }else{
            $fieldsStatusArray['third_priority_status']=false;
        }
        if($autoTaskModel->due_to_date=="0000-00-00"){
            $autoTaskModel->due_to_date="";
        }
        $project = Project::model()->findByPk($autoTaskModel->project_id);
        echo $this->renderPartial('_form',array(
            'autoTaskModel'=>$autoTaskModel,
            'currentUser'=>$currentUser,
            'fieldsStatusArray'=>$fieldsStatusArray,
        ),false,true);
    }

    /**
     *
     *  This method is called Via Ajax POST request.
     *  \@param integer $_POST['Task']['id'] the id of the AutoTask model we want to modify
     *  @return string
     *  @dataType json
     *
     */

	public function actionUpdate()
	{
		if(isset($_POST['AutoTask']))
		{
            $model=$this->loadModel(intval($_POST['AutoTask']['id']));
            $model->attributes=$_POST['AutoTask'];
            $model->activation_date = date('Y-m-d',strtotime($_POST['AutoTask']['activation_date']));
            if (strlen($_POST['AutoTask']['due_to_date']) > 0) {
                $model->due_to_date = date('Y-m-d',strtotime($_POST['AutoTask']['due_to_date']));
            }
            if(isset($_POST['AutoTask']['end_repeat'])){
                if (strlen($_POST['AutoTask']['end_repeat']) > 0) {
                    $model->end_repeat = date('Y-m-d',strtotime($_POST['AutoTask']['end_repeat']));
                }
            }
            if($model->save()){
                $this->addStatusLog("<span style='color: #000;'>".Yii::app()->user->username." :</span> <span style='color: #5C5C5C;'>Update</span> <span style='color: #000;'>|</span> " . date('j-m-Y h:i A', strtotime($model->update_date)), $model->id);
                echo CJSON::encode(array(
                    'status'=>true,
                    'message'=>'The auto task was updated',
                    'taskObject'=>array(
                        "id"=>$model->id,
                        "serial_no"=>$model->serial_no,
                        "title"=>$model->title,
                        "description"=>$model->description,
                        "note"=>$model->note,
                    ),
                ));
            }else{
                echo CJSON::encode(array(
                    'status'=>false,
                    'message'=>'error updating the auto task',
                ));
            }
		}
	}

    /**
     *
     * Deletes a particular AutoTask model.
     * @param integer $_POST['id'] the ID of the model to be deleted
     *
     */

    public function actionDelete(){
        if(isset($_POST['id'])){
            $id = intval($_POST['id']);
            $autoTask = $this->loadModel($id);
            $comments = $autoTask->comments;
            foreach($comments as $comment){
                $comment->delete();
            }
            if($autoTask->delete()){
                echo CJSON::encode(array(
                    'status'=>true,
                    'message'=>'Task was deleted',
                ));
            }else{
                echo CJSON::encode(array(
                    'status'=>false,
                    'message'=>'Error Occurred!',
                ));
            }
        }else{
            throw new CHttpException(400);
        }
    }
    /**
     *
     *  This method is called Via Ajax POST request.
     *  this method toggles the value of the status attribute between the AutoTask::STATUS_DISABLED and AutoTask::STATUS_ENABLED
     *  for a particular AutoTask model
     *  @param integer $_POST['task_id'] the id of the Task model for which we want to change the status attribute value
     *  @return string
     *  @dataType json
     *
     */
    public function actionChangeStatus(){
        if(isset($_POST['id'])){
            $task = $this->loadModel(intval($_POST['id']));
            if($task->status==AutoTask::STATUS_DISABLED){
                $task->status = AutoTask::STATUS_ENABLED;
                $task->save(false);
            }else if($task->status==AutoTask::STATUS_ENABLED){
                $task->status = AutoTask::STATUS_DISABLED;
                $task->save(false);
            }
            echo CJSON::encode(array(
                'status'=>true,
            ));
        }else{
            throw new CHttpException(400);
        }
    }

    /**
     * This method renders the main AutoTask Management screen
     */

	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('AutoTask',array(
            'pagination'=>array(
                'pageSize'=>30,
            ),
        ));
        $autoTaskModel = new AutoTask('create');
        $autoTaskModel->status = AutoTask::STATUS_ENABLED;
        $autoTaskModel->second_priority = AutoTask::SECOND_PRIORITY_NEVER;
        $autoTaskModel->third_priority = AutoTask::THIRD_PRIORITY_NEVER;
        $currentUser = User::model()->findByPk(Yii::app()->user->id);
        $currentUser->role = $currentUser->getRole($currentUser->id);
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
            'autoTaskModel'=>$autoTaskModel,
            'currentUser'=>$currentUser,
		));
	}

    // AutoTask's comments management
    public function actionAddComment()
    {
        if (Yii::app()->request->isAjaxRequest) {
            if (strlen($_POST['comment_body']) > 0) {
                $comment = new AutoTaskComment;
                $comment->body = $_POST['comment_body'];
                $comment->auto_task_id = $_POST['task_id'];
                $comment->create_date = date('Y-m-d H:i:s');
                $comment->create_id = Yii::app()->user->id;
                $comment->status = AutoTaskComment::STATUS_COMMENT;
                if ($comment->save()) {
                    echo CJSON::encode(array(
                        'status' => true,
                        'message' => 'save',
                        'taskId' => $comment->auto_task_id,
                    ));
                } else {
                    echo CJSON::encode(array(
                        'status' => false,
                        'message' => $comment->errors,
                    ));
                }
            } else {
                echo CJSON::encode(array(
                    'status' => false,
                    'message' => 'no body',
                ));
            }
        }
    }

    // method to add a log entry for a particular Task
    public function addStatusLog($logMessage, $taskId)
    {
        $comment = new AutoTaskComment;
        $comment->status = AutoTaskComment::STATUS_LOG;
        $comment->body = $logMessage;
        $comment->auto_task_id = $taskId;
        $comment->create_date = date('Y-m-d H:i:s');
        $comment->create_id = Yii::app()->user->id;
        $comment->save();
    }

    /* this method is called via Ajax GET request
   *
   *   It brings the comment of a particular AutoTask model when a task is seleted from the AutoTask Grid List
   *   @param integer $task_id
   *   @return string
   *   @dataType html
   */

    public function actionGetTaskComments($task_id)
    {
        $autoTask = $this->loadModel(intval($task_id));

        echo $this->renderCommentData($autoTask->comments(array('order' => 'create_date')));
    }
    // This method is used by actionGetTaskComment method to render comments
    public function renderCommentData($comments)
    {
        return $this->renderPartial('partials/comments', array('comments' => $comments), false, true);
    }
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return AutoTask the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=AutoTask::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

    public function filterAjaxRequest($chain){
        if(Yii::app()->request->isAjaxRequest){
            $chain->run();
        }else{
            throw new CHttpException(400);
        }
    }

    public function renderAutoTaskActionsColumn($data, $row)
    {
        echo $this->renderPartial('partials/_auto-task-action-cell', array('task' => $data), false, false);
    }

	/**
	 * Performs the AJAX validation.
	 * @param AutoTask $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='auto-task-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    public function filterCheckAccess($chain){
        if(!Yii::app()->user->isGuest){
            if(Yii::app()->user->checkAccess('manage_auto_tasks')){
                $chain->run();
            }else{
                throw new CHttpException(403);
            }
        }else{
            Yii::app()->request->redirect(array('site/login'));
        }
    }
}
