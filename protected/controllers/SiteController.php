<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */

    public function filters(){
        array(
            'checkAccess',
        );
    }
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */

    public function actionIndex()
	{
        // The first time code to create the new user entity
        /*$user = new User;
        $user->first_name = "Zaher";
        $user->last_name = "Saad";
        $user->username = 'zaher';
        $user->login_name = 'zaher';
        $user->role = User::ROLE_ADMIN;
        $user->password = '1';
        $user->email = 'borhan@gmail.com';
        $user->password_repeat = '1';
        $user->save();
        Yii::app()->authManager->assign('administrator', $user->id);
        Yii::app()->authManager->assign('reassign', $user->id);
        Yii::app()->authManager->assign('view_archive', $user->id);
        Yii::app()->authManager->assign('end_task', $user->id);
        Yii::app()->user->logout();
        */
        if(Yii::app()->user->isGuest){
            $this->redirect(array('site/login'));
        }else{
            $user = User::model()->findByPk(Yii::app()->user->id);
            $this->redirect(array('task/index'));
        }
    }

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}else{
            $this->redirect('site/index');
        }
	}

	/**
	 * Displays the login page
	 */

	public function actionLogin()
	{
        Yii::app()->user->logout();
        $this->pageTitle = "User Login";
        $loginModel=new LoginForm;
		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($loginModel);
			Yii::app()->end();
		}
		// collect user input data
		if(isset($_POST['LoginForm']))
		{

            $loginModel->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($loginModel->validate() && $loginModel->login()){
                $user = User::model()->findByPk(Yii::app()->user->id);
                $user->setStatus(User::STATE_ONLINE);
                $this->redirect(array('site/index'));
            }
		}
		// display the login form
		$this->render('login',array('loginModel'=>$loginModel));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */

	public function actionLogout()
	{
        if(Yii::app()->user->isGuest){
            $this->redirect(array('site/login'));
        }else{
            $user = User::model()->findByPk(Yii::app()->user->id);
            $user->setStatus(User::STATE_OFFLINE);
            Yii::app()->user->logout();
            $this->redirect(array('site/login'));
        }

	}

}