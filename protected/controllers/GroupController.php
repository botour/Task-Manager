<?php

class GroupController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'checkAccess',
            'adminAccess',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     *
     * This method does the following:
     * 1- creates a new Group model and store it in the database.
     * 2- Add the "creation of Task entry" to the Task Log
     * @return string
     * @dataType json
     *
     */
    public function actionCreate()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $model = new Group;
            if (isset($_POST['Group'])) {
                $model->attributes = $_POST['Group'];
                $model->create_id = Yii::app()->user->id;
                $model->create_date = date('Y-m-d H:i:s');
                $model->status = Group::STATUS_ACTIVE;
                if ($model->save()){
                    echo CJSON::encode(array(
                        'status'=>true,
                        'message'=>'A new Group was created',
                    ));
                }else{
                    echo CJSON::encode(array(
                        'status'=>false,
                        'message'=> 'Could\'t create a new Group',
                    ));
                }
            }
        } else {
            throw new CHttpException(400);
        }
    }


    /**
     *
     *  This method is called Via Ajax GET request.
     *  It return an update group info form for a particular Group.
     *  @param integer $id the id of the Group to be modified
     *  @return string
     *  @dataType html
     *
     */

    public function actionGetUpdateForm($id){
        if(Yii::app()->request->isAjaxRequest){
            $groupModel = $this->loadModel(intval($id));
            echo $this->renderPartial('_form',array(
                'groupModel'=>$groupModel,
            ),false,true);
        }else{
            throw new CHttpException(400);
        }
    }


    /**
     *
     *  This method is called Via Ajax POST request.
     *  @param integer $_POST['Group']['id'] the id of the Group model we want to modify
     *  @return string
     *  @dataType json
     *
     */
    public function actionUpdate()
    {
        if(Yii::app()->request->isAjaxRequest){
            $model = $this->loadModel($_POST['Group']['id']);
            $model->attributes = $_POST['Group'];
            if($model->save()){
                echo CJSON::encode(array(
                    'status'=>true,
                    'message'=>'The Group\'s info has been updated'
                ));
            }else{
                echo CJSON::encode(array(
                    'status'=>false,
                    'message'=>'Error'
                ));
            }
        }else{
            throw new CHttpException(400);
        }

    }

    /**
     *
     * Deletes a particular Group model.
     * The method also deletes the association between the group and its members
     * @param integer $_POST['id'] the ID of the model to be deleted
     *
     */
    public function actionDelete()
    {
        if(Yii::app()->request->isAjaxRequest){
            if(isset($_POST['id'])){
                $groupId = intval($_POST['id']);
                $group = Group::model()->findByPk($groupId);
                $deleteCount = Yii::app()->db->createCommand()->delete('tbl_user_group_assignment','group_id=:group_id',array(
                    ':group_id'=>$group->id,
                ));
                if($group->delete()){
                    echo CJSON::encode(array(
                        'status'=>true,
                        'message'=>'The Group "'.$group->name.'" was deleted successfully.'
                    ));
                }else{
                    echo CJSON::encode(array(
                        'status'=>false,
                        'message'=>'The Group "'.$group->name.'" couldn\'t be deleted.',
                    ));
                }
            }
        }else{
            throw new CHttpException(400);
        }
    }

    public function actionGetCreateForm()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $groupModel = new Group;
            echo $this->renderPartial('_form', array(
                'groupModel' => $groupModel,
            ), false, true);
        }
    }


    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $groupModel = new Group('search');
        $groupModel->unsetAttributes();

        if (isset($_GET['Group'])) {
            $groupModel->attributes = $_GET['Group'];
        }

        $this->render('index', array(
            'groupModel' => $groupModel,
        ));
    }

    public function actionUpdateUsers($id){
        if(Yii::app()->request->isAjaxRequest){
            $model = $this->loadModel(intval($id));
            if(isset($_POST['user_id'])){
                foreach($_POST['user_id'] as $userId){
                    $model->addMember($userId);
                }
            }
        }else{
            throw new CHttpException(400);
        }
    }

    public function actionUsers($id){
        $group = Group::model()->findByPk(intval($id));
        $users = User::model()->findAll();
        foreach($users as $user){
            $user->groupId = $group->id;
        }
        $userDataProvider = new CActiveDataProvider('User');
        $userDataProvider->setData($users);
        $this->render("users",array(
            'groupModel'=>$group,
            'userDataProvider'=>$userDataProvider
        ));
    }

    public function actionDeleteFromGroup($userId,$groupId){
        if(Yii::app()->request->isAjaxRequest){
            $user = User::model()->findByPk($userId);
            $group = Group::model()->findByPk($groupId);

            $group->deleteMember($user->id);
            echo CJSON::encode(array(
                'status'=>true,
                'message'=>'The User Was Deleted!',
            ));

        }
    }
    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Group('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Group']))
            $model->attributes = $_GET['Group'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionGetGroupMembers($id){
        $groupId = intval($id);
        $group = Group::model()->findByPk($groupId);
        $members = $group->users;
        echo $this->renderPartial('partials/members-list',array(
            'members'=>$members,
        ),false,true);
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Group the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Group::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Group $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'group-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    public function filterCheckAccess($chain){
        if(!Yii::app()->user->isGuest){
            $chain->run();
        }else{
            Yii::app()->request->redirect(array('site/login'));
        }
    }
    public function filterAdminAccess($chain){
        if(Yii::app()->user->checkAccess('administrator')){
            $chain->run();
        }else{
            throw new CHttpException(403);
        }
    }
}
