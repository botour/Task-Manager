<?php

class SettingController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
        return array(
            'checkAccess',
            'adminAccess',
        );
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionIndex()
	{
		$setting = Setting::model()->find('name=:name',array(
            ':name'=>Setting::SETTING_APP_NAME,
        ));
        if(isset($_POST['Setting'])){
            $setting->value = $_POST['Setting']['value'];

            if($setting->save()){
                $this->redirect(array('task/index'));
            }
        }
		$this->render('index',array(
			'setting'=>$setting,
		));
	}

	public function loadModel($id)
	{
		$model=Setting::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

    public function filterCheckAccess($chain){
        if(!Yii::app()->user->isGuest){
            $chain->run();
        }else{
            Yii::app()->request->redirect(array('site/login'));
        }
    }
    public function filterAdminAccess($chain){
        if(Yii::app()->user->checkAccess('administrator')){
            $chain->run();
        }else{
            throw new CHttpException(403);
        }
    }
}
