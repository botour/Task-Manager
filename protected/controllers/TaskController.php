<?php

class TaskController extends Controller
{
    private $_projectId;
    private $_project;
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    public $isSupervisorOrAdministrator = false;

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'checkAccess',
            'endTask +changeTaskStatusToClose',
            'reassign +changeTaskAssignment',
            'supervisorAccess +changeTaskReminded',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     *
     * This method does the following:
     * 1- creates a new Task model and store it in the database.
     * 2- Store the needed notifications so the notification system can do its function and notifying members & supervisors related to the Task
     * 3- Add the "creation of Task entry" to the Task Log
     * @return string
     * @dataType json
     *
     */
    public function actionCreate()
    {
        if (isset($_POST['Task'])) {
            $currentUser = User::model()->with('supervisors')->findByPk(Yii::app()->user->id);
            $currentUser->role = User::model()->getRole($currentUser->id);
            $model = new Task;
            $model->attributes = $_POST['Task'];

            $model->type = Task::TYPE_MANUAL;
            $model->second_priority = Task::SECOND_PRIORITY_NEVER;
            $model->third_priority = Task::THIRD_PRIORITY_NEVER;

            $model->reminded = Task::TASK_NOT_REMINDED;

            if (strlen($_POST['Task']['due_to_date']) > 0) {
                $model->due_to_date = date('Y-m-d',strtotime($_POST['Task']['due_to_date']));
            }
            // Set the user_id value of the Task Object
            if ($currentUser->role == User::ROLE_PROJECT_SUPERVISOR) {
                if (strlen($_POST['Task']['user_id']) != 0) {
                    $model->user_id = intval($_POST['Task']['user_id']);
                }
            } else if ($currentUser->role == User::ROLE_MEMBER) {
                $model->user_id = Yii::app()->user->id;
            }
            $successSave = $model->save();
            if ($successSave) {
                // Add to Log
                $this->addStatusLog("<span style='color: #000;'>".Yii::app()->user->username." :</span> <span style='color: #5C5C5C;'>Create</span>  <span class='glyphicon glyphicon-arrow-right'></span> ".$model->user->username . " <span style='color: #000;'>|</span> " . date('j-m-Y h:i A', strtotime($model->create_date)), $model->id);
                // Set the notification
                if ($currentUser->role == User::ROLE_PROJECT_SUPERVISOR) {
                    // Supervisor may assign task to him self
                    if($model->user_id!=$currentUser->id){
                        $model->notified = Task::USER_NOT_NOTIFIED;
                    }else{
                        $model->notified = Task::USER_NOTIFIED;
                    }
                    $model->save(false);
                    $supervisors = User::model()->with('supervisors')->findByPk($model->user_id)->supervisors;
                    foreach ($supervisors as $supervisorUser) {
                        if ($supervisorUser->id != $currentUser->id) {
                            $supervisorNotification = new SupervisorNotification;
                            $supervisorNotification->task_id = $model->id;
                            $supervisorNotification->supervisor_id = $supervisorUser->id;
                            $supervisorNotification->supervisor_notified = SupervisorNotification::SUPERVISOR_NOT_NOTIFIED;
                            $supervisorNotification->project_id = $currentUser->current_project_id;
                            $supervisorNotification->update_type = SupervisorNotification::UPDATE_TYPE_TASK_CREATE;
                            $supervisorNotification->save();
                        }
                    }
                } else if ($currentUser->role == User::ROLE_MEMBER) {
                    $model->notified = Task::USER_NOTIFIED;
                    $model->save(false);
                    $currentUserSupervisors = $currentUser->supervisors;
                    foreach ($currentUserSupervisors as $supervisorUser) {
                        $supervisorNotification = new SupervisorNotification;
                        $supervisorNotification->project_id = $currentUser->current_project_id;
                        $supervisorNotification->task_id = $model->id;
                        $supervisorNotification->supervisor_id = $supervisorUser->id;
                        $supervisorNotification->supervisor_notified = SupervisorNotification::SUPERVISOR_NOT_NOTIFIED;
                        $supervisorNotification->update_type = SupervisorNotification::UPDATE_TYPE_TASK_CREATE;
                        $supervisorNotification->save();
                    }
                }else if($currentUser->role == User::ROLE_ADMIN){
                    // Admin may assign task to him self
                    if($model->user_id!=$currentUser->id){
                        $model->notified = Task::USER_NOT_NOTIFIED;
                    }else{
                        $model->notified = Task::USER_NOTIFIED;
                    }
                    $model->save(false);
                    $supervisors = User::model()->with('supervisors')->findByPk($model->user_id)->supervisors;
                    foreach ($supervisors as $supervisorUser) {
                        if ($supervisorUser->id != $currentUser->id) {
                            $supervisorNotification = new SupervisorNotification;
                            $supervisorNotification->task_id = $model->id;
                            $supervisorNotification->supervisor_id = $supervisorUser->id;
                            $supervisorNotification->supervisor_notified = SupervisorNotification::SUPERVISOR_NOT_NOTIFIED;
                            $supervisorNotification->project_id = $currentUser->current_project_id;
                            $supervisorNotification->update_type = SupervisorNotification::UPDATE_TYPE_TASK_CREATE;
                            $supervisorNotification->save();
                        }
                    }
                }
                echo CJSON::encode(array(
                    'status' => $successSave,
                    'message' => 'Serial No: '.$model->serial_no,
                    'id' => $model->id,
                ));
            } else {
                echo CJSON::encode(array(
                    'status' => $successSave,
                    'message' => "Could not add the task.",
                ));
            }
        }
    }


    public function actionEmpty(){
        $this->render('empty');
    }
    /**
     * This method renders the main Task Manager screen
     */
    public function actionIndex()
    {
        // Force Default Order
        $forceDefaultToDoTasksOrder = true;
        $forceDefaultFollowUpTasksOrder = true;
        $forceDefaultCompleteTasksOrder = true;
        $forceDefaultArchivedTasksOrder = true;

        if (isset($_GET['sort']) && strlen($_GET['sort']) > 0) {
            if ($_GET['ajax'] == 'follow-up-task-grid') {
                $forceDefaultFollowUpTasksOrder = false;
            }
            if ($_GET['ajax'] == 'task-grid') {
                $forceDefaultToDoTasksOrder = false;
            }
            if ($_GET['ajax'] == 'complete-task-grid') {
                $forceDefaultCompleteTasksOrder = false;
            }
            if ($_GET['ajax'] == 'archived-task-grid') {
                $forceDefaultArchivedTasksOrder = false;
            }
        }

        // For the Task create Form
        $model = new Task;
        //Get the current user model
        $currentUser = User::model()->findByPk(Yii::app()->user->id);
        $currentUser->role = $currentUser->getRole($currentUser->id);
        // Check current user row and save the state to the controller
        $this->isSupervisorOrAdministrator = $currentUser->role == User::ROLE_PROJECT_SUPERVISOR || $currentUser->role == User::ROLE_ADMIN ? true : false;
        //Create the todo Task model search criteria
        $taskModel = new Task('search');
        $taskModel->unsetAttributes();
        //Create the complete Task model search criteria
        $completeTaskList = new Task('search');
        $completeTaskList->unsetAttributes();
        $completeTaskList->status = Task::STATE_COMPLETE;
        $taskModel->status = array(Task::STATE_NOT_READ, Task::STATE_INCOMPLETE);

        $followTaskModel = new Task('search');
        $followTaskModel->unsetAttributes();
        $archivedTaskModel = new Task('search');
        $archivedTaskModel->unsetAttributes();
        $archivedTaskModel->status = Task::STATE_VERIFIED;
        $taskModel->createUsername = "";
        if ($this->isSupervisorOrAdministrator) {
            // Setup the followList users's tasks
            $followTaskModel->status = array(Task::STATE_NOT_READ, Task::STATE_INCOMPLETE);
            $followUpUsers = $currentUser->followUpUsers;
            $followUpUserIds = array();
            foreach ($followUpUsers as $fUser) {
                $followUpUserIds[] = $fUser->id;
            }
            if (!empty($followUpUserIds)) {
                $followTaskModel->user_id = $followUpUserIds;
                $followUpUserIds[] = $currentUser->id;
                $completeTaskList->user_id = $followUpUserIds;
            } else {
                $followTaskModel->user_id = 0;
            }
            // Setup the complete Task List
        } else {
            $completeTaskList->user_id = $currentUser->id;
        }
        $taskModel->user_id = $currentUser->id;
        $project = null;
        if (isset($_GET['project_id'])) {
            $project_id = intval($_GET['project_id']);
            if ($project_id != 0) {
                $taskModel->project_id = $project_id;
                $completeTaskList->project_id = $project_id;
                if ($this->isSupervisorOrAdministrator) {
                    $followTaskModel->project_id = $project_id;
                }
                if (Yii::app()->user->checkAccess('view_archive')) {
                    $archivedTaskModel->project_id = $project_id;
                }
            }
            $currentUser->current_project_id = $project_id;
            $currentUser->save(false);
            $project = $this->loadProjectModel(intval($_GET['project_id']));
        } else {
            // This is when using the ajax update for todo-grid-view
            if ($currentUser->current_project_id != 0) {
                $project = $this->loadProjectModel($currentUser->current_project_id);
                $project_id = $currentUser->current_project_id;
                $taskModel->project_id = $project_id;
                $completeTaskList->project_id = $project_id;
                if ($this->isSupervisorOrAdministrator) {
                    $followTaskModel->project_id = $project_id;
                }
                if (Yii::app()->user->checkAccess('view_archive')) {
                    $archivedTaskModel->project_id = $project_id;
                }
            }else{
                $this->redirect(array('task/empty'));
            }
        }
        $model->project_id = $currentUser->current_project_id;
        if (isset($_GET['Task'])) {
            $forceDefaultToDoTasksOrder = true;
            $taskModel->title = $_GET['Task']['title'];
            $taskModel->priority = $_GET['Task']['priority'];
            // Search By Data
            if (strlen($_GET['Task']['daysNo'])) {
                $taskModel->daysNo = $_GET['Task']['daysNo'];
                $searchTime = strtotime(date('Y-m-d')) - 3600 * 24 * $taskModel->daysNo;
                $taskModel->create_date = date('Y-m-d', $searchTime);
            }
            //Search By User
            if (strlen($_GET['Task']['createUsername']) > 0) {
                $searchUser = User::model()->find('username=:username', array(
                    'username' => $_GET['Task']['createUsername'],
                ));
                if (!is_null($searchUser)) {
                    $taskModel->create_id = $searchUser->id;
                    $taskModel->createUsername = $searchUser->username;
                } else {
                    $taskModel->create_id = 0;
                    $taskModel->createUsername = $_GET['Task']['createUsername'];
                }
            }
            $taskModel->tag = $_GET['Task']['tag'];
        }

        if (isset($_GET['FollowTask'])) {
            $forceDefaultFollowUpTasksOrder = true;
            $followTaskModel->title = $_GET['FollowTask']['title'];
            $followTaskModel->priority = $_GET['FollowTask']['priority'];
            // Search By Data
            if (strlen($_GET['FollowTask']['daysNo'])) {
                $followTaskModel->daysNo = $_GET['FollowTask']['daysNo'];
                $searchTime = strtotime(date('Y-m-d')) - 3600 * 24 * $followTaskModel->daysNo;
                $followTaskModel->create_date = date('Y-m-d', $searchTime);
            }
            if (strlen($_GET['FollowTask']['createUsername']) > 0) {
                $searchUser = User::model()->find('username=:username', array(
                    'username' => $_GET['FollowTask']['createUsername'],
                ));
                if (!is_null($searchUser)) {
                    $followTaskModel->create_id = $searchUser->id;
                    $followTaskModel->createUsername = $searchUser->username;
                } else {
                    $followTaskModel->create_id = 0;
                    $followTaskModel->createUsername = $_GET['FollowTask']['createUsername'];
                }
            } else {
                $followTaskModel->unsetAttributes(array('create_id'));
            }
            $followTaskModel->tag = $_GET['FollowTask']['tag'];
            if (strlen($_GET['FollowTask']['user_id']) > 0) {
                $followTaskModel->user_id = $_GET['FollowTask']['user_id'];
            }else{

            }
        }
        if (isset($_GET['CompleteTask'])) {
            $forceDefaultCompleteTasksOrder = true;
            $completeTaskList->title = $_GET['CompleteTask']['title'];
            $completeTaskList->priority = $_GET['CompleteTask']['priority'];
            // Search By Data
            if (strlen($_GET['CompleteTask']['daysNo'])) {
                $completeTaskList->daysNo = $_GET['CompleteTask']['daysNo'];
                $searchTime = strtotime(date('Y-m-d')) - 3600 * 24 * $completeTaskList->daysNo;
                $completeTaskList->create_date = date('Y-m-d', $searchTime);
            }
            if (strlen($_GET['CompleteTask']['createUsername']) > 0) {
                $searchUser = User::model()->find('username=:username', array(
                    'username' => $_GET['CompleteTask']['createUsername'],
                ));
                if (!is_null($searchUser)) {
                    $completeTaskList->create_id = $searchUser->id;
                    $completeTaskList->createUsername = $searchUser->username;
                } else {
                    $completeTaskList->create_id = 0;
                    $completeTaskList->createUsername = $_GET['CompleteTask']['createUsername'];
                }
            } else {
                $completeTaskList->unsetAttributes(array('create_id'));
            }
            $completeTaskList->tag = $_GET['CompleteTask']['tag'];
            if (strlen($_GET['CompleteTask']['user_id']) > 0) {
                $completeTaskList->user_id = $_GET['CompleteTask']['user_id'];
            }
        }
        if (isset($_GET['ArchivedTask'])) {
            $forceDefaultArchivedTasksOrder = true;
            $archivedTaskModel->title = $_GET['ArchivedTask']['title'];
            // Search By Data
            if (strlen($_GET['ArchivedTask']['daysNo'])) {
                $archivedTaskModel->daysNo = $_GET['ArchivedTask']['daysNo'];
                $searchTime = strtotime(date('Y-m-d')) - 3600 * 24 * $archivedTaskModel->daysNo;
                $archivedTaskModel->create_date = date('Y-m-d', $searchTime);
            }
            if (strlen($_GET['ArchivedTask']['endByUsername']) > 0) {
                $searchEndByUser = User::model()->find('username=:username', array(
                    'username' => $_GET['ArchivedTask']['endByUsername'],
                ));
                if (!is_null($searchEndByUser)) {
                    $archivedTaskModel->update_id = $searchEndByUser->id;
                    $archivedTaskModel->endByUsername = $searchEndByUser->username;
                } else {
                    $archivedTaskModel->update_id = 0;
                    $archivedTaskModel->endByUsername = $_GET['ArchivedTask']['endByUsername'];
                }
            } else {
                $archivedTaskModel->unsetAttributes(array('update_id'));
            }
            if (strlen($_GET['ArchivedTask']['createUsername']) > 0) {
                $searchUser = User::model()->find('username=:username', array(
                    'username' => $_GET['ArchivedTask']['createUsername'],
                ));
                if (!is_null($searchUser)) {
                    $archivedTaskModel->create_id = $searchUser->id;
                    $archivedTaskModel->createUsername = $searchUser->username;
                } else {
                    $archivedTaskModel->create_id = 0;
                    $archivedTaskModel->createUsername = $_GET['ArchivedTask']['createUsername'];
                }
            } else {
                $archivedTaskModel->unsetAttributes(array('create_id'));
            }
            $archivedTaskModel->tag = $_GET['ArchivedTask']['tag'];
            if (strlen($_GET['ArchivedTask']['user_id']) > 0) {
                $archivedTaskModel->user_id = $_GET['ArchivedTask']['user_id'];
            }
        }

        $this->render('index', array(
            'forceDefaultToDoTasksOrder' => $forceDefaultToDoTasksOrder,
            'forceDefaultFollowUpTasksOrder' => $forceDefaultFollowUpTasksOrder,
            'forceDefaultCompleteTasksOrder' => $forceDefaultCompleteTasksOrder,
            'forceDefaultArchivedTasksOrder' => $forceDefaultArchivedTasksOrder,
            'taskModel' => $taskModel,
            'followTaskModel' => $followTaskModel,
            'completeTaskList' => $completeTaskList,
            'archivedTaskModel' => $archivedTaskModel,
            'currentUser' => $currentUser,
            'model' => $model,
        ));
    }


    /*
     * Ajax Methods .
     */

     /**
     *  This method is called Via Ajax GET request.
     *   Creates a new form to Send a new Task
     *   If creation is successful, the function will return an html represents the creation Task Form
     */
    public function actionGetCreateForm()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $currentUser = User::model()->findByPk(Yii::app()->user->id);
            $currentUser->role = $currentUser->getRole($currentUser->id);
            $taskModel = new Task;
            $taskModel->project_id = $currentUser->current_project_id;
            echo $this->renderPartial('_form', array(
                'taskModel' => $taskModel,
                'currentUser' => $currentUser,
            ), false, true);
        }else{
            throw new CHttpException(400);
        }
    }

    /**
     *
     *  This method is called Via Ajax GET request.
     *  It return an update task info form for a particular Task.
     *  @param integer $_GET['task_id'] the id of the Task to be modified
     *  @return string
     *  @dataType html
     *
     *
     */

    public function actionGetUpdateTaskInfoForm()
    {
        if (isset($_GET['task_id'])) {
            $taskId = intval($_GET['task_id']);
            $task = Task::model()->findByPk($taskId);
            $currentUser = User::model()->findByPk(Yii::app()->user->id);
            echo $this->renderPartial('_form', array(
                'model' => $task,
                'currentUser' => $currentUser,
            ), false, true);
            Yii::app()->end();
        } else {

        }
    }

    /**
     *
     *  This method is called Via Ajax POST request.
     *  It toggles the value of the Task model reminded attribute between Task::TASK_REMINDED and Task::TASK_NOT_REMINDED
     *  @param integer $_POST['task_id'] the id of the Task to be modified
     *  @return string
     *  @dataType json
     *
     */

    public function actionChangeTaskReminded()
    {
        if (isset($_POST['task_id'])) {
            $task = $this->loadModel(intval($_POST['task_id']));
            if ($task->reminded == Task::TASK_NOT_REMINDED) {
                $task->reminded = Task::TASK_REMINDED;
                $task->notified = Task::USER_NOT_RE_NOTIFIED;
                $task->update_type = Task::UPDATE_TYPE_REMINDED;
                $taskUser = User::model()->findByPk($task->user_id);
                $task->notified = Task::USER_NOT_RE_NOTIFIED;
                $task->update_type = Task::UPDATE_TYPE_REMINDED;
                $task->save(false);
                $this->addStatusLog("Reminded By ".User::model()->findByPk(Yii::app()->user->id)->username,$task->id);
                echo CJSON::encode(array(
                    'status' => true,
                    'reminded' => true,
                ));
            } else {
                $task->reminded = Task::TASK_NOT_REMINDED;
                $task->notified = Task::USER_NOTIFIED;
                $task->save(false);
                echo CJSON::encode(array(
                    'status' => true,
                    'reminded' => false,
                ));
            }
        } else {
            throw new CHttpException(400);
        }
    }


    /**
     *
     *  This method is called Via Ajax GET request.
     *  This method represents the main notification system logic for the Task Manager
     *  @return string
     *  @dataType json
     *
     */

    public function actionGetNewTasks()
    {
        if (Yii::app()->user->isGuest) {
            throw new CHttpException(403);
        } else {
            $currentUser = User::model()->findByPk(Yii::app()->user->id);
            $currentUser->role = User::model()->getRole($currentUser->id);

            $tasks = Task::model()->findAll('notified=:notified AND user_id=:user_id AND project_id=:project_id', array(
                ':notified' => Task::USER_NOT_NOTIFIED,
                ':user_id' => $currentUser->id,
                ':project_id' => $currentUser->current_project_id,
            ));

            // New Tasks to follow
            $followUpTaskStatus = false;
            $followUpTaskCount = 0;
            $followUpTaskMessage = "";

            // Follow Up Tasks to complete
            $followUpTaskCompleteStatus = false;
            $followUpTaskCompleteCount = 0;
            $followUpTaskCompleteMessage = "";

            // Follow Up Tasks to not complete
            $followUpTaskNotCompleteStatus = false;
            $followUpTaskNotCompleteCount = 0;
            $followUpTaskNotCompleteMessage = "";

            // Follow Up Tasks that were reassigned
            $followUpTaskReassignStatus = false;
            $followUpTaskReassignCount = 0;
            $followUpTaskReassignMessage = "";

            // Change Task assignment
            $reassignedStatus = false;
            $reassignedTaskCount = 0;
            $reassignedMessage = "";
            $reassignedByMemberStatus = false;
            $reassignedByMemberTaskCount = 0;
            $reassignedByMemberMessage = "";

            // Change Task Status to complete
            $changeToCompleteStatus = false;
            $changeToCompleteCount = 0;
            $changeToCompleteMessage = "";

            // Change Task Status to not complete
            $changeToNotCompleteByMemberStatus = false;
            $changeToNotCompleteByMemberCount = 0;
            $changeToNotCompleteByMemberMessage = "";

            // Change Task Status to not complete by supervisor
            $changeToNotCompleteStatus = false;
            $changeToNotCompleteCount = 0;
            $changeToNotCompleteMessage = "";

            // Reminder Notification
            $changeToRemindedStatus = false;
            $changeToRemindedCount = 0;
            $changeToRemindedMessage = "";

            // To know how to upgrade the grid
            $supervisorNotified = false;
            $userNotified = false;
            if ($currentUser->role == User::ROLE_PROJECT_SUPERVISOR) {
                $followUpUsers = $currentUser->followUpUsers;
                $followUpUsersId = array();
                foreach ($followUpUsers as $user) {
                    $followUpUsersId[] = $user->id;
                }

                // Get the new Tasks to Follow
                $newFollowUpTasksNotifications = SupervisorNotification::model()->findAll('supervisor_id=:supervisor_id AND supervisor_notified=:supervisor_notified AND project_id=:project_id AND update_type=:update_type', array(
                    ':supervisor_notified' => Task::SUPERVISOR_NOT_NOTIFIED,
                    ':project_id' => $currentUser->current_project_id,
                    ':update_type' => SupervisorNotification::UPDATE_TYPE_TASK_CREATE,
                    ':supervisor_id' => $currentUser->id,
                ));
                $followUpTaskCount = count($newFollowUpTasksNotifications);
                if ($followUpTaskCount > 0) {
                    $followUpTaskStatus = true;
                    if ($followUpTaskCount == 1) {
                        $followUpTaskMessage = 'New Task To Follow';
                    } else {
                        $followUpTaskMessage = 'New Tasks To Follow';
                    }
                    Yii::app()->db->createCommand()->delete('tbl_supervisor_notification', 'supervisor_id=:supervisor_id AND supervisor_notified=:supervisor_notified AND project_id=:project_id AND update_type=:update_type', array(
                        ':supervisor_notified' => Task::SUPERVISOR_NOT_NOTIFIED,
                        ':project_id' => $currentUser->current_project_id,
                        ':update_type' => SupervisorNotification::UPDATE_TYPE_TASK_CREATE,
                        ':supervisor_id' => $currentUser->id,
                    ));
                }
                //Get the tasks which were set to complete
                $followUpTasksCompleteNotifications = SupervisorNotification::model()->findAll('supervisor_id=:supervisor_id AND supervisor_notified=:supervisor_notified AND project_id=:project_id AND update_type=:update_type', array(
                    ':supervisor_notified' => Task::SUPERVISOR_NOT_NOTIFIED,
                    ':project_id' => $currentUser->current_project_id,
                    ':update_type' => SupervisorNotification::UPDATE_TYPE_TASK_COMPLETE,
                    ':supervisor_id' => $currentUser->id,
                ));
                $followUpTaskCompleteCount = count($followUpTasksCompleteNotifications);
                if ($followUpTaskCompleteCount > 0) {
                    $followUpTaskCompleteStatus = true;
                    if ($followUpTaskCompleteCount == 1) {
                        $followUpTaskCompleteMessage = 'Task was set To complete';
                    } else {
                        $followUpTaskCompleteMessage = 'Tasks were set To complete';
                    }
                    Yii::app()->db->createCommand()->delete('tbl_supervisor_notification', 'supervisor_id=:supervisor_id AND supervisor_notified=:supervisor_notified AND project_id=:project_id AND update_type=:update_type', array(
                        ':supervisor_notified' => Task::SUPERVISOR_NOT_NOTIFIED,
                        ':project_id' => $currentUser->current_project_id,
                        ':update_type' => SupervisorNotification::UPDATE_TYPE_TASK_COMPLETE,
                        ':supervisor_id' => $currentUser->id,
                    ));
                }
                //Get the tasks which were set to not complete
                $followUpTasksNotCompleteNotifications = SupervisorNotification::model()->findAll('supervisor_id=:supervisor_id AND supervisor_notified=:supervisor_notified AND project_id=:project_id AND update_type=:update_type', array(
                    ':supervisor_notified' => Task::SUPERVISOR_NOT_NOTIFIED,
                    ':project_id' => $currentUser->current_project_id,
                    ':update_type' => SupervisorNotification::UPDATE_TYPE_NOT_TASK_COMPLETE,
                    ':supervisor_id' => $currentUser->id,
                ));
                $followUpTaskNotCompleteCount = count($followUpTasksNotCompleteNotifications);
                if ($followUpTaskNotCompleteCount > 0) {
                    $followUpTaskNotCompleteStatus = true;
                    if ($followUpTaskNotCompleteCount == 1) {
                        $followUpTaskNotCompleteMessage = 'Task was set To Not complete';
                    } else {
                        $followUpTaskNotCompleteMessage = 'Tasks were set To Not complete';
                    }
                    Yii::app()->db->createCommand()->delete('tbl_supervisor_notification', 'supervisor_id=:supervisor_id AND supervisor_notified=:supervisor_notified AND project_id=:project_id AND update_type=:update_type', array(
                        ':supervisor_notified' => Task::SUPERVISOR_NOT_NOTIFIED,
                        ':project_id' => $currentUser->current_project_id,
                        ':update_type' => SupervisorNotification::UPDATE_TYPE_NOT_TASK_COMPLETE,
                        ':supervisor_id' => $currentUser->id,
                    ));
                }
                //Get the tasks which were set to not complete
                $followUpTasksReAssignNotifications = SupervisorNotification::model()->findAll('supervisor_id=:supervisor_id AND supervisor_notified=:supervisor_notified AND project_id=:project_id AND update_type=:update_type', array(
                    ':supervisor_notified' => Task::SUPERVISOR_NOT_NOTIFIED,
                    ':project_id' => $currentUser->current_project_id,
                    ':update_type' => SupervisorNotification::UPDATE_TYPE_TASK_REASSIGN,
                    ':supervisor_id' => $currentUser->id,
                ));
                $followUpTaskReassignCount = count($followUpTasksReAssignNotifications);
                if ($followUpTaskReassignCount > 0) {
                    $followUpTaskReassignStatus = true;
                    if ($followUpTaskReassignCount == 1) {
                        $followUpTaskReassignMessage = 'Task was reassigned';
                    } else {
                        $followUpTaskReassignMessage = 'Tasks were reassigned';
                    }
                    Yii::app()->db->createCommand()->delete('tbl_supervisor_notification', 'supervisor_id=:supervisor_id AND supervisor_notified=:supervisor_notified AND project_id=:project_id AND update_type=:update_type', array(
                        ':supervisor_notified' => Task::SUPERVISOR_NOT_NOTIFIED,
                        ':project_id' => $currentUser->current_project_id,
                        ':update_type' => SupervisorNotification::UPDATE_TYPE_TASK_REASSIGN,
                        ':supervisor_id' => $currentUser->id,
                    ));
                }
            }
            $reassignedTasks = Task::model()->findAll('notified=:notified AND user_id=:user_id AND project_id=:project_id AND update_type=:update_type', array(
                ':notified' => Task::USER_NOT_RE_NOTIFIED,
                ':project_id' => $currentUser->current_project_id,
                ':update_type' => Task::UPDATE_TYPE_REASSIGN,
                ':user_id' => Yii::app()->user->id,
            ));
            if (count($reassignedTasks) > 0) {
                $reassignedStatus = true;
                $reassignedTaskCount = count($reassignedTasks);
                $userNotified = true;
                foreach ($reassignedTasks as $reassignedTask) {
                    $reassignedTask->notified = Task::USER_NOTIFIED;
                    $reassignedTask->save(false);
                }
                if ($reassignedTaskCount == 1) {
                    $reassignedMessage = "A new Task was Reassigned To You";
                } else {
                    $reassignedMessage = "New Tasks were Reassigned To You";
                }
            }
            $notCompletedTasks = Task::model()->findAll('notified=:notified AND user_id=:user_id AND project_id=:project_id AND update_type=:update_type', array(
                ':notified' => Task::USER_NOT_RE_NOTIFIED,
                ':project_id' => $currentUser->current_project_id,
                ':update_type' => Task::UPDATE_TYPE_NOT_COMPLETE,
                ':user_id' => Yii::app()->user->id,
            ));

            // Change Task Status to not complete by supervisor

            if (count($notCompletedTasks) > 0) {
                $changeToNotCompleteStatus = true;
                $changeToNotCompleteCount = count($notCompletedTasks);
                $userNotified = true;
                foreach ($notCompletedTasks as $notCompletedTask) {
                    $notCompletedTask->notified = Task::USER_NOTIFIED;
                    $notCompletedTask->save(false);
                }
                if ($changeToRemindedCount == 1) {
                    $changeToNotCompleteMessage = "A Task to remember";
                } else {
                    $changeToNotCompleteMessage = "Tasks to remember";
                }
            }

            $remindedTasks =  Task::model()->findAll('notified=:notified AND user_id=:user_id AND project_id=:project_id AND update_type=:update_type', array(
                ':notified' => Task::USER_NOT_RE_NOTIFIED,
                ':project_id' => $currentUser->current_project_id,
                ':update_type' => Task::UPDATE_TYPE_REMINDED,
                ':user_id' => Yii::app()->user->id,
            ));
            if (count($remindedTasks) > 0) {
                $changeToRemindedStatus = true;
                $changeToRemindedCount = count($remindedTasks);
                $userNotified = true;
                foreach ($remindedTasks as $remindedTask) {
                    $remindedTask->notified = Task::USER_NOTIFIED;
                    $remindedTask->save(false);
                }
                if ($changeToRemindedCount == 1) {
                    $changeToRemindedMessage = "A Task to remember";
                } else {
                    $changeToRemindedMessage = "Tasks to remember";
                }
            }
            $todoStatus = false;
            $message = "";
            $taskCount = count($tasks);
            if ($taskCount > 0) {
                $todoStatus = true;
                $creatorId = $tasks[0]->create_id;
                foreach ($tasks as $task) {
                    $task->notified = Task::USER_NOTIFIED;
                    $task->save();
                }
                $user = User::model()->findByPk($creatorId);
                if ($taskCount == 1) {
                    $message = 'A new Task was added by: ' . $user->username;
                } else {
                    $message = $taskCount . ' new Tasks were added.';
                }
            }

            echo CJSON::encode(array(
                    'changeToRemindedStatus'=>$changeToRemindedStatus,
                    'changeToRemindedCount'=>$changeToRemindedCount,
                    'changeToRemindedMessage'=>$changeToRemindedMessage,
                    'reassignedByMemberStatus' => $reassignedByMemberStatus,
                    'reassignedByMemberTaskCount' => $reassignedByMemberTaskCount,
                    'reassignedByMemberMessage' => $reassignedByMemberMessage,
                    'changeToCompleteStatus' => $changeToCompleteStatus,
                    'changeToCompleteCount' => $changeToCompleteCount,
                    'changeToCompleteMessage' => $changeToCompleteMessage,
                    'changeToNotCompleteByMemberStatus' => $changeToNotCompleteByMemberStatus,
                    'changeToNotCompleteByMemberCount' => $changeToNotCompleteByMemberCount,
                    'changeToNotCompleteByMemberMessage' => $changeToNotCompleteByMemberMessage,
                    'changeToNotCompleteStatus' => $changeToNotCompleteStatus,
                    'changeToNotCompleteCount' => $changeToNotCompleteCount,
                    'changeToNotCompleteMessage' => $changeToNotCompleteMessage,
                    'todoStatus' => $todoStatus,
                    'taskCount' => $taskCount,
                    'message' => $message,
                    'followUpStatus' => $followUpTaskStatus,
                    'followUpMessage' => $followUpTaskMessage,
                    'followUpTaskCount' => $followUpTaskCount,
                    'reassignedStatus' => $reassignedStatus,
                    'reassignedTaskCount' => $reassignedTaskCount,
                    'reassignedMessage' => $reassignedMessage,
                    'supervisorNotified' => $supervisorNotified,
                    'userNotified' => $userNotified,
                    'followUpTaskCompleteMessage' => $followUpTaskCompleteMessage,
                    'followUpTaskCompleteStatus' => $followUpTaskCompleteStatus,
                    'followUpTaskCompleteCount' => $followUpTaskCompleteCount,
                    'followUpTaskNotCompleteMessage' => $followUpTaskNotCompleteMessage,
                    'followUpTaskNotCompleteStatus' => $followUpTaskNotCompleteStatus,
                    'followUpTaskNotCompleteCount' => $followUpTaskNotCompleteCount,
                    'followUpTaskReAssignMessage' => $followUpTaskReassignMessage,
                    'followUpTaskReAssignStatus' => $followUpTaskReassignStatus,
                    'followUpTaskReAssignCount' => $followUpTaskReassignCount,
                )
            );
        }
    }

    /**
     *
     *  This method is called Via Ajax GET request.
     *  this method return all members of a particalar project in the form of list elements
     *  @param integer $project_id the id of the Project model from which we want to get members
     *  @return string
     *  @dataType html
     *
     */

    public function actionGetMemberList($project_id)
    {
        if (Yii::app()->request->isAjaxRequest) {
            if (strlen($project_id) == 0) {
                echo '<option value="">Select Workspace to Display Users</option>';
                Yii::app()->end();
            }
            $project = Project::model()->find('id=:id', array(
                ':id' => $project_id,
            ));
            echo $this->renderPartial('member_list', array(
                'members' => $project->getMembersList(),
            ), false, true);
        }
    }

    /**
     *
     *  This method is called Via Ajax POST request.
     *  this method change the value of the user_id attribute for a particular Task model
     *  @param integer $_POST['Task']['id'] the id of the Task model for which we want to change the user_id attribute value
     *  @return string
     *  @dataType json
     *
     */

    public function actionChangeTaskAssignment()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $taskId = intval($_POST['Task']['id']);
            $taskModel = $this->loadModel($taskId);
            // Previous Task Owner
            $previousUser = $taskModel->user;
            // Let the supervisor now about the Task too ..

            $taskModel->update_type = Task::UPDATE_TYPE_REASSIGN;
            $taskModel->user_id = intval($_POST['Task']['user_id']);
            $taskModel->notified = Task::USER_NOT_RE_NOTIFIED;
            $taskModel->status = Task::STATE_NOT_READ;
            // The Task Current Owner
            $user = User::model()->findByPk($taskModel->user_id);
            $supervisors = $user->supervisors;

            if ($taskModel->save()) {
                //Add to Log
                $this->addStatusLog("<span style='color: #000;'>".Yii::app()->user->username." :</span> <span style='color: #5C5C5C;'>Send</span> ( ".$previousUser->username ." <span class='glyphicon glyphicon-arrow-right'></span> ".$user->username ." ) <span style='color: #000;'>|</span> " . date('j-m-Y h:i A', strtotime($taskModel->update_date)), $taskModel->id);
                // Setup the notification
                //Delete All Previous notification related to this task
                Yii::app()->db->createCommand()->delete('tbl_supervisor_notification','task_id=:task_id',array(
                    ':task_id'=>$taskModel->id,
                ));
                // Add new Notifications
                if ($previousUser->id == Yii::app()->user->id) {
                    foreach ($supervisors as $supervisorUser) {
                        $supervisorNotification = new SupervisorNotification;
                        $supervisorNotification->task_id = $taskModel->id;
                        $supervisorNotification->supervisor_id = $supervisorUser->id;
                        $supervisorNotification->supervisor_notified = SupervisorNotification::SUPERVISOR_NOT_NOTIFIED;
                        $supervisorNotification->project_id = $taskModel->project->id;
                        $supervisorNotification->update_type = SupervisorNotification::UPDATE_TYPE_TASK_REASSIGN;
                        $supervisorNotification->save();
                    }
                } else {
                    foreach ($supervisors as $supervisorUser) {
                        if ($supervisorUser->id != Yii::app()->user->id) {
                            $supervisorNotification = new SupervisorNotification;
                            $supervisorNotification->task_id = $taskModel->id;
                            $supervisorNotification->supervisor_id = $supervisorUser->id;
                            $supervisorNotification->supervisor_notified = SupervisorNotification::SUPERVISOR_NOT_NOTIFIED;
                            $supervisorNotification->project_id = $taskModel->project->id;
                            $supervisorNotification->update_type = SupervisorNotification::UPDATE_TYPE_TASK_REASSIGN;
                            $supervisorNotification->save();
                        }
                    }
                }
                echo CJSON::encode(array(
                    'status' => true,
                    'message' => 'Task Assignment Was changed , set to member:' . $user->username,
                ));
            } else {
                echo CJSON::encode(array(
                    'status' => true,
                    'message' => 'Couldn\'t Assign Task to member:' . $user->username,
                ));
            }
        }
    }

    /**
     *
     * Deletes a particular Task model.
     * @param integer $id the ID of the model to be deleted
     *
     */

    public function actionDelete()
    {
        if (Yii::app()->request->isAjaxRequest) {
            if (isset($_POST['id'])) {
                $id = intval($_POST['id']);
            } else {
                throw new CHttpException(400);
            }
            $task = Task::model()->findByPk($id);
            if (!is_null($task)) {
                if ($task->status == Task::STATE_PENDING) {
                    $task->delete();
                    echo CJSON::encode(array(
                        'state' => true,
                        'message' => 'The Task was deleted!',
                    ));
                } else {
                    echo CJSON::encode(array(
                        'state' => false,
                        'message' => 'Could not delete the task, it is on operation!',
                    ));
                }
            } else {
                echo CJSON::encode(array(
                    'state' => false,
                    'message' => 'There is no such task!',
                ));
            }
        }
    }


    /*public function actionGetStateChangeForm($id)
    {
        if (Yii::app()->request->isAjaxRequest) {
            $task = $this->loadModel($id);
            echo $this->renderPartial('change-state-form', array(
                    'task' => $task,
                ),
                false, true);
        }
    }*/

/*    public function actionChangeState()
    {
        if (Yii::app()->request->isAjaxRequest) {
            if (isset($_POST['Task']['id'], $_POST['Task']['status'])) {
                $task_id = intval($_POST['Task']['id']);
                $state = intval($_POST['Task']['status']);
                $task = Task::model()->findByPk($task_id);
                if (is_null($task)) {
                    echo CJSON::encode(array(
                        'state' => false,
                        'message' => 'There is no such Task!',
                    ));
                } else {
                    $bool = $task->setStatus($state);
                    $stateArray = Task::getStatusOptionsArray();
                    if ($bool) {
                        if ($task->save()) {
                            $message = 'The task is now: ' . $stateArray[$state];
                            echo CJSON::encode(array(
                                'state' => true,
                                'message' => $message,
                            ));
                        } else {
                            $message = "Could not change the state of the task!";
                            echo CJSON::encode(array(
                                'state' => false,
                                'message' => $message,
                            ));
                        }
                    } else {
                        $message = "Could not change the state of the task!";
                        echo CJSON::encode(array(
                            'state' => false,
                            'message' => $message,
                        ));
                    }


                }
            } else {
                throw new CHttpException(400);
            }
        } else {
            throw new CHttpException(400);
        }
    }

*/

    /**
     *
     *  This method is called Via Ajax GET request.
     *  It return an change priority task form for a particular Task.
     *  @param integer $id the id of the Task to be modified
     *  @return string
     *  @dataType html
     *
     */
    public function actionGetPriorityLevelChangeForm($id)
    {
        if (Yii::app()->request->isAjaxRequest) {
            $task = $this->loadModel($id);
            echo $this->renderPartial('change-priority-form', array(
                    'task' => $task,
                ),
                false, true);
        }
    }

    /**
     *
     *  This method is called Via Ajax POST request.
     *  this method change the value of the priority attribute for a particular Task model
     *  @param integer $_POST['Task']['id'] the id of the Task model for which we want to change the user_id attribute value
     *  @return string
     *  @dataType json
     *
     */

    public function actionChangePriority()
    {
        if (isset($_POST['Task']['id'], $_POST['Task']['priority'])) {
            $task_id = intval($_POST['Task']['id']);
            $priority = intval($_POST['Task']['priority']);
            $task = Task::model()->findByPk($task_id);
            if (is_null($task)) {
                echo CJSON::encode(array(
                    'state' => false,
                    'message' => 'There is no such Task!',
                ));
            } else {
                $task->priority = $priority;
                $priorityArray = Task::getPriorityOptionsArray();

                if ($task->save()) {
                    $this->addStatusLog("Priority Changed to: " . $priorityArray[$task->priority] . " by: " . Yii::app()->user->username . " | " . date('j-m-Y h:i A', strtotime($task->update_date)), $task->id);
                    $message = 'The task is now of priority level: ' . $priorityArray[$priority];
                    echo CJSON::encode(array(
                        'state' => true,
                        'message' => $message,
                    ));
                } else {
                    $message = "Could not change the priority level of the task!";
                    echo CJSON::encode(array(
                        'state' => false,
                        'message' => $message,
                    ));
                }

            }
        } else {
            throw new CHttpException(400);
        }

    }

    /**
     *
     *  This method is called Via Ajax GET request.
     *  @param integer $task_id the id of the Task model from which we want to get info
     *  @return string
     *  @dataType html
     *
     */

    public function actionGetTaskInfo($task_id)
    {
        $task = $this->loadModel($task_id);
        echo $this->renderPartial('task-info', array(
            'data' => $task,
        ), false, true);
    }

    /**
     *
     *  This method is called Via Ajax GET request.
     *  @param integer $task_id the id of the Task model for which we want to get option
     *  @return string
     *  @dataType html
     *
     */

    public function actionGetTaskOptions($task_id)
    {
        $task = $this->loadModel($task_id);
        echo $this->renderPartial('task-options', array(
            'taskModel' => $task,
        ), false, true);
    }


    //Methods for the Index Screen
    public function actionGetToDOListView()
    {
        //Get the current user model
        $currentUser = User::model()->findByPk(Yii::app()->user->id);
        $currentUser->role = $currentUser->getRole($currentUser->id);
        $taskModel = new Task('search');
        $taskModel->unsetAttributes();
        $taskModel->status = array(Task::STATE_NOT_READ, Task::STATE_INCOMPLETE);
        $taskModel->createUsername = "";
        $taskModel->user_id = $currentUser->id;

        if (isset($_GET['project_id'])) {
            $project_id = intval($_GET['project_id']);
            if ($project_id != 0) {
                $taskModel->project_id = $project_id;
            }
            $currentUser->current_project_id = $project_id;
            $currentUser->save(false);
        } else {
            // This is when using the ajax update for todo-grid-view
            if ($currentUser->current_project_id != 0) {
                $project_id = $currentUser->current_project_id;
                $taskModel->project_id = $project_id;
            }
        }
        if (isset($_GET['Task'])) {
            $taskModel->title = $_GET['Task']['title'];
            $taskModel->priority = $_GET['Task']['priority'];
            // Search By Data
            $taskModel->daysNo = $_GET['Task']['daysNo'];
            $searchTime = strtotime(date('Y-m-d')) - 3600 * 24 * $taskModel->daysNo;
            $taskModel->create_date = date('Y-m-d', $searchTime);
            //Search By User
            if (strlen($_GET['Task']['createUsername']) > 0) {
                $searchUser = User::model()->find('username=:username', array(
                    'username' => $_GET['Task']['createUsername'],
                ));
                if (!is_null($searchUser)) {
                    $taskModel->create_id = $searchUser->id;
                    $taskModel->createUsername = $searchUser->username;
                } else {
                    $taskModel->create_id = 0;
                    $taskModel->createUsername = $_GET['Task']['createUsername'];
                }
            }
            $taskModel->tag = $_GET['Task']['tag'];
        }

        echo $this->renderPartial('partials/todo-task-list', array(
            'taskModel' => $taskModel,
        ), false, true);
        Yii::app()->end();
    }
    // Task's comments management
    public function actionAddComment()
    {
        if (Yii::app()->request->isAjaxRequest) {
            if (strlen($_POST['comment_body']) > 0) {
                $comment = new Comment;
                $comment->body = $_POST['comment_body'];
                $comment->task_id = $_POST['task_id'];
                $comment->create_date = date('Y-m-d H:i:s');
                $comment->create_id = Yii::app()->user->id;
                $comment->status = Comment::STATUS_COMMENT;
                if ($comment->save()) {
                    echo CJSON::encode(array(
                        'status' => true,
                        'message' => 'save',
                        'taskId' => $comment->task_id,
                    ));
                } else {
                    echo CJSON::encode(array(
                        'status' => false,
                        'message' => $comment->errors,
                    ));
                }
            } else {
                echo CJSON::encode(array(
                    'status' => false,
                    'message' => 'no body',
                ));
            }
        }
    }
    // method to add a log entry for a particular Task
    public function addStatusLog($logMessage, $taskId)
    {
        $comment = new Comment;
        $comment->status = Comment::STATUS_LOG;
        $comment->body = $logMessage;
        $comment->task_id = $taskId;
        $comment->create_date = date('Y-m-d H:i:s');
        $comment->create_id = Yii::app()->user->id;
        $comment->save();
    }

    /**
     *
     *  This method is called Via Ajax POST request.
     *  \@param integer $_POST['Task']['id'] the id of the Task model we want to modify
     *  @return string
     *  @dataType json
     *
     */

    public function actionEditTaskInfo()
    {
        if (isset($_POST['Task'])) {
            $task = $this->loadModel(intval($_POST['Task']['id']));
            $task->title = $_POST['Task']['title'];
            $task->description = $_POST['Task']['description'];
            $task->tag = $_POST['Task']['tag'];
            $task->note = $_POST['Task']['note'];

            if (strlen($_POST['Task']['due_to_date']) > 0) {
                $task->due_to_date = date('Y-m-d',strtotime($_POST['Task']['due_to_date']));
            }
            if ($task->save()) {
                $this->addStatusLog("<span style='color: #000;'>".Yii::app()->user->username." :</span> <span style='color: #5C5C5C;'>Update</span> <span style='color: #000;'>|</span> " . date('j-m-Y h:i A', strtotime($task->update_date)), $task->id);
                $jsonArray = array(
                    'status' => true,
                    'id' => $task->id,
                    'title' => $task->title,
                    'titleShortcut' => strlen($task->title) <= 70 ? $task->title : substr($task->title, 0, 70) . " ...",
                    'description' => $task->description,
                    'note' => $task->note,
                    'message' => 'The Task was updated!',
                );
                if (!is_null($task->due_to_date)) {
                    $jsonArray['dueToDate'] = date("d-m-Y",strtotime($task->due_to_date));
                }
                echo CJSON::encode($jsonArray);
            } else {
                echo CJSON::encode(array(
                    'status' => false,
                    'message' => 'The Task failed to update!',
                ));
            }
        } else {
            throw new CHttpException(400);
        }
    }


    /**
     *
     *  This method is called Via Ajax POST request.
     *  this method change the value of the status attribute to Task::UPDATE_TYPE_COMPLETE for a particular Task model
     *  @param integer $_POST['task_id'] the id of the Task model for which we want to change the status attribute value
     *  @return string
     *  @dataType json
     *
     */

    public function actionChangeTaskStatusToComplete()
    {
        if (isset($_POST['task_id'])) {
            $currentUser = User::model()->findByPk(Yii::app()->user->id);
            $taskId = intval($_POST['task_id']);
            $task = $this->loadModel($taskId);
            $task->status = Task::STATE_COMPLETE;
            $task->reminded = Task::TASK_NOT_REMINDED;
            $task->notified = Task::USER_NOTIFIED;
            if ($task->save()) {
                // Add to Log
                $this->addStatusLog("<span style='color: #000;'>".Yii::app()->user->username. ": </span> <span style='color: #72B472;'>Complete</span> <span style='color: #000;'>|</span> " . date('j-m-Y h:i A', strtotime($task->update_date)), $task->id);
                $currentUserSupervisors = $currentUser->supervisors;
                Yii::app()->db->createCommand()->delete('tbl_supervisor_notification','task_id=:task_id',array(
                    ':task_id'=>$task->id,
                ));
                foreach ($currentUserSupervisors as $supervisorUser) {
                    $supervisorNotification = new SupervisorNotification;
                    $supervisorNotification->project_id = $currentUser->current_project_id;
                    $supervisorNotification->task_id = $task->id;
                    $supervisorNotification->supervisor_id = $supervisorUser->id;
                    $supervisorNotification->supervisor_notified = SupervisorNotification::SUPERVISOR_NOT_NOTIFIED;
                    $supervisorNotification->update_type = SupervisorNotification::UPDATE_TYPE_TASK_COMPLETE;
                    $supervisorNotification->save();
                }
                echo CJSON::encode(array(
                    'status' => true,
                    'message' => 'The Task is complete',
                ));
            } else {
                echo CJSON::encode(array(
                    'status' => false,
                    'message' => 'Error!',
                ));
            }
        } else {
            throw new CHttpException(400);
        }
    }

/*
    public function actionDeleteComment()
    {
        if (isset($_POST['comment_id'])) {
            $comment = Comment::model()->findByPk(intval($_POST['comment_id']));
            if ($comment->delete()) {
                $commentsCount = count(Comment::model()->findAll());
                echo CJSON::encode(array(
                    'status' => true,
                    'commentsCount' => $commentsCount,
                ));
            } else {
                echo CJSON::encode(array(
                    'status' => false,
                ));
            }

        } else {

        }
    }*/



    /* this method is called via Ajax GET request
    *
    *   It brings the comment of a particular task model when a task is seleted fromt the Task Grid List
    *   @param integer $task_id
    *   @return string
    *   @dataType json
    */

    public function actionGetTaskComments($task_id)
    {
        $task = $this->loadModel(intval($task_id));
        if ($task->user_id == Yii::app()->user->id) {
            $task->status = Task::STATE_INCOMPLETE;
            $task->save();
        }
        echo $this->renderCommentData($task->comments(array('order' => 'create_date')));
    }
    // This method is used by actionGetTaskComment method to render comments
    public function renderCommentData($comments)
    {
        return $this->renderPartial('partials/comments', array('comments' => $comments), false, true);
    }

    // This method is used to get back the change task assignment form of a particular Task model

    public function actionGetTaskAssignmentForm($id)
    {
        $taskModel = $this->loadModel($id);
        echo $this->renderPartial('change-task-assignment-form', array(
            'taskModel' => $taskModel,
        ), false, true);

    }

    public function actionAssignmentForm($id)
    {
        $taskModel = $this->loadModel($id);
        echo $this->renderPartial('change-task-assignment-form', array(
            'taskModel' => $taskModel,
        ), false, true);
    }

    /**
     *
     *  This method is called Via Ajax POST request.
     *  this method change the value of the status attribute to Task::UPDATE_TYPE_NOT_COMPLETE for a particular Task model
     *  @param integer $_POST['task_id'] the id of the Task model for which we want to change the status attribute value
     *  @return string
     *  @dataType json
     *
     */


    public function actionChangeTaskStatusToNotComplete()
    {
        if (isset($_POST['task_id'])) {
            $taskId = intval($_POST['task_id']);
            $task = $this->loadModel($taskId);
            $task->status = Task::STATE_INCOMPLETE;
            $task->reminded = Task::TASK_NOT_REMINDED;
            $taskUser = User::model()->with('supervisors')->findByPk($task->user_id);
            $supervisors = $taskUser->supervisors;
            $task->not_complete_return_count = $task->not_complete_return_count + 1;
            $task->update_type = Task::UPDATE_TYPE_NOT_COMPLETE;
            $currentUser = User::model()->findByPk(Yii::app()->user->id);
            if ($task->save()) {
                //Add to Log
                $this->addStatusLog("<span style='color: #000;'>".Yii::app()->user->username. ": </span> <span style='color: #D9534F;'>Not Complete</span> <span style='color: #000;'>|</span> " . date('j-m-Y h:i A', strtotime($task->update_date)), $task->id);
                Yii::app()->db->createCommand()->delete('tbl_supervisor_notification','task_id=:task_id',array(
                    ':task_id'=>$task->id,
                ));
                if ($task->user_id != Yii::app()->user->id) {
                    //Not the owner of the Task
                    $task->notified = Task::USER_NOT_RE_NOTIFIED;
                    $task->update_type = Task::UPDATE_TYPE_NOT_COMPLETE;
                    $task->save(false);
                    foreach ($supervisors as $supervisorUser) {
                        if ($supervisorUser->id != Yii::app()->user->id) {
                            $supervisorNotification = new SupervisorNotification;
                            $supervisorNotification->task_id = $task->id;
                            $supervisorNotification->supervisor_id = $supervisorUser->id;
                            $supervisorNotification->supervisor_notified = SupervisorNotification::SUPERVISOR_NOT_NOTIFIED;
                            $supervisorNotification->project_id = $currentUser->current_project_id;
                            $supervisorNotification->update_type = SupervisorNotification::UPDATE_TYPE_NOT_TASK_COMPLETE;
                            $supervisorNotification->save();
                        }
                    }
                } else {
                    foreach ($supervisors as $supervisorUser) {
                        $supervisorNotification = new SupervisorNotification;
                        $supervisorNotification->task_id = $task->id;
                        $supervisorNotification->supervisor_id = $supervisorUser->id;
                        $supervisorNotification->supervisor_notified = SupervisorNotification::SUPERVISOR_NOT_NOTIFIED;
                        $supervisorNotification->project_id = $currentUser->current_project_id;
                        $supervisorNotification->update_type = SupervisorNotification::UPDATE_TYPE_NOT_TASK_COMPLETE;
                        $supervisorNotification->save();
                    }
                }
                echo CJSON::encode(array(
                    'status' => true,
                    'message' => 'The Task is not complete',
                ));
            } else {
                echo CJSON::encode(array(
                    'status' => false,
                    'message' => 'Error!',
                ));
            }
        } else {
            throw new CHttpException(400);
        }
    }

    /**
     *
     *  This method is called Via Ajax POST request.
     *  this method change the value of the status attribute to Task::UPDATE_TYPE_VERIFIED for a particular Task model
     *  @param integer $_POST['task_id'] the id of the Task model for which we want to change the status attribute value
     *  @return string
     *  @dataType json
     *
     */
    public function actionChangeTaskStatusToClose()
    {
        if (isset($_POST['task_Ids'])) {
            if (strlen($_POST['task_Ids']) == 0) {
                echo CJSON::encode(array(
                    'status' => false,
                ));
            } else {
                $taskIds = $_POST['task_Ids'];
                $tasks = Task::model()->findAll("id in ($taskIds)");
                $deleteCount = 0;
                $status = false;
                if(!empty($tasks)){
                    $status = true;
                    foreach ($tasks as $task) {
                        $task->status = Task::STATE_VERIFIED;
                        $task->save(false);
                        $deleteCount += 1;
                        Yii::app()->db->createCommand()->delete('tbl_supervisor_notification','task_id=:task_id',array(
                            ':task_id'=>$task->id,
                        ));
                        $this->addStatusLog("<span style='color: #000;'>".Yii::app()->user->username." :</span> <span style='color: #5C5C5C;'>********** END **********</span>  <span style='color: #000;'>|</span> " . date('j-m-Y h:i A', strtotime($task->update_date)), $task->id);
                    }
                }
                echo CJSON::encode(array(
                    'status' => $status,
                    'deleteCount' => $deleteCount,
                ));
            }
        } else {
            throw new CHttpException(400);
        }
    }
    // Custom Grid Content ....
    public function renderToDoTaskActionsColumn($data, $row)
    {
        echo $this->renderPartial('partials/_todo-task-action-cell', array('task' => $data), false, false);
    }

    public function renderFollowUpTaskActionsColumn($data, $row)
    {
        echo $this->renderPartial('partials/_follow-up-task-action-cell', array('task' => $data), false, false);
    }

    public function renderCompleteTaskActionsColumn($data, $row)
    {
        echo $this->renderPartial('partials/_complete-task-action-cell', array('task' => $data), false, false);
    }

    public function renderCompleteTaskListHeaderOfActionColumn()
    {
        echo $this->renderPartial('partials/_complete-task-list-action-column-header', array(), false, false);
    }

    // This method is used by the end task grid to determine the content of the header cell of the Actions Column
    public function renderEndTaskDoneButton()
    {
        if (Yii::app()->user->checkAccess('end_task')) {
            return CHtml::button('End', array(
                'class' => 'btn btn-danger btn-sm',
                'style' => 'padding: 1px 4px !important',
                'id' => 'task-done-button',
            )) . " " . CHtml::checkBox('task_id_all', false, array(
                'class' => 'complete-grid-checkbox-all',
            ));
        } else {
            return false;
        }
    }

    // Filters and loaders

    public function loadModel($id)
    {
        $model = Task::model()->with('project')->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function loadProjectModel($id)
    {
        $model = Project::model()->findByPk($id);
        if ($model === null)
            $this->redirect(array('task/empty'));
        return $model;
    }


    // a Yii filter . It controls who can access the Task Manager actions
    public function filterProjectAccess($filterChain)
    {
        //Get the User Model
        if (!User::model()->isAdmin(Yii::app()->user->id)) {
            $user = User::model()->findByPk(Yii::app()->user->id);
            if ($user->isMemberInProject($this->_project->id)) {
                $filterChain->run();
            } else {
                throw new CHttpException(403);
            }
        } else {
            $filterChain->run();
        }
    }

    // a Yii filter . It ensures that the task manager is opened within a specific project context
    public function filterProjectContext($filterChain)
    {
        if (isset($_GET['project_id'])) {
            $this->_projectId = intval($_GET['project_id']);
            $this->_project = $this->loadProjectModel($this->_projectId);
            $filterChain->run();
        } else {
            throw new CHttpException(400, "Bad Request");
        }
    }

    /**
     * Performs the AJAX validation.
     * @param Task $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'task-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function filterCheckAccess($chain){
        if(!Yii::app()->user->isGuest){
            $chain->run();
        }else{
            Yii::app()->request->redirect(array('site/login'));
        }
    }
    public function filterEndTask($chain){
        if(Yii::app()->user->checkAccess('end_task') || Yii::app()->user->checkAccess('administrator')){
            $chain->run();
        }else{
            throw new CHttpException(403);
        }
    }
    public function filterReassign($chain){
        if(Yii::app()->user->checkAccess('reassign') || Yii::app()->user->checkAccess('administrator')){
            $chain->run();
        }else{
            throw new CHttpException(403);
        }
    }

    public function filterSupervisorAccess($chain){
        if(Yii::app()->user->checkAccess('project_supervisor') || Yii::app()->user->checkAccess('administrator')){
            $chain->run();
        }else{
            throw new CHttpException(403);
        }
    }
}
