<?php

class UserController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'checkAccess',
            'adminAccess - memberPasswordReset',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     */

    public function actionCreate()
    {
        $model = new User('create');
        $model->role = User::ROLE_MEMBER;
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['User'])) {

            $model->attributes = $_POST['User'];
            $model->status = User::STATE_OFFLINE;
            if ($model->validate()) {
                $model->first_name = strtolower($model->first_name);
                $model->last_name = strtolower($model->last_name);

                $model->save(false);
                switch ($model->role) {
                    case User::ROLE_ADMIN:
                        Yii::app()->authManager->assign('administrator', $model->id);
                        break;
                    case User::ROLE_PROJECT_SUPERVISOR:
                        Yii::app()->authManager->assign('project_supervisor', $model->id);
                        break;
                    case User::ROLE_MEMBER:
                        Yii::app()->authManager->assign('member', $model->id);
                        break;
                }

                if($model->viewArchivePermission){
                    Yii::app()->authManager->assign('view_archive', $model->id);
                }
                if($model->endTaskPermission){
                    Yii::app()->authManager->assign('end_task', $model->id);
                }
                if($model->reassignPermission){
                    Yii::app()->authManager->assign('reassign', $model->id);
                }
                if($model->viewReportPermission){
                    Yii::app()->authManager->assign('view_report', $model->id);
                }
                if($model->manageAutoTaskPermission){
                    Yii::app()->authManager->assign('manage_auto_tasks', $model->id);
                }

                $this->redirect(array('index'));
            }
        }
        if ($model->hasErrors()) {
            $model->password_repeat = "";
            $model->password = "";
        }
        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id the ID of the model to be updated
     */

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        $model->setScenario('update');
        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            if ($model->validate()) {
                $model->first_name = strtolower($model->first_name);
                $model->last_name = strtolower($model->last_name);
                $model->save(false);
                $model->clearAssignment();
                $model->clearPermissions();
                switch ($model->role) {
                    case User::ROLE_ADMIN:
                        Yii::app()->authManager->assign('administrator', $model->id);
                        break;
                    case User::ROLE_PROJECT_SUPERVISOR:
                        Yii::app()->authManager->assign('project_supervisor', $model->id);
                        break;
                    case User::ROLE_MEMBER:
                        Yii::app()->authManager->assign('member', $model->id);
                        break;
                }
                if($model->viewArchivePermission){
                    Yii::app()->authManager->assign('view_archive', $model->id);
                }
                if($model->endTaskPermission){
                    Yii::app()->authManager->assign('end_task', $model->id);
                }
                if($model->reassignPermission){
                    Yii::app()->authManager->assign('reassign', $model->id);
                }
                if($model->viewReportPermission){
                    Yii::app()->authManager->assign('view_report', $model->id);
                }
                if($model->manageAutoTaskPermission){
                    Yii::app()->authManager->assign('manage_auto_tasks', $model->id);
                }
                $this->redirect(array('index'));
            }
        } else {
            $model->role = User::model()->getRole($model->id);
            $model->reassignPermission = $model->hasReassignPermission($model->id);
            $model->endTaskPermission = $model->hasEndTaskPermission($model->id);
            $model->viewArchivePermission = $model->hasViewArchivePermission($model->id);
            $model->viewReportPermission = $model->hasViewReportPermission($model->id);
            $model->manageAutoTaskPermission = $model->hasManageAutoTaskPermission($model->id);
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * @param integer $id the ID of the User of type (member,supervisor) you want to change his password
     */
    public function actionMemberPasswordReset($id){
        $userModel = $this->loadModel(intval($id));
        $userModel->setScenario('member_password_reset');
        $userModel->role = $userModel->getRole($userModel->id);
        if (isset($_POST['User'])) {
            $userModel->oldPassword = $_POST['User']['oldPassword'];
            if($userModel->password != $userModel->hashPassword($userModel->oldPassword)){
                $userModel->addError('oldPassword','Old Password is not correct');
            }
            $userModel->password = $_POST['User']['password'];
            $userModel->password_repeat = $_POST['User']['password_repeat'];

            if(!$userModel->hasErrors('oldPassword')){
                if ($userModel->validate()) {
                    $userModel->password = $userModel->hashPassword($userModel->password);
                    if($userModel->save(false)){
                        if($userModel->role==User::ROLE_ADMIN){
                            $this->redirect(array('index'));
                        }else{
                            $this->redirect(array('task/index'));
                        }
                    }
                }
            }

        }
        $userModel->password_repeat = "";
        $userModel->password = "";
        $this->render('member-password-reset',array(
            'model'=>$userModel,
        ));
    }

    /**
     * @param integer $id the ID of the User of type (administrator) you want to change his password
     */
    public function actionPasswordReset($id)
    {
        $userModel = $this->loadModel(intval($id));
        $userModel->setScenario('password_reset');
        if (isset($_POST['User'])) {
            $userModel->password = $_POST['User']['password'];
            $userModel->password_repeat = $_POST['User']['password_repeat'];
            if ($userModel->validate()) {
                $userModel->password = $userModel->password = $userModel->hashPassword($userModel->password);
                if($userModel->save(false)){
                    $this->redirect(array('index'));
                }
            }
        }
        $userModel->password_repeat = "";
        $userModel->password = "";

        $this->render('password_reset', array(
            'model' => $userModel,
        ));
    }

    public function actionChangeStatus(){
        if(isset($_POST['status_code'])){
            $statusCode = intval($_POST['status_code']);
            $user = $this->loadModel(Yii::app()->user->id);
            $p_status_code = $user->status;
            if($user->setStatus($statusCode)){
                echo CJSON::encode(array(
                    'status'=>true,
                    'p_status_code'=>$p_status_code,
                    'status_code'=>$statusCode,
                ));
            }else{
                echo CJSON::encode(array(
                    'status'=>false,
                    'p_status_code'=>$p_status_code,
                ));
            }
        }else{
            throw new CHttpException(400);
        }
    }

    public function actionDelete($id)
    {
        try{
            if($this->loadModel($id)->delete()){
                echo CJSON::encode(array(
                    'message'=>'The User was deleted Successfully',
                    'status'=>true,
                ));
            }else{
                echo CJSON::encode(array(
                    'message'=>'Could not delete the user',
                    'status'=>false,
                ));
            }
        }catch (CDbException $e){
            echo CJSON::encode(array(
                'message'=>'Could not delete the user',
                'status'=>false,
            ));
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('User');
        $this->pageTitle = "All Registered Users in the system: ";
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */

    public function actionGetAssignToProjectForm($uId)
    {
        $user_id = intval($uId);
        $user = $this->loadModel($user_id);
        echo $this->renderPartial('assign-project-form', array(
            'user' => $user,
        ), false, true);
    }

    public function actionAssignProject()
    {
        if (Yii::app()->request->isAjaxRequest) {

            $project_id = $_POST['project_id'];
            $user_id = $_POST['User']['id'];
            $user = User::model()->findByPk($user_id);
            if ($user->assignToProject($project_id)) {
                echo CJSON::encode(array(
                    'status' => true,
                    'message' => 'The User Has been assigned',
                ));
            } else {
                echo CJSON::encode(array(
                    'status' => false,
                    'message' => "The User couldn't be assigned",
                ));
            }
        } else {
            throw new CHttpException(400);
        }
    }

    public function actionAdmin()
    {
        $model = new User('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['User']))
            $model->attributes = $_GET['User'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionEditFollowUpList($id){
        $user = User::model()->findByPk(intval($id));
        $followUpUsers = $user->followUpUsers;
        $users = User::model()->with('supervisors')->findAll();
        foreach($users as $u){
            //$u->supervisorId = Yii::app()->user->id;
            $u->supervisorId = $user->id;
        }
        $usersDataProvider = new CActiveDataProvider('User');
        $usersDataProvider->setData($users);
        $groupsDataProvider = new CActiveDataProvider('Group');

        $this->render('edit-user-followup-list',array(
            'usersDataProvider'=>$usersDataProvider,
            'groupsDataProvider'=>$groupsDataProvider,
            'followUpUsers'=>$followUpUsers,
            'supervisorId'=>$user->id,
            'supervisor'=>$user,
        ));
    }

    public function actionUpdateFList(){
        if(Yii::app()->request->isAjaxRequest){
            $supervisorId = $_POST['supervisor_id'];
            $supervisor = User::model()->findByPk($supervisorId);
            $userIds = array();
            if(isset($_POST['group_id'])){
                $groupIds = $_POST['group_id'];
                foreach($groupIds as $gId){
                    $group = Group::model()->findByPk($gId);
                    $groupUsers = $group->users;
                    foreach($groupUsers as $gU){
                        $userIds[] = intval($gU->id);
                    }
                }
            }
            if(isset($_POST['user_id'])){
                $uIds = $_POST['user_id'];
                foreach($uIds as $id){
                    $userIds[] = intval($id);
                }
            }
            $userIds = array_unique($userIds);
            $followUpUsers = $supervisor->followUpUsers;
            $followUpUserIds = array();
            foreach($followUpUsers as $fu){
                $followUpUserIds[] = $fu->id;
            }
            $userIds = array_diff($userIds,$followUpUserIds);
            foreach($userIds as $id){
                $supervisor->addToFollowUpList($id);
            }
            echo CJSON::encode(array(
                'status'=>true,
                'sId'=>$supervisor->id,
            ));
        }
    }

    public function actionRemoveFromFollowList(){
        if(isset($_POST['uId'],$_POST['sId'])){
            $user_id = intval($_POST['uId']);
            $supervisor_id = intval($_POST['sId']);
            $supervisor = User::model()->findByPk($supervisor_id);
            if($supervisor->removeFromFollowUpList($user_id)){
                echo CJSON::encode(array(
                    'status'=>true,
                ));
            }else{
                echo CJSON::encode(array(
                    'status'=>false,
                ));
            }
        }
    }
    public function actionGetFollowingUserList($id){
        $user = User::model()->findByPk(intval($id));
        $followUpUsers = $user->followUpUsers;
        echo $this->renderPartial('partials/_follow-up-list',array(
            'followUpUsers'=>$followUpUsers,
            'supervisorId'=>$user->id,
            'supervisor'=>$user,
        ),false,true);
    }


    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return User the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = User::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    public function filterCheckAccess($chain){
        if(!Yii::app()->user->isGuest){
            $chain->run();
        }else{
            Yii::app()->request->redirect(array('site/login'));
        }
    }
    public function filterAdminAccess($chain){
        if(Yii::app()->user->checkAccess('administrator')){
            $chain->run();
        }else{
            throw new CHttpException(403);
        }
    }
    /**
     * Performs the AJAX validation.
     * @param User $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
