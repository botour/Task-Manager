<?php

class AutotaskCommand extends CConsoleCommand
{
    public function getHelp()
    {
        $description = "DESCRIPTION\n";
        $description .= ' ' . "This command generates auto tasks.\n";
        return parent::getHelp() . $description;
    }
    public function actionIndex()
    {
        $todayAutoTask = AutoTask::model()->findAll('status=:status AND activation_date=:activation_date', array(
            ':status' => AutoTask::STATUS_ENABLED,
            ':activation_date' => date('Y-m-d'),
        ));

        foreach ($todayAutoTask as $autoTask) {
            $this->createTask($autoTask);
        }

        $upcommingAutoTasks = AutoTask::model()->findAll('status=:status AND ((activation_date<:activation_date AND end_type=:end_type) OR (activation_date<:activation_date AND end_repeat>=:end_repeat AND end_type=:end_type_specific))',array(
            ':status'=>AutoTask::STATUS_ENABLED,
            ':activation_date'=>date('Y-m-d'),
            ':end_repeat'=>date('Y-m-d'),
            ':end_type'=>AutoTask::END_TYPE_NEVER,
            ':end_type_specific'=>AutoTask::END_TYPE_SPECIFIC_DATE,
        ));

        foreach($upcommingAutoTasks as $autoTask){
            switch($autoTask->repeat_type){
                case AutoTask::REPEAT_TYPE_DAILY:
                    $this->createTask($autoTask,true);
                    break;
                case AutoTask::REPEAT_TYPE_WEEKLY:
                    if(date('D',strtotime($autoTask->activation_date))==date('D',time())){
                        $this->createTask($autoTask,true);
                    }
                    break;
                case AutoTask::REPEAT_TYPE_MONTHLY:
                    if(date('j',strtotime($autoTask->activation_date))==date('j',time())){
                        $this->createTask($autoTask,true);
                    }
                    break;
                case AutoTask::REPEAT_TYPE_YEARLY:
                    if(date('j',strtotime($autoTask->activation_date))==date('j',time()) && date('n',strtotime($autoTask->activation_date))==date('n',time())){
                        $this->createTask($autoTask,true);
                    }
                    break;
            }
        }
    }
    public function actionDelete()
    {

    }
    public function createTask($autoTask,$absolute_status = false)
    {
        $task = new Task('create');
        $task->title = $autoTask->title;
        $task->description = $autoTask->description;
        $task->priority = $autoTask->priority;
        if($absolute_status){
            if($autoTask->due_to_date!="0000-00-00"){
                $days_offset = strtotime($autoTask->due_to_date)-strtotime($autoTask->activation_date);
                $dueToDateTime = time()+$days_offset;
                $task->due_to_date = date('Y-m-d',$dueToDateTime);
            }else{
                $task->due_to_date = $autoTask->due_to_date;
            }
        }else{
            $task->due_to_date = $autoTask->due_to_date;
        }
        $task->note = $autoTask->note;
        $task->tag = $autoTask->tag;
        $task->project_id = $autoTask->project_id;
        $task->user_id = $autoTask->user_id;
        $task->type = Task::TYPE_AUTOMATIC;
        $task->second_priority = $autoTask->second_priority;
        $task->second_priority_days = $autoTask->second_priority_days;
        $task->third_priority = $autoTask->third_priority;
        $task->third_priority_days = $autoTask->third_priority_days;
        $task->reminded = Task::TASK_NOT_REMINDED;
        $task->notified = Task::USER_NOT_NOTIFIED;
        $task->create_id = $autoTask->create_id;
        $task->create_date = date('Y-m-d');
        $task->skipBeforeSave = true;
        if($task->type==Task::TYPE_AUTOMATIC){
            $tasksCount = 0;
            $tasksCount = intval($count = Task::model()->count('YEAR(create_date)=:date AND type=:type',array(
                ':date'=>date('Y'),
                ':type'=>Task::TYPE_AUTOMATIC,
            )));
            $task->serial_no = $autoTask->serial_no."-".$tasksCount;
        }
        $task->status = Task::STATE_NOT_READ;
        if($task->save()){
            $autoTask->last_repeat_date = date("Y-m-d");
            $autoTask->skipBeforeSave = true;
            $autoTask->occurrence = $autoTask->occurrence + 1;
            $autoTask->save(false);
            // Generate Jobs
            if($autoTask->second_priority != AutoTask::SECOND_PRIORITY_NEVER){
                $firstJob = new Job;
                $firstJob->task_id = $task->id;
                $firstJob->date = date('Y-m-d',strtotime($task->create_date) + $autoTask->second_priority_days * 3600 * 24);
                switch($autoTask->second_priority){
                    case AutoTask::SECOND_PRIORITY_LOW:
                        $firstJob->priority = Task::PRIORITY_LOW;
                        break;
                    case AutoTask::SECOND_PRIORITY_MEDIUM:
                        $firstJob->priority = Task::PRIORITY_MEDIUM;
                        break;
                    case AutoTask::SECOND_PRIORITY_HIGH:
                        $firstJob->priority = Task::PRIORITY_HIGH;
                        break;
                }
                $firstJob->save();
            }
            if($autoTask->third_priority != AutoTask::THIRD_PRIORITY_NEVER){
                $secondJob = new Job;
                $secondJob->task_id = $task->id;
                $secondJob->date = date('Y-m-d',strtotime($task->create_date) + $autoTask->third_priority_days * 3600 * 24);
                switch($autoTask->third_priority){
                    case AutoTask::THIRD_PRIORITY_LOW:
                        $secondJob->priority = Task::PRIORITY_LOW;
                        break;
                    case AutoTask::THIRD_PRIORITY_MEDIUM:
                        $secondJob->priority = Task::PRIORITY_MEDIUM;
                        break;
                    case AutoTask::THIRD_PRIORITY_HIGH:
                        $secondJob->priority = Task::PRIORITY_HIGH;
                        break;
                }
                $secondJob->save();
            }
        }
        $this->addStatusLog("<span style='color: #000;'>System :</span> <span style='color: #5C5C5C;'>Create</span>  <span class='glyphicon glyphicon-arrow-right'></span> ".$task->user->username . " <span style='color: #000;'>|</span> " . date('Y-m-j h:i A', strtotime($task->create_date)),$task);
        $supervisors = User::model()->with('supervisors')->findByPk($task->user_id)->supervisors;

        foreach ($supervisors as $supervisorUser) {
            $supervisorNotification = new SupervisorNotification;
            $supervisorNotification->task_id = $task->id;
            $supervisorNotification->supervisor_id = $supervisorUser->id;
            $supervisorNotification->supervisor_notified = SupervisorNotification::SUPERVISOR_NOT_NOTIFIED;
            $supervisorNotification->project_id = $task->project->id;
            $supervisorNotification->update_type = SupervisorNotification::UPDATE_TYPE_TASK_CREATE;
            $supervisorNotification->save();
        }

        $jobs = Job::model()->with('task')->findAll('date=:date',array(
            ':date'=>date('Y-m-d'),
        ));
        foreach($jobs as $job){
            $taskToModify = $job->task;
            $taskToModify->skipBeforeSave = true;
            $taskToModify->priority = $job->priority;
            $taskToModify->save(false);
            $job->delete();
        }

    }


    // method to add a log entry for a particular Task
    public function addStatusLog($logMessage, $task)
    {
        $comment = new Comment;
        $comment->status = Comment::STATUS_LOG;
        $comment->body = $logMessage;
        $comment->task_id = $task->id;
        $comment->create_date = date('Y-m-d H:i:s');
        $comment->create_id = $task->create_id;
        $comment->save();
    }
}