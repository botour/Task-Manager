<?php

class RbacCommand extends CConsoleCommand
{
    private $_authManager;

    public function getHelp()
    {
        $description = "DESCRIPTION\n";
        $description .= ' ' . "This command generates an initial RBAC authorization hierarchy For ProjStar Project.\n";
        return parent::getHelp() . $description;
    }

    public function actionIndex()
    {
        $this->ensureAuthManagerDefined();
        $message = "Hey This is Borhan :):This command will create three roles: administrator, project manager, and member\n";
        $message .= "Would you like to continue?";
        if ($this->confirm($message)) {
            $this->_authManager->clearAll();
            $this->_authManager->createOperation(
                "createTask",
                "create a new Task"
            );
            $this->_authManager->createOperation(
                "updateTask",
                "update a Task"
            );
            $role = $this->_authManager->createRole("member");
            $role->addChild("createTask");
            $role->addChild("updateTask");
            $this->_authManager->createOperation(
                "deleteProject"
            );
            $this->_authManager->createOperation(
                "viewProject"
            );
            $this->_authManager->createOperation(
                "updateProject"
            );
            $this->_authManager->createOperation(
                "createProject"
            );

            $this->_authManager->createOperation(
                "deleteUser"
            );
            $this->_authManager->createOperation(
                "viewUser"
            );
            $this->_authManager->createOperation(
                "updateUser"
            );
            $this->_authManager->createOperation(
                "end_task"
            );

            $this->_authManager->createOperation(
                "reassign"
            );
            $this->_authManager->createOperation(
                "view_archive"
            );
            $this->_authManager->createOperation(
                "view_report"
            );
            $this->_authManager->createOperation(
                "createUser"
            );

            $projectManagement = $this->_authManager->createTask("ProjectManagement", 'Manage Projects(create,update,delete,Reply)');
            $projectManagement->addChild("deleteProject");
            $projectManagement->addChild("viewProject");
            $projectManagement->addChild("updateProject");
            $projectManagement->addChild("createProject");

            $userManagement = $this->_authManager->createTask("UserManagement", 'Manage Projects(create,update,delete,Reply)');
            $userManagement->addChild("deleteUser");
            $userManagement->addChild("viewUser");
            $userManagement->addChild("updateUser");
            $userManagement->addChild("createUser");

            $adminRole = $this->_authManager->createRole("administrator");
            $adminRole->addChild('ProjectManagement');
            $adminRole->addChild('UserManagement');
            $adminRole->addChild('member');
            $projectManagerRole = $this->_authManager->createRole("project_manager");
            $projectManagerRole->addChild('ProjectManagement');

            echo "Authorization hierarchy successfully generated.\n";

        } else {
            echo "Operation cancelled.\n";
        }
    }

    public function actionDelete()
    {
        $this->ensureAuthManagerDefined();
        $message = "This command will clear all RBAC definitions.\n";
        $message .= "Would you like to continue?";
        if ($this->confirm($message)) {
            $this->_authManager->clearAll();
            echo "Authorization hierarchy removed.\n";
        } else {
            echo "Delete operation cancelled.\n";
        }
    }

    protected function ensureAuthManagerDefined()
    {
        if (($this->_authManager = Yii::app()->authManager) === null) {
            $message = "Error: an authorization manager, named 'authManager' must be con-figured to use this command.";
            $this->usageError($message);
        }
    }
}