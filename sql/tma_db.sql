-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 01, 2017 at 04:49 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tma_db`
--
CREATE DATABASE IF NOT EXISTS `tma_db` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `tma_db`;

-- --------------------------------------------------------

--
-- Table structure for table `authassignment`
--

DROP TABLE IF EXISTS `authassignment`;
CREATE TABLE `authassignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` varchar(64) NOT NULL,
  `bizrule` text,
  `data` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `authassignment`
--

INSERT INTO `authassignment` (`itemname`, `userid`, `bizrule`, `data`) VALUES
('administrator', '1', NULL, 'N;'),
('end_task', '1', NULL, 'N;'),
('end_task', '2', NULL, 'N;'),
('end_task', '3', NULL, 'N;'),
('manage_auto_tasks', '1', NULL, 'N;'),
('manage_auto_tasks', '2', NULL, 'N;'),
('manage_auto_tasks', '3', NULL, 'N;'),
('manage_auto_tasks', '5', NULL, 'N;'),
('member', '4', NULL, 'N;'),
('member', '5', NULL, 'N;'),
('project_supervisor', '2', NULL, 'N;'),
('project_supervisor', '3', NULL, 'N;'),
('reassign', '2', NULL, 'N;'),
('reassign', '3', NULL, 'N;'),
('view_archive', '1', NULL, 'N;'),
('view_archive', '2', NULL, 'N;'),
('view_archive', '3', NULL, 'N;'),
('view_archive', '4', NULL, 'N;'),
('view_archive', '5', NULL, 'N;'),
('view_report', '2', NULL, 'N;'),
('view_report', '4', NULL, 'N;');

-- --------------------------------------------------------

--
-- Table structure for table `authitem`
--

DROP TABLE IF EXISTS `authitem`;
CREATE TABLE `authitem` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `authitem`
--

INSERT INTO `authitem` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('administrator', 2, '', NULL, 'N;'),
('createProject', 0, '', NULL, 'N;'),
('createTask', 0, 'create a new Task', NULL, 'N;'),
('createUser', 0, '', NULL, 'N;'),
('deleteProject', 0, '', NULL, 'N;'),
('deleteUser', 0, '', NULL, 'N;'),
('end_task', 0, '', NULL, 'N;'),
('manage_auto_tasks', 0, NULL, NULL, NULL),
('member', 2, '', NULL, 'N;'),
('ProjectManagement', 1, 'Manage Projects(create,update,delete,Reply)', NULL, 'N;'),
('project_supervisor', 2, '', NULL, 'N;'),
('reassign', 0, '', NULL, 'N;'),
('updateProject', 0, '', NULL, 'N;'),
('updateTask', 0, 'update a Task', NULL, 'N;'),
('updateUser', 0, '', NULL, 'N;'),
('UserManagement', 1, 'Manage Projects(create,update,delete,Reply)', NULL, 'N;'),
('viewProject', 0, '', NULL, 'N;'),
('viewUser', 0, '', NULL, 'N;'),
('view_archive', 0, '', NULL, 'N;'),
('view_report', 0, '', NULL, 'N;');

-- --------------------------------------------------------

--
-- Table structure for table `authitemchild`
--

DROP TABLE IF EXISTS `authitemchild`;
CREATE TABLE `authitemchild` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `authitemchild`
--

INSERT INTO `authitemchild` (`parent`, `child`) VALUES
('administrator', 'member'),
('administrator', 'ProjectManagement'),
('administrator', 'UserManagement'),
('member', 'createTask'),
('member', 'updateTask'),
('ProjectManagement', 'createProject'),
('ProjectManagement', 'deleteProject'),
('ProjectManagement', 'updateProject'),
('ProjectManagement', 'viewProject'),
('project_supervisor', 'ProjectManagement'),
('UserManagement', 'createUser'),
('UserManagement', 'deleteUser'),
('UserManagement', 'updateUser'),
('UserManagement', 'viewUser');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_auto_task`
--

DROP TABLE IF EXISTS `tbl_auto_task`;
CREATE TABLE `tbl_auto_task` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `serial_no` varchar(55) NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '(enabled - disabled)',
  `priority` tinyint(4) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` tinytext NOT NULL,
  `note` tinytext NOT NULL,
  `tag` varchar(255) NOT NULL,
  `occurrence` int(11) NOT NULL,
  `activation_date` date NOT NULL,
  `repeat_type` tinyint(4) NOT NULL COMMENT '(never - daily - monthly - yearly)',
  `last_repeat_date` datetime DEFAULT NULL,
  `end_type` int(11) NOT NULL COMMENT '(never - specifi date)',
  `end_repeat` date DEFAULT NULL,
  `due_to_date` date DEFAULT NULL,
  `second_priority` tinyint(4) NOT NULL COMMENT '(None - low - medium - high)',
  `second_priority_days` tinyint(4) DEFAULT NULL COMMENT '(None - low - medium - high)',
  `third_priority` tinyint(4) NOT NULL COMMENT '(None - low - medium - high)',
  `third_priority_days` tinyint(4) NOT NULL,
  `create_date` int(11) NOT NULL,
  `create_id` int(11) NOT NULL,
  `update_date` datetime DEFAULT NULL,
  `update_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_auto_task`
--

INSERT INTO `tbl_auto_task` (`id`, `user_id`, `project_id`, `serial_no`, `status`, `priority`, `title`, `description`, `note`, `tag`, `occurrence`, `activation_date`, `repeat_type`, `last_repeat_date`, `end_type`, `end_repeat`, `due_to_date`, `second_priority`, `second_priority_days`, `third_priority`, `third_priority_days`, `create_date`, `create_id`, `update_date`, `update_id`) VALUES
(6, 4, 1, 'W15-1', 1, 1, 'New Automatic Task of the day', 'Descriptio', '', '', 23, '2015-02-14', 1, '2015-02-14 00:00:00', 1, '2015-02-22', '0000-00-00', 0, NULL, 1, 3, 2015, 1, '2015-02-16 09:07:17', 1),
(7, 4, 1, 'M15-1', 0, 0, 'This is a title', '', '', '', 0, '2015-02-17', 3, NULL, 0, NULL, '2015-02-18', 0, NULL, 0, 0, 2015, 1, '2015-02-16 08:24:44', 1),
(8, 4, 1, 'W15-1', 1, 0, 'This is a test for datepickers', '', '', '', 0, '2015-02-17', 2, NULL, 1, '2015-02-22', '2015-02-19', 0, NULL, 0, 0, 2015, 1, '2015-02-16 08:32:48', 1),
(9, 2, 1, 'W15-1', 1, 0, 'This is a new Task', '', '', '', 0, '2015-02-16', 2, NULL, 0, NULL, '2015-02-18', 0, NULL, 0, 0, 2015, 1, '2015-02-16 08:55:09', 1),
(10, 4, 1, 'W15-1', 1, 0, 'This is a test for due date', '', '', '', 0, '2015-02-17', 2, NULL, 0, NULL, '0000-00-00', 0, NULL, 0, 0, 2015, 1, '2015-02-16 08:58:15', 1),
(11, 4, 1, 'W15-1', 1, 0, 'This is a test for due to date', '', '', '', 0, '2015-02-25', 2, NULL, 0, NULL, '2015-02-28', 0, NULL, 0, 0, 2015, 1, '2015-02-16 08:59:24', 1),
(12, 4, 1, 'W15-1', 1, 0, 'This is a new AutoTask', '', '', '', 0, '2015-02-10', 2, NULL, 0, NULL, '0000-00-00', 0, NULL, 0, 0, 2015, 1, '2015-02-16 09:05:17', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_auto_task_comment`
--

DROP TABLE IF EXISTS `tbl_auto_task_comment`;
CREATE TABLE `tbl_auto_task_comment` (
  `id` int(11) NOT NULL,
  `auto_task_id` int(11) NOT NULL,
  `body` tinytext NOT NULL,
  `create_date` datetime NOT NULL,
  `create_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_auto_task_comment`
--

INSERT INTO `tbl_auto_task_comment` (`id`, `auto_task_id`, `body`, `create_date`, `create_id`, `status`) VALUES
(25, 6, 'created by: burhan | 2015-Feb-12 05:38 PM', '2015-02-12 17:38:31', 1, 0),
(26, 6, 'updated by: burhan | 16-02-2015 08:22 AM', '2015-02-16 08:22:03', 1, 0),
(27, 6, 'updated by: burhan | 16-02-2015 08:22 AM', '2015-02-16 08:22:49', 1, 0),
(28, 7, 'created by: burhan | 16-02-2015 08:24 AM', '2015-02-16 08:24:44', 1, 0),
(29, 8, 'created by: burhan | 16-02-2015 08:32 AM', '2015-02-16 08:32:49', 1, 0),
(30, 6, 'updated by: burhan | 16-02-2015 08:36 AM', '2015-02-16 08:36:17', 1, 0),
(31, 9, 'created by: burhan | 16-02-2015 08:55 AM', '2015-02-16 08:55:09', 1, 0),
(32, 10, 'created by: burhan | 16-02-2015 08:58 AM', '2015-02-16 08:58:16', 1, 0),
(33, 11, 'created by: burhan | 16-02-2015 08:59 AM', '2015-02-16 08:59:25', 1, 0),
(34, 12, '<span style=\'color: #000;\'>burhan :</span> <span style=\'color: #5C5C5C;\'>Create</span>  <span class=\'glyphicon glyphicon-arrow-right\'></span> omar <span style=\'color: #000;\'>|</span> 16-02-2015 09:03 AM', '2015-02-16 09:03:28', 1, 0),
(35, 12, '<span style=\'color: #000;\'>burhan :</span> <span style=\'color: #5C5C5C;\'>Update</span> <span style=\'color: #000;\'>|</span> 16-02-2015 09:05 AM', '2015-02-16 09:05:17', 1, 0),
(36, 6, '<span style=\'color: #000;\'>burhan :</span> <span style=\'color: #5C5C5C;\'>Update</span> <span style=\'color: #000;\'>|</span> 16-02-2015 09:07 AM', '2015-02-16 09:07:17', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_auto_task_param`
--

DROP TABLE IF EXISTS `tbl_auto_task_param`;
CREATE TABLE `tbl_auto_task_param` (
  `id` int(11) NOT NULL,
  `tbl_auto_task_id` int(11) NOT NULL,
  `repeat_every` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_comment`
--

DROP TABLE IF EXISTS `tbl_comment`;
CREATE TABLE `tbl_comment` (
  `id` int(11) NOT NULL,
  `body` tinytext NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `task_id` int(11) NOT NULL,
  `create_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_comment`
--

INSERT INTO `tbl_comment` (`id`, `body`, `create_date`, `status`, `task_id`, `create_id`) VALUES
(53, 'mark as complete by: omar | 2015-Feb-12 12:43 PM', '2015-02-12 12:43:47', 0, 7, 4),
(54, 'mark as verified task by: maher | 2015-Feb-12 12:44 PM', '2015-02-12 12:44:14', 0, 7, 2),
(55, 'created by: maher | 2015-Feb-12 12:44 PM', '2015-02-12 12:44:45', 0, 8, 2),
(56, 'mark as complete by: burhan | 2015-Feb-12 12:47 PM', '2015-02-12 12:47:14', 0, 8, 1),
(57, 'mark as not complete by: burhan | 2015-Feb-12 12:47 PM', '2015-02-12 12:47:33', 0, 8, 1),
(58, 'mark as complete by: burhan | 2015-Feb-12 12:47 PM', '2015-02-12 12:47:40', 0, 8, 1),
(59, 'mark as verified task by: burhan | 2015-Feb-12 12:48 PM', '2015-02-12 12:48:40', 0, 8, 1),
(60, 'created by: burhan | 2015-Feb-12 12:49 PM', '2015-02-12 12:49:16', 0, 9, 1),
(61, 'created by: burhan | 2015-Feb-12 12:49 PM', '2015-02-12 12:49:44', 0, 10, 1),
(62, 'mark as complete by: burhan | 2015-Feb-12 12:49 PM', '2015-02-12 12:49:55', 0, 9, 1),
(63, 'mark as complete by: burhan | 2015-Feb-12 12:49 PM', '2015-02-12 12:49:58', 0, 10, 1),
(64, 'mark as complete by: burhan | 2015-Feb-12 12:50 PM', '2015-02-12 12:50:04', 0, 9, 1),
(65, 'mark as verified task by: burhan | 2015-Feb-12 12:50 PM', '2015-02-12 12:50:37', 0, 9, 1),
(66, 'mark as verified task by: burhan | 2015-Feb-12 12:50 PM', '2015-02-12 12:50:38', 0, 10, 1),
(67, 'created by: burhan | 2015-Feb-12 12:52 PM', '2015-02-12 12:52:40', 0, 11, 1),
(68, 'created by: burhan | 2015-Feb-12 12:52 PM', '2015-02-12 12:52:52', 0, 12, 1),
(69, 'mark as complete by: burhan | 2015-Feb-12 12:53 PM', '2015-02-12 12:53:10', 0, 11, 1),
(70, 'mark as complete by: burhan | 2015-Feb-12 12:53 PM', '2015-02-12 12:53:39', 0, 12, 1),
(71, 'mark as verified task by: burhan | 2015-Feb-12 12:53 PM', '2015-02-12 12:53:50', 0, 11, 1),
(72, 'mark as verified task by: burhan | 2015-Feb-12 12:53 PM', '2015-02-12 12:53:52', 0, 12, 1),
(73, 'mark as complete by: burhan | 2015-Feb-12 02:11 PM', '2015-02-12 14:11:06', 0, 10, 1),
(74, 'mark as complete by: burhan | 2015-Feb-12 02:55 PM', '2015-02-12 14:55:56', 0, 9, 1),
(75, 'mark as verified task by: burhan | 2015-Feb-12 02:56 PM', '2015-02-12 14:56:09', 0, 9, 1),
(76, 'created by: burhan | 2015-Feb-12 02:57 PM', '2015-02-12 14:57:42', 0, 13, 1),
(77, 'created by: burhan | 2015-Feb-12 02:57 PM', '2015-02-12 14:57:48', 0, 14, 1),
(78, 'created by: burhan | 2015-Feb-12 02:57 PM', '2015-02-12 14:57:57', 0, 15, 1),
(79, 'created by: burhan | 2015-Feb-12 02:58 PM', '2015-02-12 14:58:24', 0, 16, 1),
(80, 'created by: burhan | 2015-Feb-12 02:58 PM', '2015-02-12 14:58:32', 0, 17, 1),
(81, 'created by: burhan | 2015-Feb-12 02:58 PM', '2015-02-12 14:58:51', 0, 18, 1),
(82, 'created by: burhan | 2015-Feb-12 02:59 PM', '2015-02-12 14:59:56', 0, 19, 1),
(83, 'created by: burhan | 2015-Feb-12 03:00 PM', '2015-02-12 15:00:05', 0, 20, 1),
(84, 'created by: burhan | 2015-Feb-12 03:00 PM', '2015-02-12 15:00:13', 0, 21, 1),
(85, 'created by: burhan | 2015-Feb-12 03:00 PM', '2015-02-12 15:00:21', 0, 22, 1),
(86, 'created by: burhan | 2015-Feb-12 03:00 PM', '2015-02-12 15:00:26', 0, 23, 1),
(87, 'mark as complete by: burhan | 2015-Feb-12 03:03 PM', '2015-02-12 15:03:05', 0, 10, 1),
(88, 'mark as complete by: burhan | 2015-Feb-12 03:03 PM', '2015-02-12 15:03:12', 0, 12, 1),
(89, 'mark as complete by: burhan | 2015-Feb-12 03:03 PM', '2015-02-12 15:03:21', 0, 8, 1),
(90, 'mark as complete by: burhan | 2015-Feb-12 03:03 PM', '2015-02-12 15:03:28', 0, 1, 1),
(91, 'mark as verified task by: burhan | 2015-Feb-12 03:03 PM', '2015-02-12 15:03:37', 0, 1, 1),
(92, 'mark as verified task by: burhan | 2015-Feb-12 03:03 PM', '2015-02-12 15:03:37', 0, 8, 1),
(93, 'mark as verified task by: burhan | 2015-Feb-12 03:03 PM', '2015-02-12 15:03:37', 0, 10, 1),
(94, 'mark as verified task by: burhan | 2015-Feb-12 03:03 PM', '2015-02-12 15:03:38', 0, 12, 1),
(95, 'created Automatically by the system', '2015-02-12 19:03:24', 0, 26, 1),
(96, 'created Automatically by the system', '2015-02-12 19:05:50', 0, 27, 1),
(97, 'created Automatically by the system', '2015-02-12 19:06:06', 0, 28, 1),
(98, 'created Automatically by the system', '2015-02-12 19:11:37', 0, 30, 1),
(99, 'created Automatically by the system', '2015-02-14 09:56:57', 0, 31, 1),
(100, 'created Automatically by the system', '2015-02-14 09:58:02', 0, 32, 1),
(101, 'created Automatically by the system', '2015-02-14 10:10:17', 0, 33, 1),
(102, 'created Automatically by the system', '2015-02-14 10:28:27', 0, 34, 1),
(103, 'created Automatically by the system', '2015-02-14 12:52:00', 0, 35, 1),
(104, 'created Automatically by the system', '2015-02-14 17:06:56', 0, 37, 1),
(105, 'created Automatically by the system', '2015-02-14 17:08:15', 0, 38, 1),
(106, 'created Automatically by the system', '2015-02-14 17:18:04', 0, 39, 1),
(107, 'created Automatically by the system', '2015-02-14 17:20:21', 0, 40, 1),
(108, 'created Automatically by the system', '2015-02-14 17:22:21', 0, 41, 1),
(109, 'created Automatically by the system', '2015-02-14 17:24:28', 0, 42, 1),
(110, 'created Automatically by the system', '2015-02-14 17:25:52', 0, 43, 1),
(111, 'created Automatically by the system', '2015-02-14 17:26:30', 0, 44, 1),
(112, 'created Automatically by the system', '2015-02-14 17:26:50', 0, 45, 1),
(113, 'created Automatically by the system', '2015-02-14 17:27:45', 0, 46, 1),
(114, 'created Automatically by the system', '2015-02-14 17:30:26', 0, 47, 1),
(115, 'created Automatically by the system', '2015-02-14 17:32:37', 0, 48, 1),
(116, 'created Automatically by the system', '2015-02-14 17:33:19', 0, 49, 1),
(117, 'This is a comment for Burhan', '2015-02-14 19:08:13', 1, 11, 1),
(118, '<span style=\'color: #000;\'>burhan:</span> create   | 2015-Feb-14 07:13 PM', '2015-02-14 19:13:36', 0, 50, 1),
(119, '<span style=\'color: #000;\'>burhan :</span> create  <span class=\'glyphicon glyphicon-arrow-right\'></span> burhan | 2015-02-14 07:20 PM', '2015-02-14 19:20:29', 0, 51, 1),
(120, '<span style=\'color: #000;\'>burhan :</span> create  <span class=\'glyphicon glyphicon-arrow-right\'></span> burhan | 2015-02-14 07:21 PM', '2015-02-14 19:21:25', 0, 52, 1),
(121, '<span style=\'color: #000;\'>burhan :</span> create  <span class=\'glyphicon glyphicon-arrow-right\'></span> burhan | 2015-02-14 07:24 PM', '2015-02-14 19:24:49', 0, 53, 1),
(122, '<span style=\'color: #000;\'>burhan: </span> complete | 2015-02-14 07:30 PM', '2015-02-14 19:30:17', 0, 53, 1),
(123, '<span style=\'color: #000;\'>burhan: </span> <span style=\'color: #FF2200;\'>not complete</span> <span style=\'color: #000;\'>|</span> 2015-02-14 07:32 PM', '2015-02-14 19:32:53', 0, 53, 1),
(124, '<span style=\'color: #000;\'>burhan: </span> <span style=\'color: #22FF00;\'>complete</span> <span style=\'color: #000;\'>|</span> 2015-02-14 07:33 PM', '2015-02-14 19:33:12', 0, 53, 1),
(125, '<span style=\'color: #000;\'>burhan: </span> <span style=\'color: #D9534F;\'>not complete</span> <span style=\'color: #000;\'>|</span> 2015-02-14 07:36 PM', '2015-02-14 19:36:17', 0, 53, 1),
(126, '<span style=\'color: #000;\'>burhan: </span> <span style=\'color: #72B472;\'>complete</span> <span style=\'color: #000;\'>|</span> 2015-02-14 07:36 PM', '2015-02-14 19:36:34', 0, 53, 1),
(127, '<span style=\'color: #000;\'>burhan :</span> <span style:\'color: #8B8A8A;\'>create</span>  <span class=\'glyphicon glyphicon-arrow-right\'></span> burhan <span style=\'color: #000;\'>|</span> 2015-02-14 07:38 PM', '2015-02-14 19:38:38', 0, 54, 1),
(128, '<span style=\'color: #000;\'>burhan :</span> <span style:\'color: #5C5C5C;\'>create</span>  <span class=\'glyphicon glyphicon-arrow-right\'></span> burhan <span style=\'color: #000;\'>|</span> 2015-02-14 07:39 PM', '2015-02-14 19:39:52', 0, 55, 1),
(129, '<span style=\'color: #000;\'>burhan :</span> <span style=\'color: #5C5C5C;\'>create</span>  <span class=\'glyphicon glyphicon-arrow-right\'></span> burhan <span style=\'color: #000;\'>|</span> 2015-02-14 07:40 PM', '2015-02-14 19:40:53', 0, 56, 1),
(130, '<span style=\'color: #000;\'>burhan :</span> <span style=\'color: #5C5C5C;\'>Update</span> <span style=\'color: #000;\'>|</span> 2015-02-14 07:44 PM', '2015-02-14 19:44:12', 0, 56, 1),
(131, '<span style=\'color: #000;\'>burhan :</span> <span style=\'color: #5C5C5C;\'>Create</span>  <span class=\'glyphicon glyphicon-arrow-right\'></span> burhan <span style=\'color: #000;\'>|</span> 2015-02-14 07:44 PM', '2015-02-14 19:44:31', 0, 57, 1),
(132, '<span style=\'color: #000;\'>zaher :</span> <span style=\'color: #5C5C5C;\'>Send</span> ( omar <span class=\'glyphicon glyphicon-arrow-right\'></span> omar ) <span style=\'color: #000;\'>|</span> 2015-Feb-14 07:53 PM', '2015-02-14 19:53:41', 0, 31, 3),
(133, '<span style=\'color: #000;\'>zaher :</span> <span style=\'color: #5C5C5C;\'>Create</span>  <span class=\'glyphicon glyphicon-arrow-right\'></span> zaher <span style=\'color: #000;\'>|</span> 2015-02-15 01:12 PM', '2015-02-15 13:12:04', 0, 58, 3),
(134, '<span style=\'color: #000;\'>zaher: </span> <span style=\'color: #72B472;\'>Complete</span> <span style=\'color: #000;\'>|</span> 2015-02-15 01:12 PM', '2015-02-15 13:12:13', 0, 58, 3),
(135, '<span style=\'color: #000;\'>zaher :</span> <span style=\'color: #5C5C5C;\'>********** END **********</span>  <span style=\'color: #000;\'>|</span> 2015-Feb-15 01:12 PM', '2015-02-15 13:12:25', 0, 58, 3),
(136, '<span style=\'color: #000;\'>zaher :</span> <span style=\'color: #5C5C5C;\'>Create</span>  <span class=\'glyphicon glyphicon-arrow-right\'></span> zaher <span style=\'color: #000;\'>|</span> 16-Feb-2015 07:58 AM', '2015-02-16 07:58:56', 0, 59, 3),
(137, '<span style=\'color: #000;\'>zaher :</span> <span style=\'color: #5C5C5C;\'>Create</span>  <span class=\'glyphicon glyphicon-arrow-right\'></span> zaher <span style=\'color: #000;\'>|</span> 16-02-2015 07:59 AM', '2015-02-16 07:59:47', 0, 60, 3),
(138, '<span style=\'color: #000;\'>zaher: </span> <span style=\'color: #72B472;\'>Complete</span> <span style=\'color: #000;\'>|</span> 16-02-2015 08:02 AM', '2015-02-16 08:02:21', 0, 58, 3),
(139, '<span style=\'color: #000;\'>zaher :</span> <span style=\'color: #5C5C5C;\'>********** END **********</span>  <span style=\'color: #000;\'>|</span> 16-02-2015 08:02 AM', '2015-02-16 08:02:36', 0, 58, 3),
(140, 'Priority Changed to: H by: zaher | 16-02-2015 08:03 AM', '2015-02-16 08:03:44', 0, 32, 3),
(141, 'Reminded By zaher', '2015-02-16 08:03:54', 0, 32, 3),
(142, '<span style=\'color: #000;\'>burhan :</span> <span style=\'color: #5C5C5C;\'>Create</span>  <span class=\'glyphicon glyphicon-arrow-right\'></span> burhan <span style=\'color: #000;\'>|</span> 16-02-2015 08:40 AM', '2015-02-16 08:40:51', 0, 61, 1),
(143, '<span style=\'color: #000;\'>burhan: </span> <span style=\'color: #72B472;\'>Complete</span> <span style=\'color: #000;\'>|</span> 17-02-2015 12:46 PM', '2015-02-17 12:46:54', 0, 61, 1),
(144, '<span style=\'color: #000;\'>burhan: </span> <span style=\'color: #D9534F;\'>Not Complete</span> <span style=\'color: #000;\'>|</span> 17-02-2015 12:47 PM', '2015-02-17 12:47:13', 0, 61, 1),
(145, 'This is a comment ', '2015-02-17 12:47:46', 1, 61, 1),
(146, '<span style=\'color: #000;\'>burhan: </span> <span style=\'color: #72B472;\'>Complete</span> <span style=\'color: #000;\'>|</span> 18-02-2015 07:48 AM', '2015-02-18 07:48:01', 0, 53, 1),
(147, '<span style=\'color: #000;\'>burhan :</span> <span style=\'color: #5C5C5C;\'>Create</span>  <span class=\'glyphicon glyphicon-arrow-right\'></span> omar <span style=\'color: #000;\'>|</span> 18-02-2015 07:48 AM', '2015-02-18 07:48:27', 0, 62, 1),
(148, '<span style=\'color: #000;\'>omar: </span> <span style=\'color: #72B472;\'>Complete</span> <span style=\'color: #000;\'>|</span> 18-02-2015 07:50 AM', '2015-02-18 07:50:01', 0, 62, 4);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_group`
--

DROP TABLE IF EXISTS `tbl_group`;
CREATE TABLE `tbl_group` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL COMMENT '0 -> idle\n1 -> active',
  `description` tinytext,
  `note` tinytext,
  `create_date` date NOT NULL,
  `create_id` int(11) DEFAULT NULL,
  `update_date` date DEFAULT NULL,
  `update_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_job`
--

DROP TABLE IF EXISTS `tbl_job`;
CREATE TABLE `tbl_job` (
  `id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `priority` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_job`
--

INSERT INTO `tbl_job` (`id`, `task_id`, `date`, `priority`) VALUES
(15, 49, '2015-02-16', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_log`
--

DROP TABLE IF EXISTS `tbl_log`;
CREATE TABLE `tbl_log` (
  `id` int(11) NOT NULL,
  `text` varchar(255) NOT NULL,
  `create_date` date DEFAULT NULL,
  `create_id` int(11) NOT NULL,
  `ip_address` varchar(15) DEFAULT NULL,
  `component_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_project`
--

DROP TABLE IF EXISTS `tbl_project`;
CREATE TABLE `tbl_project` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` tinytext,
  `create_date` date NOT NULL,
  `create_id` int(11) NOT NULL,
  `update_date` date DEFAULT NULL,
  `update_id` int(11) DEFAULT NULL,
  `note` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_project`
--

INSERT INTO `tbl_project` (`id`, `name`, `description`, `create_date`, `create_id`, `update_date`, `update_id`, `note`) VALUES
(1, 'Setup the Office', '', '2015-02-11', 1, '2015-02-12', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_project_user_assignment`
--

DROP TABLE IF EXISTS `tbl_project_user_assignment`;
CREATE TABLE `tbl_project_user_assignment` (
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_project_user_assignment`
--

INSERT INTO `tbl_project_user_assignment` (`project_id`, `user_id`) VALUES
(1, 1),
(1, 2),
(1, 4),
(1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_setting`
--

DROP TABLE IF EXISTS `tbl_setting`;
CREATE TABLE `tbl_setting` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_setting`
--

INSERT INTO `tbl_setting` (`id`, `name`, `value`) VALUES
(2, 'app_name', 'Task Manager App');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_supervision`
--

DROP TABLE IF EXISTS `tbl_supervision`;
CREATE TABLE `tbl_supervision` (
  `supervisor_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_supervision`
--

INSERT INTO `tbl_supervision` (`supervisor_id`, `user_id`) VALUES
(2, 1),
(2, 4),
(3, 4),
(2, 5),
(3, 5),
(3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_supervisor_notification`
--

DROP TABLE IF EXISTS `tbl_supervisor_notification`;
CREATE TABLE `tbl_supervisor_notification` (
  `id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `supervisor_id` int(11) NOT NULL,
  `supervisor_notified` tinyint(4) NOT NULL,
  `update_type` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_supervisor_notification`
--

INSERT INTO `tbl_supervisor_notification` (`id`, `task_id`, `project_id`, `supervisor_id`, `supervisor_notified`, `update_type`) VALUES
(5, 15, 1, 2, 0, 0),
(7, 19, 1, 2, 0, 0),
(9, 20, 1, 2, 0, 0),
(11, 21, 1, 2, 0, 0),
(13, 22, 1, 2, 0, 0),
(15, 23, 1, 2, 0, 0),
(25, 24, 1, 2, 0, 0),
(27, 25, 1, 2, 0, 0),
(29, 26, 1, 2, 0, 0),
(31, 27, 1, 2, 0, 0),
(33, 28, 1, 2, 0, 0),
(35, 30, 1, 2, 0, 0),
(39, 32, 1, 2, 0, 0),
(41, 33, 1, 2, 0, 0),
(43, 34, 1, 2, 0, 0),
(45, 35, 1, 2, 0, 0),
(47, 37, 1, 2, 0, 0),
(49, 38, 1, 2, 0, 0),
(51, 39, 1, 2, 0, 0),
(53, 40, 1, 2, 0, 0),
(55, 41, 1, 2, 0, 0),
(57, 42, 1, 2, 0, 0),
(59, 43, 1, 2, 0, 0),
(61, 44, 1, 2, 0, 0),
(63, 45, 1, 2, 0, 0),
(65, 46, 1, 2, 0, 0),
(67, 47, 1, 2, 0, 0),
(69, 48, 1, 2, 0, 0),
(71, 49, 1, 2, 0, 0),
(73, 50, 1, 2, 0, 0),
(75, 51, 1, 2, 0, 0),
(77, 52, 1, 2, 0, 0),
(91, 54, 1, 2, 0, 0),
(93, 55, 1, 2, 0, 0),
(95, 56, 1, 2, 0, 0),
(97, 57, 1, 2, 0, 0),
(99, 31, 1, 2, 0, 1),
(104, 61, 1, 2, 0, 4),
(106, 53, 1, 2, 0, 2),
(110, 62, 1, 2, 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_task`
--

DROP TABLE IF EXISTS `tbl_task`;
CREATE TABLE `tbl_task` (
  `id` int(11) NOT NULL,
  `serial_no` varchar(55) DEFAULT NULL,
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` tinytext,
  `note` tinytext,
  `priority` tinyint(4) NOT NULL COMMENT 'low - medium - high',
  `tag` varchar(255) DEFAULT NULL,
  `status` tinyint(4) NOT NULL COMMENT 'pending 0 - on going 1 - hold 2 - done 3',
  `type` tinyint(4) NOT NULL,
  `second_priority` int(11) NOT NULL,
  `third_priority` int(11) NOT NULL,
  `second_priority_days` int(11) NOT NULL,
  `third_priority_days` int(11) NOT NULL,
  `not_complete_return_count` int(11) NOT NULL,
  `notified` int(11) NOT NULL,
  `supervisor_notified` tinyint(4) DEFAULT NULL,
  `update_type` tinyint(4) DEFAULT NULL,
  `reminded` tinyint(4) NOT NULL,
  `create_date` datetime NOT NULL,
  `create_id` int(11) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `due_to_date` date DEFAULT NULL,
  `update_id` int(11) DEFAULT NULL,
  `close_date` date DEFAULT NULL,
  `close_id` int(11) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `expended_work_time` int(11) DEFAULT NULL,
  `first_start_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_task`
--

INSERT INTO `tbl_task` (`id`, `serial_no`, `project_id`, `user_id`, `title`, `description`, `note`, `priority`, `tag`, `status`, `type`, `second_priority`, `third_priority`, `second_priority_days`, `third_priority_days`, `not_complete_return_count`, `notified`, `supervisor_notified`, `update_type`, `reminded`, `create_date`, `create_id`, `update_date`, `due_to_date`, `update_id`, `close_date`, `close_id`, `start_time`, `end_time`, `expended_work_time`, `first_start_time`) VALUES
(1, 'T15-1', 1, 1, 'This is a task', '', '', 2, '', 1, 0, 0, 0, 0, 0, 3, 1, NULL, 4, 0, '2015-02-11 00:00:00', 1, '2015-02-14 00:00:00', '2015-02-14', 1, '2015-02-14', 1, NULL, NULL, NULL, NULL),
(2, 'T15-2', 1, 3, 'This is for Omar', '', '', 0, '', 3, 0, 0, 0, 0, 0, 1, 1, NULL, 4, 0, '2015-02-12 00:00:00', 2, '2015-02-12 00:00:00', NULL, 3, '2015-02-12', 3, NULL, NULL, NULL, NULL),
(3, 'T15-3', 1, 4, 'This is for Omar again', 'This is a new Description Hello There\r\n', '', 2, '', 3, 0, 0, 0, 0, 0, 0, 1, NULL, 5, 0, '2015-02-12 00:00:00', 2, '2015-02-12 00:00:00', '2015-02-18', 2, '2015-02-12', 2, NULL, NULL, NULL, NULL),
(4, 'T15-4', 1, 4, 'This is a new Task', '', '', 0, '', 3, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, 0, '2015-02-12 00:00:00', 1, '2015-02-12 00:00:00', NULL, 2, '2015-02-12', 2, NULL, NULL, NULL, NULL),
(5, 'T15-5', 1, 4, 'Notify All', '', '', 0, '', 3, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, 0, '2015-02-12 00:00:00', 1, '2015-02-12 00:00:00', '2015-02-16', 2, '2015-02-12', 2, NULL, NULL, NULL, NULL),
(6, 'T15-6', 1, 4, 'Notify All with complete', '', '', 0, '', 3, 0, 0, 0, 0, 0, 1, 1, NULL, 5, 0, '2015-02-12 00:00:00', 1, '2015-02-12 00:00:00', NULL, 2, '2015-02-12', 2, NULL, NULL, NULL, NULL),
(7, 'T15-7', 1, 4, 'This is a new Task', '', '', 0, '', 3, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, 0, '2015-02-12 00:00:00', 2, '2015-02-12 00:00:00', NULL, 2, '2015-02-12', 2, NULL, NULL, NULL, NULL),
(8, 'T15-8', 1, 1, 'This is a new Task', '', '', 0, '', 1, 0, 0, 0, 0, 0, 1, 1, NULL, 4, 0, '2015-02-12 00:00:00', 2, '2015-02-14 00:00:00', NULL, 1, '2015-02-14', 1, NULL, NULL, NULL, NULL),
(9, 'T15-9', 1, 1, 'My Admin Task', '', '', 0, '', 1, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, 0, '2015-02-12 00:00:00', 1, '2015-02-17 11:52:59', '2015-02-16', 1, '2015-02-17', 1, NULL, NULL, NULL, NULL),
(10, 'T15-10', 1, 1, 'A new new admin task', '', '', 0, '', 1, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, 0, '2015-02-12 00:00:00', 1, '2015-02-17 09:34:27', NULL, 1, '2015-02-17', 1, NULL, NULL, NULL, NULL),
(11, 'T15-11', 1, 1, 'a new admin task', '', '', 0, '', 1, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, 0, '2015-02-12 00:00:00', 1, '2015-02-14 00:00:00', NULL, 1, '2015-02-14', 1, NULL, NULL, NULL, NULL),
(12, 'T15-12', 1, 1, 'A new Admin Task Again', '', '', 0, '', 1, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, 0, '2015-02-12 00:00:00', 1, '2015-02-14 00:00:00', NULL, 1, '2015-02-14', 1, NULL, NULL, NULL, NULL),
(13, 'T15-13', 1, 2, 'شسشسيشيشسيشسي', '', '', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, '2015-02-12 00:00:00', 1, '2015-02-12 00:00:00', NULL, 1, '2015-02-12', 1, NULL, NULL, NULL, NULL),
(14, 'T15-14', 1, 2, 'شسييشسشيشسيشسي', '', '', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, '2015-02-12 00:00:00', 1, '2015-02-12 00:00:00', NULL, 1, '2015-02-12', 1, NULL, NULL, NULL, NULL),
(15, 'T15-15', 1, 4, 'شسيشسيسشيشسيشسيشسيسشي', '', '', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, 0, '2015-02-12 00:00:00', 1, '2015-02-12 00:00:00', NULL, 4, '2015-02-12', 4, NULL, NULL, NULL, NULL),
(16, 'T15-16', 1, 2, 'سيشسيسشيشسي', '', '', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, '2015-02-12 00:00:00', 1, '2015-02-12 00:00:00', NULL, 1, '2015-02-12', 1, NULL, NULL, NULL, NULL),
(17, 'T15-17', 1, 2, 'شسبشسشببسشبسشب', '', '', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, '2015-02-12 00:00:00', 1, '2015-02-12 00:00:00', NULL, 1, '2015-02-12', 1, NULL, NULL, NULL, NULL),
(18, 'T15-18', 1, 2, 'بشسبسشبسشبشسبسبشسبشسب', '', '', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, '2015-02-12 00:00:00', 1, '2015-02-12 00:00:00', NULL, 1, '2015-02-12', 1, NULL, NULL, NULL, NULL),
(19, 'T15-19', 1, 4, 'شسيشيسشيشي', '', '', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, 0, '2015-02-12 00:00:00', 1, '2015-02-12 00:00:00', NULL, 4, '2015-02-12', 4, NULL, NULL, NULL, NULL),
(20, 'T15-20', 1, 4, 'شسيشيشسيسشيسشي', '', '', 0, '', 1, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, 0, '2015-02-12 00:00:00', 1, '2015-02-14 00:00:00', NULL, 4, '2015-02-14', 4, NULL, NULL, NULL, NULL),
(21, 'T15-21', 1, 4, 'شسبشبشسبشسبشبشسبشسبشسب', '', '', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, 0, '2015-02-12 00:00:00', 1, '2015-02-12 00:00:00', NULL, 4, '2015-02-12', 4, NULL, NULL, NULL, NULL),
(22, 'T15-22', 1, 4, 'شسشسبشسبشبشسبشسبشسب', '', '', 0, '', 1, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, 0, '2015-02-12 00:00:00', 1, '2015-02-14 00:00:00', NULL, 4, '2015-02-14', 4, NULL, NULL, NULL, NULL),
(23, 'T15-23', 1, 4, 'سشيشيشسيلضصبضصبصض', '', '', 0, '', 1, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, 0, '2015-02-12 00:00:00', 1, '2015-02-14 00:00:00', NULL, 4, '2015-02-14', 4, NULL, NULL, NULL, NULL),
(24, 'W15-1-0', 1, 4, 'New Automatic Task of the day', 'Descriptio', '', 1, '', 1, 1, 0, 0, 0, 0, 0, 1, NULL, NULL, 0, '0000-00-00 00:00:00', 1, '2015-02-12 00:00:00', '2015-02-14', 4, '2015-02-12', 4, NULL, NULL, NULL, NULL),
(25, 'W15-1-0', 1, 4, 'New Automatic Task of the day', 'Descriptio', '', 1, '', 1, 1, 0, 0, 0, 0, 0, 1, NULL, NULL, 0, '0000-00-00 00:00:00', 1, '2015-02-12 00:00:00', '2015-02-14', 4, '2015-02-12', 4, NULL, NULL, NULL, NULL),
(26, 'W15-1-0', 1, 4, 'New Automatic Task of the day', 'Descriptio', '', 1, '', 1, 1, 0, 0, 0, 0, 0, 1, NULL, NULL, 0, '2015-02-12 00:00:00', 1, '2015-02-14 00:00:00', '2015-02-14', 4, '2015-02-14', 4, NULL, NULL, NULL, NULL),
(27, 'W15-1-1', 1, 4, 'New Automatic Task of the day', 'Descriptio', '', 1, '', 1, 1, 0, 0, 0, 0, 0, 1, NULL, NULL, 0, '2015-02-12 00:00:00', 1, '2015-02-12 00:00:00', '2015-02-14', 4, '2015-02-12', 4, NULL, NULL, NULL, NULL),
(28, 'W15-1-2', 1, 4, 'New Automatic Task of the day', 'Descriptio', '', 1, '', 1, 1, 0, 0, 0, 0, 0, 1, NULL, NULL, 0, '2015-02-12 00:00:00', 1, '2015-02-14 00:00:00', '2015-02-14', 4, '2015-02-14', 4, NULL, NULL, NULL, NULL),
(29, 'W15-1-3', 1, 4, 'New Automatic Task of the day', 'Descriptio', '', 1, '', 1, 1, 0, 0, 0, 0, 0, 1, NULL, NULL, 0, '2015-02-12 00:00:00', 1, '2015-02-14 00:00:00', '2015-02-14', 4, '2015-02-14', 4, NULL, NULL, NULL, NULL),
(30, 'W15-1-4', 1, 4, 'New Automatic Task of the day', 'Descriptio', '', 1, '', 1, 1, 0, 0, 0, 0, 0, 1, NULL, NULL, 0, '2015-02-12 00:00:00', 1, '2015-02-14 00:00:00', '2015-02-14', 4, '2015-02-14', 4, NULL, NULL, NULL, NULL),
(31, 'W15-1-5', 1, 4, 'New Automatic Task of the day', 'Descriptio', '', 1, '', 0, 1, 0, 0, 0, 0, 0, 1, NULL, 1, 0, '2015-02-14 00:00:00', 1, '2015-02-18 07:48:55', '2015-02-14', 4, '2015-02-18', 4, NULL, NULL, NULL, NULL),
(32, 'W15-1-6', 1, 4, 'New Automatic Task of the day', 'Descriptio', '', 2, '', 0, 1, 0, 0, 0, 0, 0, 1, NULL, 5, 1, '2015-02-14 00:00:00', 1, '2015-02-18 07:48:55', '2015-02-14', 4, '2015-02-18', 4, NULL, NULL, NULL, NULL),
(33, 'W15-1-7', 1, 4, 'New Automatic Task of the day', 'Descriptio', '', 1, '', 0, 1, 0, 0, 0, 0, 0, 1, NULL, NULL, 0, '2015-02-14 00:00:00', 1, '2015-02-18 07:48:56', '2015-02-14', 4, '2015-02-18', 4, NULL, NULL, NULL, NULL),
(34, 'W15-1-8', 1, 4, 'New Automatic Task of the day', 'Descriptio', '', 1, '', 0, 1, 0, 0, 0, 0, 0, 1, NULL, NULL, 0, '2015-02-14 00:00:00', 1, '2015-02-18 07:48:56', '2015-02-14', 4, '2015-02-18', 4, NULL, NULL, NULL, NULL),
(35, 'W15-1-9', 1, 4, 'New Automatic Task of the day', 'Descriptio', '', 1, '', 0, 1, 0, 0, 0, 0, 0, 1, NULL, NULL, 0, '2015-02-14 00:00:00', 1, '2015-02-18 07:48:56', '2015-03-17', 4, '2015-02-18', 4, NULL, NULL, NULL, NULL),
(36, 'W15-1-10', 1, 4, 'New Automatic Task of the day', 'Descriptio', '', 1, '', 0, 1, 2, 1, 5, 2, 0, 1, NULL, NULL, 0, '2015-02-14 00:00:00', 1, '2015-02-18 07:48:56', '2015-02-15', 4, '2015-02-18', 4, NULL, NULL, NULL, NULL),
(37, 'W15-1-11', 1, 4, 'New Automatic Task of the day', 'Descriptio', '', 1, '', 0, 1, 2, 1, 5, 2, 0, 1, NULL, NULL, 0, '2015-02-14 00:00:00', 1, '2015-02-18 07:48:56', '2015-02-15', 4, '2015-02-18', 4, NULL, NULL, NULL, NULL),
(38, 'W15-1-12', 1, 4, 'New Automatic Task of the day', 'Descriptio', '', 1, '', 0, 1, 0, 1, 5, 2, 0, 1, NULL, NULL, 0, '2015-02-14 00:00:00', 1, '2015-02-18 07:48:56', '2015-02-15', 4, '2015-02-18', 4, NULL, NULL, NULL, NULL),
(39, 'W15-1-13', 1, 4, 'New Automatic Task of the day', 'Descriptio', '', 1, '', 0, 1, 0, 1, 5, 2, 0, 1, NULL, NULL, 0, '2015-02-14 00:00:00', 1, '2015-02-18 07:48:56', '2015-02-15', 4, '2015-02-18', 4, NULL, NULL, NULL, NULL),
(40, 'W15-1-14', 1, 4, 'New Automatic Task of the day', 'Descriptio', '', 2, '', 0, 1, 0, 1, 5, 2, 0, 1, NULL, NULL, 0, '2015-02-14 00:00:00', 1, '2015-02-18 07:48:56', '2015-02-15', 4, '2015-02-18', 4, NULL, NULL, NULL, NULL),
(41, 'W15-1-15', 1, 4, 'New Automatic Task of the day', 'Descriptio', '', 2, '', 0, 1, 0, 1, 5, 2, 0, 1, NULL, NULL, 0, '2015-02-14 00:00:00', 1, '2015-02-18 07:48:57', '2015-02-15', 4, '2015-02-18', 4, NULL, NULL, NULL, NULL),
(42, 'W15-1-16', 1, 4, 'New Automatic Task of the day', 'Descriptio', '', 1, '', 0, 1, 0, 1, 5, 2, 0, 1, NULL, NULL, 0, '2015-02-14 00:00:00', 1, '2015-02-18 07:48:57', '2015-02-15', 4, '2015-02-18', 4, NULL, NULL, NULL, NULL),
(43, 'W15-1-17', 1, 4, 'New Automatic Task of the day', 'Descriptio', '', 1, '', 0, 1, 0, 1, 5, 2, 0, 1, NULL, NULL, 0, '2015-02-14 00:00:00', 1, '2015-02-18 07:48:57', '2015-02-15', 4, '2015-02-18', 4, NULL, NULL, NULL, NULL),
(44, 'W15-1-18', 1, 4, 'New Automatic Task of the day', 'Descriptio', '', 1, '', 0, 1, 0, 1, 5, 2, 0, 1, NULL, NULL, 0, '2015-02-14 00:00:00', 1, '2015-02-18 07:48:57', '2015-02-15', 4, '2015-02-18', 4, NULL, NULL, NULL, NULL),
(45, 'W15-1-19', 1, 4, 'New Automatic Task of the day', 'Descriptio', '', 1, '', 0, 1, 0, 1, 5, 2, 0, 1, NULL, NULL, 0, '2015-02-14 00:00:00', 1, '2015-02-18 07:48:57', '2015-02-15', 4, '2015-02-18', 4, NULL, NULL, NULL, NULL),
(46, 'W15-1-20', 1, 4, 'New Automatic Task of the day', 'Descriptio', '', 1, '', 0, 1, 0, 1, 5, 2, 0, 1, NULL, NULL, 0, '2015-02-14 00:00:00', 1, '2015-02-18 07:48:57', '2015-02-15', 4, '2015-02-18', 4, NULL, NULL, NULL, NULL),
(47, 'W15-1-21', 1, 4, 'New Automatic Task of the day', 'Descriptio', '', 1, '', 0, 1, 0, 1, 5, 2, 0, 1, NULL, NULL, 0, '2015-02-14 00:00:00', 1, '2015-02-18 07:48:57', '2015-02-15', 4, '2015-02-18', 4, NULL, NULL, NULL, NULL),
(48, 'W15-1-22', 1, 4, 'New Automatic Task of the day', 'Descriptio', '', 1, '', 0, 1, 0, 1, 5, 2, 0, 1, NULL, NULL, 0, '2015-02-14 00:00:00', 1, '2015-02-18 07:48:57', '2015-02-15', 4, '2015-02-18', 4, NULL, NULL, NULL, NULL),
(49, 'W15-1-23', 1, 4, 'New Automatic Task of the day', 'Descriptio', '', 1, '', 0, 1, 0, 1, 5, 2, 0, 1, NULL, NULL, 0, '2015-02-14 00:00:00', 1, '2015-02-18 07:48:57', '2015-02-15', 4, '2015-02-18', 4, NULL, NULL, NULL, NULL),
(50, 'T15-24', 1, 1, 'This is a test task', '', '', 0, '', 1, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, 0, '2015-02-14 00:00:00', 1, '2015-02-17 11:54:06', NULL, 1, '2015-02-17', 1, NULL, NULL, NULL, NULL),
(51, 'T15-25', 1, 1, 'From me to Zaher', '', '', 0, '', 1, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, 0, '2015-02-14 00:00:00', 1, '2015-02-17 11:52:54', NULL, 1, '2015-02-17', 1, NULL, NULL, NULL, NULL),
(52, 'T15-26', 1, 1, 'Test Order ', '', '', 0, '', 1, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, 0, '2015-02-14 00:00:00', 1, '2015-02-17 11:52:57', NULL, 1, '2015-02-17', 1, NULL, NULL, NULL, NULL),
(53, 'T15-27', 1, 1, 'Test the new Task Order', '', '', 0, '', 2, 0, 0, 0, 0, 0, 2, 1, NULL, 4, 0, '2015-02-14 19:24:49', 1, '2015-02-18 07:48:01', NULL, 1, '2015-02-18', 1, NULL, NULL, NULL, NULL),
(54, 'T15-28', 1, 1, 'test the create action', '', '', 0, '', 1, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, 0, '2015-02-14 19:38:37', 1, '2015-02-17 11:52:50', NULL, 1, '2015-02-17', 1, NULL, NULL, NULL, NULL),
(55, 'T15-29', 1, 1, 'test the create action again', '', '', 0, '', 1, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, 0, '2015-02-14 19:39:52', 1, '2015-02-18 08:26:38', NULL, 1, '2015-02-18', 1, NULL, NULL, NULL, NULL),
(56, 'T15-30', 1, 1, 'sorry', '', '', 0, '', 1, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, 0, '2015-02-14 19:40:52', 1, '2015-02-17 17:10:13', NULL, 1, '2015-02-17', 1, NULL, NULL, NULL, NULL),
(57, 'T15-31', 1, 1, 'asdasdsad', '', '', 0, '', 1, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, 0, '2015-02-14 19:44:30', 1, '2015-02-18 08:26:34', NULL, 1, '2015-02-18', 1, NULL, NULL, NULL, NULL),
(58, 'T15-32', 1, 3, 'This is a new Task For me', '', '', 0, '', 3, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, 0, '2015-02-15 13:12:03', 3, '2015-02-16 08:02:36', NULL, 3, '2015-02-16', 3, NULL, NULL, NULL, NULL),
(59, 'T15-33', 1, 3, 'This is a new Task', '', '', 0, '', 1, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, 0, '2015-02-16 07:58:56', 3, '2015-02-16 08:02:50', NULL, 3, '2015-02-16', 3, NULL, NULL, NULL, NULL),
(60, 'T15-34', 1, 3, 'This is my collage', '', '', 0, '', 1, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, 0, '2015-02-16 07:59:47', 3, '2015-02-16 08:02:52', NULL, 3, '2015-02-16', 3, NULL, NULL, NULL, NULL),
(61, 'T15-35', 1, 1, 'This is a test for date pickers', '', '', 0, '', 1, 0, 0, 0, 0, 0, 1, 1, NULL, 4, 0, '2015-02-16 08:40:51', 1, '2015-02-17 17:10:17', '2015-02-17', 1, '2015-02-17', 1, NULL, NULL, NULL, NULL),
(62, 'T15-36', 1, 4, 'This is a new Task', '', '', 0, '', 2, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, 0, '2015-02-18 07:48:27', 1, '2015-02-18 07:50:01', NULL, 4, '2015-02-18', 4, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `emp_no` varchar(255) DEFAULT NULL,
  `current_project_id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `login_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` int(11) NOT NULL COMMENT 'online - offline - break',
  `active` int(11) NOT NULL,
  `note` tinytext NOT NULL,
  `create_date` date NOT NULL,
  `create_id` int(11) NOT NULL,
  `update_date` date DEFAULT NULL,
  `update_id` int(11) DEFAULT NULL,
  `tbl_auto_task_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `emp_no`, `current_project_id`, `first_name`, `last_name`, `username`, `login_name`, `email`, `password`, `status`, `active`, `note`, `create_date`, `create_id`, `update_date`, `update_id`, `tbl_auto_task_id`) VALUES
(1, 'HQ-310', 1, 'burhan', 'otour', 'burhan', 'burhan', 'borhan@gmail.com', '68e76ae625987d3b05fa39f99298e7d27e5803e7', 1, 0, '', '2015-02-11', 1, '2015-02-18', 1, 0),
(2, '3321', 1, 'maher', 'maher', 'maher', 'maher', '', '625356652f70fce3a42bfebab57943441a0c5da7', 2, 0, '', '2015-02-11', 1, '2015-02-12', 2, 0),
(3, '', 1, 'zaher', 'zaher', 'zaher', 'zaher', '', '625356652f70fce3a42bfebab57943441a0c5da7', 1, 0, '', '2015-02-12', 1, '2017-11-01', 3, 0),
(4, '', 1, 'omar', 'omar', 'omar', 'omar', '', '625356652f70fce3a42bfebab57943441a0c5da7', 2, 0, '', '2015-02-12', 1, '2015-02-18', 4, 0),
(5, '', 0, 'basil', 'basil', 'basil', 'basil', '', '625356652f70fce3a42bfebab57943441a0c5da7', 2, 0, '', '2015-02-12', 3, '2015-02-12', 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_group_assignment`
--

DROP TABLE IF EXISTS `tbl_user_group_assignment`;
CREATE TABLE `tbl_user_group_assignment` (
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `authassignment`
--
ALTER TABLE `authassignment`
  ADD PRIMARY KEY (`itemname`,`userid`);

--
-- Indexes for table `authitem`
--
ALTER TABLE `authitem`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `authitemchild`
--
ALTER TABLE `authitemchild`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indexes for table `tbl_auto_task`
--
ALTER TABLE `tbl_auto_task`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tbl_auto_task_tbl_user1_idx` (`user_id`),
  ADD KEY `fk_tbl_auto_task_tbl_project1_idx` (`project_id`);

--
-- Indexes for table `tbl_auto_task_comment`
--
ALTER TABLE `tbl_auto_task_comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tbl_auto_task_comment_tbl_auto_task1_idx` (`auto_task_id`),
  ADD KEY `fk_tbl_auto_task_comment_tbl_user1_idx` (`create_id`);

--
-- Indexes for table `tbl_auto_task_param`
--
ALTER TABLE `tbl_auto_task_param`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tbl_auto_task_param_tbl_auto_task1_idx` (`tbl_auto_task_id`);

--
-- Indexes for table `tbl_comment`
--
ALTER TABLE `tbl_comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tbl_comment_tbl_task1_idx` (`task_id`),
  ADD KEY `fk_tbl_comment_tbl_user1_idx` (`create_id`);

--
-- Indexes for table `tbl_group`
--
ALTER TABLE `tbl_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_job`
--
ALTER TABLE `tbl_job`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_log`
--
ALTER TABLE `tbl_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tbl_log_tbl_user1_idx` (`create_id`);

--
-- Indexes for table `tbl_project`
--
ALTER TABLE `tbl_project`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_project_user_assignment`
--
ALTER TABLE `tbl_project_user_assignment`
  ADD KEY `fk_tbl_project_user_assignment_tbl_project1_idx` (`project_id`),
  ADD KEY `fk_tbl_project_user_assignment_tbl_user1_idx` (`user_id`);

--
-- Indexes for table `tbl_setting`
--
ALTER TABLE `tbl_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_supervision`
--
ALTER TABLE `tbl_supervision`
  ADD KEY `fk_supervison_id_idx` (`supervisor_id`),
  ADD KEY `fk_user_id_idx` (`user_id`);

--
-- Indexes for table `tbl_supervisor_notification`
--
ALTER TABLE `tbl_supervisor_notification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tbl_supervisor_notification_tbl_task1_idx` (`task_id`),
  ADD KEY `fk_tbl_supervisor_notification_tbl_user1_idx` (`supervisor_id`);

--
-- Indexes for table `tbl_task`
--
ALTER TABLE `tbl_task`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tbl_task_tbl_project_idx` (`project_id`),
  ADD KEY `fk_tbl_task_tbl_user1_idx` (`user_id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user_group_assignment`
--
ALTER TABLE `tbl_user_group_assignment`
  ADD KEY `fk_tbl_user_group_assignment_tbl_group1_idx` (`group_id`),
  ADD KEY `fk_tbl_user_group_assignment_tbl_user1_idx` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_auto_task`
--
ALTER TABLE `tbl_auto_task`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tbl_auto_task_comment`
--
ALTER TABLE `tbl_auto_task_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `tbl_auto_task_param`
--
ALTER TABLE `tbl_auto_task_param`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_comment`
--
ALTER TABLE `tbl_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=149;
--
-- AUTO_INCREMENT for table `tbl_group`
--
ALTER TABLE `tbl_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_job`
--
ALTER TABLE `tbl_job`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `tbl_log`
--
ALTER TABLE `tbl_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_project`
--
ALTER TABLE `tbl_project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_setting`
--
ALTER TABLE `tbl_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_supervisor_notification`
--
ALTER TABLE `tbl_supervisor_notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;
--
-- AUTO_INCREMENT for table `tbl_task`
--
ALTER TABLE `tbl_task`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `authassignment`
--
ALTER TABLE `authassignment`
  ADD CONSTRAINT `authassignment_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `authitemchild`
--
ALTER TABLE `authitemchild`
  ADD CONSTRAINT `authitemchild_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `authitemchild_ibfk_2` FOREIGN KEY (`child`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_auto_task`
--
ALTER TABLE `tbl_auto_task`
  ADD CONSTRAINT `fk_tbl_auto_task_tbl_project1` FOREIGN KEY (`project_id`) REFERENCES `tbl_project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_auto_task_tbl_user1` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_auto_task_comment`
--
ALTER TABLE `tbl_auto_task_comment`
  ADD CONSTRAINT `fk_tbl_auto_task_comment_tbl_auto_task1` FOREIGN KEY (`auto_task_id`) REFERENCES `tbl_auto_task` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_auto_task_comment_tbl_user1` FOREIGN KEY (`create_id`) REFERENCES `tbl_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_auto_task_param`
--
ALTER TABLE `tbl_auto_task_param`
  ADD CONSTRAINT `fk_tbl_auto_task_param_tbl_auto_task1` FOREIGN KEY (`tbl_auto_task_id`) REFERENCES `tbl_auto_task` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_comment`
--
ALTER TABLE `tbl_comment`
  ADD CONSTRAINT `fk_tbl_comment_tbl_task1` FOREIGN KEY (`task_id`) REFERENCES `tbl_task` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_comment_tbl_user1` FOREIGN KEY (`create_id`) REFERENCES `tbl_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_log`
--
ALTER TABLE `tbl_log`
  ADD CONSTRAINT `fk_tbl_log_tbl_user1` FOREIGN KEY (`create_id`) REFERENCES `tbl_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_project_user_assignment`
--
ALTER TABLE `tbl_project_user_assignment`
  ADD CONSTRAINT `fk_tbl_project_user_assignment_tbl_project1` FOREIGN KEY (`project_id`) REFERENCES `tbl_project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_project_user_assignment_tbl_user1` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_supervision`
--
ALTER TABLE `tbl_supervision`
  ADD CONSTRAINT `fk_supervison_id` FOREIGN KEY (`supervisor_id`) REFERENCES `tbl_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_supervisor_notification`
--
ALTER TABLE `tbl_supervisor_notification`
  ADD CONSTRAINT `fk_tbl_supervisor_notification_tbl_task1` FOREIGN KEY (`task_id`) REFERENCES `tbl_task` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_supervisor_notification_tbl_user1` FOREIGN KEY (`supervisor_id`) REFERENCES `tbl_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_task`
--
ALTER TABLE `tbl_task`
  ADD CONSTRAINT `fk_tbl_task_tbl_project` FOREIGN KEY (`project_id`) REFERENCES `tbl_project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_task_tbl_user1` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_user_group_assignment`
--
ALTER TABLE `tbl_user_group_assignment`
  ADD CONSTRAINT `fk_tbl_user_group_assignment_tbl_group1` FOREIGN KEY (`group_id`) REFERENCES `tbl_group` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_user_group_assignment_tbl_user1` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
